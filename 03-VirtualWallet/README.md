# Virtual Wallet
*by Nikola Kostadinov & Spas Ormandzhiev*   
*Team: A16-Team-03*
[Inital GitLab repo](https://gitlab.com/Ajku1/virtual-wallet)

Link to the team [Trello](https://trello.com/b/Mgi2DOqg/a16-team-03-virtualwallet)


A team project assignment from Telerik Academy in partnership with Scalefocus.

## The application    
VIRTUAL WALLET is a web application that enables you to continently manage your budget. Every user can send and receive money (user to user) and put money in his VIRTUAL WALLET (bank to app). 

### Implemented functionalities:
There are regular users and admin users. All users have username, email, password, photo, credit/debit card, phone number. A user can login/logout, change their password, email, photo, add and change their credit/debit card, make transfers to other users and view the history of their transfers.
* Users can view and edit their profile information, except their username, which is selected on registration and cannot be changed afterwards. The required fields for registration are username, email and phone number.
* Each user can register one credit or debit card, which is used to transfer money into their Virtual Wallet. The transfer is done by the provided REST API with separate documentation. DO NOT use actual credit card information!
* Users transfer money to other users by entering another user's phone number, username or email and desired amount to be transferred. Users can search by phone number, username or email in order to select the recipient user for the transfer.
* Users can view a paginated list of their transactions filtered by period in time, recipient and direction (incoming or outgoing) and sort them by amount and date.
* Admin users can see and search by phone number, username or email a paginated list of all users and block or unblock them. A blocked user, can do everything as normaly, except to make transactions and edit/delete his credit or debit card.
* Admin users can view a paginated list of all user transactions filtered by period in time, sender, recipient and direction (incoming or outgoing) and sort them by amount and date.    
* Multiple Cards – A user can register multiple credit and or debit cards, from which to add funds to their accounts. When adding funds to their wallet, the user is prompted to select from which bank account to do so.
* Multiple Virtual Wallets – A user can create more than one wallet. When creating a transaction, the user is prompted to select, which wallet to use. The Transaction History Page show which wallet was used for the transaction. The user can set a default wallet, which is preselected when creating transactions.
* Joint Virtual Wallets – User can create joint virtual wallets. They function as the regular wallets, however, multiple users can use them. The original creator of the wallet has an administration panel for the wallet, where they can grant or revoke other user’s access to spend or add money to the wallet. When making a transaction or adding money to wallet, users with access to multiple wallets have to select, which one to use. The Transaction History Page show which wallet was used for the transaction.       

### Front-end
#### Public Part
The public part of your application should be accessible without authentication. This includes the application start page, the user login and user registration form.    

#### Private Part (regular users)      
Registered users should have private part in the web application accessible after successful login.     
Once logged in users have access to the following pages:
* View/Edit Profile, where users can view and edit their personal information.
* Add Funds, where the user can register a credit or debit card. A credit or debit card can only be linked to one account. A registered card can be removed or edited. Once a card is registered, the user has the option to add money to their Virtual Wallet. The user has to select the amount, which they want transferred into their Virtual Wallet. After the amount is selected the user can review the details of the transaction and choose to confirm it or go back to edit it.
* Crate Transactions Page, where the user can search the application’s users by phone number, username or email. The user list should be pageable and should only expose the user’s username and profile picture (you should not be able to find out all emails and phone numbers of all users with names starting with “a”). When the user selects the recipient, they are prompted to enter the transfer amount. Once user and amount are entered, the user can review the transaction details and confirm the transaction or go back to edit it. Blocked usersdo not have access to the page, instead they are show a message explaining that their account has been blocked and that they should contact the application support.
* Transaction History Page, where the user can enter a start and end date and optionally the recipient and/or direction of the transaction (incoming or outgoing). When submitting the search criteria, the user is shown a paginated list of their transactions, matching the criteria and has the option to sort them by date and amount.

#### Administration Part    
System administrators should have two additional pages:
* Manage Users, which contains a paginated list of all users (username, email and phone number). The list can be filtered by username, email or phone number. Next to each user row, there is a button to block or unblock the user, based on their current status.
* Master Transaction History Page, which has the same functionality as the Transaction History Page but contains the transactions history of all users and has an additional filter by sender in addition to the one for recipient.


                    
                   
                    
## How To Run
To run the application you should:
1. Clone the project Git repository.
2. Run the DB scripts to create tables and populate them with data.
3. Edit the DB url, username and password fields in application.properties file according to your configuration.  
4. Edit paths for storing files in the application.properties file.
6. Build and run the applicataion

The application will be available [here](http://localhost:8080).

The REST part of the appliactaion is documented with Swagger and is available [here](http://localhost:8080/swagger-ui.html) while the application is running.

                  
                  

## Sample Screenshots

### Home page
![Home page](Home.jpg)

### User login page
![Login page](Login.jpg)

### User register page
![Register page](Register.jpg)

### My Profile - Account page
![My profile Page](MyProfile.jpg)

### My Profile - Cards page
![My profile Page - Cards](Cards.jpg)

### My Profile - Wallets page
![My profile Page - Wallets](Wallets.jpg)

### Send Money Pick a User
![Send Money Initial Window](SendMoneyStep1.jpg)

### Send Money Enter Transaction Details
![Send Money Initial Window](SendMoneyStep2.jpg)

### Deposit Money page
![Deposit Money Page](DepositMoney.jpg)
