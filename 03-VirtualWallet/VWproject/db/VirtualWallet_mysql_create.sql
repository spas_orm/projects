use virtualwallet;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS cards;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS wallets;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS wallets_types;
DROP TABLE IF EXISTS users_wallets;
DROP TABLE IF EXISTS card_transactions;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `cards` (
	`card_id` int(11) NOT NULL AUTO_INCREMENT,
	`card_number` varchar(16) NOT NULL UNIQUE,
	`owner_name` varchar(50) NOT NULL,
	`exp_date` char(5) NOT NULL,
	`cvc` char(3) NOT NULL,
	`card_owner` int(11) NOT NULL,
	`is_deleted` tinyint NOT NULL,
	PRIMARY KEY (`card_id`)
);

CREATE TABLE `users` (
	`user_id` int(11) NOT NULL AUTO_INCREMENT,
	`username` varchar(15) NOT NULL UNIQUE,
	`password` varchar(68) NOT NULL,
	`enabled` tinyint(4) NOT NULL DEFAULT '1',
	`full_name` varchar(50),
	`email` varchar(50) NOT NULL UNIQUE,
	`phone` varchar(10) NOT NULL UNIQUE,
	`profile_pic_url` varchar(2048),
	`blocked` tinyint(4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`user_id`)
);

CREATE TABLE `wallets` (
	`wallet_id` int NOT NULL AUTO_INCREMENT,
	`name` varchar(25) NOT NULL DEFAULT 'Wallet',
	`balance` DECIMAL(10,2) NOT NULL DEFAULT '0',
	`wallet_type` int NOT NULL,
	PRIMARY KEY (`wallet_id`)
);

CREATE TABLE `transactions` (
	`transaction_id` bigint NOT NULL AUTO_INCREMENT,
	`amount` DECIMAL(10,2) NOT NULL DEFAULT '0',
	`sender` int NOT NULL,
	`sender_wallet` int NOT NULL,
	`receiver` int NOT NULL,
	`timestamp` TIMESTAMP NOT NULL,
	`confirmed` tinyint(4) NOT NULL,
	`description` varchar(255),
	PRIMARY KEY (`transaction_id`)
);

CREATE TABLE `wallets_types` (
	`wallet_type_id` int NOT NULL AUTO_INCREMENT,
	`wallet_type` varchar(25) NOT NULL,
	PRIMARY KEY (`wallet_type_id`)
);

CREATE TABLE `users_wallets` (
	`user_wallet_id` int NOT NULL AUTO_INCREMENT,
	`user_id` int NOT NULL,
	`wallet_id` int NOT NULL,
	`is_default` tinyint(4) DEFAULT '0',
	`is_created_by_user` tinyint(4) DEFAULT '0',
	`is_deleted` tinyint(4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`user_wallet_id`)
);

CREATE TABLE `authorities` (
	`username` varchar(25) NOT NULL,
	`authority` varchar(25) NOT NULL
);

CREATE TABLE `card_transactions` (
	`id` int NOT NULL AUTO_INCREMENT,
	`amount` int NOT NULL,
	`currency` varchar(255) NOT NULL,
	`description` varchar(255) NOT NULL,
	`idempotency_key` varchar(255) NOT NULL,
	`card_id` int NOT NULL,
	`receiving_user` int NOT NULL,
	`receiving_wallet` int NOT NULL,
	`timestamp` TIMESTAMP NOT NULL,
	`confirmed` BOOLEAN NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
);

ALTER TABLE `cards` ADD CONSTRAINT `cards_fk0` FOREIGN KEY (`card_owner`) REFERENCES `users`(`user_id`);

ALTER TABLE `wallets` ADD CONSTRAINT `wallets_fk0` FOREIGN KEY (`wallet_type`) REFERENCES `wallets_types`(`wallet_type_id`);

ALTER TABLE `transactions` ADD CONSTRAINT `transactions_fk0` FOREIGN KEY (`sender`) REFERENCES `users`(`user_id`);

ALTER TABLE `transactions` ADD CONSTRAINT `transactions_fk1` FOREIGN KEY (`sender_wallet`) REFERENCES `wallets`(`wallet_id`);

ALTER TABLE `transactions` ADD CONSTRAINT `transactions_fk2` FOREIGN KEY (`receiver`) REFERENCES `users`(`user_id`);

ALTER TABLE `users_wallets` ADD CONSTRAINT `users_wallets_fk0` FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`);

ALTER TABLE `users_wallets` ADD CONSTRAINT `users_wallets_fk1` FOREIGN KEY (`wallet_id`) REFERENCES `wallets`(`wallet_id`);

ALTER TABLE `authorities` ADD CONSTRAINT `authorities_fk0` FOREIGN KEY (`username`) REFERENCES `users`(`username`);

ALTER TABLE `card_transactions` ADD CONSTRAINT `card_transactions_fk0` FOREIGN KEY (`card_id`) REFERENCES `cards`(`card_id`);

ALTER TABLE `card_transactions` ADD CONSTRAINT `card_transactions_fk1` FOREIGN KEY (`receiving_user`) REFERENCES `users`(`user_id`);

ALTER TABLE `card_transactions` ADD CONSTRAINT `card_transactions_fk2` FOREIGN KEY (`receiving_wallet`) REFERENCES `wallets`(`wallet_id`);

