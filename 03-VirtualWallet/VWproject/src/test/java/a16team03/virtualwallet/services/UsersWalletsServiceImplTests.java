package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.DefaultEntityNotFoundException;
import a16team03.virtualwallet.exceptions.DuplicateUserWalletException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.UserWalletDTO;
import a16team03.virtualwallet.models.dtos.UserWalletUpdateDTO;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.models.mappers.UserWalletMapper;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.UsersWalletsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static a16team03.virtualwallet.Factory.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class UsersWalletsServiceImplTests {

    @Mock
    WalletMapper mockWalletMapper;

    @Mock
    WalletsService mockWalletsService;

    @Mock
    UserWalletMapper mockUserWalletMapper;

    @Mock
    UsersWalletsRepository mockUsersWalletsRepository;

    @InjectMocks
    UsersWalletsServiceImpl usersWalletsService;

    @Test
    public void createUserWallet_Should_ReturnDefaultUserWallet_WhenSuccessful() {
        //Arrange
        UserWallet expectedUW = createUserWallet();
        UserWalletDTO userWalletDTO = createUserWalletDTO();
        UserWallet otherUW = createUserWallet();
        otherUW.setId(2);
        otherUW.setDefaultWallet(true);
        List<UserWallet> userWalletList = new ArrayList<>();
        userWalletList.add(otherUW);
        Mockito.when(mockUsersWalletsRepository.getAllByUserId(anyInt()))
                .thenReturn(userWalletList);
        Mockito.when(mockUsersWalletsRepository.save(expectedUW))
                .thenReturn(expectedUW);
        Mockito.when(mockUserWalletMapper.dtoToUserWallet(userWalletDTO))
                .thenReturn(expectedUW);

        //Act
        UserWallet result = usersWalletsService.createUserWallet(userWalletDTO);

        //Assert
        Assert.assertSame(expectedUW, result);
    }

    @Test
    public void createUserWallet_Should_ReturnNonDefaultUserWallet_WhenSuccessful() {
        //Arrange
        UserWallet expectedUW = createUserWallet();
        UserWalletDTO userWalletDTO = createUserWalletDTO();
        Mockito.when(mockUsersWalletsRepository.save(expectedUW))
                .thenReturn(expectedUW);
        Mockito.when(mockUserWalletMapper.dtoToUserWallet(userWalletDTO))
                .thenReturn(expectedUW);

        //Act
        UserWallet result = usersWalletsService.createUserWallet(userWalletDTO);

        //Assert
        Assert.assertSame(expectedUW, result);
    }


    @Test
    public void createUserWallet_Should_ReturnCorrectUserWallet_WhenSuccessful() {
        //Arrange
        UserWallet expectedUW = createUserWallet();
        UserWalletDTO userWalletDTO = createUserWalletDTO();
        List<UserWallet> list = new ArrayList<>();
        list.add(expectedUW);

        Mockito.when(mockUsersWalletsRepository.save(expectedUW))
                .thenReturn(expectedUW);
        Mockito.when(mockUserWalletMapper.dtoToUserWallet(userWalletDTO))
                .thenReturn(expectedUW);

        //Act
        UserWallet result = usersWalletsService.createUserWallet(userWalletDTO);

        //Assert
        Assert.assertSame(expectedUW, result);
    }

    @Test(expected = DuplicateUserWalletException.class) //Assert
    public void createUserWallet_ShouldThrow_IfRecordWithSameUserAndWalletExist() {
        //Arrange
        UserWallet userWallet = createUserWallet();
        UserWalletDTO uwDto = createUserWalletDTO();

        Mockito.when(mockUserWalletMapper.dtoToUserWallet(uwDto)).thenReturn(userWallet);
        Mockito.when(mockUsersWalletsRepository.existsByUserUserIdAndWalletId(uwDto.getUserId(), uwDto.getWalletId()))
                .thenReturn(true);

        //Act
        UserWallet result = usersWalletsService.createUserWallet(uwDto);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void createUserWallet_ShouldThrow_IfMapperThrow() {
        //Arrange
        UserWallet expectedUW = createUserWallet();
        UserWalletDTO userWalletDTO = createUserWalletDTO();

        Mockito.when(mockUserWalletMapper.dtoToUserWallet(userWalletDTO))
                .thenThrow(new EntityNotFoundException());

        //Act
        UserWallet result = usersWalletsService.createUserWallet(userWalletDTO);
    }

    @Test
    public void createDefaultUserWallet_Should_ReturnUserWallet_WhenSuccessful() {
        //Arrange

        Wallet createdWallet = createWallet();
        UserWallet expectedUW = createUserWallet();

        Mockito.when(mockWalletsService.create(any())).thenReturn(createdWallet);
        Mockito.when(mockUsersWalletsRepository.save(any()))
                .thenReturn(expectedUW);
        Mockito.when(mockUserWalletMapper.dtoToUserWallet(any()))
                .thenReturn(expectedUW);

        //Act
        UserWallet result = usersWalletsService.createDefaultUserWallet(1);

        //Assert
        Assert.assertSame(expectedUW, result);
    }

    @Test
    public void addUserToWallet_Should_ReturnUserWallet_WhenSuccessful() {
        //Arange
        UserWallet expectedUW = createUserWallet();
        UserWalletDTO userWalletDTO = createUserWalletDTO();

        Mockito.when(mockUserWalletMapper.dtoToUserWallet(any())).thenReturn(expectedUW);
        Mockito.when(mockUsersWalletsRepository.save(any())).thenReturn(expectedUW);

        //Act
        UserWallet result = usersWalletsService.addUserToWallet(userWalletDTO);

        //Assert
        Assert.assertSame(expectedUW, result);

    }


    @Test(expected = EntityNotFoundException.class) //Assert
    public void addUserToWallet_ShouldThrow_IfMapperThrow() {
        //Arange

        UserWalletDTO userWalletDTO = createUserWalletDTO();

        Mockito.when(mockUserWalletMapper.dtoToUserWallet(any()))
                .thenThrow(new EntityNotFoundException());

        //Act
        UserWallet result = usersWalletsService.addUserToWallet(userWalletDTO);

    }

    @Test(expected = DuplicateUserWalletException.class) //Assert
    public void addUserToWallet_ShouldThrow_IfAlreadyExists() {
        //Arange
        UserWallet expectedUW = createUserWallet();
        UserWalletDTO userWalletDTO = createUserWalletDTO();

        Mockito.when(mockUserWalletMapper.dtoToUserWallet(any()))
                .thenReturn(expectedUW);
        Mockito.when(usersWalletsService.checkUserWithWalletExist(anyInt(), anyInt()))
                .thenReturn(true);

        //Act
        UserWallet result = usersWalletsService.addUserToWallet(userWalletDTO);

    }

    @Test
    public void getById_Should_ReturnUserWallet_WhenSuccessful() {
        //Arrange
        UserWallet expectedUW = createUserWallet();
        Mockito.when(mockUsersWalletsRepository.getById(anyInt())).thenReturn(expectedUW);

        //Act
        UserWallet result = usersWalletsService.getById(anyInt());

        //Assert
        Assert.assertSame(expectedUW, result);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_ShouldThrow_WhenUserWallet_NotFound() {
        //Arrange
        Mockito.when(mockUsersWalletsRepository.getById(anyInt())).thenReturn(null);

        //Act
        usersWalletsService.getById(anyInt());
    }

    @Test
    public void updateUserWallet_Should_ReturnUserWallet_WhenSuccessful() {
        //Arrange
        UserWallet uwToUpdate = createUserWallet();
        uwToUpdate.setCreatedByUser(true);
        uwToUpdate.setDefaultWallet(true);
        UserWalletUpdateDTO userWalletUpdateDTO = createUserWalletUpdateDTO();
        WalletDTO walletDTO = createWalletDTO();
        Wallet wallet = createWallet();
        Mockito.when(mockUsersWalletsRepository.getById(anyInt())).thenReturn(uwToUpdate);
        Mockito.when(mockWalletMapper.walletToDTO(any())).thenReturn(walletDTO);
        Mockito.when(mockWalletsService.updateById(anyInt(), any())).thenReturn(wallet);

        //Act
        UserWallet result = usersWalletsService.updateUserWallet(anyInt(), userWalletUpdateDTO);

        //Assert
        Assert.assertSame(uwToUpdate, result);
    }

    @Test
    public void updateUserWallet_Should_ReturnDefaultUserWallet_WhenSuccessful() {
        //Arrange
        UserWallet uwToUpdate = createUserWallet();
        uwToUpdate.setCreatedByUser(true);
        uwToUpdate.setDefaultWallet(false);
        UserWallet oldDefaultUW = createUserWallet();
        oldDefaultUW.setId(2);
        oldDefaultUW.setDefaultWallet(true);
        UserWalletUpdateDTO userWalletUpdateDTO = createUserWalletUpdateDTO();
        userWalletUpdateDTO.setDefaultWallet(true);
        WalletDTO walletDTO = createWalletDTO();
        Wallet wallet = createWallet();
        Mockito.when(mockUsersWalletsRepository.getById(anyInt())).thenReturn(uwToUpdate);
        Mockito.when(mockWalletMapper.walletToDTO(any())).thenReturn(walletDTO);
        Mockito.when(mockWalletsService.updateById(anyInt(), any())).thenReturn(wallet);
        Mockito.when(mockUsersWalletsRepository.getDefaultUserWalletByUserId(anyInt()))
                .thenReturn(oldDefaultUW);
        //Act
        UserWallet result = usersWalletsService.updateUserWallet(anyInt(), userWalletUpdateDTO);

        //Assert
        Assert.assertSame(uwToUpdate, result);
    }

    @Test(expected = OperationNotAllowedException.class)
    public void updateUserWallet_ShouldThrow_WhenUserIsNotCreator() {
        //Arrange
        UserWallet uwToUpdate = createUserWallet();
        uwToUpdate.setDefaultWallet(true);
        UserWalletUpdateDTO userWalletUpdateDTO = createUserWalletUpdateDTO();
        Mockito.when(mockUsersWalletsRepository.getById(anyInt())).thenReturn(uwToUpdate);

        //Act
        UserWallet result = usersWalletsService.updateUserWallet(anyInt(), userWalletUpdateDTO);

    }

    @Test(expected = OperationNotAllowedException.class)
    public void updateUserWallet_ShouldThrow_WhenPassedUserIdDoesntMatch() {
        //Arrange
        UserWallet uwToUpdate = createUserWallet();
        uwToUpdate.setCreatedByUser(true);
        uwToUpdate.setDefaultWallet(true);
        UserWalletUpdateDTO userWalletUpdateDTO = createUserWalletUpdateDTO();
        userWalletUpdateDTO.setUserId(101);

        Mockito.when(mockUsersWalletsRepository.getById(anyInt())).thenReturn(uwToUpdate);

        //Act
        UserWallet result = usersWalletsService.updateUserWallet(anyInt(), userWalletUpdateDTO);

    }

    @Test
    public void getUserWalletsByUserId_Should_ReturnListOfUserWalletsWhenSuccessful() {
        //Arrange
        int userId = 1;
        UserWallet uw1 = createUserWallet();
        uw1.getUser().setUserId(userId);
        List<UserWallet> uwList = new ArrayList<>();
        uwList.add(uw1);
        Mockito.when(mockUsersWalletsRepository.getAllByUserId(userId)).thenReturn(uwList);

        //Act
        List<UserWallet> result = usersWalletsService.getUsersWalletsByUserId(userId);

        //Assert
        Assert.assertEquals(userId, result.get(0).getUser().getUserId());
        Assert.assertSame(uwList.get(0), result.get(0));
    }

    @Test
    public void getUserWalletsByWalletId_Should_ReturnListOfUserWalletsWhenSuccessful() {
        //Arrange
        int walletId = 1;
        UserWallet uw1 = createUserWallet();
        uw1.getWallet().setId(walletId);
        List<UserWallet> uwList = new ArrayList<>();
        uwList.add(uw1);
        Mockito.when(mockUsersWalletsRepository.getAllByWalletId(walletId)).thenReturn(uwList);

        //Act
        List<UserWallet> result = usersWalletsService.getUsersWalletsByWalletId(walletId);

        //Assert
        Assert.assertEquals(walletId, (long) result.get(0).getWallet().getId());
        Assert.assertSame(uwList.get(0), result.get(0));
    }

    @Test
    public void geDefaultUserWalletByUserId_Should_ReturnDefaultUserWalletWhenSuccessful() {
        //Arrange
        UserWallet userWallet = createUserWallet();
        userWallet.setDefaultWallet(true);

        Mockito.when(mockUsersWalletsRepository.getDefaultUserWalletByUserId(anyInt()))
                .thenReturn(userWallet);

        //Act
        UserWallet result = usersWalletsService.getDefaultUserWalletByUserId(anyInt());

        //Assert
        Assert.assertTrue(result.getDefaultWallet());
    }

    @Test(expected = DefaultEntityNotFoundException.class) //Assert
    public void geDefaultUserWalletByUserId_ShouldThrow_WhenNoDefaultUserWalletForUserId() {
        //Arrange
        Mockito.when(mockUsersWalletsRepository.getDefaultUserWalletByUserId(anyInt()))
                .thenReturn(null);

        //Act
        UserWallet result = usersWalletsService.getDefaultUserWalletByUserId(anyInt());
    }

    @Test
    public void removeOwnWallet_Should_CallRepositoryWhenSuccessful() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(uwToDelete);

        //Act
        usersWalletsService.removeOwnWallet(1, uwDTO);

        //Assert
        Mockito.verify(mockUsersWalletsRepository, Mockito.times(1))
                .save(uwToDelete);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void removeOwnWallet_ShouldThrow_WhenUserWalletDoesntExists() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(null);

        //Act
        usersWalletsService.removeOwnWallet(1, uwDTO);
    }

    @Test(expected = OperationNotAllowedException.class) //Assert
    public void removeOwnWallet_ShouldThrow_WhenUserWalletNotCreatedByUser() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.setCreatedByUser(false);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(uwToDelete);

        //Act
        usersWalletsService.removeOwnWallet(1, uwDTO);
    }

    @Test(expected = OperationNotAllowedException.class) //Assert
    public void removeOwnWallet_ShouldThrow_WhenUserWalletIsDefaultForUser() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(true);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(uwToDelete);

        //Act
        usersWalletsService.removeOwnWallet(1, uwDTO);
    }

    @Test(expected = OperationNotAllowedException.class) //Assert
    public void removeOwnWallet_ShouldThrow_WhenUserWalletIsNotEmpty() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ONE);
        UserWalletDTO uwDTO = createUserWalletDTO();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(uwToDelete);

        //Act
        usersWalletsService.removeOwnWallet(1, uwDTO);
    }

    @Test
    public void removeUserForSharedWallet_Should_CallRepositoryWhenSuccessful() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.getUser().setUserId(2);
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();
        uwDTO.setUserId(2);
        List<Wallet> walletList = new ArrayList<>();
        walletList.add(uwToDelete.getWallet());

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(uwToDelete);
        Mockito.when(mockUsersWalletsRepository.getWalletsCreatedByUserId(anyInt()))
                .thenReturn(walletList);

        //Act
        usersWalletsService.removeUserForSharedWallet(1, uwDTO);

        //Assert
        Mockito.verify(mockUsersWalletsRepository, Mockito.times(1))
                .save(uwToDelete);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void removeUserForSharedWallet_ShouldThrow_WhenUserWalletDoesntExists() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(null);

        //Act
        usersWalletsService.removeUserForSharedWallet(1, uwDTO);
    }

    @Test(expected = OperationNotAllowedException.class) //Assert
    public void removeUserForSharedWallet__ShouldThrow_WhenUserWalletNotCreatedByRequester() {
        //Arrange
        UserWallet uwToDelete = createUserWallet();
        uwToDelete.getUser().setUserId(2);
        uwToDelete.setCreatedByUser(true);
        uwToDelete.setDefaultWallet(false);
        uwToDelete.getWallet().setBalance(BigDecimal.ZERO);
        UserWalletDTO uwDTO = createUserWalletDTO();
        uwDTO.setUserId(2);
        List<Wallet> walletList = new ArrayList<>();

        Mockito.when(mockUsersWalletsRepository.getByUserUserIdAndWalletId(anyInt(), anyInt()))
                .thenReturn(uwToDelete);
        Mockito.when(mockUsersWalletsRepository.getWalletsCreatedByUserId(anyInt()))
                .thenReturn(walletList);

        //Act
        usersWalletsService.removeUserForSharedWallet(1, uwDTO);
    }

    @Test
    public void getTotalBalanceInAllWalletsByUserId_Should_ReturnCorrectSum() {
        //Arrange
        UserWallet uw1 = createUserWallet();
        uw1.getWallet().setBalance(BigDecimal.ONE);
        UserWallet uw2 = createUserWallet();
        uw2.getWallet().setBalance(BigDecimal.TEN);
        List<UserWallet> uwList = new ArrayList<>();
        uwList.add(uw1);
        uwList.add(uw2);
        Mockito.when(mockUsersWalletsRepository.getAllByUserId(anyInt()))
                .thenReturn(uwList);
        //Act
        BigDecimal result = usersWalletsService.getTotalBalanceInAllWalletsByUserId(anyInt());

        //Assert
        Assert.assertEquals(11, result.intValue());

    }
}
