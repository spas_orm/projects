package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.CardTransactionAlreadyConfirmedException;
import a16team03.virtualwallet.models.CardTransaction;
import a16team03.virtualwallet.models.CardTransactionSearch;
import a16team03.virtualwallet.models.dtos.CardTransactionDTO;
import a16team03.virtualwallet.models.mappers.CardTransactionMapper;
import a16team03.virtualwallet.repositories.CardTransactionsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.net.ConnectException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Optional;

import static a16team03.virtualwallet.Factory.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CardTransactionsServiceImplTests {
    @Mock
    CardTransactionMapper cardTransactionMapper;
    
    @Mock
    CardTransactionsRepository mockRepository;

    @InjectMocks
    CardTransactionsServiceImpl mockService;

    @Test
    public void create_Should_ReturnCardTransaction_WhenSuccessful() {
        //Arrange
        CardTransaction cardTransaction = createCardTransaction();
        CardTransactionDTO cardTransactionDTO = createCardTransactionDTO();

        Mockito.when(cardTransactionMapper.dtoToCardTransaction(cardTransactionDTO))
                .thenReturn(cardTransaction);
        //Act
        mockService.create(cardTransactionDTO);
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .save(any());
    }

    @Test
    public void findAll_Should_CallRepository() {
        //Arrange
        Page<CardTransaction> page = new PageImpl<CardTransaction>(new ArrayList<>());
        Pageable mockPageable = createPageable();

        CardTransactionSearch cardTransactionSearch = createCardTransactionSearch();
        Mockito.when(mockRepository.findAll(cardTransactionSearch.getAmountFrom(),
                cardTransactionSearch.getAmountTo(),
                Timestamp.valueOf(cardTransactionSearch.getFromDate().atStartOfDay()),
                Timestamp.valueOf(cardTransactionSearch.getTillDate().atTime(23, 59)),
                cardTransactionSearch.getUsername(),
                mockPageable))
                .thenReturn(page);
        //Act
        mockService.findAll(cardTransactionSearch, mockPageable);
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAll(cardTransactionSearch.getAmountFrom(),
                cardTransactionSearch.getAmountTo(),
                Timestamp.valueOf(cardTransactionSearch.getFromDate().atStartOfDay()),
                Timestamp.valueOf(cardTransactionSearch.getTillDate().atTime(23, 59)),
                cardTransactionSearch.getUsername(),
                mockPageable);
    }

    @Test
    public void findAllByUserId() {
        //Arrange
        Pageable mockPageable = createPageable();

        CardTransactionSearch cardTransactionSearch = createCardTransactionSearch();
        //Act
        mockService.findAllByUserId(cardTransactionSearch, mockPageable);
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllForUser(cardTransactionSearch.getUserId(),
                cardTransactionSearch.getAmountFrom(),
                cardTransactionSearch.getAmountTo(),
                Timestamp.valueOf(cardTransactionSearch.getFromDate().atStartOfDay()),
                Timestamp.valueOf(cardTransactionSearch.getTillDate().atTime(23, 59)),
                cardTransactionSearch.getUsername(),
                mockPageable);
    }

    @Test(expected = CardTransactionAlreadyConfirmedException.class) //Assert
    public void execute_Should_Throw_WhenTransactionAlreadyConfirmed() throws ConnectException {
        //Arrange
        CardTransaction cardTransaction = createCardTransaction();
        cardTransaction.setConfirmed(true);
        //Act
        mockService.execute(cardTransaction);
    }

    @Test(expected = CardTransactionAlreadyConfirmedException.class) //Assert
    public void getLastUnconfirmedCardTransactionByUserId_Should_Throw_IfNoUnconfirmedTransactionsExist() {
        //Arrange
        Mockito.when(mockRepository.findFirstByReceivingUserUserIdAndConfirmedFalseOrderByIdDesc(anyInt()))
                .thenReturn(Optional.empty());
        //Act
        mockService.getLastUnconfirmedCardTransactionByUserId(anyInt());
    }

    @Test
    public void getLastUnconfirmedCardTransactionByUserId_ShouldReturnCardTransactionIfSuccessful() {
        //Arrange
        CardTransaction cardTransaction = createCardTransaction();
        Optional<CardTransaction> optionalCardTransaction = Optional.of(cardTransaction);
        Mockito.when(mockRepository.findFirstByReceivingUserUserIdAndConfirmedFalseOrderByIdDesc(anyInt()))
                .thenReturn(optionalCardTransaction);
        //Act
        CardTransaction receivedCardTransaction = mockService.getLastUnconfirmedCardTransactionByUserId(anyInt());
        //Assert
        Assert.assertEquals(cardTransaction, receivedCardTransaction);
    }
}
