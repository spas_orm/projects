package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.exceptions.InsufficientAmountException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.exceptions.TransactionAlreadyConfirmedException;
import a16team03.virtualwallet.models.*;
import a16team03.virtualwallet.models.dtos.TransactionDTO;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.models.mappers.TransactionMapper;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.TransactionsRepository;
import a16team03.virtualwallet.repositories.UsersWalletsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Optional;

import static a16team03.virtualwallet.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsServiceImplTests {

    @Mock
    TransactionMapper transactionMapper;

    @Mock
    TransactionsRepository transactionsRepository;

    @Mock
    UsersWalletsRepository usersWalletsRepository;

    @Mock
    UsersService usersService;

    @Mock
    WalletsService walletsService;

    @Mock
    UsersWalletsService usersWalletsService;

    @Mock
    WalletMapper walletMapper;

    @InjectMocks
    TransactionsServiceImpl transactionsService;

    @Test
    public void create_Should_ReturnTransaction_WhenSuccessful() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        TransactionDTO transactionDto = createTransactionDto();
        BigDecimal bigDecimal = BigDecimal.ONE;
        Wallet wallet=expectedTransaction.getSenderWallet();
        Mockito.when(transactionsRepository.save(expectedTransaction)).thenReturn(expectedTransaction);
        Mockito.when(transactionMapper.dtoToTransaction(transactionDto)).thenReturn(expectedTransaction);
        Mockito.when(walletsService.checkWalletHasSufficientAmount(wallet.getId(),bigDecimal)).thenReturn(true);
        //Act
        Transaction resultTransaction = transactionsService.create(transactionDto);

        //Assert
        Assert.assertSame(expectedTransaction, resultTransaction);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void create_ShouldThrow_IfUsersInTransactionDoesNotExist() {
        //Arrange
        TransactionDTO transactionDto = createTransactionDto();
        Mockito.when(transactionMapper.dtoToTransaction(transactionDto))
                .thenThrow(new EntityNotFoundException("exception"));

        //Act
        Transaction resultTransaction = transactionsService.create(transactionDto);

    }

    @Test(expected = OperationNotAllowedException.class) //Assert
    public void create_ShouldThrow_IfSenderIsBlocked() {
        //Arrange
        TransactionDTO transactionDto = createTransactionDto();
        Transaction expectedTransaction = createTransaction();
        expectedTransaction.getSendingUser().setBlocked(true);
        Mockito.when(transactionMapper.dtoToTransaction(transactionDto)).thenReturn(expectedTransaction);

        //Act
        Transaction resultTransaction = transactionsService.create(transactionDto);

    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void create_ShouldThrow_IfTransactionAmountIsZERO() {
        //Arrange
        TransactionDTO transactionDto = createTransactionDto();
        Transaction expectedTransaction = createTransaction();
        expectedTransaction.setAmount(BigDecimal.ZERO);
        Mockito.when(transactionMapper.dtoToTransaction(transactionDto)).thenReturn(expectedTransaction);

        //Act
        Transaction resultTransaction = transactionsService.create(transactionDto);

    }

    @Test(expected = InsufficientAmountException.class) //Assert
    public void create_ShouldThrow_IfInsufficientAmountInSenderWallet() {
        //Arrange
        TransactionDTO transactionDto = createTransactionDto();
        Transaction expectedTransaction = createTransaction();
        expectedTransaction.getSenderWallet().setBalance(BigDecimal.ZERO);
        Mockito.when(transactionMapper.dtoToTransaction(transactionDto)).thenReturn(expectedTransaction);

        //Act
        Transaction resultTransaction = transactionsService.create(transactionDto);

    }

    @Test(expected = OperationNotAllowedException.class) //Assert
    public void create_ShouldThrow_IfSenderAndRecipientIDSame() {
        //Arrange
        TransactionDTO transactionDto = createTransactionDto();
        transactionDto.setReceiverId(1);

        //Act
        Transaction resultTransaction = transactionsService.create(transactionDto);

    }

    @Test
    public void findAll_Should_ReturnPageTransactions_WhenSuccessful() {
        //Arrange
        TransactionSearch ts = new TransactionSearch();
        Pageable pageable = createPageable();
        Page<Transaction> transactionPage = new PageImpl<>(new ArrayList<>());

        Mockito.when(transactionsRepository.findAll(anyString(), anyString(), any(Timestamp.class), any(Timestamp.class), any()))
                .thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.findAll(ts, pageable);

        Assert.assertSame(transactionPage, result);
    }


    @Test
    public void getById_Should_ReturnTransaction_WhenSuccessful() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(java.util.Optional.of(expectedTransaction));

        //Act
        Transaction resultTransaction = transactionsService.getById(anyInt());

        //Assert
        Assert.assertSame(resultTransaction, expectedTransaction);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_ShouldThrow_WhenTransactionDoesntExist() {
        //Arrange
        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(Optional.empty());

        //Act
        Transaction resultTransaction = transactionsService.getById(anyInt());
    }

    @Test
    public void findAllForUserId_Should_ReturnPageTransactions_WhenSuccessful() {
        //Arrange
        TransactionSearch ts = new TransactionSearch();
        Pageable pageable = createPageable();
        Page<Transaction> transactionPage = new PageImpl<>(new ArrayList<>());
        User user = createUser();

        Mockito.when(usersService.getById(anyInt())).thenReturn(user);
        Mockito.when(transactionsRepository.findAllForUser(any(User.class), anyString(), anyString(),
                any(Timestamp.class), any(Timestamp.class), any()))
                .thenReturn(transactionPage);

        //Act
        Page<Transaction> result = transactionsService.findAllForUserId(anyInt(), ts, pageable);

        //Assert
        Assert.assertSame(transactionPage, result);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void findAllForUserId_ShouldThrow_WhenInvalidUserId() {
        //Arrange
        TransactionSearch ts = new TransactionSearch();
        Pageable pageable = createPageable();

        Mockito.when(usersService.getById(anyInt())).thenThrow(new EntityNotFoundException("exception"));

        //Act
        Page<Transaction> result = transactionsService.findAllForUserId(anyInt(), ts, pageable);

    }


    @Test
    public void isTransactionAlreadyConfirmed_Should_ReturnBoolean_WhenSuccessful() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        boolean confirmed = expectedTransaction.isConfirmed();

        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(java.util.Optional.of(expectedTransaction));

        //Act
        boolean resultConfirmed = transactionsService
                .isTransactionAlreadyConfirmed(anyInt());

        //Assert
        Assert.assertSame(confirmed, resultConfirmed);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void isTransactionAlreadyConfirmed_ShouldThrow_WhenTransactionDoesntExist() {
        //Arrange
        Mockito.when(transactionsService.getById(anyInt()))
                .thenThrow(new EntityNotFoundException("exception"));

        //Act
        boolean resultConfirmed = transactionsService
                .isTransactionAlreadyConfirmed(anyInt());
    }

    @Test(expected = TransactionAlreadyConfirmedException.class) //Assert
    public void changeBeforeConfirmById_ShouldThrow_WhenTransactionIsAlreadyConfirmed() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        expectedTransaction.setConfirmed(true);
        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(java.util.Optional.of(expectedTransaction));

        //Act
        Transaction result = transactionsService
                .changeBeforeConfirmById(anyInt(), createTransaction());
    }

    @Test
    public void changeBeforeConfirmById_Should_ReturnTransaction_WhenSuccessful() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        Mockito.when(transactionsRepository.findById(anyInt())).thenReturn(java.util.Optional.of(expectedTransaction));
        Mockito.when(transactionsRepository.save(expectedTransaction)).thenReturn(expectedTransaction);
        Mockito.when(walletsService.checkWalletHasSufficientAmount(anyInt(),any())).thenReturn(true);

        //Act
        Transaction result = transactionsService
                .changeBeforeConfirmById(anyInt(), expectedTransaction);

        //Assert
        Assert.assertSame(expectedTransaction, result);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void changeBeforeConfirmById_ShouldThrow_WhenGetByIdThrows() {
        //Arrange
        Mockito.when(transactionsRepository.findById(anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act
        Transaction result = transactionsService
                .changeBeforeConfirmById(anyInt(), createTransaction());
    }

    @Test
    public void confirmById_Should_ReturnConfirmedTransaction_WhenSuccessful() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        UserWallet userWallet = createUserWallet();
        BigDecimal bigDecimal = BigDecimal.ONE;

        Mockito.when(transactionsRepository.findById(1))
                .thenReturn(java.util.Optional.of(expectedTransaction));
        Mockito.when(walletsService.checkWalletHasSufficientAmount(1,bigDecimal))
                .thenReturn(true);
        Mockito.when(usersWalletsService.getDefaultUserWalletByUserId(2))
                .thenReturn(userWallet);
        //Act
        Transaction result = transactionsService.confirmById(1);

        //Assert
        Assert.assertSame(true, result.isConfirmed());
    }

    @Test(expected = TransactionAlreadyConfirmedException.class) //Assert
    public void confirmById_ShouldThrow_WhenTransactionAlreadyConfirmed() {
        //Arrange
        Transaction expectedTransaction = createTransaction();
        expectedTransaction.setConfirmed(true);
        Mockito.when(transactionsRepository.findById(anyInt()))
                .thenReturn(Optional.of(expectedTransaction));

        //Act
        Transaction result = transactionsService.confirmById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void confirmById_ShouldThrow_WhenTransactionDoesntExist() {
        //Arrange

        //Act
        Transaction result = transactionsService.confirmById(anyInt());
    }

    @Test
    public void setTransactionSearchValues_Should_ReturnNotNullFields() {
        TransactionSearch ts = new TransactionSearch();

        TransactionSearch result = transactionsService.setTransactionSearchValues(ts);

        Assert.assertSame(result.getRecipientUsername(), "");
        Assert.assertSame(result.getSenderUsername(), "");
        Assert.assertNotNull(result.getFromDate());
        Assert.assertNotNull(result.getTillDate());
    }

}
