//package a16team03.virtualwallet.services;
//
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.Spy;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.FileInputStream;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.nio.file.StandardCopyOption;
//
//import static org.mockito.ArgumentMatchers.*;
//
//@RunWith(MockitoJUnitRunner.class)
//@ContextConfiguration(classes = {ImageFileServiceImpl.class, ImageFileServiceImplTests.class})
//@TestPropertySource("/test.properties")
//public class ImageFileServiceImplTests {
////    private InputStream is;
//
////    @Spy
////    private Files files;
//
//    @Mock
//    MultipartFile mockImage;
//
//    @InjectMocks
//    ImageFileServiceImpl imageService;
//
//
//    @Test
//    public void uploadFile_Should_ReturnNewImgUrl_WhenImgUploadedSuccessfully() throws Exception{
//        //Arrange;
//
//        //Act
//        String result = imageService.uploadFile(mockImage);
//        System.out.println(result);
//        //
//        Assert.assertTrue(result.startsWith("http://localhost:8080/uploads/gallery"));
//    }
//}
//
////    @Test
////    public void uploadFile_Should_ReturnNewImgUrl_WhenImgUploadedSuccessfully() throws Exception{
////        //Arrange;
////        FileInputStream is = new FileInputStream("D:/test-1/myFile.png");
////        MockMultipartFile mockMultipartFile = new MockMultipartFile("myFile","myFile.png","image/png", is);
//////        Path copyLocation = Paths.get("D:/test","testfile.png");
//////        Mockito.doNothing().when(Files.copy(new FileInputStream("D:/test-1/myFile.png"),copyLocation));
////        //Act
////        String result = imageService.uploadFile(mockMultipartFile,"User");
////        System.out.println(result);
////        //
////        Assert.assertTrue(result.startsWith("http://localhost:8080/uploads/gallery"));
////    }
////}
