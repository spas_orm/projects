package a16team03.virtualwallet.services;

import a16team03.virtualwallet.configurations.jwt.JwtTokenProvider;
import a16team03.virtualwallet.exceptions.*;
import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserSearch;
import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.dtos.UserPasswordChangeDTO;
import a16team03.virtualwallet.models.dtos.UserPersonalDetailsDTO;
import a16team03.virtualwallet.models.dtos.UserRegistrationDTO;
import a16team03.virtualwallet.models.enums.Authority;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.UsersRepository;
import a16team03.virtualwallet.repositories.WalletsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.multipart.MultipartFile;
import sun.security.acl.PrincipalImpl;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static a16team03.virtualwallet.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {

    @Mock
    UsersWalletsService mockUsersWalletsService;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    UserDetailsManager userDetailsManager;

    @Mock
    ImageFileService mockImageService;

    @Mock
    MultipartFile mockImage;

    @Mock
    UsersRepository mockUsersRepository;

    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    Authentication mockAuthentication;

    @Mock
    JwtTokenProvider jwtTokenProvider;

    @InjectMocks
    UsersServiceImpl mockUsersService;

    @Test
    public void create_Should_ReturnUser_WhenSuccessful() {
        //Arrange
        UserRegistrationDTO receivedUserRegDTO = createUserRegistrationDTO();
        User expectedUser = createUser();
        UserWallet userWallet = createUserWallet();
        userWallet.setUser(expectedUser);

        userWallet.setDefaultWallet(true);

        Mockito.when(mockUsersRepository.save(any()))
                .thenReturn(expectedUser);

        Mockito.when(passwordEncoder.encode(receivedUserRegDTO.getPassword()))
                .thenReturn(receivedUserRegDTO.getPassword());

        Mockito.when(mockImageService.getDefaultUserImgPath()).thenReturn("http://localhost:8080/images/no_image.png");

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User securityUser =
                new org.springframework.security.core.userdetails.User(receivedUserRegDTO.getUsername(),
                        receivedUserRegDTO.getPassword(),
                        authorities);

        Mockito.when(mockUsersWalletsService.createDefaultUserWallet(expectedUser.getUserId())).
            thenReturn(userWallet);

        


        //Act
        User returnedUser = mockUsersService.create(receivedUserRegDTO);
        //Assert
        Assert.assertEquals(expectedUser, returnedUser);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_Should_Throw_WhenUserWithTheSameUsernameExists() {
        //Arrange
        UserRegistrationDTO duplicateUser = createUserRegistrationDTO();

        Mockito.when(mockUsersRepository.existsUserByUsername(anyString()))
                .thenReturn(true);
        //Act
        mockUsersService.create(duplicateUser);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_Should_Throw_WhenUserWithTheSameEmailExists() {
        //Arrange
        UserRegistrationDTO duplicateUser = createUserRegistrationDTO();

        Mockito.when(mockUsersRepository.existsUserByUsername(anyString()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUsersByEmail(anyString()))
                .thenReturn(true);
        //Act
        mockUsersService.create(duplicateUser);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_Should_Throw_WhenUserWithTheSamePhoneExists() {
        //Arrange
        UserRegistrationDTO duplicateUser = createUserRegistrationDTO();

        Mockito.when(mockUsersRepository.existsUserByUsername(anyString()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUsersByEmail(anyString()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhone(anyString()))
                .thenReturn(true);
        //Act
        mockUsersService.create(duplicateUser);
    }

    @Test(expected = PasswordMismatchException.class) //Assert
    public void create_Should_Throw_WhenPasswordsDoNotMatch() {
        //Arrange
        UserRegistrationDTO userWithMismatchedPasswords = createUserRegistrationDTO();
        userWithMismatchedPasswords.setPassword("password");
        userWithMismatchedPasswords.setPasswordConfirmation("mismatch");

        Mockito.when(mockUsersRepository.existsUserByUsername(anyString()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUsersByEmail(anyString()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhone(anyString()))
                .thenReturn(false);
        //Act
        mockUsersService.create(userWithMismatchedPasswords);
    }

    @Test
    public void signIn_Should_ReturnTokenString_WhenSuccessful(){
        //Arrange
        User user =createUser();
        String token = "TokenString";
        String username = "username";
        String password = "password";
        List<Authority> authorityList = new ArrayList<>();
        user.setRoles(authorityList);

        Mockito.when(authenticationManager.authenticate(any()))
                .thenReturn(mockAuthentication);
        Mockito.when(mockUsersRepository.findByUsername(anyString())).thenReturn(user);

        Mockito.when(jwtTokenProvider.createToken(anyString(),anyList()))
                .thenReturn(token);
        //Act
        String result = mockUsersService.signIn(username,password);

        //Assert
        Assert.assertTrue(token.equals(result));
    }

    @Test(expected = CustomException.class)//Assert
    public void signIn_Should_Throw_WhenAuthenticateThrow(){
        //Arrange
        User user =createUser();
        String token = "TokenString";
        String username = "username";
        String password = "password";
        List<Authority> authorityList = new ArrayList<>();
        user.setRoles(authorityList);

        Mockito.when(authenticationManager.authenticate(any()))
                .thenThrow(new AuthenticationServiceException(""));

        //Act
        String result = mockUsersService.signIn(username,password);

    }


    @Test
    public void getAll_Should_ReturnEmptyIterable_WhenNoUsersExist() {
        //Arrange
        Iterable<User> usersList = new ArrayList<>();
        Mockito.when(mockUsersRepository.findAll())
                .thenReturn(usersList);

        //Act
        List<User> usersListResult = (List<User>) mockUsersService.getAll();

        //Assert
        Assert.assertTrue(usersListResult.isEmpty());
    }

    @Test
    public void getAll_Should_ReturnCorrectIterable_WhenUsersExist() {
        //Arrange
        List<User> users = new ArrayList<>();
        users.add(createUser());
        Mockito.when(mockUsersRepository.findAll())
                .thenReturn(users);

        //Act
        List<User> userListResult = (List<User>) mockUsersService.getAll();

        //Assert
        Assert.assertEquals(users, userListResult);
    }

    @Test
    public void getAll_Should_ReturnPageUsers_WhenSuccessful(){
        //Arrange
        UserSearch userSearch = new UserSearch();
        Pageable pageable = createPageable();
        Page<User> userPage = new PageImpl<>(new ArrayList<>());
        Mockito.when(mockUsersRepository.findAll(anyString(),anyString(),anyString(),any()))
                .thenReturn(userPage);
        //Act
        Page<User> result = mockUsersService.getAll(userSearch,pageable);

        //Assert
        Assert.assertSame(userPage,result);
    }

    @Test
    public void findAllUsersByDetail_Should_ReturnPageUsers_WhenUserDetailNotNull(){
        //Arrange
        String userDetail = "user";
        Pageable pageable = createPageable();
        Page<User> userPage = new PageImpl<>(new ArrayList<>());
        Mockito.when(mockUsersRepository.findAllUsersByDetail(anyString(),any()))
                .thenReturn(userPage);
        //Act
        Page<User> result = mockUsersService.findAllUsersByDetail(userDetail,pageable);

        //Assert
        Assert.assertSame(userPage,result);
    }

    @Test
    public void findAllUsersByDetail_Should_ReturnPageUsers_WhenUserDetailNull(){
        //Arrange
        String userDetail=null;
        Pageable pageable = createPageable();
        Page<User> userPage = new PageImpl<>(new ArrayList<>());
        Mockito.when(mockUsersRepository.findAllUsersByDetail(anyString(),any()))
                .thenReturn(userPage);
        //Act
        Page<User> result = mockUsersService.findAllUsersByDetail(userDetail,pageable);

        //Assert
        Assert.assertSame(userPage,result);
    }

    @Test
    public void getById_Should_ReturnUser_WhenUserExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUsersRepository.findById(expectedUser.getUserId()))
                .thenReturn(java.util.Optional.of(expectedUser));
        //Act
        User resultUser = mockUsersService.getById(expectedUser.getUserId());
        //Assert
        Assert.assertEquals(expectedUser, resultUser);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_Should_Throw_WhenUserWithIdDoesNotExist() {
        //Arrange
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.empty());
        //Act
        mockUsersService.getById(anyInt());
    }

    @Test
    public void updateById_Should_ReturnUser_WhenSuccessful() throws IOException {
        //Arrange
        User expectedUser = createUser();

        Mockito.when(mockUsersRepository.findById(expectedUser.getUserId()))
                .thenReturn(Optional.of(expectedUser));

        Mockito.when(mockUsersRepository.existsUserByUsernameAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhoneAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockImageService.uploadFile(mockImage))
                .thenReturn("Example file path");

        Mockito.when(mockUsersRepository.save(expectedUser))
                .thenReturn(expectedUser);
        //Act
        User returnedUser = mockUsersService.updateById(expectedUser.getUserId(), expectedUser, mockImage);
        //Assert
        Assert.assertEquals(expectedUser, returnedUser);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void updateById_Should_Throw_WhenUserWithTheSameUsernameExists() throws IOException {
        //Arrange
        User duplicateUser = createUser();

        Mockito.when(mockUsersRepository.findById(duplicateUser.getUserId()))
                .thenReturn(Optional.of(duplicateUser));

        Mockito.when(mockUsersRepository.existsUserByUsernameAndUserIdNot(anyString(), anyInt()))
                .thenReturn(true);
        //Act
        mockUsersService.updateById(duplicateUser.getUserId(), duplicateUser, mockImage);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void updateById_Should_Throw_WhenUserWithTheSameEmailExists() throws IOException {
        //Arrange
        User duplicateUser = createUser();

        Mockito.when(mockUsersRepository.findById(duplicateUser.getUserId()))
                .thenReturn(Optional.of(duplicateUser));

        Mockito.when(mockUsersRepository.existsUserByUsernameAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(true);

        //Act
        mockUsersService.updateById(duplicateUser.getUserId(), duplicateUser, mockImage);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void updateById_Should_Throw_WhenUserWithTheSamePhoneExists() throws IOException {
        //Arrange
        User duplicateUser = createUser();

        Mockito.when(mockUsersRepository.findById(duplicateUser.getUserId()))
                .thenReturn(Optional.of(duplicateUser));

        Mockito.when(mockUsersRepository.existsUserByUsernameAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhoneAndUserIdNot(anyString(), anyInt()))
                .thenReturn(true);

        //Act
        mockUsersService.updateById(duplicateUser.getUserId(), duplicateUser, mockImage);
    }

    @Test(expected = PasswordMismatchException.class) //Assert
    public void updateById_Should_Throw_WhenPasswordDoNotMatchConfirmation() throws IOException {
        //Arrange
        User user = createUser();
        user.setPasswordConfirmation("different");

        Mockito.when(mockUsersRepository.findById(user.getUserId()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockUsersRepository.existsUserByUsernameAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhoneAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        //Act
        mockUsersService.updateById(user.getUserId(), user, mockImage);
    }

    @Test
    public void deleteById_Should_CallRepository() {
        //Arrange
        Optional<User> optionalUser = Optional.of(createUser());
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(optionalUser);
        //Act
        mockUsersService.deleteById(anyInt());
        //Assert
        Mockito.verify(mockUsersRepository, times(1))
                .save(any());
    }

    @Test
    public void changePassword_Should_CallRepository_WhenSuccessful() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        UserPasswordChangeDTO userPasswordChangeDTO = createUserPasswordChangeDTO();

        Mockito.when(passwordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);

        userPasswordChangeDTO.setNewPasswordConfirmation(userPasswordChangeDTO.getNewPassword());

        //Act
        mockUsersService.changePassword(userPasswordChangeDTO);
        //Assert
        Mockito.verify(mockUsersRepository, Mockito.times(1))
                .save(user);
    }

    @Test(expected = WrongPasswordException.class) //Assert
    public void changePassword_Should_Throw_WhenPasswordIsWrong() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        UserPasswordChangeDTO userPasswordChangeDTO = createUserPasswordChangeDTO();
        userPasswordChangeDTO.setOldPassword(user.getPassword() + user.getPassword());
        //Act
        mockUsersService.changePassword(userPasswordChangeDTO);
    }

    @Test(expected = PasswordMismatchException.class) //Assert
    public void changePassword_Should_Throw_WhenPasswordsDoNotMatch() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        UserPasswordChangeDTO userPasswordChangeDTO = createUserPasswordChangeDTO();

        Mockito.when(passwordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);

        userPasswordChangeDTO.setNewPasswordConfirmation
                (userPasswordChangeDTO.getNewPassword() + userPasswordChangeDTO.getNewPassword());
        //Act
        mockUsersService.changePassword(userPasswordChangeDTO);
    }

    @Test
    public void changePersonalDetails_Should_CallRepository_WhenSuccessful() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhoneAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        UserPersonalDetailsDTO userPersonalDetailsDTO = createUserPersonalDetailsDTO();
        //Act
        mockUsersService.changePersonalDetails(userPersonalDetailsDTO);
        //Assert
        Mockito.verify(mockUsersRepository, Mockito.times(1))
                .save(user);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void changePersonalDetails_Should_Throw_When_AnotherUserWithTheSameEmailExists() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(true);

        UserPersonalDetailsDTO userPersonalDetailsDTO = createUserPersonalDetailsDTO();
        //Act
        mockUsersService.changePersonalDetails(userPersonalDetailsDTO);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void changePersonalDetails_Should_Throw_When_AnotherUserWithTheSamePhoneExists() {
        //Arrange
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        Mockito.when(mockUsersRepository.existsUserByEmailAndUserIdNot(anyString(), anyInt()))
                .thenReturn(false);

        Mockito.when(mockUsersRepository.existsUserByPhoneAndUserIdNot(anyString(), anyInt()))
                .thenReturn(true);

        UserPersonalDetailsDTO userPersonalDetailsDTO = createUserPersonalDetailsDTO();
        //Act
        mockUsersService.changePersonalDetails(userPersonalDetailsDTO);
    }

    @Test(expected = UserNotLoggedInException.class) //Assert
    public void getIdFromPrincipal_Should_Throw_WhenPrincipalIsNull() {
        //Arrange
        Principal principal = null;
        //Act
        mockUsersService.getIdFromPrincipal(principal);
    }

    @Test
    public void getIdFromPrincipal_Should_ReturnUserId_WhenPrincipalNotNull() {
        //Arrange
        Principal principal = new PrincipalImpl("username");
        User user = createUser();
        Mockito.when(mockUsersRepository.findByUsername(anyString())).thenReturn(user);

        //Act
        int result = mockUsersService.getIdFromPrincipal(principal);

        //Assert
        Assert.assertEquals(user.getUserId(),result);

    }

    @Test
    public void updateUserBlockedStatus_Should_Call_Repository() {
        //Arrange
        User userToUpdate = createUser();

        Mockito.when(mockUsersRepository.findById(anyInt()))
                .thenReturn(Optional.of(userToUpdate));
        //Act
        mockUsersService.updateUserBlockedStatus(anyInt());
        //Assert
        Mockito.verify(mockUsersRepository, Mockito.times(1))
                .save(userToUpdate);
    }

    @Test
    public void changeProfilePicture_ShouldCallRepository_WhenSuccessful()throws IOException{
        //Arrange
//        String fileName = "test.jpg";
//        MockMultipartFile mockMultipartFile = new MockMultipartFile("user-file",fileName,
//                "text/plain", "test data".getBytes());
        User user = createUser();
        Mockito.when(mockUsersRepository.findById(anyInt())).thenReturn(Optional.of(user));
        Mockito.when(mockImageService.uploadFile(mockImage))
                .thenReturn("Example file path");
        //Act
        mockUsersService.changeProfilePicture(anyInt(),mockImage);

        //Assert
        Mockito.verify(mockUsersRepository,Mockito.times(1)).save(user);

    }
}
