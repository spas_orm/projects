package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.dtos.CardDTO;
import a16team03.virtualwallet.models.dtos.CardDetailsDTO;
import a16team03.virtualwallet.models.mappers.CardMapper;
import a16team03.virtualwallet.repositories.CardsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static a16team03.virtualwallet.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class CardsServiceImplTests {
    @Mock
    CardMapper cardMapper;

    @Mock
    CardsRepository mockCardsRepository;

    @InjectMocks
    CardsServiceImpl mockCardsService;

    @Test
    public void create_Should_ReturnCard_WhenSuccessful() {
        //Arrange
        Card expectedCard = createCard();
        CardDTO receivedCardDTO = createCardDTO();

        Mockito.when(cardMapper.dtoToCard(receivedCardDTO))
                .thenReturn(expectedCard);

        Mockito.when(mockCardsRepository.save(expectedCard))
                .thenReturn(expectedCard);
        //Act
        Card returnedCard = mockCardsService.create(receivedCardDTO);
        //Assert
        Assert.assertEquals(expectedCard, returnedCard);
    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_Should_Throw_WhenCardWithTheSameCardNumberExists() {
        //Arrange
        CardDTO duplicateCardDTO = createCardDTO();

        Mockito.when(mockCardsRepository.existsCardByCardNumber(anyString()))
                .thenReturn(true);
        //Act
        mockCardsService.create(duplicateCardDTO);
    }

    @Test
    public void getById_Should_ReturnCard_WhenCardExists() {
        //Arrange
        Card expectedCard = createCard();
        Mockito.when(mockCardsRepository.findById(expectedCard.getCardId()))
                .thenReturn(java.util.Optional.of(expectedCard));

        //Act
        Card resultCard = mockCardsService.getById(expectedCard.getCardId());

        //Assert
        Assert.assertEquals(expectedCard, resultCard);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_Should_Throw_WhenCardWithIdDoesNotExist() {
        //Arrange
        Mockito.when(mockCardsRepository.findById(anyInt()))
                .thenReturn(Optional.empty());
        //Act
        mockCardsService.getById(anyInt());
    }

    @Test
    public void getAll_Should_CallRepository() {
        //Arrange
        List<Card> list = new ArrayList<>();

        Mockito.when(mockCardsRepository.findAll())
                .thenReturn(list);
        //Act
        mockCardsService.getAll();
        //Assert
        Mockito.verify(mockCardsRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void updateById_Should_ReturnCard_WhenSuccessful() {
        //Arrange
        Card expectedCard = createCard();
        CardDTO cardDTO = createCardDTO();

        Mockito.when(mockCardsRepository.findById(expectedCard.getCardId()))
                .thenReturn(Optional.of(expectedCard));

        Mockito.when(mockCardsRepository.save(expectedCard))
                .thenReturn(expectedCard);
        //Act
        Card returnedCard = mockCardsService.updateById(expectedCard.getCardId(), cardDTO);
        //Assert
        Assert.assertEquals(expectedCard, returnedCard);
    }

//    @Test(expected = DuplicateEntityException.class) //Assert
//    public void updateById_Should_Throw_WhenCardWithTheSameCardNumberExists() {
//        //Arrange
//        Card duplicateCard = createCard();
//        CardDTO duplicateCardDTO = createCardDTO();
//
//        List<Card> cards = new ArrayList<>();
//        cards.add(duplicateCard);
//
//        Mockito.when(mockCardsService.getAll())
//                .thenReturn(cards);
//        //Act
//        mockCardsService.updateById(duplicateCard.getCardId(), duplicateCardDTO);
//    }

    @Test
    public void deleteById_Should_CallRepository() {
        //Arrange
        Optional<Card> optionalCard = Optional.of(createCard());

        Mockito.when(mockCardsRepository.findById(anyInt()))
                .thenReturn(optionalCard);
        //Act
        mockCardsService.deleteById(anyInt());

        //Assert
        Mockito.verify(mockCardsRepository, Mockito.times(1))
                .save(optionalCard.get());
    }

    @Test
    public void getAllByUserId_Should_ReturnListWithAllCards_BelongingToTheUser() {
        //Arrange
        User user = createUser();
        Card card = createCard();
        card.setCardOwner(user);

        List<Card> cards = new ArrayList<>();
        cards.add(card);

        Mockito.when(mockCardsService.getAllByUserId(user.getUserId()))
                .thenReturn(cards);
        //Act
        List<Card> receivedList = mockCardsService.getAllByUserId(user.getUserId());

        //
        Assert.assertEquals(cards, receivedList);
    }

    @Test
    public void getAllByUserId_Should_ReturnEmptyList_IfNoCardsBelongToTheUser() {
        //Arrange
        User user = createUser();

        List<Card> emptyList = new ArrayList<>();

        Mockito.when(mockCardsService.getAllByUserId(user.getUserId()))
                .thenReturn(emptyList);
        //Act
        List<Card> receivedList = mockCardsService.getAllByUserId(user.getUserId());

        //
        Assert.assertEquals(emptyList, receivedList);
    }

    @Test
    public void changeCardDetails_Should_CallRepository_WhenSuccessful() {
        //Arrange
        Card card = createCard();
        CardDetailsDTO cardDetailsDTO = createCardDetailsDTO();

        Mockito.when(mockCardsRepository.findById(anyInt()))
                .thenReturn(Optional.of(card));
        //Act
        mockCardsService.changeCardDetails(cardDetailsDTO);
        //Assert
        Mockito.verify(mockCardsRepository, Mockito.times(1)).save(any());
    }
}
