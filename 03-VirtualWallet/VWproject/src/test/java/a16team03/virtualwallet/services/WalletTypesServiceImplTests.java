package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.WalletType;
import a16team03.virtualwallet.repositories.WalletTypesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static a16team03.virtualwallet.Factory.createWalletType;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class WalletTypesServiceImplTests {
    @Mock
    WalletTypesRepository mockWalletTypesRepository;

    @InjectMocks
    WalletTypesServiceImpl mockWalletTypesService;

    @Test
    public void getById_Should_ReturnWalletType_WhenWalletTypeExists() {
        //Arrange
        WalletType expectedWalletType = createWalletType();
        Mockito.when(mockWalletTypesRepository.findById(expectedWalletType.getId()))
                .thenReturn(java.util.Optional.of(expectedWalletType));
        //Act
        WalletType receivedWalletType = mockWalletTypesService.getWalletTypeById(expectedWalletType.getId());
        //Assert
        Assert.assertEquals(expectedWalletType, receivedWalletType);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_Should_Throw_WhenWalletTypeWithIdDoesNotExist() {
        //Arrange
        Mockito.when(mockWalletTypesRepository.findById(anyInt()))
                .thenReturn(Optional.empty());
        //Act
        mockWalletTypesService.getWalletTypeById(anyInt());
    }

    @Test
    public void getAll_Should_ReturnEmptyIterable_WhenNoWalletTypesExist() {
        //Arrange
        List<WalletType> walletTypes = new ArrayList<>();
        Mockito.when(mockWalletTypesRepository.findAll())
                .thenReturn(walletTypes);

        //Act
        List<WalletType> walletTypesResult = (List<WalletType>) mockWalletTypesService.getAll();

        //Assert
        Assert.assertTrue(walletTypesResult.isEmpty());
    }

    @Test
    public void getAll_Should_ReturnCorrectIterable_WhenWalletTypesExist() {
        //Arrange
        List<WalletType> walletTypes = new ArrayList<>();
        WalletType walletType = createWalletType();
        walletTypes.add(walletType);

        Mockito.when(mockWalletTypesRepository.findAll())
                .thenReturn(walletTypes);

        //Act
        List<WalletType> walletTypesResult = (List<WalletType>) mockWalletTypesService.getAll();

        //Assert
        Assert.assertEquals(walletTypes, walletTypesResult);
    }

    @Test
    public void getTypeNameById_ShouldReturnString_WhenPassedValidId() {
        //Arrange
        WalletType walletType = createWalletType();
        String walletTypeName = walletType.getTypeName();

        Mockito.when(mockWalletTypesRepository.findById(walletType.getId()))
                .thenReturn(Optional.of(walletType));
        //Act
        String receivedTypeName = mockWalletTypesService.getTypeNameById(walletType.getId());
        //Assert
        Assert.assertEquals(walletTypeName, receivedTypeName);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getTypeNameById_ShouldThrow_WhenPassedInvalidId() {
        //Arrange
        Mockito.when(mockWalletTypesRepository.findById(anyInt()))
                .thenReturn(Optional.empty());
        //Act
        String receivedTypeName = mockWalletTypesService.getTypeNameById(anyInt());
    }
}
