package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.WalletsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static a16team03.virtualwallet.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class WalletsServiceImplTests {
    @Mock
    WalletMapper walletMapper;

    @Mock
    WalletsRepository mockWalletsRepository;

    @Mock
    WalletTypesService mockWalletTypesService;

    @InjectMocks
    WalletsServiceImpl mockWalletsService;

    @Test
    public void create_Should_ReturnWallet_WhenSuccessful() {
        //Arrange
        Wallet expectedWallet = createWallet();
        WalletDTO receivedWalletDTO = createWalletDTO();

        Mockito.when(walletMapper.dtoToWallet(receivedWalletDTO))
                .thenReturn(expectedWallet);

        Mockito.when(mockWalletsRepository.save(expectedWallet))
                .thenReturn(expectedWallet);
        //Act
        Wallet returnedWallet = mockWalletsService.create(receivedWalletDTO);
        //Assert
        Assert.assertEquals(expectedWallet, returnedWallet);
    }

    @Test
    public void getById_Should_ReturnWallet_WhenWalletExists() {
        //Arrange
        Wallet expectedWallet = createWallet();
        Mockito.when(mockWalletsRepository.findById(expectedWallet.getId()))
                .thenReturn(java.util.Optional.of(expectedWallet));

        //Act
        Wallet returnedWallet = mockWalletsService.getById(expectedWallet.getId());

        //Assert
        Assert.assertEquals(expectedWallet, returnedWallet);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_Should_Throw_WhenWalletWithIdDoesNotExist() {
        //Arrange
        Mockito.when(mockWalletsRepository.findById(anyInt()))
                .thenReturn(Optional.empty());
        //Act
        mockWalletsService.getById(anyInt());
    }

    @Test
    public void findAll_Should_ReturnListOfWallets(){
        //Arrange
        Wallet wallet1 = createWallet();
        Collection<Wallet> collection = new ArrayList<>();
        collection.add(wallet1);
        Mockito.when(mockWalletsRepository.findAll()).thenReturn(collection);
        //Act
        Collection<Wallet> result = (Collection<Wallet>) mockWalletsService.findAll();

        Assert.assertSame(collection,result);
    }

    @Test
    public void updateById_Should_ReturnWallet_WhenSuccessful() {
        //Arrange
        Wallet expectedWallet = createWallet();
        WalletDTO walletDTO = createWalletDTO();

        Mockito.when(mockWalletsRepository.findById(expectedWallet.getId()))
                .thenReturn(Optional.of(expectedWallet));
        Mockito.when(mockWalletTypesService.getWalletTypeById(anyInt())).thenReturn(createWalletType());
        Mockito.when(mockWalletsRepository.save(any())).thenReturn(expectedWallet);
        //Act
        Wallet returnedWallet = mockWalletsService.updateById(expectedWallet.getId(), walletDTO);
        //Assert
        Assert.assertEquals(expectedWallet, returnedWallet);
    }

    @Test
    public void addMoney_Should_ReturnCorrectWalletAmount(){
        //Arrange
        Wallet wallet = createWallet();
        wallet.setBalance(BigDecimal.valueOf(100));
        Wallet expectedWallet = createWallet();
        expectedWallet.setBalance(BigDecimal.valueOf(110));
        int addAmount = 1000;
        Mockito.when(mockWalletsRepository.save(wallet)).thenReturn(expectedWallet);
        //Act
        Wallet result = mockWalletsService.addMoney(addAmount,wallet);
        //Assert
        Assert.assertEquals(expectedWallet.getBalance(),result.getBalance());
    }

    @Test
    public void checkWalletHasSufficientAmount_Should_ReturnTrue_WhenWalletHasEnoughMoney(){
        //Arrange
        Wallet wallet = createWallet();
        BigDecimal amount = BigDecimal.valueOf(50);
        Mockito.when(mockWalletsRepository.findById(anyInt()))
                .thenReturn(Optional.of(wallet));
        //Act
        boolean result = mockWalletsService.checkWalletHasSufficientAmount(anyInt(),amount);
        //Assert
        Assert.assertTrue(result);
    }

    @Test
    public void checkWalletHasSufficientAmount_Should_ReturnFalse_WhenWalletDoesntHaveEnoughMoney(){
        //Arrange
        Wallet wallet = createWallet();
        BigDecimal amount = BigDecimal.valueOf(500);
        Mockito.when(mockWalletsRepository.findById(anyInt()))
                .thenReturn(Optional.of(wallet));
        //Act
        boolean result = mockWalletsService.checkWalletHasSufficientAmount(anyInt(),amount);
        //Assert
        Assert.assertFalse(result);
    }

    @Test
    public void checkWalletHasMoney_Should_ReturnTrue_WhenWalletHasMoney(){
        //Arrange
        Wallet wallet = createWallet();
        Mockito.when(mockWalletsRepository.findById(anyInt()))
                .thenReturn(Optional.of(wallet));
        //Act
        boolean result = mockWalletsService.checkWalletHasMoney(anyInt());
        //Assert
        Assert.assertTrue(result);
    }

    @Test
    public void checkWalletHasMoney_Should_ReturnFalse_WhenWalletBalanceIsZERO(){
        //Arrange
        Wallet wallet = createWallet();
        wallet.setBalance(BigDecimal.ZERO);
        Mockito.when(mockWalletsRepository.findById(anyInt()))
                .thenReturn(Optional.of(wallet));
        //Act
        boolean result = mockWalletsService.checkWalletHasMoney(anyInt());
        //Assert
        Assert.assertFalse(result);
    }
}
