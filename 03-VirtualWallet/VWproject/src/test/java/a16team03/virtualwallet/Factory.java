package a16team03.virtualwallet;

import a16team03.virtualwallet.models.*;
import a16team03.virtualwallet.models.dtos.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;

public class Factory {
    private static final String WALLET_NAME = "Wallet";

    public static Transaction createTransaction() {
        Transaction transaction = new Transaction();

        transaction.setTransactionId(1);
        transaction.setSendingUser(new User());
        transaction.setSenderWallet(createWallet());
        transaction.setReceivingUser(new User());
        transaction.getReceivingUser().setUserId(2);
        transaction.setAmount(BigDecimal.valueOf(1));
        transaction.setTimestamp(Timestamp.from(Instant.now()));
        transaction.setConfirmed(false);

        return transaction;
    }

    public static TransactionDTO createTransactionDto() {
        TransactionDTO transactionDto = new TransactionDTO();

        transactionDto.setAmount(BigDecimal.valueOf(1));
        transactionDto.setSenderId(1);
        transactionDto.setReceiverId(2);
        transactionDto.setTimestamp(Timestamp.from(Instant.now()));
        transactionDto.setConfirmed(false);

        return transactionDto;
    }

    public static User createUser() {
        User user = new User();

        user.setUserId(1);
        user.setUsername("username");
        user.setPassword("password123");
        user.setPasswordConfirmation("password123");
        user.setFullName("Full Name");
        user.setPhone("1234567890");
        user.setEmail("user@users.com");
        user.setImageUrl("imageUrl");
        user.setBlocked(false);
        user.setEnabled(true);

        return user;
    }


    public static Pageable createPageable() {
        return PageRequest.of(0, 5);
    }

    public static Card createCard() {
        Card card = new Card();

        User cardOwner = createUser();

        card.setCardId(1);
        card.setCardNumber("1234567890");
        card.setOwnerName("Owner Name");
        card.setExpirationDate("07/20");
        card.setCvc("123");
        card.setCardOwner(cardOwner);

        return card;
    }

    public static CardDTO createCardDTO() {
        CardDTO cardDTO = new CardDTO();

        User cardOwner = createUser();

        cardDTO.setCardNumber("1234123412341234");
        cardDTO.setOwnerName("Owner Name");
        cardDTO.setExpirationDate("07/20");
        cardDTO.setCvc("123");
        cardDTO.setCardOwnerId(cardOwner.getUserId());

        return cardDTO;
    }

    public static WalletType createWalletType() {
        WalletType walletType = new WalletType();

        walletType.setId(1);
        walletType.setTypeName("Wallet Type 1");

        return walletType;
    }

    public static Wallet createWallet() {
        Wallet wallet = new Wallet();

        WalletType walletType = createWalletType();

        wallet.setId(1);
        wallet.setName(WALLET_NAME);
        wallet.setBalance(BigDecimal.valueOf(123.45));
        wallet.setWalletType(walletType);

        return wallet;
    }

    public static WalletDTO createWalletDTO() {
        WalletDTO walletDTO = new WalletDTO();

        WalletType walletType = createWalletType();

        walletDTO.setWalletId(1);
        walletDTO.setName(WALLET_NAME);
        walletDTO.setBalance(BigDecimal.valueOf(123.45));
        walletDTO.setWalletTypeId(walletType.getId());

        return walletDTO;
    }

    public static UserWallet createUserWallet() {
        UserWallet userWallet = new UserWallet();

        userWallet.setUser(createUser());
        userWallet.setWallet(createWallet());

        return userWallet;
    }

    public static UserWalletDTO createUserWalletDTO() {
        UserWalletDTO userWalletDTO = new UserWalletDTO();

        userWalletDTO.setUserId(1);
        userWalletDTO.setWalletId(1);

        return userWalletDTO;
    }

    public static UserPasswordChangeDTO createUserPasswordChangeDTO() {
        UserPasswordChangeDTO userPasswordChangeDTO = new UserPasswordChangeDTO();

        userPasswordChangeDTO.setId(1);
        userPasswordChangeDTO.setOldPassword("oldPass");
        userPasswordChangeDTO.setNewPassword("newPass");
        userPasswordChangeDTO.setNewPasswordConfirmation("newPass");

        return userPasswordChangeDTO;
    }

    public static UserPersonalDetailsDTO createUserPersonalDetailsDTO() {
        UserPersonalDetailsDTO userPersonalDetailsDTO = new UserPersonalDetailsDTO();

        userPersonalDetailsDTO.setId(1);
        userPersonalDetailsDTO.setFullName("Full Name");
        userPersonalDetailsDTO.setEmail("Email");
        userPersonalDetailsDTO.setPhone("1234567890");

        return userPersonalDetailsDTO;
    }

    public static CardDetailsDTO createCardDetailsDTO() {
        CardDetailsDTO cardDetailsDTO = new CardDetailsDTO();

        cardDetailsDTO.setCardId(1);
        cardDetailsDTO.setCardNumber("1234567812345678");
        cardDetailsDTO.setCvc("123");
        cardDetailsDTO.setExpirationDate("12/22");
        cardDetailsDTO.setOwnerName("Ivan Petrov");

        return cardDetailsDTO;
    }

    public static CardTransaction createCardTransaction() {
        CardTransaction cardTransaction = new CardTransaction();

        cardTransaction.setId(1);
        cardTransaction.setAmount(123);
        cardTransaction.setCurrency("BGN");
        cardTransaction.setIdempotencyKey("test_key");

        CardDetailsDTO cardDetailsDTO = createCardDetailsDTO();
        cardTransaction.setCardDetailsDTO(cardDetailsDTO);

        Wallet wallet = createWallet();
        cardTransaction.setReceivingWallet(wallet);

        User user = createUser();
        cardTransaction.setReceivingUser(user);

        cardTransaction.setConfirmed(false);

        return cardTransaction;
    }

    public static CardTransactionDTO createCardTransactionDTO() {
        CardTransactionDTO cardTransactionDTO = new CardTransactionDTO();

        cardTransactionDTO.setAmount(456);
        cardTransactionDTO.setCurrency("BGN");
        cardTransactionDTO.setIdempotencyKey("test_key2");
        cardTransactionDTO.setDescription("sample description");
        cardTransactionDTO.setReceivingUserId(1);
        cardTransactionDTO.setReceivingWalletId(1);
        cardTransactionDTO.setCardId(1);

        return cardTransactionDTO;
    }

    public static UserRegistrationDTO createUserRegistrationDTO() {
        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();

        userRegistrationDTO.setUsername("username");
        userRegistrationDTO.setPassword("password123");
        userRegistrationDTO.setPasswordConfirmation("password123");
        userRegistrationDTO.setEmail("user123@users.com");
        userRegistrationDTO.setPhone("0885123123");

        return userRegistrationDTO;
    }

    public static UserWalletUpdateDTO createUserWalletUpdateDTO() {
        UserWalletUpdateDTO userWalletUpdateDTO = new UserWalletUpdateDTO();

        userWalletUpdateDTO.setUserId(1);
        userWalletUpdateDTO.setWalletId(1);
        userWalletUpdateDTO.setWalletName(WALLET_NAME);

        return userWalletUpdateDTO;
    }

    public static CardTransactionSearch createCardTransactionSearch() {
        CardTransactionSearch cardTransactionSearch = new CardTransactionSearch();

        cardTransactionSearch.setAmountFrom(0);
        cardTransactionSearch.setAmountTo(10000);
        cardTransactionSearch.setFromDate(LocalDate.now());
        cardTransactionSearch.setTillDate(LocalDate.now());
        cardTransactionSearch.setUserId(1);
        cardTransactionSearch.setUsername("");
        cardTransactionSearch.setDirection("DESC");
        cardTransactionSearch.setOrderBy("timestamp");
        cardTransactionSearch.setSort(cardTransactionSearch.getOrderBy() + "," + cardTransactionSearch.getDirection());

        return cardTransactionSearch;
    }
}
