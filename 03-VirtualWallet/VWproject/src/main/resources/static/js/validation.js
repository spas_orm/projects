function validateAddCardForm() {
    var errorMessage = "";
    var result = true;

    var expiryDate = document.getElementById("ExpiryDateAddCard").value;
    if (!expiryDate.match(/(0[1-9]|1[0-2])[/][0-9]{2}/)) {
        errorMessage += ("The expiry date format is not correct.\n");
        result = false;
    } else {

        var d = new Date();
        var currentYear = d.getFullYear();
        var currentMonth = d.getMonth() + 1;

        var parts = expiryDate.split('/');
        var year = parseInt(parts[1], 10) + 2000;
        var month = parseInt(parts[0], 10);

        if (year < currentYear || (year === currentYear && month < currentMonth)) {
            errorMessage += ("The expiry date has passed.\n");
            result = false;
        }
    }

    var cardNumber = document.getElementById("cardNumberAddCard").value;
    if (!cardNumber.match(/[0-9]{16}/)) {
        errorMessage += ("Card Number should contain only digits.\n");
        result = false;
    }

    var cvc = document.getElementById("cvcNumberAddCard").value;
    if (!cvc.match(/[0-9]{3}/)) {
        errorMessage += ("CVC should contain only digits.\n");
        result = false;
    }

    document.getElementById("addCardErrorMessage").innerHTML = errorMessage.replace(/(\r\n|\n|\r)/gm, "<br>");
    return result;
}

function validateEditCardForm() {
    var errorMessage = "";
    var result = true;


    var expiryDate = document.getElementById("expiryDateEditCard").value;
    if (!expiryDate.match(/(0[1-9]|1[0-2])[/][0-9]{2}/)) {
        errorMessage += "The expiry date format is not correct.\n";
        result = false;
    } else {

        var d = new Date();
        var currentYear = d.getFullYear();
        var currentMonth = d.getMonth() + 1;

        var parts = expiryDate.split('/');
        var year = parseInt(parts[1], 10) + 2000;
        var month = parseInt(parts[0], 10);

        if (year < currentYear || (year === currentYear && month < currentMonth)) {
            errorMessage += "The expiry date has passed.\n";
            result = false;
        }
    }

    var cvc = document.getElementById("cvcNumberEditCard").value;
    if (!cvc.match(/[0-9]{3}/)) {
        errorMessage += "CVC should contain only digits.\n";
        result = false;
    }

    document.getElementById("editCardErrorMessage").innerHTML = errorMessage.replace(/(\r\n|\n|\r)/gm, "<br>");
    return result;
}

function validateUserSignupForm() {
    var errorMessage = "";
    var result = true;

    var registerUsername = document.getElementById("registerUsername").value;
    if (!registerUsername.match(/^[A-Za-z\d]{4,15}$/)) {
        errorMessage += ("Your username should be at least 4 symbols long and at most 15 symbols.\n");
        result = false;
    }

    var registerPassword = document.getElementById("registerPassword").value;
    if (!registerPassword.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,25}$/)) {
        errorMessage += ("Your password should be at least 8 digits long and contain a number and a letter.\n");
        result = false;
    }

    var registerPasswordConfirmation = document.getElementById("registerPasswordConfirmation").value;
    if (registerPassword !== registerPasswordConfirmation) {
        errorMessage += ("Passwords do not match.\n");
        result = false;
    }

    var phoneNumber = document.getElementById("phoneNumber").value;
    if (!phoneNumber.match(/^\d{10}$/)) {
        errorMessage += ("Phone number should be 10 digits.\n");
        result = false;
    }

    document.getElementById("signupErrorMessage").innerHTML = errorMessage.replace(/(\r\n|\n|\r)/gm, "<br>");
    return result;
}

function validateLoginForm() {
    var errorMessage = "";
    var result = true;

    var loginUsername = document.getElementById("loginUsername").value;
    if (!loginUsername.match(/^[A-Za-z\d]{4,15}$/)) {
        errorMessage += ("Your username should be at least 4 symbols long and at most 15 symbols.\n");
        result = false;
    }

    var loginPassword = document.getElementById("loginPassword").value;
    if (!loginPassword.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,25}$/)) {
        errorMessage += ("Your password should be at least 8 digits long and contain a number and a letter.\n");
        result = false;
    }

    document.getElementById("loginErrorMessage").innerHTML = errorMessage.replace(/(\r\n|\n|\r)/gm, "<br>");
    return result;
}

function validateUserPersonalDetailsChange() {
    var errorMessage = "";
    var result = true;

    var phoneNumber = document.getElementById("phoneNumberEdit").value;
    if (!phoneNumber.match(/^\d{10}$/)) {
        errorMessage += ("Phone number should be 10 digits.\n");
        result = false;
    }

    document.getElementById("editPersonalDetailsErrorMessage").innerHTML = errorMessage.replace(/(\r\n|\n|\r)/gm, "<br>");
    return result;
}

function validateUserPasswordChange() {
    var errorMessage = "";
    var result = true;

    var existingPassword = document.getElementById("editExistingPassword").value;
    if (!existingPassword.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,25}$/)) {
        errorMessage += ("Your old password should be at least 8 digits long and contain a number and a letter.\n");
        result = false;
    }

    var editNewPassword = document.getElementById("editNewPassword").value;
    if (!editNewPassword.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,25}$/)) {
        errorMessage += ("Your new password should be at least 8 digits long and contain a number and a letter.\n");
        result = false;
    }

    var editNewPasswordConfirmation = document.getElementById("editConfirmNewPassword").value;
    if (editNewPasswordConfirmation !== editNewPassword) {
        errorMessage += ("Passwords do not match.\n");
        result = false;
    }

    document.getElementById("editPasswordErrorMessage").innerHTML = errorMessage.replace(/(\r\n|\n|\r)/gm, "<br>");
    return result;
}

$(function forbidSpaces() {
    $(".inputFieldNoSpaces").on({
        keydown: function (e) {
            if (e.which === 32)
                return false;
        },
        change: function () {
            this.value = this.value.replace(/\s/g, "");
        }
    })
});