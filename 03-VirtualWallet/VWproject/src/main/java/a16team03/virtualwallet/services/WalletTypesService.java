package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.WalletType;

public interface WalletTypesService {
    WalletType getWalletTypeById(int id);

    Iterable<WalletType> getAll();

    String getTypeNameById(int id);
}
