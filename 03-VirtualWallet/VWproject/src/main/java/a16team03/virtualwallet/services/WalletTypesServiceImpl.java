package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.WalletType;
import a16team03.virtualwallet.repositories.WalletTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WalletTypesServiceImpl implements WalletTypesService {
    private static final String ENTITY_WALLET_TYPE = "Wallet type";

    private WalletTypesRepository walletTypesRepository;

    @Autowired
    public WalletTypesServiceImpl(WalletTypesRepository walletTypesRepository) {
        this.walletTypesRepository = walletTypesRepository;
    }

    @Override
    public WalletType getWalletTypeById(int id) {
        Optional<WalletType> walletTypeOptional = walletTypesRepository.findById(id);
        if(!walletTypeOptional.isPresent()) {
            throw new EntityNotFoundException(ENTITY_WALLET_TYPE, id);
        }

        return walletTypeOptional.get();
    }

    @Override
    public Iterable<WalletType> getAll() {
        return walletTypesRepository.findAll();
    }

    @Override
    public String getTypeNameById(int id) {
        WalletType walletType = getWalletTypeById(id);
        return walletType.getTypeName();
    }
}
