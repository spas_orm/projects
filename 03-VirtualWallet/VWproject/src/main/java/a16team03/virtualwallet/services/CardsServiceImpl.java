package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.dtos.CardDTO;
import a16team03.virtualwallet.models.dtos.CardDetailsDTO;
import a16team03.virtualwallet.models.mappers.CardMapper;
import a16team03.virtualwallet.repositories.CardsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardsServiceImpl implements CardsService {
    private static final String ENTITY_TYPE_CARD = "Card";
    public static final String CARD_NUMBER = "card number";

    private CardsRepository cardsRepository;
    private CardMapper cardMapper;

    @Autowired
    public CardsServiceImpl(CardsRepository cardsRepository,
                            CardMapper cardMapper) {
        this.cardsRepository = cardsRepository;
        this.cardMapper = cardMapper;
    }

    @Override
    public Card create(CardDTO cardDTO) {
        if (checkIfCardNumberExists(cardDTO.getCardNumber())) {
            throw new DuplicateEntityException(ENTITY_TYPE_CARD, CARD_NUMBER);
        }
        Card cardToCreate = cardMapper.dtoToCard(cardDTO);

        checkIfCardOwnerIsBlocked(cardToCreate);

        return cardsRepository.save(cardToCreate);
    }

    @Override
    public Card getById(int id) {
        Optional<Card> cardOptional = cardsRepository.findById(id);
        if (!cardOptional.isPresent()) {
            throw new EntityNotFoundException(ENTITY_TYPE_CARD, id);
        }
        return cardOptional.get();
    }

    @Override
    public Iterable<Card> getAll() {
        return cardsRepository.findAll();
    }

    @Override
    public Card updateById(int id, CardDTO cardDTO) {
//        if (checkIfCardNumberExists(cardDTO.getCardNumber())) {
//            throw new DuplicateEntityException(ENTITY_TYPE_CARD, CARD_NUMBER);
//        }
        Card cardToUpdate = getById(id);

        checkIfCardOwnerIsBlocked(cardToUpdate);

        cardMapper.mapUpdatedCardDetails(cardDTO, cardToUpdate);

        return cardsRepository.save(cardToUpdate);
    }

    @Override
    public void deleteById(int id) {
        Card card = getById(id);

        checkIfCardOwnerIsBlocked(card);

        card.setDeleted(true);

        cardsRepository.save(card);
    }

    @Override
    public List<Card> getAllByUserId(int userId) {
        return cardsRepository.getAllByCardOwnerId(userId);
    }

    @Override
    public void changeCardDetails(CardDetailsDTO cardDetailsDTO) {
        Card originalCard = getById(cardDetailsDTO.getCardId());

        originalCard.setOwnerName(cardDetailsDTO.getOwnerName());
        originalCard.setExpirationDate(cardDetailsDTO.getExpirationDate());
        originalCard.setCvc(cardDetailsDTO.getCvc());

        checkIfCardOwnerIsBlocked(originalCard);

        cardsRepository.save(originalCard);
    }

    private boolean checkIfCardNumberExists(String cardNumber) {
        return cardsRepository.existsCardByCardNumber(cardNumber);
    }

    private void checkIfCardOwnerIsBlocked(Card card){
        if (card.getCardOwner().isBlocked()) {
            throw new OperationNotAllowedException();
        }
    }
}
