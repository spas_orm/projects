package a16team03.virtualwallet.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class ImageFileServiceImpl implements ImageFileService {
    private String uploadDir;

    private String imgLocationUrl;

    private String defaultUserImgUrl;

    @Override
    public String uploadFile(MultipartFile file) throws IOException {
        if (file == null || file.isEmpty()) {
            return defaultUserImgUrl;
        }
        String fileName = UUID.randomUUID().toString() + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());
        Path copyLocation = Paths
                .get(uploadDir + File.separator + fileName);
        Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);

        return imgLocationUrl + fileName;
    }

    @Override
    public String getDefaultUserImgPath() {
        return defaultUserImgUrl;
    }

    @Value("${app.upload.dir}")
    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    @Value("${app.imgs.url}")
    public void setImgLocationUrl(String imgLocationUrl) {
        this.imgLocationUrl = imgLocationUrl;
    }

    @Value("${user.img.default.url}")
    public void setDefaultUserImgUrl(String defaultUserImgUrl) {
        this.defaultUserImgUrl = defaultUserImgUrl;
    }
}
