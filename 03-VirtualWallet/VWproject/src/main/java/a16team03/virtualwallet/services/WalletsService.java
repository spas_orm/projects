package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.WalletDTO;

import java.math.BigDecimal;

public interface WalletsService {
    Wallet create(WalletDTO walletDTO);

    Wallet getById(int id);

    Iterable<Wallet> findAll();

    Wallet updateById(int id, WalletDTO walletDTO);

    Wallet addMoney(int amount, Wallet wallet);

    boolean checkWalletHasSufficientAmount(int walletId, BigDecimal amount);

    boolean checkWalletHasMoney(int walletId);
}