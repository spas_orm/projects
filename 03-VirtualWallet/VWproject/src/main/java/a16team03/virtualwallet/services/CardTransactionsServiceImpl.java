package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.CardTransactionAlreadyConfirmedException;
import a16team03.virtualwallet.exceptions.InsufficientAmountException;
import a16team03.virtualwallet.models.CardTransaction;
import a16team03.virtualwallet.models.CardTransactionSearch;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.CardTransactionDTO;
import a16team03.virtualwallet.models.mappers.CardTransactionMapper;
import a16team03.virtualwallet.repositories.CardTransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.net.ConnectException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class CardTransactionsServiceImpl implements CardTransactionsService {
    private static final String INSUFFICIENT_AMOUNT_IN_CARD = "Insufficient amount in card.";
    private static final String CONNECTION_FAILED = "Connection to external API failed.";
    private static final String DEFAULT_ORDER_DIRECTION = "DESC";
    private static final String DEFAULT_ORDER_PARAMETER = "timestamp";

    private CardTransactionsRepository cardTransactionsRepository;
    private CardTransactionMapper cardTransactionMapper;
    private WalletsService walletsService;
    private RestTemplate restTemplate;

    private String restUrl;
    private String apiKey;

    @Autowired
    public CardTransactionsServiceImpl(CardTransactionsRepository cardTransactionsRepository,
                                       CardTransactionMapper cardTransactionMapper,
                                       WalletsService walletsService,
                                       RestTemplate restTemplate) {
        this.cardTransactionsRepository = cardTransactionsRepository;
        this.cardTransactionMapper = cardTransactionMapper;
        this.walletsService = walletsService;
        this.restTemplate = restTemplate;
    }

    public CardTransaction create(CardTransactionDTO cardTransactionDTO) {
        CardTransaction cardTransaction = cardTransactionMapper.dtoToCardTransaction(cardTransactionDTO);

        return cardTransactionsRepository.save(cardTransaction);
    }

    @Override
    public Page<CardTransaction> findAll(CardTransactionSearch cardTransactionSearch, Pageable pageable) {
        setCardTransactionSearchValues(cardTransactionSearch);

        return cardTransactionsRepository.findAll(cardTransactionSearch.getAmountFrom(),
                cardTransactionSearch.getAmountTo(),
                Timestamp.valueOf(cardTransactionSearch.getFromDate().atStartOfDay()),
                Timestamp.valueOf(cardTransactionSearch.getTillDate().atTime(23, 59)),
                cardTransactionSearch.getUsername(),
                pageable);
    }

    @Override
    public Page<CardTransaction> findAllByUserId(CardTransactionSearch cardTransactionSearch, Pageable pageable) {
        setCardTransactionSearchValues(cardTransactionSearch);

        return cardTransactionsRepository.findAllForUser(cardTransactionSearch.getUserId(),
                cardTransactionSearch.getAmountFrom(),
                cardTransactionSearch.getAmountTo(),
                Timestamp.valueOf(cardTransactionSearch.getFromDate().atStartOfDay()),
                Timestamp.valueOf(cardTransactionSearch.getTillDate().atTime(23, 59)),
                cardTransactionSearch.getWalletName(),
                pageable);
    }

    @Transactional
    public CardTransaction execute(CardTransaction cardTransaction) throws ConnectException {
        if (cardTransaction.isConfirmed()) {
            throw new CardTransactionAlreadyConfirmedException();
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("x-api-key", apiKey);
        HttpEntity<CardTransaction> cardTransactionHttpEntity = new HttpEntity<>(cardTransaction, httpHeaders);

        try {
            HttpStatus statusCode = restTemplate
                    .exchange(restUrl, HttpMethod.POST, cardTransactionHttpEntity, Void.class).getStatusCode();
        } catch (ResourceAccessException e) {
            throw new ConnectException(CONNECTION_FAILED);

        } catch (HttpClientErrorException.Forbidden e) {
            throw new InsufficientAmountException(INSUFFICIENT_AMOUNT_IN_CARD);
        }

        cardTransaction.setConfirmed(true);

        Wallet walletToAddMoneyTo = cardTransaction.getReceivingWallet();
        walletsService.addMoney(cardTransaction.getAmount(), walletToAddMoneyTo);

        return cardTransactionsRepository.save(cardTransaction);
    }

    @Override
    public CardTransaction getLastUnconfirmedCardTransactionByUserId(int id) {
        Optional<CardTransaction> cardTransaction = cardTransactionsRepository.
                findFirstByReceivingUserUserIdAndConfirmedFalseOrderByIdDesc(id);
        if (!cardTransaction.isPresent()) {
            throw new CardTransactionAlreadyConfirmedException();
        }

        return cardTransaction.get();
    }

    private void setCardTransactionSearchValues(CardTransactionSearch cts) {
        LocalDate now = LocalDate.now();

        LocalDate fromDate = LocalDate.now().minusYears(1);

        if (cts.getAmountTo() == 0) {
            cts.setAmountTo(Integer.MAX_VALUE);
        }

        if (cts.getFromDate() == null) {
            cts.setFromDate(fromDate);
        }
        if (cts.getTillDate() == null) {
            cts.setTillDate(now);
        }

        if (cts.getUsername() == null) {
            cts.setUsername("");
        }

        if(cts.getWalletName() == null) {
            cts.setWalletName("");
        }

        if (cts.getDirection() == null) {
            cts.setDirection(DEFAULT_ORDER_DIRECTION);
        }

        if (cts.getOrderBy() == null) {
            cts.setOrderBy(DEFAULT_ORDER_PARAMETER);
        }

        cts.setSort(cts.getOrderBy() + "," + cts.getDirection());
    }

    @Value("${rest.api.url}")
    private void setRestUrl(String restUrl) {
        this.restUrl = restUrl;
    }

    @Value("${api.key}")
    private void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
