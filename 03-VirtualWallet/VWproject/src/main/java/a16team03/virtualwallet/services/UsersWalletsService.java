package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.UserWalletDTO;
import a16team03.virtualwallet.models.dtos.UserWalletUpdateDTO;

import java.math.BigDecimal;
import java.util.List;

public interface UsersWalletsService {

    UserWallet createUserWallet(UserWalletDTO uwDto);

    UserWallet createDefaultUserWallet(int userId);

    UserWallet addUserToWallet(UserWalletDTO uwDto);

    UserWallet getById(int uwId);

    UserWallet updateUserWallet(int uwId, UserWalletUpdateDTO userWalletUpdateDTO);

    List<UserWallet> getUsersWalletsByUserId(int userId);

    List<UserWallet> getUsersWalletsByWalletId(int walletId);

    UserWallet getDefaultUserWalletByUserId(int userId);

    boolean checkUserWithWalletExist(int userId, int walletId);

    void removeOwnWallet(int requestByUserId, UserWalletDTO uwDto);

    void removeUserForSharedWallet(int requestByUserId, UserWalletDTO uwDto);

    BigDecimal getTotalBalanceInAllWalletsByUserId(int userId);
}
