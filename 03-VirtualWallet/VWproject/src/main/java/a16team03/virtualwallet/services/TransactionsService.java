package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.dtos.TransactionDTO;
import a16team03.virtualwallet.models.TransactionSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TransactionsService {
    Transaction create(TransactionDTO transactionDto);

    Page<Transaction> findAll(TransactionSearch transactionSearch, Pageable pageable);

    Transaction getById(int id);

    Page<Transaction> findAllForUserId(int userId, TransactionSearch transactionSearch, Pageable pageable);

    boolean isTransactionAlreadyConfirmed(int id);

    Transaction changeBeforeConfirmById(int id, Transaction transaction);

    Transaction confirmById(int id);

    TransactionSearch setTransactionSearchValues(TransactionSearch ts);

}
