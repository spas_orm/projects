package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.WalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class WalletsServiceImpl implements WalletsService {
    private static final String ENTITY_TYPE_WALLET = "Wallet";

    private WalletsRepository walletsRepository;
    private WalletMapper walletMapper;
    private WalletTypesService walletTypesService;

    @Autowired
    public WalletsServiceImpl(WalletsRepository walletsRepository,
                              WalletMapper walletMapper,
                              WalletTypesService walletTypesService) {
        this.walletsRepository = walletsRepository;
        this.walletMapper = walletMapper;
        this.walletTypesService = walletTypesService;
    }

    @Override
    public Wallet create(WalletDTO walletDTO) {
        Wallet walletToCreate = walletMapper.dtoToWallet(walletDTO);
        return walletsRepository.save(walletToCreate);
    }

    @Override
    public Wallet getById(int id) {
        Optional<Wallet> walletOptional = walletsRepository.findById(id);
        if (!walletOptional.isPresent()) {
            throw new EntityNotFoundException(ENTITY_TYPE_WALLET, id);
        }
        return walletOptional.get();
    }

    @Override
    public Iterable<Wallet> findAll() {
        return walletsRepository.findAll();
    }

    @Override
    public Wallet updateById(int id, WalletDTO walletDTO) {
        Wallet walletToUpdate = getById(id);
        mapUpdatedWalletDetails(walletDTO, walletToUpdate);

        return walletsRepository.save(walletToUpdate);
    }

    @Override
    public Wallet addMoney(int amount, Wallet wallet) {
        wallet.setBalance(wallet.getBalance().add(BigDecimal.valueOf(amount).divide(BigDecimal.valueOf(100))));
        return walletsRepository.save(wallet);
    }

    @Override
    public boolean checkWalletHasSufficientAmount(int walletId, BigDecimal amount) {
        if (getById(walletId).getBalance().compareTo(amount) >= 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkWalletHasMoney(int walletId) {
        if (getById(walletId).getBalance().compareTo(BigDecimal.ZERO) > 0) {
            return true;
        }
        return false;
    }

    public void mapUpdatedWalletDetails(WalletDTO walletDTO, Wallet walletToUpdate) {
        walletToUpdate.setBalance(walletDTO.getBalance());
        walletToUpdate.setWalletType(walletTypesService.getWalletTypeById(walletDTO.getWalletTypeId()));
        walletToUpdate.setName(walletDTO.getName());
    }

}
