package a16team03.virtualwallet.services;

import a16team03.virtualwallet.configurations.jwt.JwtTokenProvider;
import a16team03.virtualwallet.exceptions.*;
import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserSearch;
import a16team03.virtualwallet.models.dtos.UserPasswordChangeDTO;
import a16team03.virtualwallet.models.dtos.UserPersonalDetailsDTO;
import a16team03.virtualwallet.models.dtos.UserRegistrationDTO;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {
    private static final String ENTITY_TYPE_USER = "User";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String PHONE = "Phone";

    private UsersRepository usersRepository;
    private ImageFileService imageService;
    private PasswordEncoder passwordEncoder;
    private UserDetailsManager userDetailsManager;
    private UsersWalletsService usersWalletsService;
    private JwtTokenProvider jwtTokenProvider;
    private AuthenticationManager authenticationManager;


    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository,
                            ImageFileService imageService,
                            PasswordEncoder passwordEncoder,
                            UserDetailsManager userDetailsManager,
                            @Lazy UsersWalletsService usersWalletsService,
                            JwtTokenProvider jwtTokenProvider,
                            AuthenticationManager authenticationManager) {
        this.usersRepository = usersRepository;
        this.imageService = imageService;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
        this.usersWalletsService = usersWalletsService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;

    }

    @Override
    public User create(UserRegistrationDTO userRegistrationDTO) {
        trimUserRegistrationDTOFields(userRegistrationDTO);

        if (usersRepository.existsUserByUsername(userRegistrationDTO.getUsername())) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, USERNAME);
        }
        if (usersRepository.existsUsersByEmail(userRegistrationDTO.getEmail())) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, EMAIL);
        }

        if (usersRepository.existsUserByPhone(userRegistrationDTO.getPhone())) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, PHONE);
        }

        if (!userRegistrationDTO.getPassword().equals(userRegistrationDTO.getPasswordConfirmation())) {
            throw new PasswordMismatchException();
        }

        User userToCreate = UserMapper.userRegistrationDtoToUser(userRegistrationDTO);

        userToCreate.setImageUrl(imageService.getDefaultUserImgPath());

        User savedUser = usersRepository.save(userToCreate);
        createSpringSecurityUser(userToCreate);

        usersWalletsService.createDefaultUserWallet(savedUser.getUserId());

        return savedUser;
    }

    @Override
    public String signIn(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, usersRepository.findByUsername(username).getRoles());
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public Iterable<User> getAll() {
        return usersRepository.findAll();
    }

    @Override
    public Page<User> getAll(UserSearch userSearch, Pageable pageable) {
        fillNullFields(userSearch);
        return usersRepository.findAll(userSearch.getUsername(),
                userSearch.getEmail(),
                userSearch.getPhone(),
                pageable);
    }

    @Override
    public Page<User> findAllUsersByDetail(String userDetail, Pageable pageable) {
        if (userDetail == null) {
            userDetail = "";
        }
        return usersRepository.findAllUsersByDetail(userDetail, pageable);
    }

    @Override
    public User getById(int id) {
        Optional<User> user = usersRepository.findById(id);
        if (!user.isPresent()) {
            throw new EntityNotFoundException(ENTITY_TYPE_USER, id);
        }
        return user.get();
    }

    @Override
    public User updateById(int id, User user, MultipartFile image) throws IOException {
        User userToUpdate = getById(id);

        if (usersRepository.existsUserByUsernameAndUserIdNot(userToUpdate.getUsername(), id)) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, USERNAME);
        }
        if (usersRepository.existsUserByEmailAndUserIdNot(userToUpdate.getEmail(), id)) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, EMAIL);
        }
        if (usersRepository.existsUserByPhoneAndUserIdNot(userToUpdate.getPhone(), id)) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, PHONE);
        }
        if (user.getPassword() != null && !user.getPassword().equals(user.getPasswordConfirmation())) {
            throw new PasswordMismatchException();
        }

        mapUpdatedUserDetails(id, user, userToUpdate, image);

        return usersRepository.save(userToUpdate);
    }

    @Override
    public void deleteById(int id) {
        User userToDelete = getById(id);
        userToDelete.setEnabled(false);
        usersRepository.save(userToDelete);
    }

    @Override
    public void changePassword(UserPasswordChangeDTO userPasswordChangeDTO) {
        trimUserPasswordChangeDTOFields(userPasswordChangeDTO);

        User originalUser = getById(userPasswordChangeDTO.getId());

        if (!passwordEncoder.matches(userPasswordChangeDTO.getOldPassword(), originalUser.getPassword())) {
            throw new WrongPasswordException("Wrong password");
        }

        if (!userPasswordChangeDTO.getNewPassword().equals(userPasswordChangeDTO.getNewPasswordConfirmation())) {
            throw new PasswordMismatchException();
        }

        String newHashedPassword = passwordEncoder.encode(userPasswordChangeDTO.getNewPassword());
        originalUser.setPassword(newHashedPassword);

        usersRepository.save(originalUser);
    }

    @Override
    public void changePersonalDetails(UserPersonalDetailsDTO userPersonalDetailsDTO) {
        User originalUser = getById(userPersonalDetailsDTO.getId());

        if (usersRepository.existsUserByEmailAndUserIdNot(userPersonalDetailsDTO.getEmail(), originalUser.getUserId())) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, EMAIL);
        }

        if (usersRepository.existsUserByPhoneAndUserIdNot(userPersonalDetailsDTO.getPhone(), originalUser.getUserId())) {
            throw new DuplicateEntityException(ENTITY_TYPE_USER, PHONE);
        }

        originalUser.setFullName(userPersonalDetailsDTO.getFullName());
        originalUser.setEmail(userPersonalDetailsDTO.getEmail());
        originalUser.setPhone(userPersonalDetailsDTO.getPhone());

        usersRepository.save(originalUser);
    }

    @Override
    public int getIdFromPrincipal(Principal principal) {
        if (principal == null) {
            throw new UserNotLoggedInException();
        }

        User currentUser = usersRepository.findByUsername(principal.getName());
        return currentUser.getUserId();
    }

    @Override
    public void updateUserBlockedStatus(int userId) {
        User userToUpdate = getById(userId);

        userToUpdate.setBlocked(!userToUpdate.isBlocked());

        usersRepository.save(userToUpdate);
    }

    @Override
    public void changeProfilePicture(int id, MultipartFile image) throws IOException {
        User userToUpdate = getById(id);

        String imageFilePath = imageService.uploadFile(image);
        userToUpdate.setImageUrl(imageFilePath);

        usersRepository.save(userToUpdate);
    }

    private void createSpringSecurityUser(User user) {
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User securityUser =
                new org.springframework.security.core.userdetails.User(user.getUsername(),
                        hashedPassword,
                        authorities);
        userDetailsManager.updateUser(securityUser);
    }

    private void mapUpdatedUserDetails(int id, User user, User userToUpdate, MultipartFile image) throws IOException {
        userToUpdate.setUserId(id);
        userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setFullName(user.getFullName());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setPhone(user.getPhone());
        if (image != null && !image.isEmpty()) {
            user.setImageUrl(imageService.uploadFile(image));
        }
        userToUpdate.setBlocked(user.isBlocked());
        userToUpdate.setEnabled(user.isEnabled());
    }

    private void fillNullFields(UserSearch userSearch) {
        if (userSearch.getUsername() == null) {
            userSearch.setUsername("");
        }
        if (userSearch.getEmail() == null) {
            userSearch.setEmail("");
        }
        if (userSearch.getPhone() == null) {
            userSearch.setPhone("");
        }
    }

    private void trimUserRegistrationDTOFields(UserRegistrationDTO userRegistrationDTO) {
        userRegistrationDTO.setUsername(userRegistrationDTO.getUsername().trim());
        userRegistrationDTO.setEmail(userRegistrationDTO.getEmail().trim());
        userRegistrationDTO.setPhone(userRegistrationDTO.getPhone().trim());
        userRegistrationDTO.setPassword(userRegistrationDTO.getPassword().trim());
        userRegistrationDTO.setPasswordConfirmation(userRegistrationDTO.getPasswordConfirmation().trim());
    }

    private void trimUserPasswordChangeDTOFields(UserPasswordChangeDTO userPasswordChangeDTO) {
        userPasswordChangeDTO.setOldPassword(userPasswordChangeDTO.getOldPassword().trim());
        userPasswordChangeDTO.setNewPassword(userPasswordChangeDTO.getNewPassword().trim());
        userPasswordChangeDTO.setNewPasswordConfirmation(userPasswordChangeDTO.getNewPasswordConfirmation().trim());
    }
}
