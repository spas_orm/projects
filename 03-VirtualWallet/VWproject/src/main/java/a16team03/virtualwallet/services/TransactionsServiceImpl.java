package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.exceptions.InsufficientAmountException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.exceptions.TransactionAlreadyConfirmedException;
import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.TransactionSearch;
import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.TransactionDTO;
import a16team03.virtualwallet.models.mappers.TransactionMapper;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class TransactionsServiceImpl implements TransactionsService {
    private static final String ENTITY_NAME = "Transaction";
    private static final String DEFAULT_ORDER_DIRECTION = "DESC";
    private static final String DEFAULT_ORDER_PARAMETER = "timestamp";
    private static final String INSUFFICIENT_AMOUNT_IN_WALLET = "Insufficient amount in wallet.";
    private static final String INVALID_TRANSACTION_AMOUNT = "Transaction amount must be bigger than 0.";

    private UsersService usersService;
    private TransactionsRepository transactionsRepository;
    private TransactionMapper transactionMapper;
    private UsersWalletsService usersWalletsService;
    private WalletsService walletsService;
    private WalletMapper walletMapper;


    @Autowired
    public TransactionsServiceImpl(UsersService usersService,
                                   TransactionsRepository transactionsRepository,
                                   TransactionMapper transactionMapper,
                                   UsersWalletsService usersWalletsService,
                                   WalletsService walletsService,
                                   WalletMapper walletMapper) {
        this.usersService = usersService;
        this.transactionsRepository = transactionsRepository;
        this.transactionMapper = transactionMapper;
        this.usersWalletsService = usersWalletsService;
        this.walletsService = walletsService;
        this.walletMapper = walletMapper;
    }

    @Override
    public Transaction create(TransactionDTO transactionDto) {
        try {

            if (transactionDto.getSenderId() == transactionDto.getReceiverId()) {
                throw new OperationNotAllowedException("Not allowed to send money to yourself"); //TODO
            }

            Transaction transactionToCreate = transactionMapper.dtoToTransaction(transactionDto);

            checkIfSenderIsBlocked(transactionToCreate);

            checkForZeroTransactionAmount(transactionToCreate);

            checkSenderWalletHasSufficientAmount(transactionToCreate);

            return transactionsRepository.save(transactionToCreate);
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }
    }

    @Override
    public Page<Transaction> findAll(TransactionSearch ts, Pageable pageable) {

        TransactionSearch tsUpdated = setTransactionSearchValues(ts);

        return transactionsRepository.findAll(tsUpdated.getSenderUsername(),
                tsUpdated.getRecipientUsername(),
                Timestamp.valueOf(tsUpdated.getFromDate().atStartOfDay()),
                Timestamp.valueOf(tsUpdated.getTillDate().atStartOfDay()),
                pageable);
    }


    @Override
    public Transaction getById(int id) {
        Optional<Transaction> transaction = transactionsRepository.findById(id);
        if (!transaction.isPresent()) {
            throw new EntityNotFoundException(ENTITY_NAME, id);
        }
        return transaction.get();
    }

    @Override
    public Page<Transaction> findAllForUserId(int userId, TransactionSearch ts, Pageable pageable) {
        try {
            TransactionSearch tsUpdated = setTransactionSearchValues(ts);


            return transactionsRepository.findAllForUser(usersService.getById(userId),
                    tsUpdated.getSenderUsername(),
                    tsUpdated.getSenderUsername(),
                    Timestamp.valueOf(tsUpdated.getFromDate().atStartOfDay()),
                    Timestamp.valueOf(tsUpdated.getTillDate().atTime(23, 59)),
                    pageable);
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }

    }

    @Override
    public boolean isTransactionAlreadyConfirmed(int id) {
        try {
            Transaction transaction = getById(id);
            return transaction.isConfirmed();
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }
    }

    @Override
    public Transaction changeBeforeConfirmById(int id, Transaction transaction) {
        try {
            Transaction transactionToUpdate = getById(id);

            if (isTransactionAlreadyConfirmed(id)) {
                throw new TransactionAlreadyConfirmedException(id);
            }

            transactionToUpdate.setReceivingUser(transaction.getReceivingUser());
            transactionToUpdate.setSendingUser(transaction.getSendingUser());
            transactionToUpdate.setAmount(transaction.getAmount());
            transactionToUpdate.setDescription(transaction.getDescription());

            checkIfSenderIsBlocked(transactionToUpdate);
            checkSenderWalletHasSufficientAmount(transactionToUpdate);
            checkForZeroTransactionAmount(transactionToUpdate);

            return transactionsRepository.save(transactionToUpdate);
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }
    }

    @Override
    public Transaction confirmById(int id) {        //TODO - refactor & unit test
        try {
            if (isTransactionAlreadyConfirmed(id)) {
                throw new TransactionAlreadyConfirmedException(id);
            }

            Transaction transaction = getById(id);

            checkSenderWalletHasSufficientAmount(transaction);

            Wallet senderWallet = transaction.getSenderWallet();

            Wallet recipientWallet = usersWalletsService.
                    getDefaultUserWalletByUserId(transaction.getReceivingUser().getUserId()).getWallet();

            executeTransaction(senderWallet, recipientWallet, transaction);

            return transaction;
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }
    }


    @Override
    public TransactionSearch setTransactionSearchValues(TransactionSearch ts) {
        LocalDate now = LocalDate.now();
        LocalDate fromDate = LocalDate.of(2019, 1, 1);

        if (ts.getSenderUsername() == null || ts.getSenderUsername().isEmpty()) {
            ts.setSenderUsername("");
        }
        if (ts.getRecipientUsername() == null || ts.getRecipientUsername().isEmpty()) {
            ts.setRecipientUsername("");
        }
        if (ts.getFromDate() == null) {
            ts.setFromDate(fromDate);
        }
        if (ts.getTillDate() == null) {
            ts.setTillDate(now);
        }

        if (ts.getDirection() == null) {
            ts.setDirection(DEFAULT_ORDER_DIRECTION);
        }

        if (ts.getOrderBy() == null) {
            ts.setOrderBy(DEFAULT_ORDER_PARAMETER);
        }

        ts.setSort(ts.getOrderBy() + "," + ts.getDirection());

        return ts;
    }

    private void checkSenderWalletHasSufficientAmount(Transaction transaction) {
        Wallet senderWallet = transaction.getSenderWallet();
        if (!walletsService.checkWalletHasSufficientAmount(senderWallet.getId(),
                transaction.getAmount())) {
            throw new InsufficientAmountException(INSUFFICIENT_AMOUNT_IN_WALLET);
        }
    }

//    private void checkSenderDefaultWalletHasSufficientAmount(Transaction transaction) {
//        UserWallet senderDefaultUserWallet = usersWalletsService.
//                getDefaultUserWalletByUserId(transaction.getSendingUser().getUserId());
//        if (!walletsService.checkWalletHasSufficientAmount(senderDefaultUserWallet.getWallet().getId(),
//                transaction.getAmount())) {
//            throw new InsufficientAmountException(INSUFFICIENT_AMOUNT_IN_WALLET);
//        }
//    }

    private void checkIfSenderIsBlocked(Transaction transaction){
        if (transaction.getSendingUser().isBlocked()) {
            throw new OperationNotAllowedException();
        }
    }

    private void checkForZeroTransactionAmount(Transaction transaction) {
        if (transaction.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException(INVALID_TRANSACTION_AMOUNT);
        }
    }
    
    @Transactional(rollbackFor = Exception.class)
    void executeTransaction(Wallet senderWallet, Wallet recipientWallet, Transaction transaction) {
        senderWallet.setBalance(senderWallet.getBalance().subtract(transaction.getAmount()));

        recipientWallet.setBalance(recipientWallet.getBalance().add(transaction.getAmount()));

        transaction.setConfirmed(true);

        walletsService.updateById(senderWallet.getId(), walletMapper.walletToDTO(senderWallet));

        walletsService.updateById(recipientWallet.getId(), walletMapper.walletToDTO(recipientWallet));

        transactionsRepository.save(transaction);
    }
}
