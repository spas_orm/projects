package a16team03.virtualwallet.services;

import a16team03.virtualwallet.exceptions.DefaultEntityNotFoundException;
import a16team03.virtualwallet.exceptions.DuplicateUserWalletException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.UserWalletDTO;
import a16team03.virtualwallet.models.dtos.UserWalletUpdateDTO;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.models.mappers.UserWalletMapper;
import a16team03.virtualwallet.models.mappers.WalletMapper;
import a16team03.virtualwallet.repositories.UsersWalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Service
public class UsersWalletsServiceImpl implements UsersWalletsService {
    private static final String ENTITY_TYPE = "UserWallet";
    private static final String DEFAULT_ENTITY_TYPE = "wallet";
    private static final String ENTITY_ATTRIBUTE_USER_WALLET = "User and Wallet";
    private static final String DEFAULT_WALLET_NAME = "Default Wallet";

    private UsersWalletsRepository usersWalletsRepository;
    private UserWalletMapper userWalletMapper;
    private WalletsService walletsService;
    private WalletMapper walletMapper;


    @Autowired
    public UsersWalletsServiceImpl(UsersWalletsRepository usersWalletsRepository,
                                   UserWalletMapper userWalletMapper,
                                   WalletsService walletsService,
                                   WalletMapper walletMapper) {
        this.usersWalletsRepository = usersWalletsRepository;
        this.userWalletMapper = userWalletMapper;
        this.walletsService = walletsService;
        this.walletMapper = walletMapper;
    }

    @Override
    public UserWallet createUserWallet(UserWalletDTO uwDto) {
        try {
            UserWallet userWallet = userWalletMapper.dtoToUserWallet(uwDto);

            if (checkUserWithWalletExist(uwDto.getUserId(), uwDto.getWalletId())) {
                throw new DuplicateUserWalletException(userWallet.getUser().getUsername(),
                        uwDto.getWalletId());
            }

            if (getUsersWalletsByUserId(uwDto.getUserId()).isEmpty()) {
                userWallet.setDefaultWallet(true);
            } else {
                userWallet.setDefaultWallet(false);
            }

            userWallet.setCreatedByUser(true);

            return usersWalletsRepository.save(userWallet);
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }
    }

    @Override
    public UserWallet createDefaultUserWallet(int userId) {
        WalletDTO walletDTO = new WalletDTO();
        walletDTO.setName(DEFAULT_WALLET_NAME);
        walletDTO.setBalance(BigDecimal.valueOf(0));
        walletDTO.setWalletTypeId(1);

        Wallet wallet = walletsService.create(walletDTO);

        UserWalletDTO userWalletDTO = new UserWalletDTO();
        userWalletDTO.setUserId(userId);
        userWalletDTO.setWalletId(wallet.getId());

        return createUserWallet(userWalletDTO);
    }

    @Override
    public UserWallet addUserToWallet(UserWalletDTO uwDto) {
        try {
            UserWallet userWallet = userWalletMapper.dtoToUserWallet(uwDto);

            if (checkUserWithWalletExist(uwDto.getUserId(), uwDto.getWalletId())) {
                throw new DuplicateUserWalletException(userWallet.getUser().getUsername(),
                        uwDto.getWalletId());
            }

            return usersWalletsRepository.save(userWallet);
        } catch (EntityNotFoundException enf) {
            throw new EntityNotFoundException(enf.getMessage());
        }
    }

    @Override
    public UserWallet getById(int uwId) {
        UserWallet userWallet = usersWalletsRepository.getById(uwId);
        if (userWallet == null) {
            throw new EntityNotFoundException(ENTITY_TYPE, uwId);
        }

        return userWallet;
    }

    @Override
    public UserWallet updateUserWallet(int uwId, UserWalletUpdateDTO userWalletUpdateDto) {
        UserWallet uwToUpdate = getById(uwId);

        if (!uwToUpdate.getCreatedByUser()) {
            throw new OperationNotAllowedException(ENTITY_TYPE, uwId);
        }

        if (uwToUpdate.getUser().getUserId() != userWalletUpdateDto.getUserId()) {
            throw new OperationNotAllowedException(ENTITY_TYPE, uwId);
        }

        updateWalletNameIfCreatedByUserTrue(userWalletUpdateDto, uwToUpdate);

        if (!uwToUpdate.getDefaultWallet() && userWalletUpdateDto.getDefaultWallet()) {
            changeDefaultWalletForUser(uwToUpdate.getUser(), uwToUpdate);
        }

        return getById(uwId);
    }

    @Override
    public List<UserWallet> getUsersWalletsByUserId(int userId) {
        return new ArrayList<>(usersWalletsRepository.getAllByUserId(userId));
    }

    @Override
    public List<UserWallet> getUsersWalletsByWalletId(int walletId) {
        return usersWalletsRepository.getAllByWalletId(walletId);
    }

    @Override
    public UserWallet getDefaultUserWalletByUserId(int userId) {
        UserWallet defaultWallet = usersWalletsRepository.getDefaultUserWalletByUserId(userId);

        if (defaultWallet == null) {
            throw new DefaultEntityNotFoundException(userId, DEFAULT_ENTITY_TYPE);
        }
        return defaultWallet;
    }

    @Override
    public boolean checkUserWithWalletExist(int userId, int walletId) {
        return usersWalletsRepository.existsByUserUserIdAndWalletId(userId, walletId);
    }

    @Override
    public void removeOwnWallet(int requestByUserId, UserWalletDTO uwDto) {
        UserWallet userWalletToDelete = usersWalletsRepository
                .getByUserUserIdAndWalletId(uwDto.getUserId(), uwDto.getWalletId());

        if (userWalletToDelete == null) {
            throw new EntityNotFoundException(ENTITY_TYPE, uwDto.getUserId(), uwDto.getWalletId());
        }
        if (!isUserWalletCreatedByUser(userWalletToDelete)) {
            throw new OperationNotAllowedException(uwDto.getWalletId());
        }
        if (isWalletDefaultForUser(userWalletToDelete)) {
            throw new OperationNotAllowedException(uwDto.getWalletId());
        }
        if (!isWalletEmpty(userWalletToDelete)) {
            throw new OperationNotAllowedException(uwDto.getWalletId());
        }

        userWalletToDelete.setDeleted(true);
        usersWalletsRepository.save(userWalletToDelete);
    }

    @Override
    public void removeUserForSharedWallet(int requestByUserId, UserWalletDTO uwDto) {
        UserWallet userWalletToDelete = usersWalletsRepository
                .getByUserUserIdAndWalletId(uwDto.getUserId(), uwDto.getWalletId());

        if (userWalletToDelete == null) {
            throw new EntityNotFoundException(ENTITY_TYPE, uwDto.getUserId(), uwDto.getWalletId());
        }
        if (!checkWalletCreatedByUser(userWalletToDelete.getWallet(), requestByUserId)) {
            throw new OperationNotAllowedException(uwDto.getWalletId());
        }

        userWalletToDelete.setDeleted(true);
        usersWalletsRepository.save(userWalletToDelete);
    }

    @Override
    public BigDecimal getTotalBalanceInAllWalletsByUserId(int userId) {
        BigDecimal totalBalance = BigDecimal.ZERO;

        for (UserWallet userWallet : usersWalletsRepository.getAllByUserId(userId)) {
            totalBalance = totalBalance.add(userWallet.getWallet().getBalance());
        }

        return totalBalance;
    }

    private boolean checkWalletCreatedByUser(Wallet wallet, int userId) {
        List<Wallet> list = usersWalletsRepository.getWalletsCreatedByUserId(userId);
        if (list.isEmpty()) {
            return false;
        }
        return list.contains(wallet);
    }

    private boolean isWalletDefaultForUser(UserWallet userWallet) {
        return userWallet.getDefaultWallet();
    }

    private boolean isUserWalletCreatedByUser(UserWallet userWallet) {
        return userWallet.getCreatedByUser();
    }

    private boolean isWalletEmpty(UserWallet userWallet) {
        if (userWallet.getWallet().getBalance().compareTo(BigDecimal.ZERO) == 0) {
            return true;
        }
        return false;
    }

    //Returns the new default wallet for user
    private UserWallet changeDefaultWalletForUser(User user, UserWallet newDefaultUserWallet) {
        UserWallet oldDefaultUserWallet = getDefaultUserWalletByUserId(user.getUserId());
        oldDefaultUserWallet.setDefaultWallet(false);
        newDefaultUserWallet.setDefaultWallet(true);
        usersWalletsRepository.save(oldDefaultUserWallet);
        return usersWalletsRepository.save(newDefaultUserWallet);
    }

    private void updateWalletNameIfCreatedByUserTrue(UserWalletUpdateDTO userWalletUpdateDto, UserWallet uwToUpdate) {
        WalletDTO walletToUpdateDTO = walletMapper.walletToDTO(
                walletsService.getById(userWalletUpdateDto.getWalletId()));
        walletToUpdateDTO.setName(userWalletUpdateDto.getWalletName());
        walletsService.updateById(walletToUpdateDTO.getWalletId(), walletToUpdateDTO);
    }
}
