package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserSearch;
import a16team03.virtualwallet.models.dtos.UserPasswordChangeDTO;
import a16team03.virtualwallet.models.dtos.UserPersonalDetailsDTO;
import a16team03.virtualwallet.models.dtos.UserRegistrationDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

public interface UsersService {
    String signIn(String username, String password);

    User create(UserRegistrationDTO userRegistrationDTO);

    Iterable<User> getAll();

    Page<User> getAll(UserSearch userSearch, Pageable pageable);

    Page<User> findAllUsersByDetail(String userDetail, Pageable pageable);

    User getById(int id);

    User updateById(int id, User user, MultipartFile image) throws IOException;

    void deleteById(int id);

    void changePassword(UserPasswordChangeDTO userPasswordChangeDTO);

    void changePersonalDetails(UserPersonalDetailsDTO userPersonalDetailsDTO);

    int getIdFromPrincipal(Principal principal);

    void updateUserBlockedStatus(int userId);

    void changeProfilePicture(int id, MultipartFile image) throws IOException;
}
