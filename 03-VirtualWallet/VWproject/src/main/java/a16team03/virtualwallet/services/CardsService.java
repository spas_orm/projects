package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.dtos.CardDTO;
import a16team03.virtualwallet.models.dtos.CardDetailsDTO;

import java.util.List;

public interface CardsService {
    Card create(CardDTO cardDTO);

    Card getById(int id);

    Iterable<Card> getAll();

    Card updateById(int id, CardDTO cardDTO);

    void deleteById(int id);

    List<Card> getAllByUserId(int userId);

    void changeCardDetails(CardDetailsDTO cardDetailsDTO);
}
