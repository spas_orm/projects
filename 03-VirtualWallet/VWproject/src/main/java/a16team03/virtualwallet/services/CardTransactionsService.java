package a16team03.virtualwallet.services;

import a16team03.virtualwallet.models.CardTransaction;
import a16team03.virtualwallet.models.CardTransactionSearch;
import a16team03.virtualwallet.models.dtos.CardTransactionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.net.ConnectException;

public interface CardTransactionsService {
    CardTransaction create(CardTransactionDTO cardTransaction);

    Page<CardTransaction> findAll(CardTransactionSearch cardTransactionSearch, Pageable pageable);

    Page<CardTransaction> findAllByUserId(CardTransactionSearch cardTransactionSearch, Pageable pageable);

    CardTransaction execute(CardTransaction cardTransaction) throws ConnectException;

    CardTransaction getLastUnconfirmedCardTransactionByUserId(int id);
}
