package a16team03.virtualwallet.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageFileService {
    String uploadFile(MultipartFile file) throws IOException;

    String getDefaultUserImgPath();
}
