package a16team03.virtualwallet.controllers;

import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.CardTransactionSearch;
import a16team03.virtualwallet.models.TransactionSearch;
import a16team03.virtualwallet.models.UserSearch;
import a16team03.virtualwallet.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class AdminController {
    UsersService usersService;
    TransactionsService transactionsService;
    CardTransactionsService cardTransactionsService;
    WalletsService walletsService;

    @Autowired
    public AdminController(UsersService usersService,
                           TransactionsService transactionsService,
                           CardTransactionsService cardTransactionsService,
                           WalletsService walletsService) {
        this.usersService = usersService;
        this.transactionsService = transactionsService;
        this.cardTransactionsService = cardTransactionsService;
        this.walletsService = walletsService;
    }

    @GetMapping("/admin")
    public String admin() {
        return "redirect:/admin/users";
    }

    @GetMapping("/admin/users")
    public String showUsers(Model model,
                            @ModelAttribute(name = "userSearch")
                                    UserSearch userSearch,
                            @RequestParam(defaultValue = "") String username,
                            @RequestParam(defaultValue = "") String email,
                            @RequestParam(defaultValue = "") String phone,
                            @PageableDefault(size = 7) Pageable pageable) {
        model.addAttribute("users", usersService.getAll(userSearch, pageable));
        model.addAttribute("username", username);
        model.addAttribute("email", email);
        model.addAttribute("phone", phone);
        return "admin-users";
    }

    @PostMapping("/admin/users/{id}")
    public String updateUserBlockedStatus(@PathVariable int id) {
        usersService.updateUserBlockedStatus(id);

        return "redirect:/admin/users";
    }

    @GetMapping("/admin/transactions")
    public String showTransactions(Model model,
                                   @ModelAttribute(name = "transactionSearch")
                                           TransactionSearch transactionSearch,
                                   @RequestParam(defaultValue = "") String username,
                                   @RequestParam(defaultValue = "") String email,
                                   @RequestParam(defaultValue = "") String phone,
                                   @PageableDefault(size = 7) Pageable pageable) {
        model.addAttribute("transactions", transactionsService.findAll(transactionSearch, pageable));
        model.addAttribute("username", username);
        model.addAttribute("email", email);
        model.addAttribute("phone", phone);
        return "admin-transactions";
    }

    @GetMapping("/admin/card-transactions")
    public String showCardTransactions(Model model,
                                       @ModelAttribute(name = "cardTransactionSearch")
                                               CardTransactionSearch cardTransactionSearch,
                                       @PageableDefault(size = 7, sort = "timestamp", direction = Sort.Direction.DESC) Pageable pageable) {
        cardTransactionSearch.setAmountFrom(cardTransactionSearch.getAmountFrom() * 100);
        cardTransactionSearch.setAmountTo(cardTransactionSearch.getAmountTo() * 100);

        model.addAttribute("cardTransactions", cardTransactionsService.findAll(cardTransactionSearch, pageable));

        cardTransactionSearch.setAmountFrom(cardTransactionSearch.getAmountFrom() / 100);
        cardTransactionSearch.setAmountTo(cardTransactionSearch.getAmountTo() / 100);
        return "admin-card-transactions";
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleMethodArgumentTypeMismatchException() {
        return "error-400";
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private String handleEntityNotFoundException() {
        return "error-404";
    }
}
