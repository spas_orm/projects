package a16team03.virtualwallet.controllers.rest;

import a16team03.virtualwallet.models.SignIn;
import a16team03.virtualwallet.services.ImageFileService;
import a16team03.virtualwallet.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/signin")
public class SignInRestController {
    private UsersService usersService;
    private ImageFileService imageService;

    @Autowired
    public SignInRestController(UsersService usersService,
                               ImageFileService imageService) {
        this.usersService = usersService;
        this.imageService = imageService;
    }

    @PostMapping
    public String login(@RequestBody SignIn signIn) {
        return usersService.signIn(signIn.getUsername(), signIn.getPassword());
    }
}
