package a16team03.virtualwallet.controllers;

import a16team03.virtualwallet.exceptions.AccessForbiddenException;
import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.exceptions.PasswordMismatchException;
import a16team03.virtualwallet.exceptions.WrongPasswordException;
import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.dtos.UserPasswordChangeDTO;
import a16team03.virtualwallet.models.dtos.UserPersonalDetailsDTO;
import a16team03.virtualwallet.models.dtos.UserRegistrationDTO;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.UsersWalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@ApiIgnore
@Controller
public class UsersController {
    private UsersService usersService;
    private UsersWalletsService usersWalletsService;

    @Autowired
    public UsersController(UsersService usersService,
                           UsersWalletsService usersWalletsService) {
        this.usersService = usersService;
        this.usersWalletsService = usersWalletsService;
    }

    @GetMapping("/login")
    public String showLoginForm() {
        return "login";
    }

    @GetMapping("/signup")
    public String showSignUpPage(Model model) {
        model.addAttribute("user", new UserRegistrationDTO());
        return "signup";
    }

    @PostMapping("/signup")
    public String registerUser(@ModelAttribute @Valid UserRegistrationDTO userRegistrationDTO, RedirectAttributes attributes) {
        try {
            usersService.create(userRegistrationDTO);
        } catch (DuplicateEntityException e) {
            attributes.addAttribute("status", "duplicateEntry");
            return "redirect:/signup";
        } catch (PasswordMismatchException e) {
            attributes.addAttribute("status", "passwordMismatch");
            return "redirect:/signup";
        }

        attributes.addAttribute("registration", "successful");
        return "redirect:/login";
    }

    @GetMapping("/user/me")
    public String showCurrentUser(Principal principal) {
        int userId = usersService.getIdFromPrincipal(principal);
        return String.format("redirect:/user/%d", userId);
    }

    @GetMapping("/user/{id}")
    public String showUser(@PathVariable int id, Model model, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(id)));
        model.addAttribute("userPasswordChangeDTO", new UserPasswordChangeDTO());
        model.addAttribute("userPersonalDetailsDTO", new UserPersonalDetailsDTO());
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "user";
    }

    @PostMapping("/user/{id}/changePassword")
    public String changePassword(@PathVariable int id,
                                 @ModelAttribute UserPasswordChangeDTO userPasswordChangeDTO,
                                 RedirectAttributes attributes) {
        userPasswordChangeDTO.setId(id);
        attributes.addAttribute("passStatus", "successful");

        try {
            usersService.changePassword(userPasswordChangeDTO);
        } catch (PasswordMismatchException e) {
            attributes.addAttribute("passStatus", "passwordMismatch");
        } catch (WrongPasswordException e) {
            attributes.addAttribute("passStatus", "wrongPassword");
        }

        return String.format("redirect:/user/%d", id);
    }

    @PostMapping("/user/{id}/changePersonalDetails")
    public String changePersonalDetails(@PathVariable int id,
                                        @ModelAttribute @Valid UserPersonalDetailsDTO userPersonalDetailsDTO,
                                        RedirectAttributes attributes) {
        userPersonalDetailsDTO.setId(id);
        attributes.addAttribute("detailsStatus", "successful");

        try {
            usersService.changePersonalDetails(userPersonalDetailsDTO);
        } catch (DuplicateEntityException e) {
            attributes.addAttribute("detailsStatus", "duplicateEntry");
        }

        return String.format("redirect:/user/%d", id);
    }

    @PostMapping("/user/{id}/changeProfilePicture")
    public String changeProfilePicture(@PathVariable int id,
                                       @RequestParam MultipartFile image) {
        try {
            usersService.changeProfilePicture(id, image);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return "redirect:/user/me";
    }

    private void throwIfNotLoggedInAs(Principal principal, int userId) {
        if (usersService.getIdFromPrincipal(principal) != userId) {
            throw new AccessForbiddenException();
        }
    }
}
