package a16team03.virtualwallet.controllers;


import a16team03.virtualwallet.exceptions.AccessForbiddenException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.exceptions.InsufficientAmountException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.TransactionSearch;
import a16team03.virtualwallet.models.dtos.TransactionDTO;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.services.TransactionsService;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.UsersWalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;


@ApiIgnore
@Controller
public class UserTransactionsController {
    private TransactionsService transactionsService;
    private UsersService usersService;
    private UsersWalletsService usersWalletsService;

    @Autowired
    public UserTransactionsController(TransactionsService transactionsService,
                                      UsersService usersService,
                                      UsersWalletsService usersWalletsService) {
        this.transactionsService = transactionsService;
        this.usersService = usersService;
        this.usersWalletsService = usersWalletsService;
    }

    @GetMapping("/user/me/transactions")
    public String showCurrentUser(Principal principal) {
        int userId = usersService.getIdFromPrincipal(principal);
        return String.format("redirect:/user/%d/transactions", userId);
    }


    @GetMapping(value = "/user/{id}/transactions")
    public String showUsersTransactions(@PathVariable int id, Model model,
                                        Principal principal,
                                        @ModelAttribute(name = "transactionSearch")
                                                TransactionSearch transactionSearch,
//                                        @RequestParam(defaultValue = "timestamp") String orderBy,
//                                        @RequestParam(defaultValue = "DESC") String direction,
//                                        @RequestParam(defaultValue = "timestamp") String sort,
                                        @PageableDefault(size = 5, sort = "timestamp", direction = Sort.Direction.DESC)
                                                Pageable pageable) {
        throwIfNotLoggedInAs(principal,id);

        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(id)));
        model.addAttribute("transactions", transactionsService.findAllForUserId(id,
                transactionsService.setTransactionSearchValues(transactionSearch), pageable));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
//            model.addAttribute("orderBy",orderBy);
//            model.addAttribute("direction",direction);
//            model.addAttribute("sort",sort);

        return "transactions";
    }

    @GetMapping("/transactions/new")
    public String showSelectRecipientPage(Model model, @RequestParam(defaultValue = "") String userDetail,
                                          @PageableDefault(size = 10, sort = "username") Pageable pageable,
                                          Principal principal) {
        int senderId = usersService.getIdFromPrincipal(principal);
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(senderId)));
        model.addAttribute("users", usersService.findAllUsersByDetail(userDetail, pageable));
        model.addAttribute("userDetail", userDetail);
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(senderId));
        return "send-money-step-1";
    }

    @GetMapping("/transactions/new/step2")
    public String showCreateTransactionPage(Model model, @RequestParam(defaultValue = "") int recipientId,
                                            RedirectAttributes attributes, Principal principal) {
        try {
            int senderId = usersService.getIdFromPrincipal(principal);
            model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(senderId)));
            model.addAttribute("recipient", usersService.getById(recipientId));
            model.addAttribute("senderId", senderId);
            model.addAttribute("senderWallets", usersWalletsService.getUsersWalletsByUserId(senderId));
            model.addAttribute("defaultWalletId", usersWalletsService.
                    getDefaultUserWalletByUserId(senderId).getWallet().getId());
            model.addAttribute("transactionDTO", new TransactionDTO());
            model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(senderId));
        } catch (EntityNotFoundException enf) {
            attributes.addAttribute("status", "entityNotFound");
            return "redirect:/transactions/new";
        }
        return "send-money-step-2";
    }

    @PostMapping("/transactions/new/step2")
    public String createTransaction(@ModelAttribute TransactionDTO transactionDTO,
                                    RedirectAttributes attributes) {
        try {
            Transaction resultTransaction = transactionsService.create(transactionDTO);
            return String.format("redirect:/transactions/new/confirm/%d", resultTransaction.getTransactionId());
        } catch (InsufficientAmountException iae) {
            attributes.addAttribute("status", "insufficientAmount");
            return String.format("redirect:/transactions/new/step2?recipientId=%d", transactionDTO.getReceiverId());
        }

    }

    @GetMapping("/transactions/new/update/{id}")
    public String showUpdateTransactionPage(@PathVariable int id, Model model, Principal principal) {
//        model.addAttribute("recipient",usersService.getById(transactionDTO.getReceiverId()));
        int senderId = usersService.getIdFromPrincipal(principal);
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(senderId)));
        model.addAttribute("transaction", transactionsService.getById(id));
        model.addAttribute("senderWallets", usersWalletsService.getUsersWalletsByUserId(senderId));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(senderId));
        return "send-money-update";
    }

    @PostMapping("/transactions/new/update/{id}")
    public String updateTransaction(@PathVariable int id, @ModelAttribute Transaction transaction,
                                    RedirectAttributes attributes, Principal principal) {

        checkTransactionSenderIsPrinciple(transaction, principal);

        try {
            transactionsService.changeBeforeConfirmById(id, transaction);
            return String.format("redirect:/transactions/new/confirm/%d", transaction.getTransactionId());
        } catch (InsufficientAmountException iae) {
            attributes.addAttribute("status", "insufficientAmount");
            return String.format("redirect:/transactions/new/update/%d", transaction.getTransactionId());
        }


    }

    @GetMapping("/transactions/new/confirm/{id}")
    public String showConfirmTransactionPage(@PathVariable int id, Model model,Principal principal) {
        int senderId = usersService.getIdFromPrincipal(principal);
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(senderId)));
        model.addAttribute("transaction", transactionsService.getById(id));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(senderId));
        return "send-money-confirm";
    }


    @PostMapping("/transactions/new/confirm/{id}")
    public String confirmTransaction(@PathVariable int id, @ModelAttribute Transaction transaction,
                                     RedirectAttributes attributes, Principal principal) {

        checkTransactionSenderIsPrinciple(transaction, principal);

        transactionsService.confirmById(id);

        return String.format("redirect:/transactions/new/success/%d", transaction.getTransactionId());
    }

    @GetMapping("/transactions/new/success/{id}")
    public String showSuccessTransactionPage(@PathVariable int id, Model model, Principal principal) {
        int senderId = usersService.getIdFromPrincipal(principal);
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(senderId)));
        model.addAttribute("transaction", transactionsService.getById(id));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(senderId));
        return "send-money-success";
    }


    private void checkTransactionSenderIsPrinciple(Transaction transaction, Principal principal) {
        if (!transaction.getSendingUser().getUsername().equals(principal.getName())) {
            throw new OperationNotAllowedException();
        }
    }

    private void throwIfNotLoggedInAs(Principal principal, int userId) {
        if (usersService.getIdFromPrincipal(principal)!=userId) {
            throw new AccessForbiddenException();
        }
    }
}