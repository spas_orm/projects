package a16team03.virtualwallet.controllers;

import a16team03.virtualwallet.exceptions.AccessForbiddenException;
import a16team03.virtualwallet.exceptions.InsufficientAmountException;
import a16team03.virtualwallet.models.CardTransaction;
import a16team03.virtualwallet.models.CardTransactionSearch;
import a16team03.virtualwallet.models.dtos.CardTransactionDTO;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.services.CardTransactionsService;
import a16team03.virtualwallet.services.CardsService;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.UsersWalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.net.ConnectException;
import java.security.Principal;

@ApiIgnore
@Controller
@RequestMapping("/")
public class CardTransactionsController {
    private static final int DEFAULT_CARD_TRANSACTIONS_PAGE_SIZE = 7;

    private CardTransactionsService cardTransactionsService;
    private UsersWalletsService usersWalletsService;
    private CardsService cardsService;
    private UsersService usersService;

    @Autowired
    public CardTransactionsController(CardTransactionsService cardTransactionsService,
                                      UsersWalletsService usersWalletsService,
                                      CardsService cardsService,
                                      UsersService usersService) {
        this.cardTransactionsService = cardTransactionsService;
        this.usersWalletsService = usersWalletsService;
        this.cardsService = cardsService;
        this.usersService = usersService;
    }

    @GetMapping("/user/me/card-transactions")
    public String showCurrentUser(Principal principal) {
        int userId = usersService.getIdFromPrincipal(principal);
        return String.format("redirect:/user/%d/card-transactions", userId);
    }

    @GetMapping(value = "/user/{id}/card-transactions")
    public String showTransactions(@PathVariable int id,
                                   Model model, Principal principal,
                                   @ModelAttribute(name = "cardTransactionSearch") CardTransactionSearch cardTransactionSearch,
                                   @PageableDefault(size = DEFAULT_CARD_TRANSACTIONS_PAGE_SIZE,
                                           sort = "timestamp",
                                           direction = Sort.Direction.DESC) Pageable pageable) {
        throwIfNotLoggedInAs(principal, id);

        cardTransactionSearch.setUserId(id);

        cardTransactionSearch.setAmountFrom(cardTransactionSearch.getAmountFrom() * 100);
        cardTransactionSearch.setAmountTo(cardTransactionSearch.getAmountTo() * 100);

        model.addAttribute("cardTransactions", cardTransactionsService
                .findAllByUserId(cardTransactionSearch, pageable));
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(id)));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        model.addAttribute("userWallets", usersWalletsService.getUsersWalletsByUserId(id));

        cardTransactionSearch.setAmountFrom(cardTransactionSearch.getAmountFrom() / 100);
        cardTransactionSearch.setAmountTo(cardTransactionSearch.getAmountTo() / 100);

        return "card-transactions";
    }

    @GetMapping("/deposit")
    public String showDepositMoney(Model model,
                                   Principal principal) {
        int currentUserId = usersService.getIdFromPrincipal(principal);

        model.addAttribute("cardTransactionDTO", new CardTransactionDTO());
        model.addAttribute("userWallets", usersWalletsService.getUsersWalletsByUserId(currentUserId));
        model.addAttribute("userCards", cardsService.getAllByUserId(currentUserId));
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(currentUserId)));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(currentUserId));

        return "deposit-money";
    }

    @PostMapping("/deposit")
    public String depositMoney(@ModelAttribute @Valid CardTransactionDTO cardTransactionDTO,
                               Principal principal) {
        cardTransactionDTO.setReceivingUserId(usersService.getIdFromPrincipal(principal));

        cardTransactionsService.create(cardTransactionDTO);

        return "redirect:/deposit/confirm";
    }

    @GetMapping("/deposit/confirm")
    public String showDepositMoneyConfirmation(Model model,
                                               Principal principal) {
        int currentUserId = usersService.getIdFromPrincipal(principal);

        CardTransaction cardTransaction = cardTransactionsService.getLastUnconfirmedCardTransactionByUserId(currentUserId);
        String lastFourDigitsOfCardNumber = cardsService.getById(cardTransaction.getCardId()).getCardNumber().substring(12, 16);

        model.addAttribute("cardTransactionDTO", cardTransaction);
        model.addAttribute("lastFourDigitsOfCardNumber", lastFourDigitsOfCardNumber);
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(currentUserId)));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(currentUserId));
        return "deposit-money-confirm";
    }

    @PostMapping("/deposit/confirm")
    public String executeCardTransaction(Principal principal,
                                         RedirectAttributes attributes) {
        int currentUserId = usersService.getIdFromPrincipal(principal);

        CardTransaction cardTransaction = cardTransactionsService.getLastUnconfirmedCardTransactionByUserId(currentUserId);

        try {
            cardTransactionsService.execute(cardTransaction);
        } catch (InsufficientAmountException e) {
            attributes.addAttribute("status", "insufficientFunds");
            return "redirect:/deposit";
        } catch (ConnectException e) {
            attributes.addAttribute("status", "connectionFailed");
            return "redirect:/deposit/confirm";
        }

        return "redirect:/deposit/success";
    }

    @GetMapping("/deposit/success")
    public String getDepositConfirmation(Model model,
                                         Principal principal) {
        int currentUserId = usersService.getIdFromPrincipal(principal);

        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(currentUserId)));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(currentUserId));

        return "deposit-money-success";
    }

    private void throwIfNotLoggedInAs(Principal principal, int userId) {
        if (usersService.getIdFromPrincipal(principal) != userId) {
            throw new AccessForbiddenException();
        }
    }
}
