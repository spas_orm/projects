package a16team03.virtualwallet.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
@RequestMapping
public class HomeController {
    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/index")
    public String showIndex() {
        return "index";
    }
}
