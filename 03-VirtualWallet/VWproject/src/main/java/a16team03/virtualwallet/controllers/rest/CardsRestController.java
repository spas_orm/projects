package a16team03.virtualwallet.controllers.rest;

import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.dtos.CardDTO;
import a16team03.virtualwallet.services.CardsService;
import a16team03.virtualwallet.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
//@RequestMapping("/api/v1/cards")
public class CardsRestController {
    private CardsService cardsService;
    private UsersService usersService;

    @Autowired
    public CardsRestController(CardsService cardsService, UsersService usersService) {
        this.cardsService = cardsService;
        this.usersService = usersService;
    }

    @GetMapping("/api/v1/user/cards/")
    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Card> getCardsForUser(@AuthenticationPrincipal Principal principal){
        try {
            return cardsService.getAllByUserId(usersService.getIdFromPrincipal(principal));
        }catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @GetMapping("/api/v1/user/cards/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public Card getCardById(@PathVariable int id,@AuthenticationPrincipal Principal principal){
        try {
            Card card = cardsService.getById(id);
            if (!checkCardCreatedByPrincipal(card,principal)){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You don't have access to this card.");
            }
            return card;
        }catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @PutMapping("/api/v1/user/cards/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public Card updateCardById(@PathVariable int id, @RequestBody CardDTO cardDTO,
                                 @AuthenticationPrincipal Principal principal) {
        try {
            Card card = cardsService.getById(id);
            if (!checkCardCreatedByPrincipal(card,principal)){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You don't have access to this card.");
            }

            return cardsService.updateById(id, cardDTO);

        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @DeleteMapping("/api/v1/user/cards/{id}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteCardBuId(@PathVariable int id, @AuthenticationPrincipal Principal principal) {
        try {
            Card card = cardsService.getById(id);
            if (!checkCardCreatedByPrincipal(card,principal)){
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "You don't have access to this card.");
            }
            cardsService.deleteById(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    private boolean checkCardCreatedByPrincipal(Card card, Principal principal){
        return card.getCardOwner().getUsername().equals(principal.getName());
    }
}
