package a16team03.virtualwallet.controllers;

import a16team03.virtualwallet.exceptions.AccessForbiddenException;
import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.exceptions.OperationNotAllowedException;
import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.UserDTO;
import a16team03.virtualwallet.models.dtos.UserWalletDTO;
import a16team03.virtualwallet.models.dtos.UserWalletUpdateDTO;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.UsersWalletsService;
import a16team03.virtualwallet.services.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@ApiIgnore
@Controller
public class UserWalletsController {
    private WalletsService walletsService;
    private UsersService usersService;
    private UsersWalletsService usersWalletsService;

    @Autowired
    public UserWalletsController(WalletsService walletsService, UsersService usersService,
                                 UsersWalletsService usersWalletsService) {
        this.walletsService = walletsService;
        this.usersService = usersService;
        this.usersWalletsService = usersWalletsService;
    }

    @GetMapping("/user/me/wallets")
    public String showCurrentUser(Principal principal) {
        int userId = usersService.getIdFromPrincipal(principal);
        return String.format("redirect:/user/%d/wallets", userId);
    }

    @GetMapping("/user/{id}/wallets")
    public String showWallets(@PathVariable int id, Model model, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("userWallets", usersWalletsService.getUsersWalletsByUserId(id));
        model.addAttribute("newWalletDTO", new WalletDTO());
        model.addAttribute("newUserWalletDTO", new UserWalletDTO());
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallets";
    }

    @PostMapping("/user/{id}/wallets")
    public String createUserWallet(@PathVariable int id,
                                   @ModelAttribute WalletDTO walletDTO,
                                   RedirectAttributes attributes,
                                   Principal principal) {
        int currentUserId = usersService.getIdFromPrincipal(principal);
        UserWalletDTO newUserWalletDTO = new UserWalletDTO();
        Wallet newWallet = walletsService.create(walletDTO);
        newUserWalletDTO.setUserId(currentUserId);
        newUserWalletDTO.setWalletId(newWallet.getId());
        usersWalletsService.createUserWallet(newUserWalletDTO);

        attributes.addAttribute("walletStatus", "creationSuccessful");
        return String.format("redirect:/user/%d/wallets", id);
    }


    @GetMapping("/user/{id}/wallets/{uwId}")
    public String showWalletToUpdate(@PathVariable int id, @PathVariable int uwId, Model model, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("userWallet", usersWalletsService.getById(uwId));
        model.addAttribute("updateUserWalletDTO", new UserWalletUpdateDTO());
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallet-edit";
    }

    @PostMapping("/user/{id}/wallets/{uwId}")
    public String updateUserWallet(@PathVariable int id, @PathVariable int uwId,
                                   @ModelAttribute UserWalletUpdateDTO userWalletUpdateDTO,
                                   RedirectAttributes attributes,
                                   Principal principal) {
        int currentUserId = usersService.getIdFromPrincipal(principal);
        userWalletUpdateDTO.setUserId(currentUserId);
        usersWalletsService.updateUserWallet(uwId, userWalletUpdateDTO);
        attributes.addAttribute("walletStatus", "updateSuccessful");
        return String.format("redirect:/user/%d/wallets", id);
    }


    @GetMapping("/user/{id}/wallets/{walletId}/remove")
    public String showConfirmRemoveOwnWallet(@PathVariable int id, @PathVariable int walletId,
                                             Model model, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("userWalletDTO", new UserWalletDTO());
        model.addAttribute("wallet", walletsService.getById(walletId));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallet-remove";
    }

    @PostMapping("/user/{id}/wallets/{walletId}/remove")
    public String removeOwnWallet(@PathVariable int id, @ModelAttribute UserWalletDTO userWalletDTO,
                                  Principal principal, RedirectAttributes attributes) {
        int requestByUserId = usersService.getIdFromPrincipal(principal);
        try {
            usersWalletsService.removeOwnWallet(requestByUserId, userWalletDTO);
        } catch (OperationNotAllowedException e) {
            attributes.addAttribute("walletStatus", "removeNotAllowed");
            return String.format("redirect:/user/%d/wallets", id);
        }
        attributes.addAttribute("walletStatus", "removeSuccessful");

        return String.format("redirect:/user/%d/wallets", id);
    }


    @GetMapping("/user/{id}/wallets/{walletId}/manage")
    public String showManageWallet(@PathVariable int id, @PathVariable int walletId,
                                   Model model, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("wallet", walletsService.getById(walletId));
        model.addAttribute("listUserWallets", usersWalletsService.getUsersWalletsByWalletId(walletId));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallet-manage";
    }

    @GetMapping("/user/{id}/wallets/{walletId}/addUser")
    public String showSelectUserToAdd(@PathVariable int id, @PathVariable int walletId, Model model,
                                      @RequestParam(defaultValue = "") String userDetail,
                                      @PageableDefault(size = 10, sort = "username") Pageable pageable,
                                      Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("users", usersService.findAllUsersByDetail(userDetail, pageable));
        model.addAttribute("userDetail", userDetail);
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("wallet", walletsService.getById(walletId));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallet-addUser";
    }

    @GetMapping("/user/{id}/wallets/{walletId}/addconfirm")
    public String showConfirmAddUserToWallet(@PathVariable int id, @PathVariable int walletId, Model model,
                                             @RequestParam(defaultValue = "") int addUserId, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("userWalletDTO", new UserWalletDTO());
        model.addAttribute("userToAdd", usersService.getById(addUserId));
        model.addAttribute("wallet", walletsService.getById(walletId));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallet-addUser-confirm";
    }

    @PostMapping("/user/{id}/wallets/{walletId}/addconfirm")
    public String addUserToWallet(@PathVariable int id, @PathVariable int walletId,
                                  @ModelAttribute UserWalletDTO userWalletDTO, Model model) {
        try {
            usersWalletsService.addUserToWallet(userWalletDTO);
        } catch (DuplicateEntityException e) {
            model.addAttribute("status", "duplicateEntry");
            return String.format("redirect:/user/%d/wallets/%d/manage", id, walletId);
        }

        return String.format("redirect:/user/%d/wallets/%d/manage", id, walletId);
    }

    @GetMapping("/user/{id}/wallets/{walletId}/remconfirm")
    public String showConfirmRemoveUserToWallet(@PathVariable int id, @PathVariable int walletId, Model model,
                                                @RequestParam(defaultValue = "") int removeUserId, Principal principal) {
        throwIfNotLoggedInAs(principal, id);
        UserDTO userDTO = UserMapper.userToDTO(usersService.getById(id));
        model.addAttribute("userDTO", userDTO);
        model.addAttribute("userWalletDTO", new UserWalletDTO());
        model.addAttribute("userToRemove", usersService.getById(removeUserId));
        model.addAttribute("wallet", walletsService.getById(walletId));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        return "wallet-removeUser-confirm";
    }

    @PostMapping("/user/{id}/wallets/{walletId}/remconfirm")
    public String removeUserFromWallet(@PathVariable int id, @PathVariable int walletId,
                                       @ModelAttribute UserWalletDTO userWalletDTO, Principal principal) {
        int requestByUserId = usersService.getIdFromPrincipal(principal);
        usersWalletsService.removeUserForSharedWallet(requestByUserId, userWalletDTO);

        return String.format("redirect:/user/%d/wallets/%d/manage", id, walletId);
    }

    private void throwIfNotLoggedInAs(Principal principal, int userId) {
        if (usersService.getIdFromPrincipal(principal) != userId) {
            throw new AccessForbiddenException();
        }
    }

}
