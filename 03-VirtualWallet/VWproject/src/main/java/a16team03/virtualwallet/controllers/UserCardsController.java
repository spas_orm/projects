package a16team03.virtualwallet.controllers;

import a16team03.virtualwallet.exceptions.AccessForbiddenException;
import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.dtos.CardDTO;
import a16team03.virtualwallet.models.dtos.CardDetailsDTO;
import a16team03.virtualwallet.models.mappers.CardMapper;
import a16team03.virtualwallet.models.mappers.UserMapper;
import a16team03.virtualwallet.services.CardsService;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.UsersWalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;

@ApiIgnore
@Controller
public class UserCardsController {
    CardsService cardsService;
    UsersService usersService;
    UsersWalletsService usersWalletsService;
    CardMapper cardMapper;

    @Autowired
    public UserCardsController(CardsService cardsService,
                               UsersService usersService,
                               UsersWalletsService usersWalletsService,
                               CardMapper cardMapper) {
        this.cardsService = cardsService;
        this.usersService = usersService;
        this.usersWalletsService = usersWalletsService;
        this.cardMapper = cardMapper;
    }

    @GetMapping("/user/{id}/cards")
    public String showUser(@PathVariable int id, Model model, Principal principal) {
        throwIfNotLoggedInAs(principal,id);
        model.addAttribute("cards", cardsService.getAllByUserId(id));
        model.addAttribute("userDTO", UserMapper.userToDTO(usersService.getById(id)));
        model.addAttribute("totalBalance", usersWalletsService.getTotalBalanceInAllWalletsByUserId(id));
        model.addAttribute("newCardDTO", new CardDTO());
        model.addAttribute("cardUpdateDetailsDTO", new CardDetailsDTO());
        return "cards";
    }

    @GetMapping("/user/me/cards")
    public String showUserCards(Principal principal) {
        int userId = usersService.getIdFromPrincipal(principal);
        return String.format("redirect:/user/%d/cards", userId);
    }

    @PostMapping("/user/{id}/cards")
    public String createCard(@PathVariable int id,
                             @ModelAttribute @Valid CardDTO cardDTO,
                             RedirectAttributes attributes,
                             Principal principal) {
        attributes.addAttribute("cardStatus", "creationSuccessful");

        int currentUserId = usersService.getIdFromPrincipal(principal);
        cardDTO.setCardOwnerId(currentUserId);

        try {
            cardsService.create(cardDTO);
        } catch (DuplicateEntityException e) {
            attributes.addAttribute("cardStatus", "duplicateEntry");
        }

        return String.format("redirect:/user/%d/cards", id);
    }

    @GetMapping("/user/{userId}/cards/{cardId}/update")
    public String getUpdateCard(@PathVariable int userId,
                                @PathVariable int cardId,
                                Model model, Principal principal) {
        throwIfNotLoggedInAs(principal,userId);

        Card card = cardsService.getById(cardId);
        CardDetailsDTO cardDetailsDTO = cardMapper.cardToDetailsDto(card);

        model.addAttribute("userId", userId);
        model.addAttribute("cardDetailsDTO", cardDetailsDTO);

        return "edit-card-details-modal";
    }

    @PostMapping("/user/{userId}/cards/{cardId}/update")
    public String updateCard(@PathVariable int userId,
                             @ModelAttribute @Valid CardDetailsDTO cardDetailsDTO,
                             RedirectAttributes attributes) {
        cardsService.changeCardDetails(cardDetailsDTO);

        attributes.addAttribute("cardStatus", "editSuccessful");
        return String.format("redirect:/user/%d/cards", userId);
    }

    @GetMapping("/user/{userId}/cards/{cardId}")
    public String deleteCard(@PathVariable int userId,
                             @PathVariable int cardId,
                             RedirectAttributes attributes,
                             Principal principal) {
        throwIfNotLoggedInAs(principal,userId);

        cardsService.deleteById(cardId);

        attributes.addAttribute("cardStatus", "deletionSuccessful");
        return String.format("redirect:/user/%d/cards", userId);

    }

    private void throwIfNotLoggedInAs(Principal principal, int userId) {
        if (usersService.getIdFromPrincipal(principal)!=userId) {
            throw new AccessForbiddenException();
        }
    }
}
