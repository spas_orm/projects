package a16team03.virtualwallet.controllers.rest;

import a16team03.virtualwallet.exceptions.DuplicateEntityException;
import a16team03.virtualwallet.exceptions.EntityNotFoundException;
import a16team03.virtualwallet.models.TransactionSearch;
import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserSearch;
import a16team03.virtualwallet.models.dtos.UserRegistrationDTO;
import a16team03.virtualwallet.services.ImageFileService;
import a16team03.virtualwallet.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class UsersRestController {
    private UsersService usersService;
    private ImageFileService imageService;

    @Autowired
    public UsersRestController(UsersService usersService,
                               ImageFileService imageService) {
        this.usersService = usersService;
        this.imageService = imageService;
    }

    @PostMapping("/register")
    public User create(@RequestBody @Valid UserRegistrationDTO userRegistrationDTO) {
        try {
            return usersService.create(userRegistrationDTO);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Page<User> getAll(@RequestBody Optional<UserSearch> userSearch,
            @PageableDefault(size = 10) Pageable pageable) {

        return usersService.getAll(userSearch.orElse(new UserSearch()),pageable);
    }

    @GetMapping("/users/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User getById(@PathVariable int id) {
        try {
            return usersService.getById(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('ROLE_USER')")
    public User getCurrentUser(@AuthenticationPrincipal Principal principal) {
        try {
            return usersService.getById(usersService.getIdFromPrincipal(principal));
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }


    @PutMapping("/users/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User update(@PathVariable int id, @RequestBody User user, @RequestParam(required = false) MultipartFile image) {
        try {
            return usersService.updateById(id, user, image);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/user/me")
    @PreAuthorize("hasRole('ROLE_USER')")
    public User updateCurrentUser(@AuthenticationPrincipal Principal principal,
                                  @RequestBody User user,
                                  @RequestParam(required = false) MultipartFile image) {
        try {
            return usersService.updateById(usersService.getIdFromPrincipal(principal), user, image);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/users/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void delete(@PathVariable int id) {
        try {
            usersService.deleteById(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @DeleteMapping("/user/me")
    @PreAuthorize("hasRole('ROLE_USER')")
    public void deleteCurrentUser(@AuthenticationPrincipal Principal principal) {
        try {
            usersService.deleteById(usersService.getIdFromPrincipal(principal));
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }
}
