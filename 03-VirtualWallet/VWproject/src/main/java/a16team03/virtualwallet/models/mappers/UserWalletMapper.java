package a16team03.virtualwallet.models.mappers;

import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.dtos.UserWalletDTO;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class UserWalletMapper {
    private UsersService usersService;
    private WalletsService walletsService;

    @Autowired
    @Lazy
    public UserWalletMapper(UsersService usersService, WalletsService walletsService) {
        this.usersService = usersService;
        this.walletsService = walletsService;
    }

    public UserWallet dtoToUserWallet(UserWalletDTO userWalletDTO){
        UserWallet uw = new UserWallet();

        uw.setUser(usersService.getById(userWalletDTO.getUserId()));
        uw.setWallet(walletsService.getById(userWalletDTO.getWalletId()));
        uw.setCreatedByUser(userWalletDTO.getCreatedByUser());
        uw.setDefaultWallet(userWalletDTO.getDefaultWallet());

        return uw;
    }

}
