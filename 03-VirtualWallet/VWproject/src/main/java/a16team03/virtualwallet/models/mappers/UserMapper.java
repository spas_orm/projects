package a16team03.virtualwallet.models.mappers;

import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.dtos.UserDTO;
import a16team03.virtualwallet.models.dtos.UserRegistrationDTO;

public class UserMapper {
    public static UserDTO userToDTO(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getUserId());
        userDTO.setUsername(user.getUsername());
        userDTO.setFullName(user.getFullName());
        userDTO.setEmail(user.getEmail());
        userDTO.setPhone(user.getPhone());
        userDTO.setImageUrl(user.getImageUrl());
        userDTO.setBlocked(user.isBlocked());

        return userDTO;
    }

    public static User userRegistrationDtoToUser(UserRegistrationDTO userRegistrationDTO) {
        User user = new User();

        user.setUsername(userRegistrationDTO.getUsername());
        user.setPassword(userRegistrationDTO.getPassword());
        user.setEmail(userRegistrationDTO.getEmail());
        user.setPhone(userRegistrationDTO.getPhone());

        return user;
    }
}
