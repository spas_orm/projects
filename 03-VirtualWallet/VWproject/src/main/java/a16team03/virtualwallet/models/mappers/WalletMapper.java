package a16team03.virtualwallet.models.mappers;

import a16team03.virtualwallet.models.Wallet;
import a16team03.virtualwallet.models.dtos.WalletDTO;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.WalletTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.BigInteger;

@Component
public class WalletMapper {
    private UsersService usersService;
    private WalletTypesService walletTypesService;

    @Autowired
    public WalletMapper(UsersService usersService,
                        WalletTypesService walletTypesService) {
        this.usersService = usersService;
        this.walletTypesService = walletTypesService;
    }

    public Wallet dtoToWallet(WalletDTO walletDTO) {
        Wallet wallet = new Wallet();

        wallet.setId(walletDTO.getWalletId());
        if (walletDTO.getName() == null || walletDTO.getName().isEmpty()) {
            wallet.setName("Wallet");
        } else {
            wallet.setName(walletDTO.getName());
        }
        if (walletDTO.getBalance() == null) {
            wallet.setBalance(BigDecimal.ZERO);
        } else {
            wallet.setBalance(walletDTO.getBalance());
        }

        wallet.setWalletType(walletTypesService.getWalletTypeById(walletDTO.getWalletTypeId()));

        return wallet;
    }

    public WalletDTO walletToDTO(Wallet wallet) {
        WalletDTO walletDTO = new WalletDTO();

        walletDTO.setWalletId(wallet.getId());
        walletDTO.setName(wallet.getName());
        walletDTO.setBalance(wallet.getBalance());
        walletDTO.setWalletTypeId(wallet.getWalletType().getId());

        return walletDTO;
    }


}
