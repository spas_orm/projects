package a16team03.virtualwallet.models.dtos;

public class CardTransactionDTO {
    private int id;

    private double amount;

    private String currency;

    private String description;

    private String idempotencyKey;

    private int cardId;

    private int receivingUserId;

    private int receivingWalletId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdempotencyKey() {
        return idempotencyKey;
    }

    public void setIdempotencyKey(String idempotencyKey) {
        this.idempotencyKey = idempotencyKey;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getReceivingUserId() {
        return receivingUserId;
    }

    public void setReceivingUserId(int receivingUserId) {
        this.receivingUserId = receivingUserId;
    }

    public int getReceivingWalletId() {
        return receivingWalletId;
    }

    public void setReceivingWalletId(int receivingWalletId) {
        this.receivingWalletId = receivingWalletId;
    }
}
