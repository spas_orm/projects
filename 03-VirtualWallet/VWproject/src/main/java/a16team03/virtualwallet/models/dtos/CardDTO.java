package a16team03.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CardDTO {
    @Pattern(regexp = "^[0-9]{16}")
    private String cardNumber;

    @NotNull
    private String ownerName;

    @Pattern(regexp = "^[0-9]{2}/[0-9]{2}")
    private String expirationDate;

    @Pattern(regexp = "^[0-9]{3}")
    private String cvc;

    private int cardOwnerId;

    public CardDTO() {
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public int getCardOwnerId() {
        return cardOwnerId;
    }

    public void setCardOwnerId(int cardOwnerId) {
        this.cardOwnerId = cardOwnerId;
    }
}
