package a16team03.virtualwallet.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "wallets")
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wallet_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "balance")
    private BigDecimal balance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "wallet_type")
    private WalletType walletType;

//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "owned_by")
//    private User ownedBy;

    public Wallet() {
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public WalletType getWalletType() {
        return walletType;
    }

    public void setWalletType(WalletType walletType) {
        this.walletType = walletType;
    }

//    public User getOwnedBy() {
//        return ownedBy;
//    }
//
//    public void setOwnedBy(User ownedBy) {
//        this.ownedBy = ownedBy;
//    }
}
