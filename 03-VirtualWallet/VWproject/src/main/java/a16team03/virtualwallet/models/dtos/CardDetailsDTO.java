package a16team03.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

public class CardDetailsDTO {
    @Positive
    private int cardId;

    @Pattern(regexp = "^[0-9]{16}")
    private String cardNumber;

    @NotNull
    private String ownerName;

    @Pattern(regexp = "^[0-9]{2}/[0-9]{2}")
    private String expirationDate;

    @Pattern(regexp = "^[0-9]{3}")
    private String cvc;

    public CardDetailsDTO() {
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }
}
