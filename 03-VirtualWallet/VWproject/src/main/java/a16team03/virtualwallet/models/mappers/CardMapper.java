package a16team03.virtualwallet.models.mappers;

import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.dtos.CardDTO;
import a16team03.virtualwallet.models.dtos.CardDetailsDTO;
import a16team03.virtualwallet.services.UsersService;
import org.springframework.stereotype.Component;

@Component
public class CardMapper {
    private UsersService usersService;

    public CardMapper(UsersService usersService) {
        this.usersService = usersService;
    }

    public Card dtoToCard(CardDTO cardDTO) {
        Card card = new Card();

        card.setCardNumber(cardDTO.getCardNumber());
        card.setOwnerName(cardDTO.getOwnerName());
        card.setExpirationDate(cardDTO.getExpirationDate());
        card.setCvc(cardDTO.getCvc());
        card.setCardOwner(usersService.getById(cardDTO.getCardOwnerId()));

        return card;
    }

    public CardDetailsDTO cardToDetailsDto(Card card) {
        CardDetailsDTO cardDetailsDTO = new CardDetailsDTO();

        cardDetailsDTO.setCardId(card.getCardId());
        cardDetailsDTO.setCardNumber(card.getCardNumber());
        cardDetailsDTO.setOwnerName(card.getOwnerName());
        cardDetailsDTO.setExpirationDate(card.getExpirationDate());
        cardDetailsDTO.setCvc(card.getCvc());

        return cardDetailsDTO;
    }

    public void mapUpdatedCardDetails(CardDTO cardDTO, Card cardToUpdate) {
        cardToUpdate.setOwnerName(cardDTO.getOwnerName());
        cardToUpdate.setExpirationDate(cardDTO.getExpirationDate());
        cardToUpdate.setCvc(cardDTO.getCvc());
    }
}
