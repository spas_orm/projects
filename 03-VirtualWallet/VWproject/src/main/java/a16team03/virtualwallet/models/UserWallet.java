package a16team03.virtualwallet.models;

import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "users_wallets")
@Where(clause = "is_deleted = 0")
public class UserWallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_wallet_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    @Column(name = "is_default")
    private boolean defaultWallet;

    @Column(name = "is_created_by_user")
    private boolean createdByUser;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    public UserWallet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int userId) {
        this.id = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public boolean getDefaultWallet() {
        return defaultWallet;
    }

    public void setDefaultWallet(boolean defaultWallet) {
        this.defaultWallet = defaultWallet;
    }

    public boolean getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(boolean createdByUser) {
        this.createdByUser = createdByUser;
    }

    public boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
