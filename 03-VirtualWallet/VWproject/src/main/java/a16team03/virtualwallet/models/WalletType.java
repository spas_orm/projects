package a16team03.virtualwallet.models;

import javax.persistence.*;

@Entity
@Table(name = "wallets_types")
public class WalletType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wallet_type_id")
    private int id;

    @Column(name = "wallet_type")
    private String typeName;

    public WalletType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
