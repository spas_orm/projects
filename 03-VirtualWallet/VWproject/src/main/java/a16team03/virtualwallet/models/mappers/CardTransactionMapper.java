package a16team03.virtualwallet.models.mappers;

import a16team03.virtualwallet.models.Card;
import a16team03.virtualwallet.models.CardTransaction;
import a16team03.virtualwallet.models.dtos.CardTransactionDTO;
import a16team03.virtualwallet.services.CardsService;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardTransactionMapper {
    private UsersService usersService;
    private WalletsService walletsService;
    private CardsService cardsService;
    private CardMapper cardMapper;

    @Autowired
    public CardTransactionMapper(UsersService usersService,
                                 WalletsService walletsService,
                                 CardsService cardsService,
                                 CardMapper cardMapper) {
        this.usersService = usersService;
        this.walletsService = walletsService;
        this.cardsService = cardsService;
        this.cardMapper = cardMapper;
    }

    public CardTransaction dtoToCardTransaction(CardTransactionDTO cardTransactionDTO) {
        CardTransaction cardTransaction = new CardTransaction();

        cardTransaction.setId(cardTransactionDTO.getId());
        cardTransaction.setAmount((int) (cardTransactionDTO.getAmount() * 100));
        cardTransaction.setCurrency(cardTransactionDTO.getCurrency());
        cardTransaction.setDescription(cardTransactionDTO.getDescription());
        cardTransaction.setIdempotencyKey(cardTransactionDTO.getIdempotencyKey());
        cardTransaction.setCardId(cardTransactionDTO.getCardId());

        Card card = cardsService.getById(cardTransactionDTO.getCardId());
        cardTransaction.setCardDetailsDTO(cardMapper.cardToDetailsDto(card));

        cardTransaction.setReceivingUser(usersService.getById(cardTransactionDTO.getReceivingUserId()));
        cardTransaction.setReceivingWallet(walletsService.getById(cardTransactionDTO.getReceivingWalletId()));

        return cardTransaction;
    }
}
