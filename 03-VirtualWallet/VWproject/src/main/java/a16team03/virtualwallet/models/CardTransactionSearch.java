package a16team03.virtualwallet.models;

import java.time.LocalDate;

public class CardTransactionSearch {
    private int amountFrom;

    private int amountTo;

    private String username;

    private String walletName;

    private LocalDate fromDate;

    private LocalDate tillDate;

    private String orderBy;

    private String direction;

    private String sort;

    private int userId;

    public CardTransactionSearch() {
    }

    public int getAmountFrom() {
        return amountFrom;
    }

    public void setAmountFrom(int amountFrom) {
        this.amountFrom = amountFrom;
    }

    public int getAmountTo() {
        return amountTo;
    }

    public void setAmountTo(int amountTo) {
        this.amountTo = amountTo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getTillDate() {
        return tillDate;
    }

    public void setTillDate(LocalDate tillDate) {
        this.tillDate = tillDate;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
