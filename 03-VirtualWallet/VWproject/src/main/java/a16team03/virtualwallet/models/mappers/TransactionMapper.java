package a16team03.virtualwallet.models.mappers;

import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.dtos.TransactionDTO;
import a16team03.virtualwallet.services.UsersService;
import a16team03.virtualwallet.services.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Component
public class TransactionMapper {
    private UsersService usersService;
    private WalletsService walletsService;

    @Autowired
    public TransactionMapper(UsersService usersService, WalletsService walletsService) {
        this.usersService = usersService;
        this.walletsService = walletsService;
    }

    public Transaction dtoToTransaction(TransactionDTO transactionDto){
        Transaction transaction = new Transaction();

        transaction.setTransactionId(transactionDto.getTransactionId());
        transaction.setSendingUser(usersService.getById(transactionDto.getSenderId()));
        transaction.setSenderWallet(walletsService.getById(transactionDto.getSenderWallet()));
        transaction.setReceivingUser(usersService.getById(transactionDto.getReceiverId()));
        transaction.setAmount(transactionDto.getAmount());
        if(transactionDto.getTimestamp()==null){
            transaction.setTimestamp(new Timestamp(System.currentTimeMillis()));
        } else {
        transaction.setTimestamp(transactionDto.getTimestamp());
        }
        transaction.setConfirmed(transactionDto.isConfirmed());
        transaction.setDescription(transactionDto.getDescription());

        return transaction;
    }
}
