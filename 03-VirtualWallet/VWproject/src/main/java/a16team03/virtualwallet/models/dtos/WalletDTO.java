package a16team03.virtualwallet.models.dtos;

import java.math.BigDecimal;

public class WalletDTO {
    private int walletId;

    private String name;

    private BigDecimal balance;

    private int walletTypeId;

//    private int ownedById;

    public WalletDTO() {
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getWalletTypeId() {
        return walletTypeId;
    }

    public void setWalletTypeId(int walletTypeId) {
        this.walletTypeId = walletTypeId;
    }

//    public int getOwnedById() {
//        return ownedById;
//    }
//
//    public void setOwnedById(int ownedById) {
//        this.ownedById = ownedById;
//    }
}
