package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface TransactionsRepository extends PagingAndSortingRepository<Transaction, Integer> {

    @Query("SELECT t FROM Transaction t WHERE t.sendingUser.username LIKE %?1% " +
            "AND t.receivingUser.username LIKE %?2% AND t.timestamp BETWEEN ?3 and ?4")
    Page<Transaction> findAll(String senderUsername, String recipientUsername,
                              Timestamp fromDateTime, Timestamp tillDateTime, Pageable pageable);

    @Query("SELECT t FROM Transaction t WHERE ((t.sendingUser=?1 AND t.receivingUser.username LIKE %?3%)" +
            "OR (t.sendingUser.username LIKE %?2% AND t.receivingUser=?1)) AND t.timestamp BETWEEN ?4 and ?5 AND t.confirmed=TRUE")
    Page<Transaction> findAllForUser(User user, String senderUsername, String recipientUsername,
                                     Timestamp fromDateTime, Timestamp tillDateTime, Pageable pageable);

}
