package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.WalletType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletTypesRepository extends CrudRepository<WalletType, Integer> {
}
