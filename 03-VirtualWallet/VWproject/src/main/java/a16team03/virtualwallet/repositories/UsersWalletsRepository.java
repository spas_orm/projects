package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.User;
import a16team03.virtualwallet.models.UserWallet;
import a16team03.virtualwallet.models.Wallet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersWalletsRepository extends CrudRepository<UserWallet, Integer> {

    boolean existsByUserUserIdAndWalletId(int userId, int walletId);

    UserWallet getById(int id);

    @Query(value = "SELECT uw.user FROM UserWallet uw WHERE uw.wallet = ?1 ")
    List<User> getUsersForWallet(Wallet wallet);

    @Query(value = "SELECT uw.wallet FROM UserWallet uw WHERE uw.user = ?1 ")
    List<Wallet> getWalletsForUser(User user);

    List<UserWallet> getAllByWalletId(int walletId);

    @Query(value = "SELECT uw FROM UserWallet uw WHERE uw.user.userId = ?1 ")
    List<UserWallet> getAllByUserId(int userId);

    @Query(value = "SELECT uw.wallet FROM UserWallet uw WHERE uw.user.userId = ?1 and uw.createdByUser=true")
    List<Wallet> getWalletsCreatedByUserId(int userId);

    UserWallet getByUserUserIdAndWalletId(int userId, int walletID);

    @Query(value = "SELECT uw FROM UserWallet uw WHERE uw.user.userId = ?1 and uw.defaultWallet=true")
    UserWallet getDefaultUserWalletByUserId(int userId);
}
