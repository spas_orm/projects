package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.CardTransaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Optional;

@Repository
public interface CardTransactionsRepository extends PagingAndSortingRepository<CardTransaction, Integer> {
    Optional<CardTransaction> findFirstByReceivingUserUserIdAndConfirmedFalseOrderByIdDesc(int id);

    @Query("SELECT ct FROM CardTransaction ct WHERE ct.amount between ?1 and ?2 " +
            "AND ct.timestamp BETWEEN ?3 and ?4 AND ct.receivingUser.username like %?5% and ct.confirmed = TRUE")
    Page<CardTransaction> findAll(int amountFrom,
                                  int amountTo,
                                  Timestamp fromDateTime,
                                  Timestamp tillDateTime,
                                  String username,
                                  Pageable pageable);

    @Query("SELECT ct FROM CardTransaction ct WHERE ct.receivingUser.userId = ?1 AND ct.amount between ?2 and ?3 " +
            "AND ct.timestamp BETWEEN ?4 and ?5 AND ct.receivingWallet.name like %?6% and ct.confirmed = TRUE")
    Page<CardTransaction> findAllForUser(int userId,
                                         int amountFrom,
                                         int amountTo,
                                         Timestamp fromDateTime,
                                         Timestamp tillDateTime,
                                         String walletName,
                                         Pageable pageable);
}
