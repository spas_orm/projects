package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.Wallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletsRepository extends CrudRepository<Wallet, Integer> {
}
