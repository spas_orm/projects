package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.Card;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsRepository extends PagingAndSortingRepository<Card, Integer> {

    @Query(value = "SELECT c FROM Card c WHERE c.cardOwner.userId = ?1 ")
    List<Card> getAllByCardOwnerId(int ownerId);

    boolean existsCardByCardNumber(String cardNumber);
}
