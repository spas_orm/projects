package a16team03.virtualwallet.repositories;

import a16team03.virtualwallet.models.Transaction;
import a16team03.virtualwallet.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends PagingAndSortingRepository<User, Integer> {
    User findByUsername(String username);

    boolean existsUserByUsername(String username);

    boolean existsUsersByEmail(String email);

    boolean existsUserByPhone(String phone);

    boolean existsUserByUsernameAndUserIdNot(String username, int userId);

    boolean existsUserByEmailAndUserIdNot(String email, int userId);

    boolean existsUserByPhoneAndUserIdNot(String phone, int userId);

    @Query("SELECT u FROM User u WHERE u.username LIKE %?1% OR u.email LIKE %?1% OR u.phone LIKE %?1%")
    Page<User> findAllUsersByDetail(String userDetail, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.username LIKE %?1% AND u.email LIKE %?2% AND u.phone LIKE %?3%")
    Page<User> findAll(String username, String email, String phone, Pageable pageable);
}
