package a16team03.virtualwallet.exceptions;

public class TransactionAlreadyConfirmedException extends RuntimeException {
    public TransactionAlreadyConfirmedException(String message) {
        super(message);
    }

    public TransactionAlreadyConfirmedException(int id) {
        super(String.format("Transaction with id %d is already confirmed and can not be edited.", id));
    }

}
