package a16team03.virtualwallet.exceptions;

public class PasswordMismatchException extends RuntimeException {
    private static final String PASSWORD_MISMATCH_MESSAGE = "Passwords do not match.";

    public PasswordMismatchException() {
        super(PASSWORD_MISMATCH_MESSAGE);
    }
}
