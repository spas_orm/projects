package a16team03.virtualwallet.exceptions;

public class DuplicateUserWalletException extends DuplicateEntityException {
    public DuplicateUserWalletException() {
    }

    public DuplicateUserWalletException(String message) {
        super(message);
    }

    public DuplicateUserWalletException(String username, int walletId) {
        super(String.format("UserWallet entity with the same %s and Wallet ID %d already exists.", username, walletId));
    }
}
