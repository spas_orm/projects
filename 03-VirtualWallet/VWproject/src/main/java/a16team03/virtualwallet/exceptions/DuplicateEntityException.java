package a16team03.virtualwallet.exceptions;

public class DuplicateEntityException extends RuntimeException {
    public DuplicateEntityException() {
    }
    public DuplicateEntityException(String message) {
        super(message);
    }

    public DuplicateEntityException(String entityType, String attribute) {
        super(String.format("%s with the same %s already exists.", entityType, attribute));
    }
}
