package a16team03.virtualwallet.exceptions.handlers;

import a16team03.virtualwallet.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.io.IOException;

@ControllerAdvice
public class GlobalExceptionHandler {
    //Java Exceptions
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleIllegalArgumentException() {
        return "error-400";
    }

    @ExceptionHandler(AccessDeniedException.class)
    public void handleAccessDeniedException(HttpServletResponse res) throws IOException {
        res.sendError(HttpStatus.FORBIDDEN.value(), "Access denied");
    }

    //Spring Exceptions
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleMethodArgumentTypeMismatchException() {
        return "error-400";
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String handleConstraintViolationException() {
        return "error-400";
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String handleMethodArgumentNotValidException() {
        return "error-400";
    }

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String handleBindException() {
        return "error-400";
    }

    //Custom Exceptions
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private String handleEntityNotFoundException() {
        return "error-404";
    }

    @ExceptionHandler(DefaultEntityNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleDefaultEntityNotFoundException() {
        return "error-400";
    }

    @ExceptionHandler(OperationNotAllowedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleOperationNotAllowedException() {
        return "error-400";
    }

    @ExceptionHandler(TransactionAlreadyConfirmedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleTransactionAlreadyConfirmedException() {
        return "error-400";
    }

    @ExceptionHandler(UserNotLoggedInException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    private String handleUserNotLoggedInException() {
        return "error-401";
    }

    @ExceptionHandler(CardTransactionAlreadyConfirmedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    private String handleCardTransactionAlreadyConfirmedException() {
        return "error-403";
    }

    @ExceptionHandler(AccessForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    private String handleAccessForbiddenException() {
        return "error-403";
    }

    @ExceptionHandler(CustomException.class)
    public void handleCustomException(HttpServletResponse res, CustomException ex) throws IOException {
        res.sendError(ex.getHttpStatus().value(), ex.getMessage());
    }
}
