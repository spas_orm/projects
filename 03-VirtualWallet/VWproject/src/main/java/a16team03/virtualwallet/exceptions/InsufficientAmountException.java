package a16team03.virtualwallet.exceptions;

public class InsufficientAmountException extends RuntimeException {
    public InsufficientAmountException() {
        super();
    }

    public InsufficientAmountException(String message) {
        super(message);
    }

}
