package a16team03.virtualwallet.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String objType, int id) {
        super(String.format("%s entity with id %d does not exist.", objType, id));
    }

    public EntityNotFoundException(String objType, String username) {
        super(String.format("%s entity with username %s does not exist.", objType, username));
    }

    public EntityNotFoundException(String objType, int userID, int walletId) {
        super(String.format("%s entity with user ID %d and wallet ID %d does not exist.", objType, userID, walletId));
    }
}
