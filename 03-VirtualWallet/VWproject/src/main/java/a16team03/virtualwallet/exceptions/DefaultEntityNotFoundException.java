package a16team03.virtualwallet.exceptions;

public class DefaultEntityNotFoundException extends RuntimeException {
    public DefaultEntityNotFoundException() {
    }

    public DefaultEntityNotFoundException(String message) {
        super(message);
    }

    public DefaultEntityNotFoundException(int userId, String objType) {
        super(String.format("User with id %d does not have default %s.", userId, objType));
    }

}
