package a16team03.virtualwallet.exceptions;

public class OperationNotAllowedException extends RuntimeException {
    public OperationNotAllowedException() {
    }

    public OperationNotAllowedException(String message) {
        super(message);
    }

    public OperationNotAllowedException(int walletId) {
        super(String.format("You are not allowed to perform the requested operation on wallet with ID %d", walletId));
    }

    public OperationNotAllowedException(String entityType, int walletId) {
        super(String.format("You are not allowed to perform the requested operation on %s with ID %d",entityType, walletId));
    }
}
