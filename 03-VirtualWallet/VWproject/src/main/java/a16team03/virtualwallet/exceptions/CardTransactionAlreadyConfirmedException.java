package a16team03.virtualwallet.exceptions;

public class CardTransactionAlreadyConfirmedException extends RuntimeException {
    public CardTransactionAlreadyConfirmedException() {
    }

    public CardTransactionAlreadyConfirmedException(String message) {
        super(message);
    }
}
