package com.team03.beertag;

import com.team03.beertag.models.*;

public class Factory {
    public static int MISSING_ID = 111;
    public static String WRONG_NAME = "WrongName";

    public static Beer createBeer() {
        Beer beer = new Beer();
        beer.setName("Zagorka");
        beer.setDescription("Sample description");
        beer.setAbv(4.5);
        beer.setBeerStyle(new BeerStyle());
        beer.setBrewery(new Brewery());
        beer.setCountry(new Country());
        beer.setImageURL("test_picture_url");
        beer.setCreatedBy(new User());

        return beer;
    }

    public static BeerDTO createBeerDto() {
        BeerDTO beerDTO = new BeerDTO();
        beerDTO.setName("Zagorka");
        beerDTO.setDescription("Sample description");
        beerDTO.setAbv(4.5);
        beerDTO.setBeerStyleId(1);
        beerDTO.setBreweryId(1);
        beerDTO.setCountryId(1);
        beerDTO.setImageUrl("test_picture_url");
        beerDTO.setCreatedById(1);

        return beerDTO;
    }

    public static Country createCountry() {
        return new Country("MyLand");
    }

    public static BeerStyle createStyle() {
        return new BeerStyle("MyStyle");
    }

    public static User createUser() {
        return new User("MyUser");
    }

    public static Tag createTag() {
        return new Tag("MyTag");
    }

    public static Brewery createBrewery() {
        return new Brewery("MyBrewery");
    }

    public static UserBeerRating createUBR() {
        return new UserBeerRating();
    }

    public static Status createStatus() {
        return new Status();
    }

    public static UserBeerStatus createUBS() {
        return new UserBeerStatus();
    }

    public static UserBeerStatusDTO createUbsDto() {
        return new UserBeerStatusDTO();
    }
}
