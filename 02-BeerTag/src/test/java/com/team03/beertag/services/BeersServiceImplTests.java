package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.BeerDTO;
import com.team03.beertag.models.BeerMapper;
import com.team03.beertag.models.BeerSearch;
import com.team03.beertag.repositories.BeersRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.team03.beertag.Factory.createBeer;
import static com.team03.beertag.Factory.createBeerDto;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BeersServiceImplTests {
    @Mock
    BeerMapper beerMapper;

    @Mock
    BeersRepository mockRepository;

    @InjectMocks
    BeersServiceImpl mockService;

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_ShouldThrow_IfBeerWithSameNameAlreadyExists() {
        //Arrange
        BeerDTO beerDTO = createBeerDto();

        Mockito.when(mockRepository.checkIfBeerWithNameExists(anyString()))
                .thenReturn(true);
        //Act
        mockService.create(beerDTO);
    }

    @Test
    public void create_Should_ReturnCorrectBeer() {
        //Arrange
        BeerDTO beerDTO = createBeerDto();
        Beer expectedBeer = createBeer();

        Mockito.when(mockRepository.create(expectedBeer))
                .thenReturn(expectedBeer);
        Mockito.when(beerMapper.dtoToBeer(beerDTO))
                .thenReturn(expectedBeer);
        //Act
        Beer receivedBeer = mockService.create(beerDTO);
        //Assert
        Assert.assertSame(expectedBeer, receivedBeer);
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoBeersExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<Beer> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

    @Test
    public void getAllWithBeerSearch_ShouldReturnEmptyList_WhenNoBeersExist() {
        //Arrange
        BeerSearch mockBeerSearch = new BeerSearch();
        Mockito.when(mockRepository.getAll(mockBeerSearch))
                .thenReturn(Collections.emptyList());
        //Act
        List<Beer> returnedList = mockService.getAll(mockBeerSearch);
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

    @Test
    public void getAll_ShouldReturnListOfBeers() {
        //Arrange
        List<Beer> expectedList = new ArrayList<>();
        expectedList.add(createBeer());

        Mockito.when(mockRepository.getAll())
                .thenReturn(expectedList);
        //Act
        List<Beer> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(expectedList, returnedList);
    }

    @Test
    public void getById_ShouldReturnCorrectBeer() {
        //Arrange
        Beer expectedBeer = createBeer();

        Mockito.when(mockRepository.getById(anyInt()))
                .thenReturn(expectedBeer);
        //Act
        Beer receivedBeer = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(expectedBeer, receivedBeer);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_ShouldThrow_IfBeerDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getById(anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act
        Beer receivedBeer = mockService.getById(anyInt());
    }

    @Test
    public void updateById_ShouldUpdateBeer_WhenPassedValidId() {
        //Arrange, Act
        Beer beerToUpdate = createBeer();
        mockService.updateById(beerToUpdate.getId(), beerToUpdate);
        //Assert
        Mockito.verify(mockRepository, times(1)).updateById(beerToUpdate.getId(), beerToUpdate);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void updateById_ShouldThrow_IfBeerDoesNotExist() {
        //Arrange
        Beer beerToUpdate = createBeer();
        Mockito.when(mockRepository.updateById(beerToUpdate.getId(), beerToUpdate))
                .thenThrow(new EntityNotFoundException());
        //Act
        mockService.updateById(beerToUpdate.getId(), beerToUpdate);
    }

    @Test
    public void deleteById_ShouldDeleteBeer_WhenPassedValidId() {
        //Arrange, Act
        mockService.deleteById(anyInt());
        //Assert
        Mockito.verify(mockRepository, times(1)).deleteById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void deleteById_ShouldThrow_IfBeerDoesNotExist() {
        //Arrange
        Mockito.doThrow(new EntityNotFoundException()).when(mockRepository).deleteById(anyInt());
        //Act
        mockService.deleteById(anyInt());
    }
}
