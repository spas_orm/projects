package com.team03.beertag.services;


import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.BeerStyle;
import com.team03.beertag.repositories.BeerStylesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.team03.beertag.Factory.MISSING_ID;
import static com.team03.beertag.Factory.createStyle;

@RunWith(MockitoJUnitRunner.class)
public class BeerStylesServiceImplTests {

    @Mock
    BeerStylesRepository repositoryMock;

    @InjectMocks
    BeerStylesServiceImpl service;

    @Test
    public void create_Should_ReturnStyle_WhenSuccessful() {
        //Arrange
        BeerStyle expectedStyle = createStyle();
        Mockito.when(repositoryMock.create(expectedStyle))
                .thenReturn(expectedStyle);
        //Act
        BeerStyle resultStyle = service.create(expectedStyle);

        //Assert
        Assert.assertEquals(expectedStyle, resultStyle);
    }

    @Test(expected = DuplicateEntityException.class)
    public void crete_Should_Throw_WhenStyleExists() {
        //Arrange
        BeerStyle duplicateStyle = createStyle();
        Mockito.when(repositoryMock.checkExistByName(duplicateStyle.getName()))
                .thenReturn(true);
        //Act,Assert
        service.create(duplicateStyle);
    }

    @Test
    public void getAll_Should_ReturnCorrectEmptyList_WhenNoStylesExist() {
        //Arrange
        List<BeerStyle> stylesList = new ArrayList<>();
        Mockito.when(repositoryMock.getAll())
                .thenReturn(stylesList);

        //Act
        List<BeerStyle> styleListResult = service.getAll();

        //Assert
        Assert.assertTrue(styleListResult.isEmpty());
    }

    @Test
    public void getAll_Should_ReturnCorrectList_WhenStyleExist() {
        //Arrange
        List<BeerStyle> stylesList = new ArrayList<>();
        stylesList.add(createStyle());
        Mockito.when(repositoryMock.getAll())
                .thenReturn(stylesList);

        //Act
        List<BeerStyle> stylesListResult = repositoryMock.getAll();

        //Assert
        Assert.assertFalse(stylesListResult.isEmpty());
    }


    @Test
    public void getById_Should_ReturnStyle_WhenIdExist() {
        //Arrange
        BeerStyle expectedStyle = createStyle();
        Mockito.when(repositoryMock.getById(expectedStyle.getId()))
                .thenReturn(expectedStyle);

        //Act
        BeerStyle resultStyle = service.getById(expectedStyle.getId());

        //Assert
        Assert.assertEquals(expectedStyle, resultStyle);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        repositoryMock.create(createStyle());
        Mockito.when(repositoryMock.getById(MISSING_ID))
                .thenThrow(new EntityNotFoundException());

        //Act and Assert
        service.getById(MISSING_ID);
    }

    @Test
    public void update_Should_ReturnStyle_WhenIdExist() {
        //Arrange
        BeerStyle expectedStyle = createStyle();
        Mockito.when(repositoryMock.update(expectedStyle.getId(), expectedStyle))
                .thenReturn(expectedStyle);

        //Act
        BeerStyle resultStyle = service.update(expectedStyle.getId(), expectedStyle);

        //Assert
        Assert.assertEquals(expectedStyle, resultStyle);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        BeerStyle expectedStyle = createStyle();
        Mockito.when(repositoryMock.update(MISSING_ID, expectedStyle))
                .thenThrow(new EntityNotFoundException());

        //Act and Assert
        service.update(MISSING_ID, expectedStyle);
    }

    @Test
    public void delete_Should_Call_Repository() {
        //Arrange
        Mockito.doNothing().when(repositoryMock).delete(createStyle().getId());

        //Act
        service.delete(createStyle().getId());

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1))
                .delete(createStyle().getId());

    }

    @Test(expected = EntityNotFoundException.class)
    public void delete_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        Mockito.doThrow(new EntityNotFoundException())
                .when(repositoryMock).delete(MISSING_ID);

        //Act and Assert
        service.delete(MISSING_ID);
    }
}
