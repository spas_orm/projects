package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Country;
import com.team03.beertag.repositories.CountriesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.team03.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CountriesServiceImplTest {

    @Mock
    CountriesRepository countriesRepository;

    @InjectMocks
    CountriesServiceImpl countriesService;

    @Test
    public void create_Should_ReturnCountry_WhenSuccessful() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.when(countriesRepository.create(expectedCountry))
                .thenReturn(expectedCountry);
        //Act
        Country resultCountry = countriesService.create(expectedCountry);

        //Assert
        Assert.assertEquals(expectedCountry, resultCountry);
    }

    @Test(expected = DuplicateEntityException.class)
    public void crete_Should_Throw_WhenCountryExists() {
        //Arrange
        Country duplicateCountry = createCountry();
        Mockito.when(countriesRepository.checkExistByName(duplicateCountry.getName()))
                .thenReturn(true);
        //Act,Assert
        countriesService.create(duplicateCountry);
    }

    @Test
    public void getAll_Should_ReturnCorrectEmptyList_WhenNoCountriesExist(){
        //Arrange
        List<Country> countriesList = new ArrayList<>();
        Mockito.when(countriesRepository.getAll())
                .thenReturn(countriesList);

        //Act
        List<Country> countriesListResult = countriesService.getAll();

        //Assert
        Assert.assertTrue(countriesListResult.isEmpty());
    }

    @Test
    public void getAll_Should_ReturnCorrectEmptyList_WhenCountriesExist(){
        //Arrange
        List<Country> countriesList = new ArrayList<>();
        countriesList.add(createCountry());
        Mockito.when(countriesRepository.getAll())
        .thenReturn(countriesList);

        //Act
        List<Country> countriesListResult = countriesService.getAll();

        //Assert
        Assert.assertFalse(countriesListResult.isEmpty());
    }


    @Test
    public void getById_Should_ReturnCountry_WhenIdExist() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.when(countriesRepository.getById(expectedCountry.getId()))
                .thenReturn(expectedCountry);

        //Act
        Country resultCountry = countriesService.getById(expectedCountry.getId());

        //Assert
        Assert.assertEquals(expectedCountry, resultCountry);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        countriesRepository.create(createCountry());
        Mockito.when(countriesRepository.getById(MISSING_ID))
                .thenThrow(new EntityNotFoundException());

        //Act and Assert
        countriesService.getById(MISSING_ID);
    }

    @Test
    public void update_Should_ReturnCountry_WhenIdExist() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.when(countriesRepository.update(expectedCountry.getId(),expectedCountry))
                .thenReturn(expectedCountry);

        //Act
        Country resultCountry = countriesService.update(expectedCountry.getId(),expectedCountry);

        //Assert
        Assert.assertEquals(expectedCountry, resultCountry);
    }

    @Test(expected = EntityNotFoundException.class)
    public void update_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.when(countriesRepository.update(MISSING_ID,expectedCountry))
                .thenThrow(new EntityNotFoundException());

        //Act and Assert
        countriesService.update(MISSING_ID,expectedCountry);
    }

    @Test
    public void delete_Should_Call_Repository() {
        //Arrange
        List<Country> countriesList = new ArrayList<>();
        countriesList.add(createCountry());
        Mockito.doNothing().when(countriesRepository).delete(createCountry().getId());

        //Act
        countriesService.delete(createCountry().getId());

        //Assert
        Mockito.verify(countriesRepository,Mockito.times(1)).delete(createCountry().getId());

    }

    @Test(expected = EntityNotFoundException.class)
    public void delete_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        Country expectedCountry = createCountry();
        Mockito.doThrow(new EntityNotFoundException())
                .when(countriesRepository).delete(MISSING_ID);

        //Act and Assert
        countriesService.delete(MISSING_ID);
    }





//    @Test
//    public void getByIdShould_Throw_WhenIdDoesNotExistsJ5(){
//        //Arrange
//        Mockito.when(countriesRepository.getById(FAKE_ID))
//                .thenThrow(new EntityNotFoundException());
//        //Act,Assert
//        Assertions.assertThrows(EntityNotFoundException.class,
//                ()->countriesService.getById(FAKE_ID));

//

//    }

//    @Test
//    public void creteShould_Throw_WhenCountryExistsJ5(){
//        //Arrange
//        Country expectedCountry = createCountry();
//        Mockito.when(countriesRepository.checkExistByName(createCountry().getName()))
//                .thenReturn(true);
//        //Act,Assert
//        Assertions.assertThrows(DuplicateEntityException.class,
//                ()->countriesService.create(expectedCountry));
//
//    }


}
