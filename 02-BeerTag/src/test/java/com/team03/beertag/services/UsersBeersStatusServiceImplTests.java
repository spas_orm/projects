package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateRelationException;
import com.team03.beertag.exceptions.RelationNotFoundException;
import com.team03.beertag.models.UserBeerStatus;
import com.team03.beertag.models.UserBeerStatusDTO;
import com.team03.beertag.models.UserBeerStatusMapper;
import com.team03.beertag.repositories.UsersBeersStatusRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.team03.beertag.Factory.createUBS;
import static com.team03.beertag.Factory.createUbsDto;

@RunWith(MockitoJUnitRunner.class)
public class UsersBeersStatusServiceImplTests {
    private static final int STATUS_ID_WISHED = 1;
    private static final int STATUS_ID_DRANK = 2;

    @Mock
    StatusService statusService;

    @Mock
    UserBeerStatusMapper ubsMapper;

    @Mock
    UsersBeersStatusRepository mockRepository;

    @InjectMocks
    UsersBeersStatusServiceImpl mockService;

    @Test(expected = DuplicateRelationException.class) //Assert
    public void createUserBeerStatus_ShouldThrow_WhenUBSAlreadyExists() {
        //Act
        UserBeerStatus mockUBS = createUBS();
        UserBeerStatusDTO mockUbsDto = createUbsDto();

        Mockito.when(ubsMapper.dtoToUserBeerStatus(mockUbsDto))
                .thenReturn(mockUBS);
        Mockito.when(mockService.checkUserBeerStatusExist(mockUBS))
                .thenThrow(new DuplicateRelationException());
        //Arrange
        mockService.createUserBeerStatus(mockUbsDto);
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoUBSExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<UserBeerStatus> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

//    public void createUserBeerStatus_ShouldCreateUBS_WhenStatusIsDrank() {
//        //Act
//        UserBeerStatus mockUBS = createUBS();
//        UserBeerStatusDTO mockUbsDto = createUbsDto();
//
//        Mockito.when(ubsMapper.dtoToUserBeerStatus(mockUbsDto))
//                .thenReturn(mockUBS);
//        Mockito.when(statusService.getById(STATUS_ID_WISHED))
//                .thenReturn(new Status("Wished"));
//        Mockito.when(statusService.getById(STATUS_ID_DRANK))
//                .thenReturn(new Status("Drank"));
//        mockUBS.setStatus(statusService.getById(STATUS_ID_DRANK));
//        Mockito.when(mockService.checkUserBeerStatusExist(mockUBS))
//                .thenReturn(false);
//        Mockito.doNothing()
//                .when(mockRepository).deleteUserBeerStatus(mockUBS);
//        Mockito.when(mockRepository.createUserBeerStatus(mockUBS))
//                .thenReturn(mockUBS);
//        //Arrange
//        mockService.createUserBeerStatus(mockUbsDto);
//
//        UserBeerStatus expectedUBS = mockRepository.createUserBeerStatus(mockUBS);
//        //Assert
//        Assert.assertEquals(mockUBS, expectedUBS);
//    }

    @Test(expected = RelationNotFoundException.class) //Assert
    public void updateUserBeerStatus_ShouldThrow_WhenUBS_DoesNotExist() {
        //Act
        UserBeerStatus mockUBS = createUBS();
        UserBeerStatusDTO mockUbsDto = createUbsDto();

        Mockito.when(ubsMapper.dtoToUserBeerStatus(mockUbsDto))
                .thenReturn(mockUBS);
        Mockito.when(mockService.checkUserBeerStatusExist(mockUBS))
                .thenThrow(new RelationNotFoundException());
        //Arrange
        mockService.updateUserBeerStatus(mockUbsDto);
    }
}
