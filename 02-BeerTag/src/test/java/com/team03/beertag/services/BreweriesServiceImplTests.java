package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Brewery;
import com.team03.beertag.repositories.BreweriesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.team03.beertag.Factory.createBrewery;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BreweriesServiceImplTests {
    @Mock
    BreweriesRepository mockRepository;

    @InjectMocks
    BreweriesServiceImpl mockService;

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_ShouldThrow_IfBreweryWithSameNameAlreadyExists() {
        //Arrange
        Brewery brewery = createBrewery();

        Mockito.when(mockRepository.checkIfBreweryWithNameExists(anyString()))
                .thenReturn(true);
        //Act
        mockService.create(brewery);
    }

    @Test
    public void create_Should_ReturnCorrectBrewery() {
        //Arrange
        Brewery expectedBrewery = createBrewery();

        Mockito.when(mockRepository.create(expectedBrewery))
                .thenReturn(expectedBrewery);
        //Act
        Brewery receivedBrewery = mockService.create(expectedBrewery);
        //Assert
        Assert.assertSame(expectedBrewery, receivedBrewery);
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoBreweriesExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<Brewery> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

    @Test
    public void getAll_ShouldReturnListOfBreweries() {
        //Arrange
        List<Brewery> expectedList = new ArrayList<>();
        expectedList.add(createBrewery());

        Mockito.when(mockRepository.getAll())
                .thenReturn(expectedList);
        //Act
        List<Brewery> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(expectedList, returnedList);
    }

    @Test
    public void getById_ShouldReturnCorrectBrewery() {
        //Arrange
        Brewery expectedBrewery = createBrewery();

        Mockito.when(mockRepository.getById(anyInt()))
                .thenReturn(expectedBrewery);
        //Act
        Brewery receivedBrewery = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(expectedBrewery, receivedBrewery);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_ShouldThrow_IfBreweryDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getById(anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act
        Brewery receivedBrewery = mockService.getById(anyInt());
    }

    @Test
    public void updateById_ShouldUpdateBrewery_WhenPassedValidId() {
        //Arrange, Act
        Brewery breweryToUpdate = createBrewery();

        mockService.updateById(breweryToUpdate.getId(), breweryToUpdate);
        //Assert
        Mockito.verify(mockRepository, times(1)).updateById(breweryToUpdate.getId(), breweryToUpdate);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void updateById_ShouldThrow_IfBreweryDoesNotExist() {
        //Arrange
        Brewery breweryToUpdate = createBrewery();

        Mockito.when(mockRepository.updateById(breweryToUpdate.getId(), breweryToUpdate))
                .thenThrow(new EntityNotFoundException());
        //Act
        mockService.updateById(breweryToUpdate.getId(), breweryToUpdate);
    }

    @Test
    public void deleteById_ShouldDeleteBrewery_WhenPassedValidId() {
        //Arrange, Act
        mockService.deleteById(anyInt());
        //Assert
        Mockito.verify(mockRepository, times(1)).deleteById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void deleteById_ShouldThrow_IfBreweryDoesNotExist() {
        //Arrange
        Mockito.doThrow(new EntityNotFoundException()).when(mockRepository).deleteById(anyInt());
        //Act
        mockService.deleteById(anyInt());
    }
}
