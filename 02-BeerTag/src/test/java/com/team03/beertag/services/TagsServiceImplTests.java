package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Tag;
import com.team03.beertag.repositories.TagsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.team03.beertag.Factory.createTag;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceImplTests {
    @Mock
    TagsRepository mockRepository;

    @InjectMocks
    TagsServiceImpl mockService;

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_ShouldThrow_IfTagWithSameNameAlreadyExists() {
        //Arrange
        Tag tag = createTag();

        Mockito.when(mockRepository.checkIfTagWithNameExists(anyString()))
                .thenReturn(true);
        //Act
        mockService.create(tag);
    }

    @Test
    public void create_Should_ReturnCorrectTag() {
        //Arrange
        Tag expectedTag = createTag();

        Mockito.when(mockRepository.create(expectedTag))
                .thenReturn(expectedTag);
        //Act
        Tag receivedTag = mockService.create(expectedTag);
        //Assert
        Assert.assertSame(expectedTag, receivedTag);
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoTagsExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<Tag> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

    @Test
    public void getAll_ShouldReturnListOfTags() {
        //Arrange
        List<Tag> expectedList = new ArrayList<>();
        expectedList.add(createTag());

        Mockito.when(mockRepository.getAll())
                .thenReturn(expectedList);
        //Act
        List<Tag> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(expectedList, returnedList);
    }

    @Test
    public void getById_ShouldReturnCorrectTag() {
        //Arrange
        Tag expectedTag = createTag();

        Mockito.when(mockRepository.getById(anyInt()))
                .thenReturn(expectedTag);
        //Act
        Tag receivedTag = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(expectedTag, receivedTag);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getById_ShouldThrow_IfTagDoesNotExist() {
        //Arrange
        Mockito.when(mockRepository.getById(anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act
        Tag receivedTag = mockService.getById(anyInt());
    }

    @Test
    public void updateById_ShouldUpdateTag_WhenPassedValidId() {
        //Arrange, Act
        Tag tagToUpdate = createTag();

        mockService.updateById(tagToUpdate.getId(), tagToUpdate);
        //Assert
        Mockito.verify(mockRepository, times(1)).updateById(tagToUpdate.getId(), tagToUpdate);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void updateById_ShouldThrow_IfTagDoesNotExist() {
        //Arrange
        Tag tagToUpdate = createTag();

        Mockito.when(mockRepository.updateById(tagToUpdate.getId(), tagToUpdate))
                .thenThrow(new EntityNotFoundException());
        //Act
        mockService.updateById(tagToUpdate.getId(), tagToUpdate);
    }

    @Test
    public void deleteById_ShouldDeleteTag_WhenPassedValidId() {
        //Arrange, Act
        mockService.deleteById(anyInt());
        //Assert
        Mockito.verify(mockRepository, times(1)).deleteById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void deleteById_ShouldThrow_IfTagDoesNotExist() {
        //Arrange
        Mockito.doThrow(new EntityNotFoundException()).when(mockRepository).deleteById(anyInt());
        //Act
        mockService.deleteById(anyInt());
    }
}
