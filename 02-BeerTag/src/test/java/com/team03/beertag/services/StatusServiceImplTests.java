package com.team03.beertag.services;

import com.team03.beertag.models.Status;
import com.team03.beertag.repositories.StatusesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static com.team03.beertag.Factory.createStatus;

@RunWith(MockitoJUnitRunner.class)
public class StatusServiceImplTests {
    @Mock
    StatusesRepository mockRepository;

    @InjectMocks
    StatusServiceImpl mockService;

    @Test
    public void getAll_Should_ReturnEmptyList_WhenNoStatusesExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<Status> receivedStatusList = mockService.getAll();

        //Assert
        Assert.assertTrue(receivedStatusList.isEmpty());
    }

    @Test
    public void getById_Should_ReturnStatus_WhenStatusWithIdExist() {
        //Arrange
        Status expectedStatus = createStatus();
        Mockito.when(mockRepository.getById(expectedStatus.getId()))
                .thenReturn(expectedStatus);

        //Act
        Status resultStatus = mockService.getById(expectedStatus.getId());

        //Assert
        Assert.assertEquals(expectedStatus, resultStatus);
    }
}
