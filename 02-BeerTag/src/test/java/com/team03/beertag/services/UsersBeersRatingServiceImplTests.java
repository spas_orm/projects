package com.team03.beertag.services;

import com.team03.beertag.exceptions.RelationNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerRating;
import com.team03.beertag.repositories.UsersBeersRatingRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.team03.beertag.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class UsersBeersRatingServiceImplTests {
    @Mock
    UsersBeersRatingRepository mockRepository;

    @InjectMocks
    UsersBeersRatingServiceImpl mockService;

    @Test
    public void createUserBeerStatus_ShouldThrow_WhenUBRAlreadyExists() {
        //Act
        UserBeerRating expectedUBR = createUBR();

        Mockito.when(mockService.createUserBeerRating(expectedUBR))
                .thenReturn(expectedUBR);
        //Arrange
        UserBeerRating receivedUBR = mockRepository.createUserBeerRating(expectedUBR);
        //Assert
        Assert.assertEquals(expectedUBR, receivedUBR);
    }

    @Test
    public void getAll_ShouldReturnEmptyList_WhenNoUBRExist() {
        //Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(Collections.emptyList());
        //Act
        List<UserBeerRating> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

    @Test
    public void getAll_ShouldReturnListOfUBRs() {
        //Arrange
        List<UserBeerRating> expectedList = new ArrayList<>();
        expectedList.add(createUBR());

        Mockito.when(mockRepository.getAll())
                .thenReturn(expectedList);
        //Act
        List<UserBeerRating> returnedList = mockService.getAll();
        //Assert
        Assert.assertSame(expectedList, returnedList);
    }

    @Test
    public void updateUserBeerRating_Should_Return_CorrectUBR() {
        //Arrange
        UserBeerRating expectedUBR = createUBR();
        Mockito.when(mockRepository.updateUserBeerRating(expectedUBR))
                .thenReturn(expectedUBR);

        //Act
        UserBeerRating returnedUBR = mockService.updateUserBeerRating(expectedUBR);

        //Assert
        Assert.assertEquals(expectedUBR, returnedUBR);
    }

    @Test
    public void checkUserBeerRatingExist_Should_ReturnTrue_WhenFoundUBR() {
        //Arrange
        UserBeerRating expectedUBR = createUBR();
        Mockito.when(mockRepository.checkUserBeerRatingExist(expectedUBR))
                .thenReturn(true);

        //Act
        boolean expectedResult = mockService.checkUserBeerRatingExist(expectedUBR);

        //Assert
        Assert.assertTrue(expectedResult);
    }

    @Test
    public void getRatesByUser_ShouldReturnEmptyList_WhenNoUBRsExistForThatUser() {
        //Arrange
        User mockUser = createUser();
        Mockito.when(mockRepository.getRatesByUser(mockUser))
                .thenReturn(Collections.emptyList());
        //Act
        List<UserBeerRating> returnedList = mockService.getRatesByUser(mockUser);
        //Assert
        Assert.assertSame(Collections.emptyList(), returnedList);
    }

    @Test
    public void getRatesByUser_ShouldReturnListOfUBRs_WhenUBRsExistForThatUser() {
        //Arrange
        User mockUser = createUser();
        List<UserBeerRating> expectedList = new ArrayList<>();
        expectedList.add(createUBR());

        Mockito.when(mockRepository.getRatesByUser(mockUser))
                .thenReturn(expectedList);
        //Act
        List<UserBeerRating> returnedList = mockService.getRatesByUser(mockUser);
        //Assert
        Assert.assertSame(expectedList, returnedList);
    }

    @Test(expected = RelationNotFoundException.class) //Assert
    public void getRatingByUserAndBeer_ShouldThrow_WhenNoUBRFound() {
        //Arrange
        User mockUser = createUser();
        Beer mockBeer = createBeer();
        Mockito.when(mockRepository.getRatingByUserAndBeer(mockUser, mockBeer))
                .thenThrow(new RelationNotFoundException());
        //Act
        int rating = mockService.getRatingByUserAndBeer(mockUser, mockBeer);
    }

    @Test
    public void getRatingByUserAndBeer_ShouldReturnRating() {
        //Arrange
        User mockUser = createUser();
        Beer mockBeer = createBeer();
        int expectedRating = 5;

        Mockito.when(mockRepository.getRatingByUserAndBeer(mockUser, mockBeer))
                .thenReturn(expectedRating);
        //Act
        int returnedRating = mockService.getRatingByUserAndBeer(mockUser, mockBeer);
        //Assert
        Assert.assertSame(expectedRating, returnedRating);
    }

    @Test
    public void getBeerAverageRating_ShouldReturn_Zero() {
        //Arrange
        Beer mockBeer = createBeer();
        Mockito.when(mockRepository.getBeerAverageRating(mockBeer))
                .thenReturn(0.0);
        //Act
        Assert.assertEquals(0.0, mockService.getBeerAverageRating(mockBeer), 0.1);
    }

    @Test
    public void getBeerAverageRating_ShouldReturnRating() {
        //Arrange
        Beer mockBeer = createBeer();
        double expectedRating = 5;

        Mockito.when(mockRepository.getBeerAverageRating(mockBeer))
                .thenReturn(expectedRating);
        //Act
        double returnedRating = mockService.getBeerAverageRating(mockBeer);
        //Assert
        Assert.assertEquals(expectedRating, returnedRating, 0.1);
    }
}
