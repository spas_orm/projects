package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.Status;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerStatusDTO;
import com.team03.beertag.repositories.UsersBeersStatusRepository;
import com.team03.beertag.repositories.UsersRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

import static com.team03.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {
    @Mock
    UsersBeersStatusService ubsService;

    @Mock
    UsersBeersStatusRepository ubsRepository;

    @Mock
    StatusService statusService;

    @Mock
    UsersRepository repositoryMock;

    @InjectMocks
    UsersServiceImpl usersService;

    @Test
    public void create_Should_ReturnUser_WhenSuccessful() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(repositoryMock.create(expectedUser))
                .thenReturn(expectedUser);
        //Act
        User resultUser = usersService.create(expectedUser);

        //Assert
        Assert.assertEquals(expectedUser, resultUser);
    }

    @Test(expected = DuplicateEntityException.class)
    public void create_Should_Throw_WhenUserExists() {
        //Arrange
        User duplicateUser = createUser();
        Mockito.when(repositoryMock
                .checkExistByUserName(duplicateUser.getUserName()))
                .thenReturn(true);
        //Act,Assert
        usersService.create(duplicateUser);
    }

    @Test
    public void getAll_Should_ReturnEmptyList_WhenNoUsersExist() {
        //Arrange
        List<User> usersList = new ArrayList<>();
        Mockito.when(repositoryMock.getAll())
                .thenReturn(usersList);

        //Act
        List<User> usersListResult = usersService.getAll();

        //Assert
        Assert.assertTrue(usersListResult.isEmpty());
    }

    @Test
    public void getAll_Should_ReturnCorrectList_WhenNoUsersExist() {
        //Arrange
        List<User> stylesList = new ArrayList<>();
        stylesList.add(createUser());
        Mockito.when(repositoryMock.getAll())
                .thenReturn(stylesList);

        //Act
        List<User> userListResult = repositoryMock.getAll();

        //Assert
        Assert.assertFalse(userListResult.isEmpty());
    }

    @Test
    public void getById_Should_ReturnUser_WhenIdExist() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(repositoryMock.getById(expectedUser.getId()))
                .thenReturn(expectedUser);

        //Act
        User resultUser = usersService.getById(expectedUser.getId());

        //Assert
        Assert.assertEquals(expectedUser, resultUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        repositoryMock.create(createUser());
        Mockito.when(repositoryMock.getById(MISSING_ID))
                .thenThrow(new EntityNotFoundException());

        //Act and Assert
        usersService.getById(MISSING_ID);
    }

    @Test
    public void updateById_Should_ReturnUser_WhenIdExist() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(repositoryMock
                .updateById(expectedUser.getId(), expectedUser))
                .thenReturn(expectedUser);

        //Act
        User resultUser = usersService
                .updateById(expectedUser.getId(), expectedUser);

        //Assert
        Assert.assertEquals(expectedUser, resultUser);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateById_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(repositoryMock.updateById(MISSING_ID, expectedUser))
                .thenThrow(new EntityNotFoundException());

        //Act and Assert
        usersService.updateById(MISSING_ID, expectedUser);
    }

    @Test
    public void deleteById_Should_Call_Repository() {
        //Arrange
        Mockito.doNothing().when(repositoryMock).deleteById(createUser().getId());

        //Act
        usersService.deleteById(createUser().getId());

        //Assert
        Mockito.verify(repositoryMock, Mockito.times(1))
                .deleteById(createUser().getId());

    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteById_Should_Throw_WhenIdDoesNotExists() {
        //Arrange
        Mockito.doThrow(new EntityNotFoundException())
                .when(repositoryMock).deleteById(MISSING_ID);

        //Act and Assert
        usersService.deleteById(MISSING_ID);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getWishListByUserId_ShouldThrow_WhenUserDoesNotExist() {
        //Arrange
        User mockUser = createUser();
        mockUser.setId(anyInt());

        Mockito.when(repositoryMock.getById(mockUser.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act
        usersService.getWishListByUserId(mockUser.getId());
    }

    public void getWishListByUserId_ShouldReturnList_WhenUserExists() {
        //Arrange
        User mockUser = createUser();
        mockUser.setId(anyInt());
        List<Beer> expectedList = new ArrayList<>();
        Beer beer = createBeer();
        expectedList.add(beer);
        Status mockStatus = createStatus();

        Mockito.when(statusService.getById(anyInt()))
                .thenReturn(mockStatus);
        Mockito.when(ubsRepository.getBeerListByUserAndStatus(mockUser, mockStatus))
                .thenReturn(expectedList);
        //Act
        List<Beer> receivedList = usersService.getWishListByUserId(mockUser.getId());

        //Assert
        Assert.assertEquals(expectedList, receivedList);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getDrankListByUserId_ShouldThrow_WhenUserDoesNotExist() {
        //Arrange
        User mockUser = createUser();
        mockUser.setId(anyInt());

        Mockito.when(repositoryMock.getById(mockUser.getId()))
                .thenThrow(new EntityNotFoundException());

        //Act
        usersService.getDrankListByUserId(mockUser.getId());
    }

    public void getDrankListByUserId_ShouldReturnList_WhenUserExists() {
        //Arrange
        User mockUser = createUser();
        mockUser.setId(anyInt());
        List<Beer> expectedList = new ArrayList<>();
        Beer beer = createBeer();
        expectedList.add(beer);
        Status mockStatus = createStatus();

        Mockito.when(statusService.getById(anyInt()))
                .thenReturn(mockStatus);
        Mockito.when(ubsRepository.getBeerListByUserAndStatus(mockUser, mockStatus))
                .thenReturn(expectedList);
        //Act
        List<Beer> receivedList = usersService.getDrankListByUserId(mockUser.getId());

        //Assert
        Assert.assertEquals(expectedList, receivedList);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getRatesByUser_ShouldThrow_WhenNoSuchUserExists() {
        //Arrange
        Mockito.when(repositoryMock.getById(anyInt()))
                .thenThrow(new EntityNotFoundException());
        //Act
        usersService.getRatesByUserId(anyInt());
    }

    @Test(expected = ResponseStatusException.class) //Assert
    public void addToWishList_ShouldThrow_WhenBeerIsAlreadyAddedToWishlist() {
        //Arrange
        UserBeerStatusDTO ubsDTO = createUbsDto();

        Mockito.when(ubsService.createUserBeerStatus(ubsDTO))
                .thenThrow(new ResponseStatusException(HttpStatus.CONFLICT));
        //Act
        usersService.addToWishList(ubsDTO);
    }

    @Test(expected = ResponseStatusException.class) //Assert
    public void addToDrankList_ShouldThrow_WhenBeerIsAlreadyAddedToDranklist() {
        //Arrange
        UserBeerStatusDTO ubsDTO = createUbsDto();

        Mockito.when(ubsService.createUserBeerStatus(ubsDTO))
                .thenThrow(new ResponseStatusException(HttpStatus.CONFLICT));
        //Act
        usersService.addToDrankList(ubsDTO);
    }
}
