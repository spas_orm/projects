package com.team03.beertag.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ImageFileServiceImplTests {
    @InjectMocks
    ImageFileServiceImpl imageService;

    @Test(expected = Exception.class) //Assert
    public void isValidUrl_Should_Throw_WhenURL_IsInvalid() {
        //Arrange
        String mockURL = "!@#$%^&*()";

        Mockito.when(imageService.isValidUrl(mockURL))
                .thenThrow(new Exception());
        //Act
        imageService.isValidUrl(mockURL);
    }
}
