package com.team03.beertag.controllers;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.services.BeersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static com.team03.beertag.Factory.createBeer;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc

public class BeersRestControllerTests {
    @MockBean
    BeersService mockService;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getById_ShouldReturnStatusOK_WhenBeerExists() throws Exception {
        //Arrange
        Beer expectedBeer = createBeer();

        Mockito.when(mockService.getById(anyInt()))
                .thenReturn(expectedBeer);

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/beers/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print());
    }

    @Test
    public void getById_ShouldReturnStatus4xx_WhenBeerDoesNotExist() throws Exception {
        //Arrange
        Mockito.when(mockService.getById(anyInt()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/beers/{id}", "ThereIsNoBeerHere"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError())
                .andDo(print());
    }
}
