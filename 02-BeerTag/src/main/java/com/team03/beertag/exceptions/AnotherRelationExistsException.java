package com.team03.beertag.exceptions;

public class AnotherRelationExistsException extends RuntimeException {
    public AnotherRelationExistsException() {
        super();
    }

    public AnotherRelationExistsException(String message) {
        super(message);
    }
}
