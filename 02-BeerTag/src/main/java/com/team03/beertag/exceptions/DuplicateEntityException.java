package com.team03.beertag.exceptions;

public class DuplicateEntityException extends RuntimeException {
    public DuplicateEntityException() {
        super();
    }

    public DuplicateEntityException(String itemType, String name) {
        this(itemType, "name", name);
    }

    public DuplicateEntityException(String itemType, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", itemType, attribute, value));
    }
}
