package com.team03.beertag.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException() {
        super();
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String itemType, int id) {
        this(itemType, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String itemType, String attribute, String value) {
        super(String.format("%s with %s %s does not exist.", itemType, attribute, value));
    }
}
