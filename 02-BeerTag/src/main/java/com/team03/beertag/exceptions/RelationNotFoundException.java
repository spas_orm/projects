package com.team03.beertag.exceptions;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.User;

public class RelationNotFoundException extends RuntimeException {
    public RelationNotFoundException() {
        super();
    }

    public RelationNotFoundException(User user, Beer beer, String relationType) {
        this(user.getUserName(), beer.getName(), relationType);
    }

    public RelationNotFoundException(String firstItem, String secondItem, String relationType) {
        super(String.format("%s - %s %s relation does not exist.", firstItem, secondItem, relationType));
    }
}
