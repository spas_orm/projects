package com.team03.beertag.exceptions;

public class ImageStorageException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String msg;

    public ImageStorageException(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
