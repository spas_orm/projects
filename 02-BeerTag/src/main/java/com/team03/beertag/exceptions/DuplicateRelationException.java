package com.team03.beertag.exceptions;

public class DuplicateRelationException extends RuntimeException {
    public DuplicateRelationException() {
        super();
    }

    public DuplicateRelationException(String firstItem, String secondItem, String relationType) {
        super(String.format("Relation %s - %s with %s already exists.", firstItem, secondItem, relationType));
    }
}
