package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.BeerStyle;
import com.team03.beertag.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountriesRepositoryImpl implements CountriesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CountriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    @Override
    public Country create(Country country) {
        try(Session session = sessionFactory.openSession()){
            session.save(country);
            return country;
        }

    }

    @Override
    public List<Country> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Country", Country.class)
                    .list();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new EntityNotFoundException("Country", id);
            }
            return country;
        }
    }

    @Override
    public boolean checkExistByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> countryQuery = session
                    .createQuery("from Country where name = :name", Country.class);
            countryQuery.setParameter("name", name);
            if (countryQuery.list().size() == 0) {
                return false;
            }
            return true;
        }
    }

    @Override
    public Country update(int id, Country country) {
        try(Session session = sessionFactory.openSession()) {
            Country countryToUpdate = getById(id);
            countryToUpdate.setName(country.getName());
            session.beginTransaction();
            session.update(countryToUpdate);
            session.getTransaction().commit();
            return countryToUpdate;
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Country country = getById(id);
            country.setDeleted(true);
            session.update(country);
            session.getTransaction().commit();
        }
    }
}
