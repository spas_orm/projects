package com.team03.beertag.repositories;

import com.team03.beertag.models.Status;

import java.util.List;

public interface StatusesRepository {

    Status getById(int id);

    List<Status> getAll();


}
