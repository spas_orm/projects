package com.team03.beertag.repositories;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.Status;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerStatus;

import java.util.List;

public interface UsersBeersStatusRepository {

    List<UserBeerStatus> getAll();

    UserBeerStatus createUserBeerStatus(UserBeerStatus ubs);

    void deleteUserBeerStatus(UserBeerStatus ubs);

    UserBeerStatus getStatusByUserBeer(User user, Beer beer);

    UserBeerStatus updateUserBeerStatus(UserBeerStatus ubs);

//    int currentUserBeerStatus(User user, Beer beer); //Return Status Id. Return 0 if there is no such record

    boolean checkUserBeerStatusExist(UserBeerStatus ubs);

    List<Beer> getBeerListByUserAndStatus(User user, Status status);


//    boolean checkUserWishedBeer(User user, Beer beer);
//
//    boolean checkUserDrankBeer(User user, Beer beer);

}
