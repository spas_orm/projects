package com.team03.beertag.repositories;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerRating;

import java.util.List;

public interface UsersBeersRatingRepository {
    List<UserBeerRating> getAll();

    UserBeerRating createUserBeerRating(UserBeerRating ubr);

    UserBeerRating updateUserBeerRating(UserBeerRating ubr);

    boolean checkUserBeerRatingExist(UserBeerRating ubr);

    List<UserBeerRating> getRatesByUser(User user);

    int getRatingByUserAndBeer(User user, Beer beer);

    double getBeerAverageRating(Beer beer);
}
