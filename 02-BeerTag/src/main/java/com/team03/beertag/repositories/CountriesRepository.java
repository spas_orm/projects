package com.team03.beertag.repositories;

import com.team03.beertag.models.Country;

import java.util.List;

public interface CountriesRepository {

    Country create(Country country);

    List<Country> getAll();

    Country getById(int id);

    boolean checkExistByName(String name);

    Country update(int id, Country country);

    void delete(int id);

}
