package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusesRepositoryImpl implements StatusesRepository {
    private SessionFactory sessionFactory;

    public StatusesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Status status = session.get(Status.class,id);
            if (status == null) {
                throw new EntityNotFoundException("Status", id);
            }
            return status;
        }
    }

    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Status", Status.class)
                    .list();
        }
    }
}
