package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.RelationNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerRating;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UsersBeersRatingRepositoryImpl implements UsersBeersRatingRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UsersBeersRatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserBeerRating> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from UserBeerRating order by rating desc", UserBeerRating.class)
                    .list();
        }
    }

    @Override
    public UserBeerRating createUserBeerRating(UserBeerRating ubr) {
        if (checkUserBeerRatingExist(ubr)) {
            return updateUserBeerRating(ubr);
        }
        try (Session session = sessionFactory.openSession()) {
            session.save(ubr);
            return ubr;
        }
    }

    @Override
    public UserBeerRating updateUserBeerRating(UserBeerRating ubr) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(getOriginalUserBeerRating(ubr));
            session.getTransaction().commit();
        }

        try (Session session = sessionFactory.openSession()) {
            session.save(ubr);
            return ubr;
        }
    }

    @Override
    public boolean checkUserBeerRatingExist(UserBeerRating ubr) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserBeerRating> ubrQuery = session
                    .createQuery("from UserBeerRating where " +
                            "user = :user and " +
                            "beer = :beer", UserBeerRating.class);
            ubrQuery.setParameter("user", ubr.getUser());
            ubrQuery.setParameter("beer", ubr.getBeer());

            if (ubrQuery.list().isEmpty()) {
                return false;
            }
            return true;
        }
    }

    @Override
    public List<UserBeerRating> getRatesByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserBeerRating> queryRatings = session
                    .createQuery("from UserBeerRating ubr where ubr.user = :user order by ubr.rating desc"
                            , UserBeerRating.class);
            queryRatings.setParameter("user", user);
            List<UserBeerRating> drankList = queryRatings.list();
            return drankList;
        }
    }

    @Override
    public int getRatingByUserAndBeer(User user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserBeerRating> ubrQuery = session
                    .createQuery("from UserBeerRating where user = :user and beer = :beer", UserBeerRating.class);
            ubrQuery.setParameter("user", user);
            ubrQuery.setParameter("beer", beer);
            List<UserBeerRating> ubrList = ubrQuery.list();
            if (ubrList.size() == 0) {
                throw new RelationNotFoundException(user, beer, "rating");
            }
            return ubrList.get(0).getRating();
        }
    }

    @Override
    public double getBeerAverageRating(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> ubrQuery = session
                    .createQuery("select avg(rating) from UserBeerRating where beer = :beer",
                            Double.class);
            ubrQuery.setParameter("beer", beer);
            if (ubrQuery.getSingleResult() == null) {
                return 0;
            }
            return ubrQuery.getSingleResult();
        }
    }

    private UserBeerRating getOriginalUserBeerRating(UserBeerRating ubr) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserBeerRating> ubrQuery = session
                    .createQuery("from UserBeerRating where " +
                            "user = :user and " +
                            "beer = :beer", UserBeerRating.class);
            ubrQuery.setParameter("user", ubr.getUser());
            ubrQuery.setParameter("beer", ubr.getBeer());

            if (ubrQuery.list().isEmpty()) {
                throw new RelationNotFoundException(ubr.getUser(), ubr.getBeer(), "rating");
            }

            return ubrQuery.getSingleResult();
        }
    }
}
