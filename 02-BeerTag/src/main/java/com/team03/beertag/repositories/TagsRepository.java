package com.team03.beertag.repositories;

import com.team03.beertag.models.Tag;

import java.util.List;

public interface TagsRepository {
    Tag create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    boolean checkIfTagWithNameExists(String name);

    Tag updateById(int id, Tag tag);

    void deleteById(int id);
}
