package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.exceptions.InvalidInputException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.BeerSearch;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BeersRepositoryImpl implements BeersRepository {
    private SessionFactory sessionFactory;

    public BeersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(beer);
            session.getTransaction().commit();
            return beer;
        }
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Beer", Beer.class)
                    .list();
        }
    }

    @Override
    public Beer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException("Beer", id);
            }
            return beer;
        }
    }

    @Override
    public boolean checkIfBeerWithNameExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> beerQuery = session
                    .createQuery("from Beer where name = :name", Beer.class);
            beerQuery.setParameter("name", name);
            if (beerQuery.list().size() == 0) {
                return false;
            }
            return true;
        }
    }

    @Override
    public Beer updateById(int id, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            beer.setId(id);
            session.update(beer);
            session.getTransaction().commit();
            return beer;
        }
    }

    @Override
    public void deleteById(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = getById(id);
            beer.setDeleted(true);
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Beer> getAll(BeerSearch beerSearch) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "from Beer as b " +
                    "where b.name like :name and " +
                    "b.beerStyle.name like :style and " +
                    "b.brewery.name like :brewery and " +
                    "b.country.name like :country";

            if (beerSearch.getSortBy().equals("rating")) {
                return sortByRating(session, beerSearch);
            }

            switch (beerSearch.getSortBy()) {
                case "name":
                    hql = hql.concat(" order by b.name");
                    break;
                case "abv":
                    hql = hql.concat(" order by b.abv");
                    break;
                case "":
                    break;
                default:
                    throw new InvalidInputException("Invalid sorting parameter.");
            }

            switch (beerSearch.getSortType()) {
                case "descending":
                    hql = hql.concat(" desc");
                    break;
                case "ascending":
                    hql = hql.concat(" asc");
                    break;
                case "":
                    break;
                default:
                    throw new InvalidInputException("Invalid sorting parameter.");
            }

            //TODO several tags filtering
            Query<Beer> beerQuery = session
                    .createQuery(hql, Beer.class);
            setGetAllQueryParameters(beerSearch, beerQuery);

            return beerQuery.list();
        }
    }

    private List<Beer> sortByRating(Session session, BeerSearch beerSearch) {
        String hql = "select b.id from Beer as b join UserBeerRating ubr on b.id = ubr.id " +
                "where b.name like :name and " +
                "b.beerStyle.name like :style and " +
                "b.brewery.name like :brewery and " +
                "b.country.name like :country " +
                "group by b.id " +
                "order by avg(ubr.rating)";
        Query<Integer> beerOrderQuery = session
                .createQuery(hql, Integer.class);
        setGetAllQueryParameters(beerSearch, beerOrderQuery);
        List<Integer> beerOrder = beerOrderQuery.getResultList();

        List<Beer> beerList = new ArrayList<>();
        for (Integer i : beerOrder) {
            beerList.add(getById(i));
        }
        return beerList;
    }

    private void setGetAllQueryParameters(BeerSearch beerSearch, Query beerQuery) {
        beerQuery.setParameter("name", '%' + beerSearch.getBeerName() + '%');

        if (beerSearch.getStyleName().equals("")) {
            beerSearch.setStyleName("%");
        }
        beerQuery.setParameter("style", beerSearch.getStyleName());

        if (beerSearch.getBreweryName().equals("")) {
            beerSearch.setBreweryName("%");
        }
        beerQuery.setParameter("brewery", '%' + beerSearch.getBreweryName() + '%');

        if (beerSearch.getCountryName().equals("")) {
            beerSearch.setCountryName("%");
        }
        beerQuery.setParameter("country", '%' + beerSearch.getCountryName() + '%');
    }
}
