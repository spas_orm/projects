package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.RelationNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.Status;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UsersBeersStatusRepositoryImpl implements UsersBeersStatusRepository {
    private SessionFactory sessionFactory;
    private StatusesRepository statusesRepository;

    @Autowired
    public UsersBeersStatusRepositoryImpl(SessionFactory sessionFactory, StatusesRepository statusesRepository) {
        this.sessionFactory = sessionFactory;
        this.statusesRepository = statusesRepository;
    }

    @Override
    public List<UserBeerStatus> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from UserBeerStatus", UserBeerStatus.class)
                    .list();
        }
    }

    @Override
    public UserBeerStatus createUserBeerStatus(UserBeerStatus ubs) {
        try (Session session = sessionFactory.openSession()) {
            session.save(ubs);
            return ubs;
        }
    }


    @Override
    public UserBeerStatus updateUserBeerStatus(UserBeerStatus ubs) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(ubs);
            session.getTransaction().commit();
            return ubs;
        }
    }

    @Override
    public void deleteUserBeerStatus(UserBeerStatus ubs) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            ubs.setDeleted(true);
            session.delete(ubs);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserBeerStatus getStatusByUserBeer(User user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserBeerStatus> ubsQuery = session
                    .createQuery("from UserBeerStatus where user = :user and beer = :beer", UserBeerStatus.class);
            ubsQuery.setParameter("user", user);
            ubsQuery.setParameter("beer", beer);
            List<UserBeerStatus> ubsList = ubsQuery.list();
            if (ubsList.size() == 0) {
                throw new RelationNotFoundException(user, beer, "status");
            }
            return ubsList.get(0);
        }
    }

    @Override
    public boolean checkUserBeerStatusExist(UserBeerStatus ubs) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserBeerStatus> ubsQuery = session
                    .createQuery("from UserBeerStatus where user = :user " +
                                    "and beer = :beer " +
                                    "and status = :status",
                            UserBeerStatus.class);
            ubsQuery.setParameter("user", ubs.getUser());
            ubsQuery.setParameter("beer", ubs.getBeer());
            ubsQuery.setParameter("status", ubs.getStatus());

            List<UserBeerStatus> list = ubsQuery.list();
            if (list.isEmpty()) {
                return false;
            }
            return true;
        }
    }

    @Override
    public List<Beer> getBeerListByUserAndStatus(User user, Status status) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> queryDrank = session
                    .createQuery("select beer from UserBeerStatus ubs " +
                                    "where ubs.user = :user and ubs.status = :status"
                            , Beer.class);
            queryDrank.setParameter("user", user);
            queryDrank.setParameter("status", status);
            List<Beer> drankList = queryDrank.list();
            return drankList;
        }
    }

    //    //TODO - remove???
//    @Override
//    public int currentUserBeerStatus(User user, Beer beer) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<UserBeerStatus> ubsQuery = session
//                    .createQuery("from UserBeerStatus where user = :user and beer = :beer", UserBeerStatus.class);
//            ubsQuery.setParameter("user", user);
//            ubsQuery.setParameter("beer", beer);
//            if (ubsQuery.list().size() == 0) {
//                return 0;
//            }
//            UserBeerStatus ubs = ubsQuery.list().get(1);
//            return ubs.getStatus().getId();
//        }
//    }

//    @Override
//    public boolean checkUserWishedBeer(User user, Beer beer) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<UserBeerStatus> ubsQuery = session
//                    .createQuery("from UserBeerStatus where user = :user and beer = :beer and status.id = 1", UserBeerStatus.class);
//            ubsQuery.setParameter("user", user);
//            ubsQuery.setParameter("beer", beer);
//            if (ubsQuery.list().size() == 0) {
//                return false;
//            }
//            return true;
//        }
//    }
//
//    @Override
//    public boolean checkUserDrankBeer(User user, Beer beer) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<UserBeerStatus> ubsQuery = session
//                    .createQuery("from UserBeerStatus where user = :user and beer = :beer and status.id = 2", UserBeerStatus.class);
//            ubsQuery.setParameter("user", user);
//            ubsQuery.setParameter("beer", beer);
//            if (ubsQuery.list().size() == 0) {
//                return false;
//            }
//            return true;
//        }
//    }
}
