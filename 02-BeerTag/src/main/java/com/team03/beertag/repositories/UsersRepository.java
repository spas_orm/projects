package com.team03.beertag.repositories;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.Status;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerStatus;

import java.util.List;

public interface UsersRepository {
    User create(User user);

    List<User> getAll();

    User getById(int id);

    User findByUserName(String userName);

    boolean checkExistByUserName(String name);

    boolean checkExistByEmail(String email);

    User updateById(int id, User user);

    void deleteById(int id);

//    List<Beer> getWishListById(int id);
//
//    List<Beer> getDrankListById(int id);

    }
