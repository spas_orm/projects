package com.team03.beertag.repositories;

import com.team03.beertag.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweriesRepositoryImpl implements BreweriesRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Brewery create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
            return brewery;
        }
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Brewery", Brewery.class)
                    .list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Brewery.class, id);
        }
    }

    @Override
    public boolean checkIfBreweryWithNameExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> breweryQuery = session
                    .createQuery("from Brewery where name LIKE :name", Brewery.class);
            breweryQuery.setParameter("name", name);
            if (breweryQuery.list().size() == 0) {
                return false;
            }
            return true;
        }
    }

    @Override
    public Brewery updateById(int id, Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            brewery.setId(id);
            session.update(brewery);
            session.getTransaction().commit();
            return brewery;
        }
    }

    @Override
    public void deleteById(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Brewery brewery = getById(id);
            brewery.setDeleted(true);
            session.update(brewery);
            session.getTransaction().commit();
        }
    }
}
