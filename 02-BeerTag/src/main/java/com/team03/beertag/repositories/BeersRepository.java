package com.team03.beertag.repositories;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.BeerSearch;

import java.util.List;

public interface BeersRepository {
    Beer create(Beer beer);

    List<Beer> getAll();

    List<Beer> getAll(BeerSearch beerSearch);

    Beer getById(int id);

    boolean checkIfBeerWithNameExists(String name);

    Beer updateById(int id, Beer beer);

    void deleteById(int id);
}
