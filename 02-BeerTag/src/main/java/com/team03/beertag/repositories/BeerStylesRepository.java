package com.team03.beertag.repositories;

import com.team03.beertag.models.BeerStyle;

import java.util.List;

public interface BeerStylesRepository {

    BeerStyle create(BeerStyle style);

    List<BeerStyle> getAll();

    BeerStyle getById(int id);

    boolean checkExistByName(String name);

    BeerStyle update(int id, BeerStyle style);

    void delete(int id);
}
