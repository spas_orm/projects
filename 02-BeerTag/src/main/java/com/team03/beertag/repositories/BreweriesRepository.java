package com.team03.beertag.repositories;

import com.team03.beertag.models.Brewery;

import java.util.List;

public interface BreweriesRepository {
    Brewery create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    boolean checkIfBreweryWithNameExists(String name);

    Brewery updateById(int id, Brewery brewery);

    void deleteById(int id);
}
