package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UsersRepositoryImpl implements UsersRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.save(user);
            return user;
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from User", User.class)
                    .list();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public boolean checkExistByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> userQuery = session
                    .createQuery("from User where userName = :userName", User.class);
            userQuery.setParameter("userName", userName);
            if (userQuery.list().isEmpty()) {
                return false;
            }
            return true;
        }
    }

    @Override
    public boolean checkExistByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> userQuery = session
                    .createQuery("from User where email = :email", User.class);
            userQuery.setParameter("email", email);
            if (userQuery.list().isEmpty()) {
                return false;
            }
            return true;
        }
    }


    public User findByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> userQuery = session
                    .createQuery("from User where userName = :userName", User.class);
            userQuery.setParameter("userName", userName);
            if (userQuery.list().isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("User with username %s does not exist.", userName));
            }
            return userQuery.getSingleResult();
        }
    }

    @Override
    public User updateById(int id, User user) {
        try (Session session = sessionFactory.openSession()) {
            user.setId(id);
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public void deleteById(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = getById(id);
            user.setDeleted(true);
            user.setEnabled(false);
            session.update(user);
            session.getTransaction().commit();
        }
    }

//    @Override
//    public List<Beer> getWishListById(int id) {
//        try (Session session = sessionFactory.openSession()) {
//
//            User user = getById(id);
//            Status wishedBeer = new Status();
//            wishedBeer.setStatus("Wished");
//            Query<Beer> queryWish = session
//                    .createQuery("select beer from UserBeerStatus ubs " +
//                                    "where ubs.user = :user and ubs.status.name = 'Wished'"
//                            , Beer.class);
//            queryWish.setParameter("user", user);
//            List<Beer> wishList = queryWish.list();
//            return wishList;
//        }
//    }
//
//
//    @Override
//    public List<Beer> getDrankListById(int id) {
//        try (Session session = sessionFactory.openSession()) {
//
//            User user = getById(id);
//            Query<Beer> queryDrank = session
//                    .createQuery("select beer from UserBeerStatus ubs " +
//                                    "where ubs.user = :user and ubs.status.name = 'drank'"
//                            , Beer.class);
//            queryDrank.setParameter("user", user);
//            List<Beer> drankList = queryDrank.list();
//            return drankList;
//        }
//    }


}
