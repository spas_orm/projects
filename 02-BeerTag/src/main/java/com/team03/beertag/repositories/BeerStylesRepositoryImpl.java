package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.BeerStyle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BeerStylesRepositoryImpl implements BeerStylesRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BeerStylesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory=sessionFactory;
    }

    @Override
    public BeerStyle create(BeerStyle style) {
        try (Session session = sessionFactory.openSession()){
            session.save(style);
            return style;
        }
    }

    @Override
    public List<BeerStyle> getAll() {
        try (Session session=sessionFactory.openSession()){
            return session.createQuery("from BeerStyle",BeerStyle.class)
            .list();
        }
    }

    @Override
    public BeerStyle getById(int id) {
        try(Session session = sessionFactory.openSession()){
            return getById(id, session);
        }
    }

    @Override
    public boolean checkExistByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query <BeerStyle> styleQuery = session.createQuery(
                    "from BeerStyle where name = :name", BeerStyle.class);
            styleQuery.setParameter("name", name);
            if (styleQuery.list().size() == 0){
                return false;
            }
            return true;
        }
    }

    @Override
    public BeerStyle update(int id, BeerStyle style) {
        try(Session session = sessionFactory.openSession()) {
            BeerStyle styleToUpdate = getById(id);
            styleToUpdate.setName(style.getName());
            session.beginTransaction();
            session.update(styleToUpdate);
            session.getTransaction().commit();
            return styleToUpdate;
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            BeerStyle style = getById(id);
            style.setDeleted(true);
            session.update(style);
            session.getTransaction().commit();
        }
    }


    private BeerStyle getById(int id, Session session){
        BeerStyle style = session.get(BeerStyle.class, id);
        if(style==null){
            throw new EntityNotFoundException("Style",id);
        }
        return style;
    }
}
