package com.team03.beertag.repositories;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagsRepositoryImpl implements TagsRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public TagsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
            return tag;
        }
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Tag", Tag.class)
                    .list();
        }
    }

    @Override
    public Tag getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            return tag;
        }
    }

    @Override
    public boolean checkIfTagWithNameExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> tagQuery = session
                    .createQuery("from Tag where name = :name", Tag.class);
            tagQuery.setParameter("name", name);
            if(tagQuery.list().size() == 0) {
                return false;
            }
            return true;
        }
    }

    @Override
    public Tag updateById(int id, Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            tag.setId(id);
            session.update(tag);
            session.getTransaction().commit();
            return tag;
        }
    }

    @Override
    public void deleteById(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Tag tag = getById(id);
            tag.setDeleted(true);
            session.update(tag);
            session.getTransaction().commit();
        }
    }
}
