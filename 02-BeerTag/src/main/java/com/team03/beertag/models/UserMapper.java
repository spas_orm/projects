package com.team03.beertag.models;

import com.team03.beertag.repositories.CountriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    private CountriesRepository countriesRepository;

    @Autowired
    public UserMapper(CountriesRepository countriesRepository) {
        this.countriesRepository = countriesRepository;
    }

    public User dtoToUser(UserDTO userDTO) {
        User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setPassword(userDTO.getPassword());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setBirthDate(userDTO.getBirthDate());
        user.setEmail(userDTO.getEmail());
        user.setImageUrl(userDTO.getImageUrl());
        user.setCountry(countriesRepository.getById(userDTO.getCountryId()));

        return user;
    }
}
