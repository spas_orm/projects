package com.team03.beertag.models;

import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class UserDTO {
    @Size(min = 2, max = 25, message = "UserName length should be between 2 and 25 symbols.")
    private String userName;

    @Size(min = 2, max = 25, message = "Password length should be between 2 and 25 symbols.")
    private String password;

    @Size(min = 2, max = 25, message = "Password length should be between 2 and 25 symbols.")
    @Transient
    private String passwordConfirmation;

    @Size(min = 2, max = 25, message = "First name length should be between 2 and 25 symbols.")
    private String firstName;

    @Size(min = 2, max = 25, message = "Last name length should be between 2 and 25 symbols.")
    private String lastName;

    @NotNull
    private LocalDate birthDate;

    @Size(max = 50, message = "Email length should less than 50 symbols.")
    @Email
    private String email;

    @Size(max = 500, message = "Image URL length should be less than 500 symbols.")
    private String imageUrl;

    @Positive
    private int countryId;

    public UserDTO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
