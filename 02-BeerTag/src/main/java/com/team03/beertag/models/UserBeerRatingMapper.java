package com.team03.beertag.models;


import com.team03.beertag.services.BeersService;
import com.team03.beertag.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserBeerRatingMapper {
    private BeersService beersService;
    private UsersService usersService;

    @Autowired
    public UserBeerRatingMapper(BeersService beersService, UsersService usersService) {
        this.beersService = beersService;
        this.usersService = usersService;
    }

    public UserBeerRating dtoToUBR(UserBeerRatingDTO ubrDTO) {
        UserBeerRating ubr = new UserBeerRating();
        ubr.setBeer(beersService.getById(ubrDTO.getBeerId()));
        ubr.setUser(usersService.getById(ubrDTO.getUserId()));
        ubr.setRating(ubrDTO.getRating());

        return ubr;
    }
}
