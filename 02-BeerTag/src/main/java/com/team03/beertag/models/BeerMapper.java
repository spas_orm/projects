package com.team03.beertag.models;

import com.team03.beertag.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BeerMapper {
    private BeerStylesRepository stylesRepository;
    private BreweriesRepository breweriesRepository;
    private CountriesRepository countriesRepository;
    private UsersRepository usersRepository;
    private TagsRepository tagsRepository;

    @Autowired
    public BeerMapper(BeerStylesRepository stylesRepository,
                      BreweriesRepository breweriesRepository,
                      CountriesRepository countriesRepository,
                      UsersRepository usersRepository,
                      TagsRepository tagsRepository) {
        this.stylesRepository = stylesRepository;
        this.breweriesRepository = breweriesRepository;
        this.countriesRepository = countriesRepository;
        this.usersRepository = usersRepository;
        this.tagsRepository = tagsRepository;
    }

    public Beer dtoToBeer(BeerDTO beerDTO) {
        Beer beer = new Beer();

        beer.setId(beerDTO.getId());
        beer.setName(beerDTO.getName());
        beer.setDescription(beerDTO.getDescription());
        beer.setAbv(beerDTO.getAbv());
        beer.setBeerStyle(stylesRepository.getById(beerDTO.getBeerStyleId()));
        beer.setBrewery(breweriesRepository.getById(beerDTO.getBreweryId()));
        beer.setCountry(countriesRepository.getById(beerDTO.getCountryId()));
        beer.setImageURL(beerDTO.getImageUrl());
        beer.setCreatedBy(usersRepository.getById(beerDTO.getCreatedById()));
        List<Tag> tagList =new ArrayList<>();
        for (int id:beerDTO.getTagsIds()
             ) {tagList.add(tagsRepository.getById(id));
        }
        beer.setTags(tagList);

        return beer;
    }

    public BeerDTO beerToDto(Beer beer) {
        BeerDTO beerDTO = new BeerDTO();

        beerDTO.setId(beer.getId());
        beerDTO.setName(beer.getName());
        beerDTO.setDescription(beer.getDescription());
        beerDTO.setAbv(beer.getAbv());
        beerDTO.setBeerStyleId(beer.getBeerStyle().getId());
        beerDTO.setBreweryId(beer.getBrewery().getId());
        beerDTO.setCountryId(beer.getCountry().getId());
        beerDTO.setImageUrl(beer.getImageURL());
        beerDTO.setCreatedById(beer.getCreatedBy().getId());
        List<Integer> tagsId = new ArrayList<>();
        for (Tag tag:beer.getTags()
             ) {
            tagsId.add(tag.getId());
        }
        beerDTO.setTagsIds(tagsId);
        return beerDTO;
    }
}
