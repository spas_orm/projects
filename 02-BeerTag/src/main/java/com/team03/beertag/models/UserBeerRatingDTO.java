package com.team03.beertag.models;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserBeerRatingDTO {
    @Positive
    private int userId;

    @Positive
    private int beerId;


    private int rating;

    public UserBeerRatingDTO() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
