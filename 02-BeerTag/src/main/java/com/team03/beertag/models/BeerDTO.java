package com.team03.beertag.models;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.util.List;

public class BeerDTO {
//    @Positive(message = "Beer id must be positive.")
    private int id;

    @Size(min = 2, max = 25, message = "Name length should be between 2 and 25 symbols.")
    private String name;

    @Size(max = 250, message = "Beer description should be less than 250 symbols.")
    private String description;

    @PositiveOrZero(message = "Alcohol by volume cannot be a negative number.")
    private double abv;

    @Positive(message = "Style id must not be zero or a negative number.")
    private int beerStyleId;

    @Positive(message = "Brewery id must not be zero or a negative number.")
    private int breweryId;

    @Positive(message = "Country id must not be zero or a negative number.")
    private int countryId;

//    @Positive(message = "The user that created the beer must have an id that is positive")
    private int createdById;

    private String imageUrl;

    private List<Integer> tagsIds;

    public BeerDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getBeerStyleId() {
        return beerStyleId;
    }

    public void setBeerStyleId(int beerStyleId) {
        this.beerStyleId = beerStyleId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getCreatedById() {
        return createdById;
    }

    public void setCreatedById(int createdById) {
        this.createdById = createdById;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Integer> getTagsIds() {
        return tagsIds;
    }

    public void setTagsIds(List<Integer> tagsIds) {
        this.tagsIds = tagsIds;
    }
}
