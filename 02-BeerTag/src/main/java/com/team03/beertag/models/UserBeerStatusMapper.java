package com.team03.beertag.models;

import com.team03.beertag.repositories.BeersRepository;
import com.team03.beertag.repositories.StatusesRepository;
import com.team03.beertag.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserBeerStatusMapper {
    UsersRepository usersRepository;
    BeersRepository beersRepository;
    StatusesRepository statusesRepository;

    @Autowired
    public UserBeerStatusMapper(UsersRepository usersRepository,
                                BeersRepository beersRepository,
                                StatusesRepository statusesRepository) {
        this.usersRepository = usersRepository;
        this.beersRepository = beersRepository;
        this.statusesRepository = statusesRepository;
    }

    public UserBeerStatus dtoToUserBeerStatus(UserBeerStatusDTO ubsDto){
        UserBeerStatus ubs = new UserBeerStatus();
        ubs.setUser(usersRepository.getById(ubsDto.getUserId()));
        ubs.setBeer(beersRepository.getById(ubsDto.getBeerId()));
        ubs.setStatus(statusesRepository.getById(ubsDto.getStatusId()));
        return ubs;
    }
}
