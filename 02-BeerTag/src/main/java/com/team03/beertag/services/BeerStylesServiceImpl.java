package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.BeerStyle;
import com.team03.beertag.repositories.BeerStylesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerStylesServiceImpl implements BeerStylesService {
    private BeerStylesRepository stylesRepository;

    @Autowired
    public BeerStylesServiceImpl(BeerStylesRepository stylesRepository) {
        this.stylesRepository = stylesRepository;
    }

    @Override
    public BeerStyle create(BeerStyle style) {
        if (stylesRepository.checkExistByName(style.getName())) {
            throw new DuplicateEntityException("Style", style.getName());
        }
        return stylesRepository.create(style);
    }

    @Override
    public List<BeerStyle> getAll() {
        return stylesRepository.getAll();
    }

    @Override
    public BeerStyle getById(int id) {
        try{
            return stylesRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Style", id);
        }
    }

    @Override
    public BeerStyle update(int id, BeerStyle style) {
        try{
            return stylesRepository.update(id, style);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Style", id);
        }
    }

    @Override
    public void delete(int id) {
        try{
            stylesRepository.delete(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Style", id);
        }
    }
}
