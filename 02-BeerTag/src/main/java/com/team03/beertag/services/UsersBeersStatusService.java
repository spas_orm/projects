package com.team03.beertag.services;

import com.team03.beertag.models.UserBeerStatus;
import com.team03.beertag.models.UserBeerStatusDTO;

import java.util.List;

public interface UsersBeersStatusService {

    List<UserBeerStatus> getAll();

    UserBeerStatus createUserBeerStatus(UserBeerStatusDTO ubsDTO);

    UserBeerStatus updateUserBeerStatus(UserBeerStatusDTO ubsDTO);

    boolean checkUserBeerStatusExist(UserBeerStatus ubs);
}
