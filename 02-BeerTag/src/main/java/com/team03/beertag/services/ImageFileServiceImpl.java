package com.team03.beertag.services;

import com.team03.beertag.exceptions.ImageStorageException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class ImageFileServiceImpl implements ImageFileService {
    @Value("${app.upload.dir}")
    private String uploadDir;
    @Value("${app.imgs.url}")
    private String imgLocationUrl;

    @Value("${user.img.default.url}")
    private String defaultUserImgUrl;

    @Value("${beer.img.default.url}")
    private String defaultBeerImgUrl;

    @Value("${no.img.url}")
    private String noImgUrl;

    @Override
    public String uploadFile(MultipartFile file, String objectName) {
        try {
            if (file.isEmpty()) {
                return setDefaultImgUrl(objectName);
            }
            String fileName = UUID.randomUUID().toString() + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());
            Path copyLocation = Paths
                    .get(uploadDir + File.separator + fileName);
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);

            return imgLocationUrl + fileName;
        } catch (Exception e) {
            throw new ImageStorageException("Could not store file " + file.getOriginalFilename()
                    + ". Please try again!");
        }
    }

    public boolean isValidUrl(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String setDefaultImgUrl(String object) {
        if (object.contains("Beer")) {
            return defaultBeerImgUrl;
        }
        if (object.equals("User")) {
            return defaultUserImgUrl;
        }
        return noImgUrl;
    }
}