package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Tag;
import com.team03.beertag.repositories.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagsServiceImpl implements TagsService {
    private TagsRepository tagsRepository;

    @Autowired
    public TagsServiceImpl(TagsRepository tagsRepository) {
        this.tagsRepository = tagsRepository;
    }

    @Override
    public Tag create(Tag tag) {
        if (tagsRepository.checkIfTagWithNameExists(tag.getName())) {
            throw new DuplicateEntityException("Tag", tag.getName());
        }
        return tagsRepository.create(tag);
    }

    @Override
    public List<Tag> getAll() {
        return tagsRepository.getAll();
    }

    @Override
    public Tag getById(int id) {
        try {
            return tagsRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Tag", id);
        }
    }

    @Override
    public Tag updateById(int id, Tag tag) {
        try {
            return tagsRepository.updateById(id, tag);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Tag", id);
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            tagsRepository.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Tag", id);
        }
    }
}
