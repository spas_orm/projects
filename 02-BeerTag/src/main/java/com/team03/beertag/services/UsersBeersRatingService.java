package com.team03.beertag.services;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerRating;
import com.team03.beertag.models.UserBeerRatingDTO;

import java.util.List;

public interface UsersBeersRatingService {
    List<UserBeerRating> getAll();

    UserBeerRating createUserBeerRating(UserBeerRating ubs);

    UserBeerRating updateUserBeerRating(UserBeerRating ubs);

    boolean checkUserBeerRatingExist(UserBeerRating ubs);

    List<UserBeerRating> getRatesByUser(User user);

    int getRatingByUserAndBeer(User user, Beer beer);

    double getBeerAverageRating(Beer beer);
}
