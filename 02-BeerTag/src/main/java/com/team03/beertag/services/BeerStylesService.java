package com.team03.beertag.services;

import com.team03.beertag.models.BeerStyle;

import java.util.List;

public interface BeerStylesService {

    BeerStyle create(BeerStyle style);

    List<BeerStyle> getAll();

    BeerStyle getById(int id);

    BeerStyle update(int id, BeerStyle style);

    void delete(int id);
}
