package com.team03.beertag.services;

import com.team03.beertag.models.Brewery;

import java.util.List;

public interface BreweriesService {
    Brewery create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    Brewery updateById(int id, Brewery brewery);

    void deleteById(int id);
}
