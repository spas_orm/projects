package com.team03.beertag.services;

import com.team03.beertag.models.Status;

import java.util.List;

public interface StatusService {

    Status getById(int id);

    List<Status> getAll();

}
