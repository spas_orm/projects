package com.team03.beertag.services;

import com.team03.beertag.models.Tag;

import java.util.List;

public interface TagsService {
    Tag create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    Tag updateById(int id, Tag tag);

    void deleteById(int id);
}
