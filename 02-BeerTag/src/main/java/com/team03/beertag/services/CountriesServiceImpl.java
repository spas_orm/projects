package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Country;
import com.team03.beertag.repositories.CountriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CountriesServiceImpl implements CountriesService {
    private CountriesRepository countriesRepository;

    @Autowired
    public CountriesServiceImpl(CountriesRepository countriesRepository) {
        this.countriesRepository = countriesRepository;
    }

    @Override
    public Country create(Country country) {
        if (countriesRepository.checkExistByName(country.getName())) {
            throw new DuplicateEntityException("Country", country.getName());
        }
        return countriesRepository.create(country);
    }

    @Override
    public List<Country> getAll() {
        return countriesRepository.getAll();
    }

    @Override
    public Country getById(int id) {
        try {
            return countriesRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Country", id);
        }
    }

    @Override
    public Country update(int id, Country country) {
        try {
            return countriesRepository.update(id, country);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Country", id);
        }
    }

    @Override
    public void delete(int id) {
        try {
            countriesRepository.delete(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Country", id);
        }
    }
}
