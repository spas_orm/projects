package com.team03.beertag.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Part;
import java.io.IOException;

public interface ImageFileService {
    public String uploadFile(MultipartFile file, String objectName);

//    public String saveFileFromPart(Part filePart, String objectName) throws IOException;
}
