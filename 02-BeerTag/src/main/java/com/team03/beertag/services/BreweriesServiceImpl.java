package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Brewery;
import com.team03.beertag.repositories.BreweriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweriesServiceImpl implements BreweriesService {
    private BreweriesRepository breweriesRepository;

    @Autowired
    public BreweriesServiceImpl(BreweriesRepository breweriesRepository) {
        this.breweriesRepository = breweriesRepository;
    }

    @Override
    public Brewery create(Brewery brewery) {
        if (breweriesRepository.checkIfBreweryWithNameExists(brewery.getName())) {
            throw new DuplicateEntityException("Brewery", brewery.getName());
        }
        return breweriesRepository.create(brewery);
    }

    @Override
    public List<Brewery> getAll() {
        return breweriesRepository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        try {
            return breweriesRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Brewery", id);
        }
    }

    @Override
    public Brewery updateById(int id, Brewery brewery) {
        try {
            return breweriesRepository.updateById(id, brewery);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Brewery", id);
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            breweriesRepository.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Brewery", id);
        }
    }
}
