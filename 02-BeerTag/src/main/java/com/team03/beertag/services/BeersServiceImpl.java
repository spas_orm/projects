package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.BeerDTO;
import com.team03.beertag.models.BeerMapper;
import com.team03.beertag.models.BeerSearch;
import com.team03.beertag.repositories.BeersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeersServiceImpl implements BeersService {
    private BeersRepository beersRepository;
    private BeerMapper beerMapper;

    @Autowired
    public BeersServiceImpl(BeersRepository beersRepository, BeerMapper beerMapper) {
        this.beersRepository = beersRepository;
        this.beerMapper = beerMapper;
    }

    @Override
    public Beer create(BeerDTO beerDTO) {
        if (beersRepository.checkIfBeerWithNameExists(beerDTO.getName())) {
            throw new DuplicateEntityException("Beer", beerDTO.getName());
        }
        try {
            Beer beerToCreate = beerMapper.dtoToBeer(beerDTO);
            return beersRepository.create(beerToCreate);
        } catch (EntityNotFoundException enf){
            throw new EntityNotFoundException(enf.getMessage());
        }

    }

    @Override
    public List<Beer> getAll() {
        return beersRepository.getAll();
    }

    @Override
    public List<Beer> getAll(BeerSearch beerSearch) {
        return beersRepository.getAll(beerSearch);
    }

    @Override
    public Beer getById(int id) {
        try {
            return beersRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer", id);
        }
    }

    @Override
    public Beer updateById(int id, Beer beer) {
        try {
            return beersRepository.updateById(id, beer);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer", id);
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            beersRepository.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer", id);
        }
    }
}
