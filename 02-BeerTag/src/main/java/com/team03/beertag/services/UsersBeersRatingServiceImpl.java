package com.team03.beertag.services;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.User;
import com.team03.beertag.models.UserBeerRating;
import com.team03.beertag.repositories.UsersBeersRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersBeersRatingServiceImpl implements UsersBeersRatingService {
    private UsersBeersRatingRepository ubrRepository;

    @Autowired
    public UsersBeersRatingServiceImpl(UsersBeersRatingRepository ubrRepository) {

        this.ubrRepository = ubrRepository;
    }

    @Override
    public List<UserBeerRating> getAll() {
        return ubrRepository.getAll();
    }

    @Override
    public UserBeerRating createUserBeerRating(UserBeerRating ubs) {
        return ubrRepository.createUserBeerRating(ubs);
    }

    @Override
    public UserBeerRating updateUserBeerRating(UserBeerRating ubs) {
        return ubrRepository.updateUserBeerRating(ubs);
    }

    @Override
    public boolean checkUserBeerRatingExist(UserBeerRating ubs) {
        return ubrRepository.checkUserBeerRatingExist(ubs);
    }

    @Override
    public List<UserBeerRating> getRatesByUser(User user) {
        return ubrRepository.getRatesByUser(user);
    }

    @Override
    public int getRatingByUserAndBeer(User user, Beer beer) {
        return ubrRepository.getRatingByUserAndBeer(user, beer);
    }

    @Override
    public double getBeerAverageRating(Beer beer) {
        return ubrRepository.getBeerAverageRating(beer);
    }
}
