package com.team03.beertag.services;

import com.team03.beertag.models.Status;
import com.team03.beertag.repositories.StatusesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {
    private StatusesRepository statusesRepository;

    @Autowired
    public StatusServiceImpl(StatusesRepository statusesRepository) {
        this.statusesRepository = statusesRepository;
    }

    @Override
    public Status getById(int id) {
        return statusesRepository.getById(id);
    }

    @Override
    public List<Status> getAll() {
        return statusesRepository.getAll();
    }
}
