package com.team03.beertag.services;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.DuplicateRelationException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.*;
import com.team03.beertag.repositories.UsersBeersStatusRepository;
import com.team03.beertag.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;
    private UsersBeersStatusService ubsService;
    private StatusService statusService;
    private UsersBeersStatusRepository ubsRepository;
    private UsersBeersRatingService ubrService;
    private ImageFileService imgService;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository,
                            UsersBeersStatusService ubsService,
                            StatusService statusService,
                            UsersBeersStatusRepository ubsRepository,
                            UsersBeersRatingService ubrService,
                            ImageFileService imgService) {
        this.usersRepository = usersRepository;
        this.ubsService = ubsService;
        this.statusService = statusService;
        this.ubsRepository = ubsRepository;
        this.ubrService = ubrService;
        this.imgService = imgService;
    }

    @Override
    public User create(User user) {
        if (usersRepository.checkExistByUserName(user.getUserName())) {
            throw new DuplicateEntityException("User", user.getUserName());
        }
        if (usersRepository.checkExistByEmail(user.getEmail())) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        return usersRepository.create(user);
    }

    @Override
    public List<User> getAll() {
        return usersRepository.getAll();
    }

    @Override
    public User getById(int id) {
        try {
            return usersRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public boolean checkExistByEmail(String email) {
        return usersRepository.checkExistByEmail(email);
    }

    @Override
    public User findByUserName(String userName) {
        return usersRepository.findByUserName(userName);
    }

    @Override
    public User updateById(int id, User user) {
        try {
            return usersRepository.updateById(id, user);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public void deleteById(int id) {
        try {
            usersRepository.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }


    @Override
    public List<Beer> getWishListByUserId(int id) {
        Status status = statusService.getById(1);
        try {
            User user = usersRepository.getById(id);
            return ubsRepository.getBeerListByUserAndStatus(user, status);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public List<Beer> getDrankListByUserId(int id) {
        Status status = statusService.getById(2);
        try {
            User user = usersRepository.getById(id);
            return ubsRepository.getBeerListByUserAndStatus(user, status);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public List<UserBeerRating> getRatesByUserId(int id) {
        try {
            User user = usersRepository.getById(id);
            return ubrService.getRatesByUser(user);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }

    @Override
    public UserBeerStatus addToWishList(UserBeerStatusDTO ubsDto) {
        try {
            return ubsService.createUserBeerStatus(ubsDto);
        } catch (DuplicateRelationException dre) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dre.getMessage());
        }
    }

    @Override
    public UserBeerStatus addToDrankList(UserBeerStatusDTO ubsDto) {
        try {
            return ubsService.createUserBeerStatus(ubsDto);
        } catch (DuplicateRelationException dre) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dre.getMessage());
        }
    }
}
