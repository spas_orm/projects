package com.team03.beertag.services;

import com.team03.beertag.models.Country;

import java.util.List;

public interface CountriesService {

    Country create(Country country);

    List<Country> getAll();

    Country getById(int id);

    Country update(int id, Country country);

    void delete(int id);

}
