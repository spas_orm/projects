package com.team03.beertag.services;

import com.team03.beertag.models.*;

import java.util.List;

public interface UsersService {
    User create(User user);

    List<User> getAll();

    User getById(int id);

    boolean checkExistByEmail(String email);

    User findByUserName(String userName);

    User updateById(int id, User user);

    void deleteById(int id);

    List<Beer> getWishListByUserId(int id);

    List<Beer> getDrankListByUserId(int id);

    UserBeerStatus addToWishList(UserBeerStatusDTO ubsDto);

    UserBeerStatus addToDrankList(UserBeerStatusDTO ubsDto);

    List<UserBeerRating> getRatesByUserId (int id);

//    List<Beer> getListByIdAndStatus(int id, Status status);

//    UserBeerStatus addBeerToWishList(int userId, int beerId);
//
//    UserBeerStatus addBeerToDrankList(int userId, int beerId);


}
