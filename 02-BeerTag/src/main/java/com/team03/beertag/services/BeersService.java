package com.team03.beertag.services;

import com.team03.beertag.models.Beer;
import com.team03.beertag.models.BeerDTO;
import com.team03.beertag.models.BeerSearch;

import java.util.List;

public interface BeersService {
    Beer create(BeerDTO beerDTO);

    List<Beer> getAll();

    List<Beer> getAll(BeerSearch beerSearch);

    Beer getById(int id);

    Beer updateById(int id, Beer beer);

    void deleteById(int id);
}
