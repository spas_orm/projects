package com.team03.beertag.services;

import com.team03.beertag.exceptions.AnotherRelationExistsException;
import com.team03.beertag.exceptions.DuplicateRelationException;
import com.team03.beertag.exceptions.RelationNotFoundException;
import com.team03.beertag.models.Status;
import com.team03.beertag.models.UserBeerStatus;
import com.team03.beertag.models.UserBeerStatusDTO;
import com.team03.beertag.models.UserBeerStatusMapper;
import com.team03.beertag.repositories.UsersBeersStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersBeersStatusServiceImpl implements UsersBeersStatusService {
    private static final int STATUS_ID_WISHED = 1;
    private static final int STATUS_ID_DRANK = 2;

    private UsersBeersStatusRepository ubsRepository;
    private StatusService statusesService;
    private UserBeerStatusMapper ubsMapper;

    @Autowired
    public UsersBeersStatusServiceImpl(UsersBeersStatusRepository ubsRepository,
                                       StatusService statusesService,
                                       UserBeerStatusMapper ubsMapper) {
        this.ubsRepository = ubsRepository;
        this.statusesService = statusesService;
        this.ubsMapper = ubsMapper;
    }

    @Override
    public List<UserBeerStatus> getAll() {
        return ubsRepository.getAll();
    }

    @Override
    public UserBeerStatus createUserBeerStatus(UserBeerStatusDTO ubsDTO) {
        UserBeerStatus ubs = ubsMapper.dtoToUserBeerStatus(ubsDTO);
        Status wishedStatus = statusesService.getById(STATUS_ID_WISHED);
        Status drankStatus = statusesService.getById(STATUS_ID_DRANK);

        if (checkUserBeerStatusExist(ubs)) {
            throw new DuplicateRelationException(ubs.getUser().getUserName(),
                    ubs.getBeer().getName(),
                    ubs.getStatus().getStatus() + " status");
        }

        if (ubs.getStatus().equals(wishedStatus)) {
            ubs.setStatus(drankStatus);
            if (checkUserBeerStatusExist(ubs)) {
                throw new AnotherRelationExistsException
                        ("You cannot have a beer in your wishlist and your dranklist at the same time");
            }
            ubs.setStatus(wishedStatus);
        }

        if (ubs.getStatus().equals(drankStatus)) {
            ubs.setStatus(wishedStatus);
            if (checkUserBeerStatusExist(ubs)) {
                ubsRepository.deleteUserBeerStatus(ubsRepository.getStatusByUserBeer(ubs.getUser(), ubs.getBeer()));
            }
            ubs.setDeleted(false);
            ubs.setStatus(drankStatus);
        }

        return ubsRepository.createUserBeerStatus(ubs);
    }

    @Override
    public UserBeerStatus updateUserBeerStatus(UserBeerStatusDTO ubsDTO) {
        UserBeerStatus ubs = ubsMapper.dtoToUserBeerStatus(ubsDTO);
        if (!checkUserBeerStatusExist(ubs)) {
            throw new RelationNotFoundException();
        }
        return ubsRepository.updateUserBeerStatus(ubs);
    }

    @Override
    public boolean checkUserBeerStatusExist(UserBeerStatus ubs) {
        return ubsRepository.checkUserBeerStatusExist(ubs);
    }
}
