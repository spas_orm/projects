package com.team03.beertag.utilties;

import com.team03.beertag.models.Beer;

import java.util.List;
import java.util.stream.Collectors;

public class BeersFilters {
    public static List<Beer> filterByBeerName(List<Beer> beers, String name) {
        if (!name.isEmpty()) {
            beers = beers.stream()
                    .filter(beer -> beer.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }

        return beers;
    }

    public static List<Beer> filterByAbv(List<Beer> beers, double abv) {
        beers = beers.stream()
                .filter(beer -> beer.getAbv() == abv)
                .collect(Collectors.toList());
        return beers;
    }

    public static List<Beer> filterByBreweryName(List<Beer> beers, String breweryName) {
        if (!breweryName.isEmpty()) {
            beers = beers.stream()
                    .filter(beer -> beer.getBrewery() != null)
                    .filter(beer -> beer.getBrewery().getName().toLowerCase().contains(breweryName.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return beers;
    }

    public static List<Beer> filterByCountryName(List<Beer> beers, String countryName) {
        if (!countryName.isEmpty()) {
            beers = beers.stream()
                    .filter(beer -> beer.getCountry() != null)
                    .filter(beer -> beer.getCountry().getName().toLowerCase().contains(countryName.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return beers;
    }

    public static List<Beer> filterByTagName(List<Beer> beers, String tagName) {
        if (!tagName.isEmpty()) {
            beers = beers.stream()
                    .filter(beer -> beer.getTags() != null)
                    .filter(beer -> beer.getTags().stream()
                            .anyMatch(tag -> tag.getName().toLowerCase().contains(tagName.toLowerCase())))
                    .collect(Collectors.toList());
        }
        return beers;
    }
}
