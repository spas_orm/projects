package com.team03.beertag.controllers.rest;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.BeerStyle;
import com.team03.beertag.services.BeerStylesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/styles")
public class StylesRestController {
    private BeerStylesService stylesService;

    @Autowired
    public StylesRestController(BeerStylesService stylesService) {
        this.stylesService = stylesService;
    }

    @GetMapping
    public List<BeerStyle> getAll(){
        return stylesService.getAll();
    }

    @GetMapping("/{id}")
    public BeerStyle getById(@PathVariable int id){
        try { return stylesService.getById(id);}
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @PostMapping
    public BeerStyle create(@RequestBody BeerStyle style){
        try {
            return stylesService.create(style);
        }
        catch (DuplicateEntityException dee){
            throw new ResponseStatusException(HttpStatus.CONFLICT,dee.getMessage());
        }
    }


    @PutMapping("/{id}")
    public BeerStyle update(@PathVariable int id, @RequestBody BeerStyle style){
        try {
            return stylesService.update(id,style);
        }
        catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,enf.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        try {
            stylesService.delete(id);
        } catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,enf.getMessage());
        }
    }
}
