package com.team03.beertag.controllers.rest;

import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Brewery;
import com.team03.beertag.services.BreweriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/breweries")
public class BreweriesRestController {
    private BreweriesService breweriesService;

    @Autowired
    public BreweriesRestController(BreweriesService breweriesService) {
        this.breweriesService = breweriesService;
    }

    @GetMapping
    public List<Brewery> getAll() {
        return breweriesService.getAll();
    }

    @PostMapping
    public Brewery create(@RequestBody Brewery brewery) {
        try {
            return breweriesService.create(brewery);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery updateById(@PathVariable int id, Brewery brewery) {
        try {
            return breweriesService.updateById(id, brewery);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        try {
            breweriesService.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
