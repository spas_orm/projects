package com.team03.beertag.controllers.rest;


import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.DuplicateRelationException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.*;
import com.team03.beertag.services.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UsersRestController {
    UsersService usersService;
    BeersService beersService;
    StatusService statusService;
    UsersBeersStatusService ubsService;
    ImageFileServiceImpl imgService;

    public UsersRestController(UsersService usersService,
                               BeersService beersService,
                               StatusService statusService,
                               UsersBeersStatusService ubsService,
                               ImageFileServiceImpl imgService) {
        this.usersService = usersService;
        this.beersService = beersService;
        this.statusService = statusService;
        this.ubsService = ubsService;
        this.imgService = imgService;
    }

    @GetMapping
    public List<User> getAll() {
        return usersService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return usersService.getById(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @PostMapping
    public User create(@ModelAttribute User user, @RequestParam(value = "file", required = false) MultipartFile file) {
        try {
            if(user.getImageUrl()==null || imgService.isValidUrl(user.getImageUrl())){
            user.setImageUrl(imgService.uploadFile(file,
                    user.getClass().getSimpleName()));
            }
            return usersService.create(user);
        } catch (DuplicateEntityException dee) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dee.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id, @RequestBody User user) {
        try {
            return usersService.updateById(id, user);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            usersService.deleteById(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @GetMapping("/{id}/wishlist")
    public List<Beer> getWishListById(@PathVariable int id) {
        try {
            return usersService.getWishListByUserId(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @GetMapping("/{id}/dranklist")
    public List<Beer> getDrankListById(@PathVariable int id) {
        try {
            return usersService.getDrankListByUserId(id);
        } catch (EntityNotFoundException enf) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enf.getMessage());
        }
    }

    @PostMapping("/{userId}/wishlist")
    public UserBeerStatus addBeerToWishList(@PathVariable int userId, @RequestBody UserBeerStatusDTO ubsDto) {
        try {
            ubsDto.setUserId(userId);
            ubsDto.setStatusId(1);
            return usersService.addToWishList(ubsDto);
        } catch (DuplicateRelationException dre) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dre.getMessage());
        }
    }

    @PostMapping("/{userId}/dranklist")
    public UserBeerStatus addBeerToDrankList(@PathVariable int userId, @RequestBody UserBeerStatusDTO ubsDto) {
        try {
            ubsDto.setUserId(userId);
            ubsDto.setStatusId(2);
            return usersService.addToDrankList(ubsDto);
        } catch (DuplicateRelationException dre) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, dre.getMessage());
        }
    }

}
