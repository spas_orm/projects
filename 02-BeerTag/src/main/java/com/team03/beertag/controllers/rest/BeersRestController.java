package com.team03.beertag.controllers.rest;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.exceptions.InvalidInputException;
import com.team03.beertag.models.Beer;
import com.team03.beertag.models.BeerDTO;
import com.team03.beertag.models.BeerSearch;
import com.team03.beertag.services.BeersService;
import com.team03.beertag.services.ImageFileServiceImpl;
import com.team03.beertag.utilties.BeersFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/beers")
public class BeersRestController {
    private BeersService beersService;
    private ImageFileServiceImpl imgService;

    @Autowired
    public BeersRestController(BeersService beersService, ImageFileServiceImpl imgService) {
        this.beersService = beersService;
        this.imgService = imgService;
    }

    @GetMapping
    public List<Beer> getAll(@RequestParam(defaultValue = "") String name,
                             @RequestParam(defaultValue = "") String style,
                             @RequestParam(defaultValue = "") String brewery,
                             @RequestParam(defaultValue = "") String country,
                             @RequestParam(defaultValue = "") String sortBy) {
        BeerSearch beerSearch = new BeerSearch();
        beerSearch.setBeerName(name);
        beerSearch.setStyleName(style);
        beerSearch.setBreweryName(brewery);
        beerSearch.setCountryName(country);
        beerSearch.setSortBy(sortBy);
        try {
            return beersService.getAll(beerSearch);
        } catch (InvalidInputException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return beersService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@ModelAttribute @Valid BeerDTO beerDTO,@RequestParam(value = "file", required = false) MultipartFile file) {
        try {
            if(beerDTO.getImageUrl()==null || imgService.isValidUrl(beerDTO.getImageUrl())){
                beerDTO.setImageUrl(imgService.uploadFile(file,
                        beerDTO.getClass().getSimpleName()));
            }
            return beersService.create(beerDTO);
        }catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,enf.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody @Valid Beer beer) {
        try {
            return beersService.updateById(id, beer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        try {
            beersService.deleteById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
