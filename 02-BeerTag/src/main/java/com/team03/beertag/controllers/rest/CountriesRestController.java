package com.team03.beertag.controllers.rest;

import com.team03.beertag.exceptions.DuplicateEntityException;
import com.team03.beertag.exceptions.EntityNotFoundException;
import com.team03.beertag.models.Country;
import com.team03.beertag.services.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/countries")
public class CountriesRestController {
    CountriesService countriesService;

    @Autowired
    public CountriesRestController(CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    @GetMapping
    public List<Country> getAll(){
        return countriesService.getAll();
    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable int id){
        try { return countriesService.getById(id);}
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @PostMapping
    public Country create(@RequestBody Country country){
        try {
            return countriesService.create(country);
        }
        catch (DuplicateEntityException dee){
            throw new ResponseStatusException(HttpStatus.CONFLICT,dee.getMessage());
        }
    }


    @PutMapping("/{id}")
    public Country update(@PathVariable int id, @RequestBody Country country){
        try {
            return countriesService.update(id,country);
        }
        catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,enf.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        try {
            countriesService.delete(id);
        } catch (EntityNotFoundException enf){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,enf.getMessage());
        }
    }
}
