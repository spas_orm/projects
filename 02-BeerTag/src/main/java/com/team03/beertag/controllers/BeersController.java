package com.team03.beertag.controllers;

import com.team03.beertag.exceptions.*;
import com.team03.beertag.models.*;
import com.team03.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;

@ApiIgnore
@Controller
public class BeersController {
    private static final int STATUS_ID_WISHED = 1;
    private static final int STATUS_ID_DRANK = 2;

    private BeersService beersService;
    private UsersService usersService;
    private UsersBeersRatingService ubrService;
    private UsersBeersStatusService ubsService;
    private CountriesService countriesService;
    private BeerStylesService stylesService;
    private BreweriesService breweriesService;
    private ImageFileService imgService;
    private TagsService tagsService;
    private BeerMapper beerMapper;


    @Autowired
    public BeersController(BeersService beersService,
                           UsersService usersService,
                           UsersBeersRatingService ubrService,
                           UsersBeersStatusService ubsService,
                           CountriesService countriesService,
                           BeerStylesService stylesService,
                           BreweriesService breweriesService,
                           TagsService tagsService,
                           ImageFileService imgService,
                           BeerMapper beerMapper) {
        this.beersService = beersService;
        this.usersService = usersService;
        this.ubrService = ubrService;
        this.ubsService = ubsService;
        this.countriesService = countriesService;
        this.breweriesService = breweriesService;
        this.stylesService = stylesService;
        this.tagsService = tagsService;
        this.imgService = imgService;
        this.beerMapper = beerMapper;
    }

    @GetMapping("/beer/{id}")
    public String showBeer(@PathVariable String id, Model model, Principal principal) {
        int beerId = parseId(id);

        try {
            model.addAttribute("beer", beersService.getById(beerId));
            model.addAttribute("averageRating", ubrService.getBeerAverageRating(beersService.getById(beerId)));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        model.addAttribute("ubr", new UserBeerRating());

        if (principal == null) {
            model.addAttribute("rating", 0);
            return "beer";
        }

        try {
            model.addAttribute("rating", ubrService.getRatingByUserAndBeer(
                    usersService.findByUserName(principal.getName()),
                    beersService.getById(beerId)));
        } catch (RelationNotFoundException e) {
            model.addAttribute("rating", 0);
            return "beer";
        }
        return "beer";
    }

    @PostMapping("/beer/{id}")
    public String rateBeer(@Valid @ModelAttribute UserBeerRating ubr, @PathVariable int id, Principal principal) {
        try {
            ubr.setUser(usersService.findByUserName(principal.getName()));
            ubr.setBeer(beersService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        ubrService.createUserBeerRating(ubr);

        return String.format("redirect:%d/?rated", id);
    }

    @GetMapping("/beer/new")
    public String showNewBeerPage(Model model, Principal principal) {
        ThrowIfNotLoggedIn(principal);
        model.addAttribute("beerDto", new BeerDTO());
        model.addAttribute("countries", countriesService.getAll());
        model.addAttribute("styles", stylesService.getAll());
        model.addAttribute("breweries", breweriesService.getAll());
        model.addAttribute("tags", tagsService.getAll());

        return "newbeer";
    }

    @PostMapping("/beer/new")
    public String createBeer(@Valid @ModelAttribute("beerDto") BeerDTO beerDto,
                             BindingResult errors,
                             Model model,
                             Principal principal,
                             @RequestParam MultipartFile file) {

        ThrowIfNotLoggedIn(principal);

        trimInputBeerDTOFields(beerDto);

        try {
            beerDto.setCreatedById(usersService.findByUserName(principal.getName()).getId());
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }

        if (beerDto.getName().isEmpty()) {
            AddModelAttributesForBeerCreationPostMapping(model, "Beer name cannot be empty!");
            return "newbeer";
        }

        if (errors.hasErrors()) {
            AddModelAttributesForBeerCreationPostMapping(model, "Error!");
            return "newbeer";
        }

        beerDto.setImageUrl(imgService.uploadFile(file,
                beerDto.getClass().getSimpleName()));
        beersService.create(beerDto);

        return "redirect:/beers";
    }

    @GetMapping("/beers")
    public String showAllBeers(@RequestParam(defaultValue = "") String name,
                               @RequestParam(defaultValue = "") String style,
                               @RequestParam(defaultValue = "") String brewery,
                               @RequestParam(defaultValue = "") String country,
                               @RequestParam(defaultValue = "") String sortBy,
                               @RequestParam(defaultValue = "") String sortType,
                               Model model) {
        BeerSearch beerSearch = new BeerSearch();
        beerSearch.setBeerName(name);
        beerSearch.setStyleName(style);
        beerSearch.setBreweryName(brewery);
        beerSearch.setCountryName(country);
        beerSearch.setSortBy(sortBy);
        beerSearch.setSortType(sortType);

        model.addAttribute("beers", beersService.getAll(beerSearch));
        model.addAttribute("styles", stylesService.getAll());
        model.addAttribute("breweries", breweriesService.getAll());
        model.addAttribute("countries", countriesService.getAll());

        return "beers";
    }

    @GetMapping("/beer/{id}/update")
    public String showUpdateBeerPage(Model model,
                                     @PathVariable String id,
                                     Principal principal) {
        int beerId = parseId(id);

        checkHasAccess(principal, beerId);

        BeerDTO beerDTO;
        try {
            beerDTO = beerMapper.beerToDto(beersService.getById(beerId));
            model.addAttribute("countries", countriesService.getAll());
            model.addAttribute("breweries", breweriesService.getAll());
            model.addAttribute("styles", stylesService.getAll());
            model.addAttribute("tags", tagsService.getAll());
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        model.addAttribute("beerDto", beerDTO);

        return "update-beer";
    }

    @PostMapping("/beer/{id}/update")
    public String updateBeer(@ModelAttribute BeerDTO beerDTO,
                             @PathVariable String id,
                             @RequestParam MultipartFile file,
                             Principal principal) {
        int beerId = parseId(id);

        trimInputBeerDTOFields(beerDTO);

        checkHasAccess(principal, beerId);

        Beer oldBeer;
        try {
            oldBeer = beersService.getById(beerId);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }

        fillBeerDTO(beerDTO, file, oldBeer);
        Beer beerToUpdate;
        try {
            beerToUpdate = beerMapper.dtoToBeer(beerDTO);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }

        try {
            beersService.updateById(beerId, beerToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        return String.format("redirect:/beer/%d", beerId);
    }

    private void trimInputBeerDTOFields(@ModelAttribute BeerDTO beerDTO) {
        if (beerDTO.getName() != null) {
            beerDTO.setName(beerDTO.getName().trim());
        }
        if (beerDTO.getDescription() != null) {
            beerDTO.setDescription(beerDTO.getDescription().trim());
        }
    }

    @PostMapping("/beer/{id}/delete")
    public String deleteBeer(@Valid @ModelAttribute Beer beer,
                             @PathVariable String id,
                             Principal principal) {
        int beerId = parseId(id);

        checkHasAccess(principal, beerId);
        try {
            beersService.deleteById(beerId);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        return "redirect:/beers";
    }

    @PostMapping("/beer/{id}/addToWishList")
    public String addBeerToWishList(@Valid @ModelAttribute Beer beer,
                                    @PathVariable String id,
                                    Principal principal) {
        ThrowIfNotLoggedIn(principal);
        UserBeerStatusDTO ubsDTO = buildUbsDto(id, principal, STATUS_ID_WISHED);
        try {
            ubsService.createUserBeerStatus(ubsDTO);
        } catch (DuplicateRelationException e) {
            return "redirect:?alreadyWished";
        } catch (AnotherRelationExistsException e) {
            return "redirect:?alreadyInOtherList";
        }

        return "redirect:?successfullyWished";
    }

    @PostMapping("/beer/{id}/addToDrankList")
    public String addBeerToDrankList(@Valid @ModelAttribute Beer beer,
                                     @PathVariable String id,
                                     Principal principal) {
        ThrowIfNotLoggedIn(principal);
        UserBeerStatusDTO ubsDTO = buildUbsDto(id, principal, STATUS_ID_DRANK);
        try {
            ubsService.createUserBeerStatus(ubsDTO);
        } catch (DuplicateRelationException e) {
            return "redirect:?alreadyDrank";
        }

        return "redirect:?successfullyDrank";
    }

    private void AddModelAttributesForBeerCreationPostMapping(Model model, String s) {
        model.addAttribute("error", s);
        model.addAttribute("countries", countriesService.getAll());
        model.addAttribute("styles", stylesService.getAll());
        model.addAttribute("breweries", breweriesService.getAll());
        model.addAttribute("tags", tagsService.getAll());
    }

    private void ThrowIfNotLoggedIn(Principal principal) {
        if (principal == null) {
            throw new AccessUnauthorizedException();
        }
    }

    private UserBeerStatusDTO buildUbsDto(@PathVariable String id, Principal principal, int statusId) {
        int beerId = parseId(id);

        UserBeerStatusDTO ubsDTO = new UserBeerStatusDTO();
        try {
            ubsDTO.setBeerId(beersService.getById(beerId).getId());
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        try {
            ubsDTO.setUserId(usersService.findByUserName(principal.getName()).getId());
        } catch (EntityNotFoundException e) {
            throw new BadRequestException();
        }
        ubsDTO.setStatusId(statusId);
        return ubsDTO;
    }

    private void checkHasAccess(Principal principal, int beerId) {
        if (principal == null) {
            throw new AccessUnauthorizedException();
        }

        if (beerId != usersService.findByUserName(principal.getName()).getId() && !isAdmin()) {
            throw new AccessForbiddenException();
        }
    }

    private int parseId(@PathVariable String id) {
        int beerId;
        try {
            beerId = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        }
        return beerId;
    }

    private void fillBeerDTO(@ModelAttribute BeerDTO beerDTO,
                             @RequestParam MultipartFile file,
                             Beer oldBeer) {
        beerDTO.setId(oldBeer.getId());
        if (beerDTO.getDescription() == null) {
            if (oldBeer.getDescription() != null) {
                beerDTO.setDescription(oldBeer.getDescription());
            } else {
                beerDTO.setDescription("");
            }
        }

        if (file.isEmpty()) {
            beerDTO.setImageUrl(oldBeer.getImageURL());
        } else {
            beerDTO.setImageUrl(imgService.uploadFile(file, beerDTO.getClass().getSimpleName()));
        }

        beerDTO.setCreatedById(oldBeer.getCreatedBy().getId());
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private String handleBadRequestException() {
        return "error-400";
    }

    @ExceptionHandler(AccessUnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    private String handleAccessUnauthorizedException() {
        return "error-401";
    }

    @ExceptionHandler(AccessForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    private String handleAccessForbiddenException() {
        return "error-403";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    private String handleResourceNotFoundException() {
        return "error-404";
    }

    private boolean isAdmin() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_ADMIN"));
    }
}
