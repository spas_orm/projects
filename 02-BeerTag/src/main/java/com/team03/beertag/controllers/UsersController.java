package com.team03.beertag.controllers;

import com.team03.beertag.exceptions.*;
import com.team03.beertag.models.Country;
import com.team03.beertag.models.User;
import com.team03.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@ApiIgnore
@Controller
public class UsersController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UsersService usersService;
    private CountriesService countriesService;
    private ImageFileService imgService;
    private UsersBeersRatingService ubrService;
    private UsersBeersStatusService ubsService;

    @Autowired
    public UsersController(UserDetailsManager userDetailsManager,
                           PasswordEncoder passwordEncoder,
                           UsersService usersService,
                           CountriesService countriesService,
                           ImageFileService imgService,
                           UsersBeersRatingService ubrService,
                           UsersBeersStatusService ubsService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
        this.countriesService = countriesService;
        this.imgService = imgService;
        this.ubrService = ubrService;
        this.ubsService = ubsService;
    }

    @GetMapping("/login")
    public String showLoginForm() {
        return "login";
    }

    @GetMapping("/users")
    public String showAllUsers(Model model) {
        model.addAttribute("users", usersService.getAll());
//        model.addAttribute("ubrs", ubrService.getAll());
        return "users";
    }

    @GetMapping("/user/{id}")
    public String showBeer(@PathVariable String id, Model model) {
        int userId = parseId(id);

        try {
            model.addAttribute("user", usersService.getById(userId));
            model.addAttribute("rates", ubrService.getRatesByUser(usersService.getById(userId)));
            model.addAttribute("wishes", usersService.getWishListByUserId(userId));
            model.addAttribute("tastes", usersService.getDrankListByUserId(userId));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }

        return "user";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("countries", countriesService.getAll());
        return "register";
    }

    @PostMapping(value = "/register")
    public String registerUser(@ModelAttribute User user,
                               @RequestParam MultipartFile file,
                               Model model) throws IOException {
        trimInputUserFields(user);

        if (user.getUserName().isEmpty()) {
            AddModelAttributesForUserCreationPostMapping(model, "Username can't be empty!");
            return "register";
        }

        if (user.getPassword().isEmpty() || user.getPasswordConfirmation().isEmpty()) {
            AddModelAttributesForUserCreationPostMapping(model, "Password fields can't be empty!");
            return "register";
        }

        if (userDetailsManager.userExists(user.getUserName())) {
            AddModelAttributesForUserCreationPostMapping(model, "User with the same username already exists!");
            return "register";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            AddModelAttributesForUserCreationPostMapping(model, "Passwords do not match!");
            return "register";
        }

        if (user.getEmail().isEmpty()) {
            AddModelAttributesForUserCreationPostMapping(model, "Email field can`t be empty!");
            return "register";
        }

        String hashedPassword = passwordEncoder.encode(user.getPassword());

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUserName(),
                        hashedPassword,
//                        user.getPassword(),
                        authorities);
        userDetailsManager.createUser(newUser);

        int userId = usersService.findByUserName(user.getUserName()).getId();
        Country country = countriesService.getById(Integer.parseInt(user.getCountry().getName()));
        user.setCountry(country);
        user.setEnabled(true);
        user.setImageUrl(imgService.uploadFile(file,
                user.getClass().getSimpleName()));
        user.setPassword(hashedPassword);
        usersService.updateById(userId, user);

        return "redirect:login?registered";
    }

    @GetMapping("/user/{id}/update")
    public String showUpdateUserPage(Model model, @PathVariable String id, Principal principal) {
        int userId = parseId(id);

        checkUserAccess(principal, userId);

        try {
            model.addAttribute("user", usersService.getById(userId));
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }

        model.addAttribute("countries", countriesService.getAll());

        return "update-user";
    }

    @PostMapping("/user/{id}/update")
    public String updateUser(@Valid @ModelAttribute User user, @PathVariable String id, Model model,
                             @RequestParam MultipartFile file, Principal principal) {
        int userId = parseId(id);
        checkUserAccess(principal, userId);
        trimInputUserFields(user);

        User oldUser;
        try {
            oldUser = usersService.getById(userId);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        User userToUpdate = user;
        userToUpdate.setUserName(oldUser.getUserName());

        if (userToUpdate.getPassword().isEmpty() && userToUpdate.getPasswordConfirmation().isEmpty()) {
            userToUpdate.setPassword(oldUser.getPassword());
            userToUpdate.setPasswordConfirmation(oldUser.getPassword());
        } else if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Passwords do not match!");
            return "update-user";
        } else {
            String hashedPassword = passwordEncoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
        }

        if (file.isEmpty()) {
            userToUpdate.setImageUrl(oldUser.getImageUrl());
        } else {
            userToUpdate.setImageUrl(imgService.uploadFile(file, user.getClass().getSimpleName()));
        }

        if (user.getCountry() == null) {
            userToUpdate.setCountry(oldUser.getCountry());
        }

        userToUpdate.setCountry(countriesService.getById(Integer.parseInt(user.getCountry().getName())));
        user.setEnabled(true);

        try {
            usersService.updateById(userId, userToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        return String.format("redirect:/user/%d", userId);
    }

    @PostMapping("/user/{id}/delete")
    public String deleteUser(@Valid @ModelAttribute User user,
                             @PathVariable String id,
                             Principal principal) {
        int userId = parseId(id);

        checkUserAccess(principal, userId);
        try {
            usersService.deleteById(userId);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException();
        }
        return "redirect:/users";
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleBadRequestException() {
        return "error-400";
    }

    @ExceptionHandler(AccessUnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public String handleAccessUnauthorizedException() {
        return "error-401";
    }

    @ExceptionHandler(AccessForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleAccessForbiddenException() {
        return "error-403";
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handleResourceNotFoundException() {
        return "error-404";
    }

    private boolean isAdmin() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_ADMIN"));
    }

    private int parseId(@PathVariable String id) {
        int userId;
        try {
            userId = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        }
        return userId;
    }

    private void checkUserAccess(Principal principal, int userId) {
        if (principal == null) {
            throw new AccessUnauthorizedException();
        }

        if (userId != usersService.findByUserName(principal.getName()).getId() && !isAdmin()) {
            throw new AccessForbiddenException();
        }
    }

    private void trimInputUserFields(@ModelAttribute User user) {
        if(user.getUserName() != null) {
            user.setUserName(user.getUserName().trim());
        }
        if(user.getPassword() != null) {
            user.setPassword(user.getPassword().trim());
        }
        if(user.getPasswordConfirmation() != null) {
            user.setPasswordConfirmation(user.getPasswordConfirmation().trim());
        }
        if(user.getFirstName() != null) {
            user.setFirstName(user.getFirstName().trim());
        }
        if(user.getLastName() != null) {
            user.setLastName(user.getLastName().trim());
        }
        if(user.getEmail() != null) {
            user.setEmail(user.getEmail().trim());
        }
    }

    private void AddModelAttributesForUserCreationPostMapping(Model model, String errorMsg) {
        model.addAttribute("error", errorMsg);
        model.addAttribute("user", new User());
        model.addAttribute("countries", countriesService.getAll());
    }
}
