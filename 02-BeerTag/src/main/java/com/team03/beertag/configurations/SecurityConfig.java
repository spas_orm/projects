package com.team03.beertag.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private DataSource dataSource;

    @Autowired
    public SecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth
                .jdbcAuthentication()
                .dataSource(dataSource);
//                .passwordEncoder(encoder);
//                .usersByUsernameQuery("SELECT username, password, enabled FROM users where username = ?")
//                .authoritiesByUsernameQuery("select username, authority from authority where username=?");
    }




    //Disable security for REST controllers
    //TODO - remove for production
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/api/**");
        web.ignoring().antMatchers("/uploads/**");
//        web.ignoring().csrf()
//        web.ignoring().antMatchers("/error");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/uploads/**").permitAll();
//        http.authorizeRequests()
//                .antMatchers("/api/**")
//                .permitAll()
//                .and().csrf().disable();
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/")
                    .permitAll()
                .antMatchers("/index")
                    .permitAll()
                .antMatchers("/beers/**")
                    .permitAll()
                .antMatchers("/beer/new")
                    .hasAnyRole("USER","ADMIN")
                .antMatchers("/beer/**")
                    .permitAll()
                .antMatchers("/users")
                    .authenticated()
                .antMatchers("/user/**")
                    .hasAnyRole("USER","ADMIN")
                .and()
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/authenticate")
                    .permitAll()
                .and()
                .logout()
                    .permitAll()
                .and()
                    .exceptionHandling()
                    .accessDeniedPage("/access-denied");
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
