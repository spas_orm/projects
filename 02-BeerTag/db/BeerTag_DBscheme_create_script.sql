SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS countries;
DROP TABLE IF EXISTS breweries;
DROP TABLE IF EXISTS statuses;
DROP TABLE IF EXISTS styles;
DROP TABLE IF EXISTS beers;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS beers_tags;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS users_details;
DROP TABLE IF EXISTS beers_creation_details;
DROP TABLE IF EXISTS users_beers_ratings;
DROP TABLE IF EXISTS users_beers_statuses;
SET FOREIGN_KEY_CHECKS = 1;


create table countries
(
    country_id int auto_increment
        primary key,
    name       varchar(25)          not null,
    is_deleted tinyint(1) default 0 not null,
    constraint countries_ak_1
        unique (name, is_deleted)
);

create table breweries
(
    brewery_id int auto_increment
        primary key,
    name       varchar(25)          not null,
    country_id int                  not null,
    is_deleted tinyint(1) default 0 not null,
    constraint breweries_ak_1
        unique (name, is_deleted),
    constraint countries_breweries
        foreign key (country_id) references countries (country_id)
);

create table statuses
(
    status_id  int auto_increment
        primary key,
    name       varchar(25)          not null,
    is_deleted tinyint(1) default 0 not null,
    constraint statuses_ak_1
        unique (name, is_deleted),
    constraint statuses_name_uindex
        unique (name)
);

create table styles
(
    style_id   int auto_increment
        primary key,
    name       varchar(25)          not null,
    is_deleted tinyint(1) default 0 not null,
    constraint styles_ak_1
        unique (name, is_deleted)
);

create table beers
(
    beer_id     int auto_increment
        primary key,
    name        varchar(50)          not null,
    description text                 null,
    abv         double(3, 1)         not null,
    style_id    int                  not null,
    brewery_id  int                  not null,
    country_id  int                  not null,
    pic_url     varchar(2048)        null,
    is_deleted  tinyint(1) default 0 not null,
    constraint beers_ak_1
        unique (name, is_deleted),
    constraint beers_breweries
        foreign key (brewery_id) references breweries (brewery_id),
    constraint beers_styles
        foreign key (style_id) references styles (style_id),
    constraint countries_beers
        foreign key (country_id) references countries (country_id)
);

create table tags
(
    tag_id     int auto_increment
        primary key,
    name       varchar(25)          not null,
    is_deleted tinyint(1) default 0 not null,
    constraint tags_ak_1
        unique (name, is_deleted)
);

create table beers_tags
(
    beer_tag_id int auto_increment
        primary key,
    beer_id     int not null,
    tag_id      int not null,
    constraint beers_tags_beers
        foreign key (beer_id) references beers (beer_id),
    constraint beers_tags_tags
        foreign key (tag_id) references tags (tag_id)
);

create table users
(
    user_id         int auto_increment
        primary key,
    username        varchar(25)          not null,
    password        varchar(68)          not null,
    enabled         tinyint              not null,
    first_name      varchar(25)          null,
    last_name       varchar(25)          null,
    birth_date      date                 null,
    email           varchar(50)          null,
    profile_pic_url varchar(2048)        null,
    country_id      int                  null,
    is_deleted      tinyint(1) default 0 not null,
    constraint users_ak_1
        unique (username),
    constraint users_ak_2
        unique (email),
    constraint users_countries
        foreign key (country_id) references countries (country_id)
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);

create table beers_creation_details
(
    beer_creation_id int auto_increment
        primary key,
    user_id          int                                   not null,
    beer_id          int                                   not null,
    createdAt        timestamp default current_timestamp() not null on update current_timestamp(),
    constraint users_beers_creation_beers
        foreign key (beer_id) references beers (beer_id),
    constraint users_beers_creation_users
        foreign key (user_id) references users (user_id)
);

create table users_beers_ratings
(
    user_beer_rating_id int auto_increment
        primary key,
    user_id             int not null,
    beer_id             int not null,
    rating              int not null,
    constraint users_beers_ratings_ak
        unique (user_id, beer_id),
    constraint users_beers_ratings_beers
        foreign key (beer_id) references beers (beer_id),
    constraint users_beers_ratings_users
        foreign key (user_id) references users (user_id)
);

create table users_beers_statuses
(
    user_beer_status_id int auto_increment
        primary key,
    user_id             int                  not null,
    beer_id             int                  not null,
    status_id           int                  not null,
    is_deleted          tinyint(1) default 0 not null,
    constraint users_beers_statuses_ak
        unique (user_id, beer_id, status_id),
    constraint users_beers_drank_beers
        foreign key (beer_id) references beers (beer_id),
    constraint users_beers_drank_users
        foreign key (user_id) references users (user_id),
    constraint users_beers_statuses
        foreign key (status_id) references statuses (status_id)
);





