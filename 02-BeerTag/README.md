# BeerTag
*by Nikola Kostadinov & Spas Ormandzhiev*   
*Team: A16-Team-03*   
   
The application is designed and developed according to the functional requirements described in the [Spring Teamwork Assignment](https://learn.telerikacademy.com/mod/page/view.php?id=3161)

Link to the team [Trello](https://trello.com/b/E1DiHQmF/a16-team-03-beertag) 

To run the application you should:
1. Clone the project Git repository.
2. Run the DB scripts to create tables and populet them with data.
3. Edit the DB url, username and password fields in applicattion.properties file according to your configuration.  
4. Edit paths for storing file in the applicattion.properties file.
6. Build and run the applicataion

The application will be available on url [http://localhost:8080/](http://localhost:8080/)

The REST part of the appliactaion is documented with Swagger and is available on url [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) while the application is running.
    
	   
	   
### Home page
![Home page](Home.jpg)

### User register page
![Registe page](Register.jpg)

### Beers page
![Beers page](Beers.jpg)

### Beers page with filter and sort fileds
![Beers filter sort page](BeersFilterSort.jpg)

### Single beer page
![BeerDetails.jpg](BeerDetails.jpg)

### User profile page
Buttons are visible only for the Admin and the User itself
![User.jpg](User.jpg)
      
User's list for top 3 rated beers, wished and tasted beers are available in the profile with scrol-down.

![UserTopRatedBeers.jpg](UserTopRatedBeers.jpg)








