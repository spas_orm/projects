package com.a16team03;

import com.a16team03.wim.core.EngineImpl;

public final class Startup {

    public static void main(String[] args) {
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
