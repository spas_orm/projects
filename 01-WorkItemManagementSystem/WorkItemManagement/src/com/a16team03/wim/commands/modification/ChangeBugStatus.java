package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.enums.BugStatus;
import com.a16team03.wim.models.items.contracts.Bug;

import java.util.Arrays;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.BUG_STATUS_CHANGED_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.models.enums.BugStatus.ILLEGAL_ENUM_BUG_STATUS;

public final class ChangeBugStatus extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public ChangeBugStatus(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int bugID;
    private String newPropertyValue;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Bug currentBug = wimRepository.getBugById(bugID);
        changeBugStatus(currentBug, newPropertyValue);

        String activityMessage = String.format(BUG_STATUS_CHANGED_MESSAGE, bugID, newPropertyValue.toUpperCase());
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        currentBug.addActivity(activityToAdd);

        return activityMessage;
    }

    private void changeBugStatus(Bug currentBug, String newStatus) {
        if (Arrays.stream(BugStatus.values())
                .noneMatch(bugStatus -> bugStatus.name().equalsIgnoreCase(newStatus))) {
            throw new IllegalArgumentException(ILLEGAL_ENUM_BUG_STATUS);
        }
        currentBug.setStatus(BugStatus.valueOf(newStatus.toUpperCase()));
    }

    private void parseParameters(List<String> parameters) {
        try {
            bugID = Integer.parseInt(parameters.get(0));
            newPropertyValue = parameters.get(1);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
