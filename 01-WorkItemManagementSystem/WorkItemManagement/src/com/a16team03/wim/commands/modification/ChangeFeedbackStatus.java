package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.enums.FeedbackStatus;
import com.a16team03.wim.models.items.contracts.Feedback;

import java.util.Arrays;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class ChangeFeedbackStatus extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public ChangeFeedbackStatus(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int feedbackID;
    private String newPropertyValue;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Feedback feedbackToModify = wimRepository.getFeedbackById(feedbackID);
        changeFeedbackStatus(feedbackToModify, newPropertyValue);

        String activityMessage = String.format(FEEDBACK_STATUS_CHANGED_MESSAGE, feedbackID, newPropertyValue.toUpperCase());
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        feedbackToModify.addActivity(activityToAdd);

        return activityMessage;
    }

    private void changeFeedbackStatus(Feedback fbToModify, String newPropertyValue) {
        String newPropertyValueUC = newPropertyValue.toUpperCase();
        if (Arrays.stream(FeedbackStatus.values())
                .noneMatch(fbStatus -> fbStatus.name().equalsIgnoreCase(newPropertyValueUC))) {
            throw new IllegalArgumentException(FEEDBACK_ILLEGAL_RATING_MESSAGE);
        }
        fbToModify.setStatus(FeedbackStatus.valueOf(newPropertyValueUC));
    }

    private void parseParameters(List<String> parameters) {
        try {
            feedbackID = Integer.parseInt(parameters.get(0));
            newPropertyValue = parameters.get(1).toUpperCase();
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}