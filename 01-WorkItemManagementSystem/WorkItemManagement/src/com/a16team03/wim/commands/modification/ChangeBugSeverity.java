package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.enums.BugSeverity;
import com.a16team03.wim.models.items.contracts.Bug;

import java.util.Arrays;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.BUG_SEVERITY_CHANGED_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.models.enums.BugSeverity.ILLEGAL_ENUM_BUG_SEVERITY;

public final class ChangeBugSeverity extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public ChangeBugSeverity(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int bugID;
    private String newPropertyValue;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Bug currentBug = wimRepository.getBugById(bugID);
        changeBugSeverity(currentBug, newPropertyValue);

        String activityMessage = String.format(BUG_SEVERITY_CHANGED_MESSAGE, bugID, newPropertyValue.toUpperCase());
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        currentBug.addActivity(activityToAdd);

        return activityMessage;
    }

    private void changeBugSeverity(Bug currentBug, String newSeverity) {
        if (Arrays.stream(BugSeverity.values())
                .noneMatch(bugSeverity -> bugSeverity.name().equalsIgnoreCase(newSeverity))) {
            throw new IllegalArgumentException(ILLEGAL_ENUM_BUG_SEVERITY);
        }
        currentBug.setSeverity(BugSeverity.valueOf(newSeverity.toUpperCase()));
    }

    private void parseParameters(List<String> parameters) {
        try {
            bugID = Integer.parseInt(parameters.get(0));
            newPropertyValue = parameters.get(1);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
