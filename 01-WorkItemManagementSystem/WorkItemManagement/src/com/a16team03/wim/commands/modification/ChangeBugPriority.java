package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.enums.WorkItemPriority;
import com.a16team03.wim.models.items.contracts.Bug;

import java.util.Arrays;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.BUG_PRIORITY_CHANGED_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.models.enums.WorkItemPriority.ILLEGAL_ENUM_WORK_ITEM_PRIORITY;

public final class ChangeBugPriority extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public ChangeBugPriority(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int bugID;
    private String newPropertyValue;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Bug currentBug = wimRepository.getBugById(bugID);
        changeBugPriority(currentBug, newPropertyValue);

        String activityMessage = String.format(BUG_PRIORITY_CHANGED_MESSAGE, bugID, newPropertyValue.toUpperCase());
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        currentBug.addActivity(activityToAdd);

        return activityMessage;
    }

    private void changeBugPriority(Bug currentBug, String newPriority) {
        if (Arrays.stream(WorkItemPriority.values())
                .noneMatch(workItemPriority -> workItemPriority.name().equalsIgnoreCase(newPriority))) {
            throw new IllegalArgumentException(ILLEGAL_ENUM_WORK_ITEM_PRIORITY);
        }
        currentBug.setPriority(WorkItemPriority.valueOf(newPriority.toUpperCase()));
    }

    private void parseParameters(List<String> parameters) {
        try {
            bugID = Integer.parseInt(parameters.get(0));
            newPropertyValue = parameters.get(1);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}

