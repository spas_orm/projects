package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.contracts.WorkItem;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import com.a16team03.wim.models.teamunits.contracts.TeamUnit;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class AssignPersonToItem extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public AssignPersonToItem(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int itemIdToAssign;
    private String personToAssignName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        WorkItem itemToAssign = wimRepository.getAssignableWorkItemById(itemIdToAssign);
        Person personToAssign = wimRepository.getPersonByName(personToAssignName);

        checkIfWorkItemIsAlreadyAssignedToPerson(itemToAssign, personToAssign);
        checkIfWorkItemCanBeAssignedToPerson(itemToAssign, personToAssign);

        itemToAssign.addAssignee(personToAssign);

        String activityMessage = (String.format(ITEM_ASSIGNED_TO_PERSON_MESSAGE, itemIdToAssign, personToAssignName));
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        itemToAssign.addActivity(activityToAdd);
        personToAssign.addActivity(activityToAdd);

        return activityMessage;
    }

    private void checkIfWorkItemIsAlreadyAssignedToPerson(WorkItem itemToAssign, Person personToAssign) {
        if (itemToAssign.getAssignees().contains(personToAssign)) {
            throw new IllegalArgumentException(String.format(ITEM_ALREADY_ASSIGNED_MESSAGE,
                    itemIdToAssign, personToAssignName));
        }
    }

    private void checkIfWorkItemCanBeAssignedToPerson(WorkItem itemToAssign, Person personToAssign) {
        List<Team> teamsContainingThePerson = wimRepository.getTeams()
                .stream()
                .filter(team -> team.getMemberList().contains(personToAssign))
                .collect(Collectors.toList());

        if (teamsContainingThePerson.isEmpty()) {
            throw new IllegalArgumentException
                    (String.format(PERSON_NOT_A_MEMBER_OF_ANY_TEAM_MESSAGE, personToAssign.getName()));
        }

        boolean itemFound = teamsContainingThePerson
                .stream()
                .map(Team::getBoardList)
                .flatMap(Collection::stream)
                .map(TeamUnit::getItems)
                .flatMap(Collection::stream)
                .anyMatch(item -> item.equals(itemToAssign));

        if (!itemFound) {
            throw new IllegalArgumentException
                    (String.format(ITEM_AND_MEMBER_ARE_NOT_IN_THE_SAME_TEAM_MESSAGE,
                            itemToAssign.getTitle(),
                            personToAssign.getName()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            itemIdToAssign = Integer.parseInt(parameters.get(0));
            personToAssignName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
