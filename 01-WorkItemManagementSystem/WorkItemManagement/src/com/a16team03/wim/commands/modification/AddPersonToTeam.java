package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class AddPersonToTeam extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public AddPersonToTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private String personToBeAddedName;
    private String teamToAddToName;

    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Person personToBeAdded = wimRepository.getPersonByName(personToBeAddedName);

        Team teamToAddTo = wimRepository.getTeamByName(teamToAddToName);

        checkIfMemberAlreadyExistsInTeam(personToBeAdded, teamToAddTo);

        teamToAddTo.addMember(personToBeAdded);

        String activityMessage = String.format(PERSON_ADDED_TO_TEAM_MESSAGE, personToBeAddedName, teamToAddToName);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        personToBeAdded.addActivity(activityToAdd);
        teamToAddTo.addActivity(activityToAdd);

        return activityMessage;
    }

    private void checkIfMemberAlreadyExistsInTeam(Person personToBeAdded, Team teamToAddTo) {
        if (teamToAddTo.getMemberList().contains(personToBeAdded)) {
            throw new IllegalArgumentException(String.format(MEMBER_EXISTS_IN_TEAM_ERROR_MESSAGE,
                    personToBeAddedName, teamToAddToName));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            personToBeAddedName = parameters.get(0);
            teamToAddToName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
