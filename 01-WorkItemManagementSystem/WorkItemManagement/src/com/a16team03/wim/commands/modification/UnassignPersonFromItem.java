package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.contracts.WorkItem;
import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class UnassignPersonFromItem extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public UnassignPersonFromItem(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int itemIDToUnassign;
    private String personToUnassignName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Person personToUnassign = wimRepository.getPersonByName(personToUnassignName);
        WorkItem itemToUnassign = wimRepository.getAssignableWorkItemById(itemIDToUnassign);

        checkIfItemIsAssignedToPerson(itemToUnassign, personToUnassign);

        itemToUnassign.removeAssignee(personToUnassign);

        String activityMessage = String.format(PERSON_UNASSIGNED_FROM_ITEM_MESSAGE, personToUnassignName, itemIDToUnassign);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        itemToUnassign.addActivity(activityToAdd);
        personToUnassign.addActivity(activityToAdd);

        return activityMessage;
    }

    private void checkIfItemIsAssignedToPerson(WorkItem itemToUnassign, Person personToUnassign) {
        if (!itemToUnassign.getAssignees().contains(personToUnassign)) {
            throw new IllegalArgumentException
                    (String.format(PERSON_NOT_ASSIGNED_TO_ITEM_MESSAGE, itemIDToUnassign, personToUnassignName));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            itemIDToUnassign = Integer.parseInt(parameters.get(0));
            personToUnassignName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
