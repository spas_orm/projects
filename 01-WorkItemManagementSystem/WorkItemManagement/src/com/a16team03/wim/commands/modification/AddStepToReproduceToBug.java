package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.contracts.Bug;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class AddStepToReproduceToBug extends BaseCommandWithFactory {
    private static final int MIN_EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public AddStepToReproduceToBug(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int bugId;
    private String step;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, MIN_EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Bug bugToAddStep = wimRepository.getBugById(bugId);

        bugToAddStep.addStep(step);

        String activityMessage = String.format(BUG_STEP_TO_REPRODUCE_ADDED_MESSAGE, bugId);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        bugToAddStep.addActivity(activityToAdd);

        return activityMessage;
    }

    @Override
    protected void validateInput(List<String> parameters, int expectedNumberOfArguments) {
        if (parameters.size() < expectedNumberOfArguments) {
            throw new IllegalArgumentException(String.format(
                    INVALID_NUMBER_OF_ARGUMENTS,
                    expectedNumberOfArguments,
                    parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            bugId = Integer.parseInt(parameters.get(0));
            step = parameters.stream()
                    .skip(1)
                    .collect(Collectors.joining(" "));
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
