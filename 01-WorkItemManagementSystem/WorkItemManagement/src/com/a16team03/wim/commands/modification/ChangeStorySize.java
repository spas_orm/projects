package com.a16team03.wim.commands.modification;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.enums.StorySize;
import com.a16team03.wim.models.items.contracts.Story;

import java.util.Arrays;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.STORY_SIZE_CHANGED_MESSAGE;
import static com.a16team03.wim.models.enums.StorySize.ILLEGAL_ENUM_STORY_SIZE;

public final class ChangeStorySize extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public ChangeStorySize(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int storyID;
    private String newPropertyValue;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Story storyToModify = wimRepository.getStoryById(storyID);
        changeStorySize(storyToModify, newPropertyValue);

        String activityMessage = String.format(STORY_SIZE_CHANGED_MESSAGE, storyID, newPropertyValue.toUpperCase());
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        storyToModify.addActivity(activityToAdd);

        return activityMessage;
    }

    private void changeStorySize(Story storyToModify, String newPropertyValue) {
        String newPropertyValueUC = newPropertyValue.toUpperCase();
        if (Arrays.stream(StorySize.values())
                .noneMatch(storyStatus -> storyStatus.name().equalsIgnoreCase(newPropertyValueUC))) {
            throw new IllegalArgumentException(ILLEGAL_ENUM_STORY_SIZE);
        }
        storyToModify.setSize(StorySize.valueOf(newPropertyValueUC));
    }

    private void parseParameters(List<String> parameters) {
        try {
            storyID = Integer.parseInt(parameters.get(0));
            newPropertyValue = parameters.get(1).toUpperCase();
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
