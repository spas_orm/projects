package com.a16team03.wim.commands;

public final class CommandConstants {
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d.";
    public static final String FAILED_TO_PARSE_COMMAND_MESSAGE = "Failed to parse %s command parameters.";


    public static final String NEW_PERSON_CREATED_MESSAGE = "Person with name %s has been created.";
    public static final String NEW_TEAM_CREATED_MESSAGE = "Team with name %s has been created.";
    public static final String NEW_BOARD_CREATED_MESSAGE = "Board with name %s has been created.";

    public static final String PERSON_NOT_FOUND_ERROR_MESSAGE = "Person %s not found.";
    public static final String TEAM_NOT_FOUND_ERROR_MESSAGE = "Team %s not found.";

    public static final String PERSON_ADDED_TO_TEAM_MESSAGE = "Person %s was added to %s team.";
    public static final String BUG_ADDED_TO_BOARD_MESSAGE = "Bug %s added to board %s.";
    public static final String STORY_ADDED_TO_BOARD_MESSAGE = "Story %s added to board %s.";
    public static final String FEEDBACK_ADDED_TO_BOARD_MESSAGE = "Feedback %s added to board %s.";

    public static final String TEAM_EXISTS_ERROR_MESSAGE = "Team %s already exists.";
    public static final String MEMBER_EXISTS_IN_TEAM_ERROR_MESSAGE = "Member %s already exists in team %s.";
    public static final String BOARD_EXISTS_ERROR_MESSAGE = "Board %s already exists.";

    public static final String BOARD_DOES_NOT_EXIST = "Board %s does not exist.";

    public static final String TEAM_NO_BOARDS_MESSAGE = "Team %s has no boards.";
    public static final String TEAM_NO_MEMBERS_MESSAGE = "Team %s has no members.";
    public static final String NO_PEOPLE_MESSAGE = "There are no people registered in the system.";
    public static final String NO_TEAMS_MESSAGE = "There are no teams registered in the system.";
    public static final String PERSON_NOT_A_MEMBER_OF_ANY_TEAM_MESSAGE = "%s is not a member of any team.";
    public static final String ITEM_AND_MEMBER_ARE_NOT_IN_THE_SAME_TEAM_MESSAGE =
            "Item %s does not exist in any of the teams %s is a part of.";

    public static final String NO_TEAM_ACTIVITY_MESSAGE = "Team %s hs no activity history.";
    public static final String NO_PERSON_ACTIVITY_MESSAGE = "Person %s has no activity history.";
    public static final String NO_BOARD_ACTIVITY_MESSAGE = "Board %s has no activity history.";

    public static final String ITEM_NOT_FOUND_MESSAGE = "Item with ID %d does not exist.";
    public static final String ITEM_NOT_FOUND_OR_NOT_ASSIGNABLE_MESSAGE = "Item with ID %d does not exist or can not be assigned.";
    public static final String ITEM_ASSIGNED_TO_PERSON_MESSAGE = "Item with ID %d was assigned to %s.";
    public static final String ITEM_ALREADY_ASSIGNED_MESSAGE = "Item with ID %d is already assigned to %s.";
    public static final String PERSON_UNASSIGNED_FROM_ITEM_MESSAGE = "%s was unassigned from item with ID %d.";
    public static final String PERSON_NOT_ASSIGNED_TO_ITEM_MESSAGE =
            "Cannot unassign item with ID %d from %s, because it was not assigned in the first place.";

    public static final String BUG_NOT_FOUND_MESSAGE = "Bug with ID %d does not exist.";
    public static final String BUG_STATUS_CHANGED_MESSAGE = "Bug with ID %d status has been changed to '%s'.";
    public static final String BUG_PRIORITY_CHANGED_MESSAGE = "Bug with ID %d priority has been changed to '%s'.";
    public static final String BUG_SEVERITY_CHANGED_MESSAGE = "Bug with ID %d severity has been changed to '%s'.";
    public static final String BUG_STEP_TO_REPRODUCE_ADDED_MESSAGE = "A step to reproduce has been added to Bug with ID %d.";

    public static final String STORY_NOT_FOUND_MESSAGE = "Story with ID %d does not exist.";
    public static final String STORY_STATUS_CHANGED_MESSAGE = "Story with ID %d status has been changed to '%s'.";
    public static final String STORY_PRIORITY_CHANGED_MESSAGE = "Story with ID %d priority has been changed to '%s'.";
    public static final String STORY_SIZE_CHANGED_MESSAGE = "Story with ID %d size has been changed to '%s'.";

    public static final String FEEDBACK_STATUS_CHANGED_MESSAGE = "Feedback with ID %d status has been changed to '%s'.";
    public static final String FEEDBACK_RATING_CHANGED_MESSAGE = "Feedback with ID %d rating has been changed to '%s'.";
    public static final String FEEDBACK_ILLEGAL_RATING_MESSAGE = "Rating should be in the range from 0 to 10.";

    public static final String PERSON_ADDED_COMMENT_MESSAGE = "Comment added by %s.";

    public static final String NO_WORK_ITEMS_WITH_STATUS_MESSAGE = "There are no work items with status '%s'.";
    public static final String NO_WORK_ITEMS_ASSIGNED_TO_MESSAGE = "There are no work items assigned to %s.";
    public static final String NO_WORK_ITEMS_WITH_STATUS_AND_ASSIGNEE_MESSAGE =
            "There are no work items with status '%s' assigned to %s.";

    public static final String NO_ITEMS_IN_THE_APPLICATION_MESSAGE = "There are no items added to the application yet.";
    public static final String NO_BUG_ITEMS_IN_THE_APPLICATION_MESSAGE =
            "There are no bug items added to the application yet.";
    public static final String NO_FEEDBACK_ITEMS_IN_THE_APPLICATION_MESSAGE =
            "There are no feedback items added to the application yet.";
    public static final String NO_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE =
            "There are no story items added to the application yet.";
    public static final String NO_BUGS_OR_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE =
            "There are no bug/story items added to the application yet.";
}
