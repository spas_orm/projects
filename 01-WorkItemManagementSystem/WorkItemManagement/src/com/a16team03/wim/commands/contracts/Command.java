package com.a16team03.wim.commands.contracts;

import java.util.List;

public interface Command {
    String execute(List<String> parameters);
}
