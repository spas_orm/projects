package com.a16team03.wim.commands.enums;

public enum CommandType {
    ADDCOMMENTTOWORKITEM,
    ADDPERSONTOTEAM,
    ADDSTEPTOREPRODUCETOBUG,
    ASSIGNPERSONTOITEM,
    CHANGEBUGSTATUS,
    CHANGEBUGSEVERITY,
    CHANGEBUGPRIORITY,
    CHANGEFEEDBACKRATING,
    CHANGEFEEDBACKSTATUS,
    CHANGESTORYPRIORITY,
    CHANGESTORYSIZE,
    CHANGESTORYSTATUS,
    CREATENEWBOARDINTEAM,
    CREATENEWPERSON,
    CREATENEWBUGINBOARD,
    CREATENEWSTORYINBOARD,
    CREATENEWFEEDBACKINBOARD,
    CREATENEWTEAM,
    LISTALLBUGS,
    LISTALLFEEDBACKS,
    LISTALLSTORIES,
    LISTALLITEMS,
    LISTITEMSSORTEDBYPRIORITY,
    LISTITEMSSORTEDBYRATING,
    LISTITEMSSORTEDBYSEVERITY,
    LISTITEMSSORTEDBYSIZE,
    LISTITEMSSORTEDBYTITLE,
    LISTITEMSWITHASSIGNEE,
    LISTITEMSWITHSTATUS,
    LISTITEMSWITHSTATUSANDASSIGNEE,
    SHOWALLPEOPLE,
    SHOWALLTEAMBOARDS,
    SHOWALLTEAMMEMBERS,
    SHOWALLTEAMS,
    SHOWBOARDACTIVITY,
    SHOWPERSONACTIVITY,
    SHOWTEAMACTIVITY,
    UNASSIGNPERSONFROMITEM
}
