package com.a16team03.wim.commands;

import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;

public abstract class BaseCommandWithFactory extends BaseCommand {
    protected final WIMFactory wimFactory;

    protected BaseCommandWithFactory(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimRepository);
        this.wimFactory = wimFactory;
    }
}
