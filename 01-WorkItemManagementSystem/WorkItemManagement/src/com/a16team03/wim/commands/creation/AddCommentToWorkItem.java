package com.a16team03.wim.commands.creation;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.items.contracts.Item;
import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class AddCommentToWorkItem extends BaseCommandWithFactory implements Command {
    private static final int MIN_EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public AddCommentToWorkItem(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private int itemId;
    private String personAddingCommentName;
    private String comment;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, MIN_EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Item itemToComment = getItemById(wimRepository, itemId);
        Person personAddingComment = wimRepository.getPersonByName(personAddingCommentName);

        Comment commentToAdd = wimFactory.createComment(personAddingComment.getName(), comment);
        itemToComment.addComment(commentToAdd);

        String activityMessage = String.format(PERSON_ADDED_COMMENT_MESSAGE, personAddingCommentName);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        itemToComment.addActivity(activityToAdd);

        return activityMessage;
    }

    private Item getItemById(WIMRepository repository, int itemId) {
        return repository.getItems()
                .stream()
                .filter(item -> item.getId() == itemId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(ITEM_NOT_FOUND_MESSAGE, itemId)));
    }

    @Override
    protected void validateInput(List<String> parameters, int expectedNumberOfArguments) {
        if (parameters.size() < expectedNumberOfArguments) {
            throw new IllegalArgumentException(String.format(
                    INVALID_NUMBER_OF_ARGUMENTS,
                    expectedNumberOfArguments,
                    parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            itemId = Integer.parseInt(parameters.get(0));
            personAddingCommentName = parameters.get(1);
            comment = parameters.stream()
                    .skip(2)
                    .collect(Collectors.joining(" "));

        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
