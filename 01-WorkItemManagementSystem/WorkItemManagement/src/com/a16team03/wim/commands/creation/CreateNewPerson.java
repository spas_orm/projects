package com.a16team03.wim.commands.creation;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.NEW_PERSON_CREATED_MESSAGE;

public final class CreateNewPerson extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public CreateNewPerson(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private String personName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Person person = wimFactory.createNewPerson(personName);
        wimRepository.addPerson(person);

        String activityMessage = String.format(NEW_PERSON_CREATED_MESSAGE, personName);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        person.addActivity(activityToAdd);

        return activityMessage;
    }


    private void parseParameters(List<String> parameters) {
        try {
            personName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
