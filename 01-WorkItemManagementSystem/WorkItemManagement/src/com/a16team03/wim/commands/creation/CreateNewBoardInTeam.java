package com.a16team03.wim.commands.creation;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class CreateNewBoardInTeam extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public CreateNewBoardInTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private String boardName;
    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Team currentTeam = wimRepository.getTeamByName(teamName);

        checkIfBoardExistsInTeam(currentTeam, boardName);

        Board board = wimFactory.createBoard(boardName);
        wimRepository.addBoard(board);
        currentTeam.addBoard(board);

        String activityMessage = String.format(NEW_BOARD_CREATED_MESSAGE, boardName);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        board.addActivity(activityToAdd);
        currentTeam.addActivity(activityToAdd);

        return activityMessage;
    }

    private void checkIfBoardExistsInTeam(Team currentTeam, String boardName) {
        if (currentTeam
                .getBoardList()
                .stream()
                .anyMatch(board -> board.getName().equals(boardName))) {
            throw new IllegalArgumentException(String.format(BOARD_EXISTS_ERROR_MESSAGE, boardName));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            boardName = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
