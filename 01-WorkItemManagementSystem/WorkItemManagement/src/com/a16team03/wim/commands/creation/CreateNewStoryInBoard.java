package com.a16team03.wim.commands.creation;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.contracts.Story;
import com.a16team03.wim.models.teamunits.contracts.Board;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class CreateNewStoryInBoard extends BaseCommandWithFactory implements Command {
    private static int MIN_EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public CreateNewStoryInBoard(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private String boardName;
    private String storyTitle;
    private String storyDescription;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, MIN_EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Story currentStory = wimFactory.createStory(storyTitle, storyDescription);

        Board currentBoard = wimRepository.getBoardByName(boardName);

        currentBoard.addItem(currentStory);
        wimRepository.addStory(currentStory);

        String activityMessage = String.format(STORY_ADDED_TO_BOARD_MESSAGE, storyTitle, boardName);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        currentStory.addActivity(activityToAdd);
        currentBoard.addActivity(activityToAdd);

        return activityMessage;
    }

    @Override
    protected void validateInput(List<String> parameters, int expectedNumberOfArguments) {
        if (parameters.size() < expectedNumberOfArguments) {
            throw new IllegalArgumentException(String.format(
                    INVALID_NUMBER_OF_ARGUMENTS,
                    expectedNumberOfArguments,
                    parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            boardName = parameters.get(0);
            storyTitle = parameters.get(1);
            storyDescription = parameters.stream()
                    .skip(2)
                    .collect(Collectors.joining(" "));
        } catch (Exception e) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
