package com.a16team03.wim.commands.creation;

import com.a16team03.wim.commands.BaseCommandWithFactory;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class CreateNewTeam extends BaseCommandWithFactory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public CreateNewTeam(WIMFactory wimFactory, WIMRepository wimRepository) {
        super(wimFactory, wimRepository);
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        checkIfTeamAlreadyExists();

        Team team = wimFactory.createTeam(teamName);
        wimRepository.addTeam(team);

        String activityMessage = String.format(NEW_TEAM_CREATED_MESSAGE, teamName);
        Activity activityToAdd = wimFactory.createActivity(activityMessage);
        team.addActivity(activityToAdd);

        return activityMessage;
    }

    private void checkIfTeamAlreadyExists() {
        if (wimRepository.getTeams()
                .stream()
                .anyMatch(team -> team.getName().equals(teamName))) {
            throw new IllegalArgumentException(String.format(TEAM_EXISTS_ERROR_MESSAGE, teamName));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
