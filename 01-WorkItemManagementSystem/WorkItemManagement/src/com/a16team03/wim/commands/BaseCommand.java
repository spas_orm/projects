package com.a16team03.wim.commands;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;

import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public abstract class BaseCommand implements Command {
    protected final WIMRepository wimRepository;

    protected BaseCommand(WIMRepository wimRepository) {
        this.wimRepository = wimRepository;
    }

    protected void validateInput(List<String> parameters, int expectedNumberOfArguments) {
        if (parameters.size() != expectedNumberOfArguments) {
            throw new IllegalArgumentException(String.format(
                    INVALID_NUMBER_OF_ARGUMENTS,
                    expectedNumberOfArguments,
                    parameters.size()));
        }
    }
}
