package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class ShowPersonActivity extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ShowPersonActivity(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String personName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        List<Activity> personActivityHistory = getPersonActivityHistoryByName();

        return createListToPrint(personActivityHistory);
    }

    private List<Activity> getPersonActivityHistoryByName() {
        return wimRepository.getPersons()
                .stream()
                .filter(person -> person.getName().equals(personName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(PERSON_NOT_FOUND_ERROR_MESSAGE, personName)))
                .getActivityHistory();
    }

    private String createListToPrint(List<Activity> personActivity) {
        if (personActivity.isEmpty()) {
            return String.format(NO_PERSON_ACTIVITY_MESSAGE, personName);
        }

        return getPersonActivityHistoryByName().stream()
                .map(Object::toString)
                .collect(Collectors.joining());
    }

    private void parseParameters(List<String> parameters) {
        try {
            personName = parameters.get(0);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
