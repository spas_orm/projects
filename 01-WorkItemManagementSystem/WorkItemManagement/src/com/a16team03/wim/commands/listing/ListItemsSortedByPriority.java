package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.items.contracts.WorkItem;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.a16team03.wim.commands.CommandConstants.NO_BUGS_OR_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE;

public final class ListItemsSortedByPriority extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    public ListItemsSortedByPriority(WIMRepository wimRepository) {
        super(wimRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String output = listByPriority().toString();

        return output.isEmpty() ? NO_BUGS_OR_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE : output;
    }

    private StringBuilder listByPriority() {
        StringBuilder strB = new StringBuilder();
        strB.append(Stream.of(wimRepository.getBugs(), wimRepository.getStories())
                .flatMap(Collection::stream)
                .sorted((Comparator<WorkItem>)
                        (o1, o2) -> o1.getPriority().getPriorityLevel() < o2.getPriority().getPriorityLevel() ? 1 : -1)
                .map(Object::toString)
                .collect(Collectors.joining()));
        return strB;
    }
}
