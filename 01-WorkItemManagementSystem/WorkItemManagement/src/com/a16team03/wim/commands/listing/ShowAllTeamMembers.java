package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class ShowAllTeamMembers extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ShowAllTeamMembers(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Team teamToShow = wimRepository.getTeamByName(teamName);

        return createTeamMemberList(teamToShow);
    }

    private String createTeamMemberList(Team team) {
        List<Person> teamMemberList = team.getMemberList();

        if (teamMemberList.isEmpty()) {
            return String.format(TEAM_NO_MEMBERS_MESSAGE, teamName);
        }

        return String.format("Team %s has %d member%s:%n",
                teamName,
                teamMemberList.size(),
                teamMemberList.size() == 1 ? "" : "s") +

                teamMemberList.stream()
                        .map(Object::toString)
                        .sorted()
                        .collect(Collectors.joining());
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamName = String.valueOf(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }

}
