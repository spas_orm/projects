package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class ShowTeamActivity extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ShowTeamActivity(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        List<Activity> teamActivityHistory = getTeamActivityByName();

        return createTeamActivityList(teamActivityHistory);
    }

    private List<Activity> getTeamActivityByName() {
        return wimRepository.getTeams()
                .stream()
                .filter(team -> team.getName().equals(teamName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamName)))
                .getActivityHistory();
    }

    private String createTeamActivityList(List<Activity> teamActivity) {
        if (teamActivity.isEmpty()) {
            return NO_TEAM_ACTIVITY_MESSAGE;
        }

        return teamActivity.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format(FAILED_TO_PARSE_COMMAND_MESSAGE,
                    this.getClass().getSimpleName()));
        }
    }
}
