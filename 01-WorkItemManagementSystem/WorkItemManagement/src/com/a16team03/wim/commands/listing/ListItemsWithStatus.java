package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.enums.contracts.Status;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.NO_WORK_ITEMS_WITH_STATUS_MESSAGE;

public final class ListItemsWithStatus extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ListItemsWithStatus(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String statusValue;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        String output = listItemsWithStatus(statusValue).toString();

        return output.isEmpty() ? String.format(NO_WORK_ITEMS_WITH_STATUS_MESSAGE, statusValue) : output;
    }

    private StringBuilder listItemsWithStatus(String status) {
        StringBuilder strB = new StringBuilder();
        strB.append(wimRepository.getItems()
                .stream()
                .filter(item -> item.getStatus().equals(Status.valueOf(status)))
                .map(Object::toString)
                .collect(Collectors.joining()));
        return strB;
    }

    private void parseParameters(List<String> parameters) {
        try {
            statusValue = parameters.get(0);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
