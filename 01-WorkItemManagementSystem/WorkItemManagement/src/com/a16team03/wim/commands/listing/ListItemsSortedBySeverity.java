package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_BUG_ITEMS_IN_THE_APPLICATION_MESSAGE;

public final class ListItemsSortedBySeverity extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    public ListItemsSortedBySeverity(WIMRepository wimRepository) {
        super(wimRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String output = listBugsBySeverity().toString();

        return output.isEmpty() ? NO_BUG_ITEMS_IN_THE_APPLICATION_MESSAGE : output;
    }

    private StringBuilder listBugsBySeverity() {
        StringBuilder strB = new StringBuilder();
        strB.append(wimRepository.getBugs()
                .stream()
                .sorted((o1, o2) -> o1.getSeverity().getSeverityLevel() < o2.getSeverity().getSeverityLevel() ? 1 : -1)
                .map(Object::toString)
                .collect(Collectors.joining()));
        return strB;
    }
}
