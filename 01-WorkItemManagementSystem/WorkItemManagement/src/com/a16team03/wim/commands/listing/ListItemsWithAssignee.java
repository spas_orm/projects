package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.a16team03.wim.commands.CommandConstants.FAILED_TO_PARSE_COMMAND_MESSAGE;
import static com.a16team03.wim.commands.CommandConstants.NO_WORK_ITEMS_ASSIGNED_TO_MESSAGE;

public final class ListItemsWithAssignee extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ListItemsWithAssignee(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String assigneeName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Person assignee = wimRepository.getPersonByName(assigneeName);

        String output = listItemsWithAssignee(assignee).toString();

        return output.isEmpty() ? String.format(NO_WORK_ITEMS_ASSIGNED_TO_MESSAGE, assigneeName) : output;
    }

    private StringBuilder listItemsWithAssignee(Person assignee) {
        StringBuilder strB = new StringBuilder();
        strB.append(Stream.of(wimRepository.getBugs(), wimRepository.getStories())
                .flatMap(Collection::stream)
                .filter(workItem -> (workItem.getAssignees().contains(assignee)))
                .map(Object::toString)
                .collect(Collectors.joining()));
        return strB;
    }

    private void parseParameters(List<String> parameters) {
        try {
            assigneeName = parameters.get(0);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
