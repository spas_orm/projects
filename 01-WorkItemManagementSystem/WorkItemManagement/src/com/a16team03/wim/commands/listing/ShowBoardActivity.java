package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.contracts.Activity;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class ShowBoardActivity extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ShowBoardActivity(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String boardName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        List<Activity> boardActivityHistory = getBoardActivityByName();

        return createListToPrint(boardActivityHistory);
    }

    private List<Activity> getBoardActivityByName() {
        return wimRepository.getBoards()
                .stream()
                .filter(board -> board.getName().equals(boardName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(BOARD_DOES_NOT_EXIST, boardName)))
                .getActivityHistory();
    }

    private String createListToPrint(List<Activity> boardActivityHistory) {
        if (boardActivityHistory.isEmpty()) {
            return String.format(NO_BOARD_ACTIVITY_MESSAGE, boardName);
        }

        return boardActivityHistory.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
    }

    private void parseParameters(List<String> parameters) {
        try {
            boardName = parameters.get(0);
        } catch (Exception ex) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }

}
