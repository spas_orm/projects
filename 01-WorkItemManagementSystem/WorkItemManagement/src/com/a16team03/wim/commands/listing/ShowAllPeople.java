package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_PEOPLE_MESSAGE;

public final class ShowAllPeople extends BaseCommand implements Command {
    public ShowAllPeople(WIMRepository wimRepository) {
        super(wimRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        return createPeopleList(wimRepository.getPersons());
    }

    private String createPeopleList(List<Person> peopleList) {
        String output = "";
        String listOfPeople = (peopleList.stream()
                .map(Object::toString)
                .sorted()
                .collect(Collectors.joining()));

        if (peopleList.isEmpty()) {
            return NO_PEOPLE_MESSAGE;
        } else if (peopleList.size() == 1) {
            output = "There is 1 person registered in the System:\n";
        } else {
            output = String.format("There are %s people registered in the System:\n", peopleList.size());
        }
        return output.concat(listOfPeople);
    }

}
