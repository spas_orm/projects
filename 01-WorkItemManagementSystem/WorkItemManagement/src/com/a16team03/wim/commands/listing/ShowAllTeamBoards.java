package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class ShowAllTeamBoards extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public ShowAllTeamBoards(WIMRepository wimRepository) {
        super(wimRepository);
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Team currentTeam = wimRepository.getTeamByName(teamName);

        return createListToPrint(currentTeam);
    }

    private String createListToPrint(Team team) {
        List<Board> teamBoardList = team.getBoardList();

        if (teamBoardList.isEmpty()) {
            return String.format(TEAM_NO_BOARDS_MESSAGE, team.getName());
        }

        return String.format("Team %s has %d board%s:%n",
                teamName,
                teamBoardList.size(),
                teamBoardList.size() == 1 ? "" : "s") +

                teamBoardList.stream()
                .map(Object::toString)
                .collect(Collectors.joining());
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamName = String.valueOf(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException
                    (String.format(FAILED_TO_PARSE_COMMAND_MESSAGE, this.getClass().getSimpleName()));
        }
    }
}
