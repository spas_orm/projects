package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.items.contracts.Item;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_ITEMS_IN_THE_APPLICATION_MESSAGE;

public final class ListItemsSortedByTitle extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;


    public ListItemsSortedByTitle(WIMRepository wimRepository) {
        super(wimRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String output = listItemsByTitle().toString();

        return output.isEmpty() ? NO_ITEMS_IN_THE_APPLICATION_MESSAGE : output;
    }

    private StringBuilder listItemsByTitle() {
        StringBuilder strB = new StringBuilder();
        strB.append(wimRepository.getItems()
                .stream()
                .sorted(Comparator.comparing(Item::getTitle))
                .map(Object::toString)
                .collect(Collectors.joining()));
        return strB;
    }
}
