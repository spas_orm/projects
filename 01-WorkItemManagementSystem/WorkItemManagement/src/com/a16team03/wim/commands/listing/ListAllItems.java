package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_ITEMS_IN_THE_APPLICATION_MESSAGE;

public final class ListAllItems extends BaseCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    public ListAllItems(WIMRepository wimRepository) {
        super(wimRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String output = listAll().toString();

        return output.isEmpty() ? NO_ITEMS_IN_THE_APPLICATION_MESSAGE : output;
    }

    private StringBuilder listAll() {
        StringBuilder strB = new StringBuilder();
        return strB.append(wimRepository.getItems()
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining()));
    }
}
