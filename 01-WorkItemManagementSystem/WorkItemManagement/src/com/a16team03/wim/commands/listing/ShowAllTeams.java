package com.a16team03.wim.commands.listing;

import com.a16team03.wim.commands.BaseCommand;
import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_TEAMS_MESSAGE;

public final class ShowAllTeams extends BaseCommand implements Command {
    public ShowAllTeams(WIMRepository wimRepository) {
        super(wimRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        return createListToPrint();
    }

    private String createListToPrint() {
        List<Team> teamList = wimRepository.getTeams();

        if (teamList.isEmpty()) {
            return NO_TEAMS_MESSAGE;
        }

        return String.format("There %s %d team%s registered in the System:%n",
                teamList.size() == 1 ? "is" : "are",
                teamList.size(),
                teamList.size() == 1 ? "" : "s") +

                teamList.stream()
                        .map(Object::toString)
                        .sorted()
                        .collect(Collectors.joining());
    }
}
