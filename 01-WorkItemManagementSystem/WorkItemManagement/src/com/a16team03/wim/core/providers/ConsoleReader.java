package com.a16team03.wim.core.providers;

import com.a16team03.wim.core.contracts.Reader;

import java.util.Scanner;

public final class ConsoleReader implements Reader {
    private final Scanner scanner;

    public ConsoleReader() {
        scanner = new Scanner(System.in);
    }

    @Override
    public String readLine() {
        return scanner.nextLine();
    }
}
