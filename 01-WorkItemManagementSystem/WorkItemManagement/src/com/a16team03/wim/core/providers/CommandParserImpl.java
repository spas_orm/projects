package com.a16team03.wim.core.providers;

import com.a16team03.wim.core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class CommandParserImpl implements CommandParser {
    @Override
    public String parseCommand(String fullCommand) {
        return fullCommand.split(" ")[0];
    }

    @Override
    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split(" ");
        return new ArrayList<>(Arrays.asList(commandParts).subList(1, commandParts.length));
    }
}
