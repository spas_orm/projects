package com.a16team03.wim.core.providers;

import com.a16team03.wim.core.contracts.Writer;

public final class ConsoleWriter implements Writer {
    @Override
    public void write(String message) {
        System.out.print(message);
    }

    @Override
    public void writeLine(String message) {
        System.out.println(message);
    }
}
