package com.a16team03.wim.core;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.core.contracts.*;
import com.a16team03.wim.core.factories.CommandFactoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.core.providers.CommandParserImpl;
import com.a16team03.wim.core.providers.ConsoleReader;
import com.a16team03.wim.core.providers.ConsoleWriter;

import java.util.List;

public final class EngineImpl implements Engine {
    private static final String NULL_OR_EMPTY_COMMAND_EXCEPTION_MESSAGE = "Command cannot be null or empty.";

    private static final String TERMINATION_COMMAND = "Exit";

    private final CommandFactory commandFactory;
    private final CommandParser commandParser;
    private final Reader reader;
    private final Writer writer;
    private final WIMFactory WIMFactory;
    private final WIMRepository WIMRepository;

    public EngineImpl() {
        commandFactory = new CommandFactoryImpl();
        commandParser = new CommandParserImpl();
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        WIMFactory = new WIMFactoryImpl();
        WIMRepository = new WIMRepositoryImpl();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().isEmpty()) {
            throw new IllegalArgumentException(NULL_OR_EMPTY_COMMAND_EXCEPTION_MESSAGE);
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, WIMFactory, WIMRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
