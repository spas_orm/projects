package com.a16team03.wim.core.factories;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.creation.*;
import com.a16team03.wim.commands.enums.CommandType;
import com.a16team03.wim.commands.listing.*;
import com.a16team03.wim.commands.modification.*;
import com.a16team03.wim.core.contracts.CommandFactory;
import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.core.contracts.WIMRepository;

public final class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s.";

    @Override
    public Command createCommand(String commandTypeAsString, WIMFactory wimFactory, WIMRepository wimRepository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType) {
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(wimFactory, wimRepository);
            case CREATENEWBOARDINTEAM:
                return new CreateNewBoardInTeam(wimFactory, wimRepository);
            case CREATENEWBUGINBOARD:
                return new CreateNewBugInBoard(wimFactory, wimRepository);
            case CREATENEWSTORYINBOARD:
                return new CreateNewStoryInBoard(wimFactory, wimRepository);
            case CREATENEWFEEDBACKINBOARD:
                return new CreateNewFeedbackInBoard(wimFactory, wimRepository);
            case CREATENEWPERSON:
                return new CreateNewPerson(wimFactory, wimRepository);
            case CREATENEWTEAM:
                return new CreateNewTeam(wimFactory, wimRepository);

            case LISTALLBUGS:
                return new ListAllBugs(wimRepository);
            case LISTALLFEEDBACKS:
                return new ListAllFeedbacks(wimRepository);
            case LISTALLSTORIES:
                return new ListAllStories(wimRepository);
            case LISTALLITEMS:
                return new ListAllItems(wimRepository);
            case LISTITEMSSORTEDBYPRIORITY:
                return new ListItemsSortedByPriority(wimRepository);
            case LISTITEMSSORTEDBYRATING:
                return new ListItemsSortedByRating(wimRepository);
            case LISTITEMSSORTEDBYSEVERITY:
                return new ListItemsSortedBySeverity(wimRepository);
            case LISTITEMSSORTEDBYSIZE:
                return new ListItemsSortedBySize(wimRepository);
            case LISTITEMSSORTEDBYTITLE:
                return new ListItemsSortedByTitle(wimRepository);
            case LISTITEMSWITHASSIGNEE:
                return new ListItemsWithAssignee(wimRepository);
            case LISTITEMSWITHSTATUS:
                return new ListItemsWithStatus(wimRepository);
            case LISTITEMSWITHSTATUSANDASSIGNEE:
                return new ListItemsWithStatusAndAssignee(wimRepository);
            case SHOWALLPEOPLE:
                return new ShowAllPeople(wimRepository);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoards(wimRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(wimRepository);
            case SHOWALLTEAMS:
                return new ShowAllTeams(wimRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(wimRepository);
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivity(wimRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivity(wimRepository);

            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(wimFactory, wimRepository);
            case ADDSTEPTOREPRODUCETOBUG:
                return new AddStepToReproduceToBug(wimFactory, wimRepository);
            case ASSIGNPERSONTOITEM:
                return new AssignPersonToItem(wimFactory, wimRepository);
            case CHANGEBUGPRIORITY:
                return new ChangeBugPriority(wimFactory, wimRepository);
            case CHANGEBUGSTATUS:
                return new ChangeBugStatus(wimFactory, wimRepository);
            case CHANGEBUGSEVERITY:
                return new ChangeBugSeverity(wimFactory, wimRepository);
            case CHANGEFEEDBACKRATING:
                return new ChangeFeedbackRating(wimFactory, wimRepository);
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedbackStatus(wimFactory, wimRepository);
            case CHANGESTORYPRIORITY:
                return new ChangeStoryPriority(wimFactory, wimRepository);
            case CHANGESTORYSIZE:
                return new ChangeStorySize(wimFactory, wimRepository);
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatus(wimFactory, wimRepository);
            case UNASSIGNPERSONFROMITEM:
                return new UnassignPersonFromItem(wimFactory, wimRepository);

        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
