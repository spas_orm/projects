package com.a16team03.wim.core.factories;

import com.a16team03.wim.core.contracts.WIMFactory;
import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.CommentImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.items.*;
import com.a16team03.wim.models.items.contracts.*;
import com.a16team03.wim.models.teamunits.*;
import com.a16team03.wim.models.teamunits.contracts.*;

public final class WIMFactoryImpl implements WIMFactory {
    public WIMFactoryImpl() {
    }

    public Team createTeam(String teamName) {
        return new TeamImpl(teamName);
    }

    public Person createNewPerson(String personName) {
        return new PersonImpl(personName);
    }

    public Board createBoard(String boardName) {
        return new BoardImpl(boardName);
    }

    public Bug createBug(String bugTitle, String bugDescription) {
        return new BugImpl(bugTitle, bugDescription);
    }

    public Story createStory(String storyTitle, String storyDescription) {
        return new StoryImpl(storyTitle, storyDescription);
    }

    public Feedback createFeedback(String feedbackTitle, String feedbackDescription) {
        return new FeedbackImpl(feedbackTitle, feedbackDescription);
    }

    public Activity createActivity(String activityMessage) {
        return new ActivityImpl(activityMessage);
    }

    public Comment createComment(String author, String comment) {
        return new CommentImpl(author, comment);
    }
}
