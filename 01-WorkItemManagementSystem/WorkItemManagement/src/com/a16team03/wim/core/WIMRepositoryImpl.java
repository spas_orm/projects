package com.a16team03.wim.core;

import com.a16team03.wim.core.contracts.WIMRepository;
import com.a16team03.wim.models.items.contracts.*;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class WIMRepositoryImpl implements WIMRepository {
    private final List<Person> persons = new ArrayList<>();
    private final List<Team> teams = new ArrayList<>();
    private final List<Board> boards = new ArrayList<>();

    private final List<Item> items = new ArrayList<>();
    private final List<Bug> bugs = new ArrayList<>();
    private final List<Story> stories = new ArrayList<>();
    private final List<Feedback> feedbacks = new ArrayList<>();

    public List<Person> getPersons() {
        return new ArrayList<>(persons);
    }

    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    public List<Item> getItems() {
        return new ArrayList<>(items);
    }

    public List<Bug> getBugs() {
        return new ArrayList<>(bugs);
    }

    public List<Story> getStories() {
        return new ArrayList<>(stories);
    }

    public List<Feedback> getFeedbacks() {
        return new ArrayList<>(feedbacks);
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    public void addTeam(Team team) {
        teams.add(team);
    }

    public void addBoard(Board board) {
        boards.add(board);
    }

    public void addBug(Bug bug) {
        bugs.add(bug);
        addItem(bug);
    }

    public void addStory(Story story) {
        stories.add(story);
        addItem(story);
    }

    public void addFeedback(Feedback feedback) {
        feedbacks.add(feedback);
        addItem(feedback);
    }

    private void addItem(Item item) {
        items.add(item);
    }

    public Person getPersonByName(String name) {
        return getPersons()
                .stream()
                .filter(person -> person.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(PERSON_NOT_FOUND_ERROR_MESSAGE, name)));
    }

    public Story getStoryById(int itemId) {
        return getStories()
                .stream()
                .filter(item -> item.getId() == itemId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(STORY_NOT_FOUND_MESSAGE, itemId)));
    }

    public Bug getBugById(int itemId) {
        return getBugs()
                .stream()
                .filter(bug -> bug.getId() == itemId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(BUG_NOT_FOUND_MESSAGE, itemId)));
    }

    public Feedback getFeedbackById(int itemId) {
        return getFeedbacks()
                .stream()
                .filter(item -> item.getId() == itemId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(STORY_NOT_FOUND_MESSAGE, itemId)));
    }

    public Board getBoardByName(String boardName) {
        return getBoards()
                .stream()
                .filter(board -> board.getName().equals(boardName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format(BOARD_DOES_NOT_EXIST, boardName)));
    }

    public Team getTeamByName(String teamName) {
        return getTeams()
                .stream()
                .filter(team -> team.getName().equals(teamName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException
                        (String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamName)));
    }

    public WorkItem getAssignableWorkItemById(int itemId) {
        return Stream.of(getBugs(), getStories())
                .flatMap(Collection::stream)
                .filter(item -> item.getId() == itemId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format(ITEM_NOT_FOUND_OR_NOT_ASSIGNABLE_MESSAGE, itemId)));
    }

}
