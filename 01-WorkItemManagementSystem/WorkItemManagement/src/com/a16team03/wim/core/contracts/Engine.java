package com.a16team03.wim.core.contracts;

public interface Engine {
    void start();
}
