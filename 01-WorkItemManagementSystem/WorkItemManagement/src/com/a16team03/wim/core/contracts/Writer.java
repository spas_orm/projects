package com.a16team03.wim.core.contracts;

public interface Writer {
    void write(String message);

    void writeLine(String message);
}
