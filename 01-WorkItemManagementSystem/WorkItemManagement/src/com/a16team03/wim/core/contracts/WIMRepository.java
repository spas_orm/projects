package com.a16team03.wim.core.contracts;

import com.a16team03.wim.models.items.contracts.*;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;

import java.util.List;

public interface WIMRepository {
    List<Person> getPersons();

    List<Team> getTeams();

    List<Board> getBoards();

    List<Item> getItems();

    List<Bug> getBugs();

    List<Story> getStories();

    List<Feedback> getFeedbacks();

    void addPerson(Person person);

    void addTeam(Team team);

    void addBoard(Board board);

    void addBug(Bug bug);

    void addStory(Story story);

    void addFeedback(Feedback feedback);

    Person getPersonByName(String name);

    Board getBoardByName(String boardName);

    Team getTeamByName(String teamName);

    Story getStoryById(int itemId);

    Bug getBugById(int bugId);

    Feedback getFeedbackById(int itemId);

    WorkItem getAssignableWorkItemById(int itemId);
}
