package com.a16team03.wim.core.contracts;

import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.items.contracts.*;
import com.a16team03.wim.models.teamunits.contracts.*;

public interface WIMFactory {
    Team createTeam(String teamName);

    Person createNewPerson(String personName);

    Board createBoard(String boardName);

    Bug createBug(String bugTitle, String bugDescription);

    Story createStory(String storyTitle, String storyDescription);

    Feedback createFeedback(String feedbackTitle, String feedbackDescription);

    Activity createActivity(String activityMessage);

    Comment createComment(String author, String comment);
}
