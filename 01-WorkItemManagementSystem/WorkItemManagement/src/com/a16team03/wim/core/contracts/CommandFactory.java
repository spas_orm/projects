package com.a16team03.wim.core.contracts;

import com.a16team03.wim.commands.contracts.Command;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, WIMFactory wimFactory, WIMRepository wimRepository);
}
