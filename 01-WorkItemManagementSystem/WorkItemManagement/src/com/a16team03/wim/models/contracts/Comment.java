package com.a16team03.wim.models.contracts;

public interface Comment {
    String getAuthor();

    String getComment();

    String getTimestamp();
}
