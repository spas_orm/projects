package com.a16team03.wim.models.contracts;

public interface Activity {
    String getActivityLogMessage();

    String getTimestamp();
}
