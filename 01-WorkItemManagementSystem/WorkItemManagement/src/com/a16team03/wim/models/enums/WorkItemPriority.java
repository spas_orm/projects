package com.a16team03.wim.models.enums;

public enum WorkItemPriority {
    LOW(1),
    MEDIUM(2),
    HIGH(3),
    UNSET(0);

    public static final String ILLEGAL_ENUM_WORK_ITEM_PRIORITY = "Illegal enum WorkItemPriority value.";
    private final int priorityLevel;

    WorkItemPriority(int priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public int getPriorityLevel() {
        return priorityLevel;
    }

    @Override
    public String toString() {
        switch (this) {
            case LOW:
                return "Low";
            case MEDIUM:
                return "Medium";
            case HIGH:
                return "High";
            case UNSET:
                return "Not set yet";
            default:
                throw new IllegalArgumentException(ILLEGAL_ENUM_WORK_ITEM_PRIORITY);
        }
    }
}
