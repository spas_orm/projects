package com.a16team03.wim.models.enums;

public enum StorySize {
    SMALL(1),
    MEDIUM(2),
    LARGE(3),
    UNSET(0);

    public static final String ILLEGAL_ENUM_STORY_SIZE = "Illegal enum StorySize value.";
    private final int storySize;

    StorySize(int storySize) {
        this.storySize = storySize;
    }

    public int getStorySize() {
        return storySize;
    }

    @Override
    public String toString() {
        switch (this) {
            case SMALL:
                return "Small";
            case MEDIUM:
                return "Medium";
            case LARGE:
                return "Large";
            case UNSET:
                return "Not set yet";
            default:
                throw new IllegalArgumentException(ILLEGAL_ENUM_STORY_SIZE);
        }
    }
}
