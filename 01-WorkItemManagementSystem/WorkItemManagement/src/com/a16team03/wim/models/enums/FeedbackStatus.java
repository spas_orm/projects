package com.a16team03.wim.models.enums;

import com.a16team03.wim.models.enums.contracts.Status;

public enum FeedbackStatus implements Status {
    NEW(2),
    UNSCHEDULED(1),
    SCHEDULED(3),
    DONE(0);

    private static final String ILLEGAL_ENUM_FEEDBACK_STATUS = "Illegal enum FeedbackStatus value.";
    private final int feedbackStatusLevel;

    FeedbackStatus(int feedbackStatusLevel) {
        this.feedbackStatusLevel = feedbackStatusLevel;
    }

    public int getFeedbackStatusLevel() {
        return feedbackStatusLevel;
    }

    @Override
    public String toString() {
        switch (this) {
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException(ILLEGAL_ENUM_FEEDBACK_STATUS);
        }
    }
}
