package com.a16team03.wim.models.enums;

public enum BugSeverity {
    MINOR(1),
    MAJOR(2),
    CRITICAL(3),
    UNSET(0);

    public static final String ILLEGAL_ENUM_BUG_SEVERITY = "Illegal enum BugSeverity value.";
    private final int severityLevel;

    BugSeverity(int severityLevel) {
        this.severityLevel = severityLevel;
    }

    public int getSeverityLevel() {
        return severityLevel;
    }

    @Override
    public String toString() {
        switch (this) {
            case MINOR:
                return "Minor";
            case MAJOR:
                return "Major";
            case CRITICAL:
                return "Critical";
            case UNSET:
                return "Not set yet";
            default:
                throw new IllegalArgumentException(ILLEGAL_ENUM_BUG_SEVERITY);
        }
    }
}
