package com.a16team03.wim.models.enums;

import com.a16team03.wim.models.enums.contracts.Status;

public enum StoryStatus implements Status {
    NOTDONE(1),
    INPROGRESS(2),
    DONE(0);

    public static final String ILLEGAL_ENUM_STORY_STATUS = "Illegal enum StoryStatus value.";
    private final int storyStatusLevel;

    StoryStatus(int storyStatusLevel) {
        this.storyStatusLevel = storyStatusLevel;
    }

    public int getStoryStatusLevel() {
        return storyStatusLevel;
    }

    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "NotDone";
            case INPROGRESS:
                return "InProgress";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException(ILLEGAL_ENUM_STORY_STATUS);
        }
    }
}
