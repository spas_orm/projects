package com.a16team03.wim.models.enums.contracts;

import com.a16team03.wim.models.enums.BugStatus;
import com.a16team03.wim.models.enums.FeedbackStatus;
import com.a16team03.wim.models.enums.StoryStatus;

public interface Status {
    String ENUM_CONSTANT_DOES_NOT_EXIST = "Enum constant %s does not exist.";

    static <T extends Status> Status valueOf(String input) {
        for (StoryStatus storyStatus : StoryStatus.values()) {
            if (storyStatus.name().equalsIgnoreCase(input)) {
                return storyStatus;
            }
        }

        for (BugStatus bugStatus : BugStatus.values()) {
            if (bugStatus.name().equalsIgnoreCase(input)) {
                return bugStatus;
            }
        }

        for (FeedbackStatus feedbackStatus : FeedbackStatus.values()) {
            if (feedbackStatus.name().equalsIgnoreCase(input)) {
                return feedbackStatus;
            }
        }
        throw new IllegalArgumentException(String.format(ENUM_CONSTANT_DOES_NOT_EXIST, input));
    }
}
