package com.a16team03.wim.models.enums;

import com.a16team03.wim.models.enums.contracts.Status;

public enum BugStatus implements Status {
    ACTIVE(1),
    FIXED(0);

    public static final String ILLEGAL_ENUM_BUG_STATUS = "Illegal enum BugStatus value.";
    private final int bugStatusLevel;

    BugStatus(int bugStatusLevel) {
        this.bugStatusLevel = bugStatusLevel;
    }

    public int getBugStatusLevel() {
        return bugStatusLevel;
    }

    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            default:
                throw new IllegalArgumentException(ILLEGAL_ENUM_BUG_STATUS);
        }
    }
}
