package com.a16team03.wim.models;

import com.a16team03.wim.models.contracts.Activity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public final class ActivityImpl implements Activity {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private String activityLogMessage;
    private final Timestamp timestamp;

    public ActivityImpl(String activityLogMessage) {
        setActivityLogMessage(activityLogMessage);
        timestamp = new Timestamp(System.currentTimeMillis());
    }

    public String getActivityLogMessage() {
        return activityLogMessage;
    }

    public String getTimestamp() {
        return sdf.format(timestamp);
    }

    @Override
    public String toString() {
        return String.format("%s -> %s\n",
                getTimestamp(), getActivityLogMessage());
    }

    private void setActivityLogMessage(String activityLogMessage) {
        this.activityLogMessage = activityLogMessage;
    }
}
