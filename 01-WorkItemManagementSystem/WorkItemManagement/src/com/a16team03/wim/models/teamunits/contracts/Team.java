package com.a16team03.wim.models.teamunits.contracts;

import com.a16team03.wim.models.contracts.Activity;

import java.util.List;

public interface Team {
    String getName();

    List<Person> getMemberList();

    List<Board> getBoardList();

    List<Activity> getActivityHistory();

    void addMember(Person member);

    void removeMember(Person member);

    void addBoard(Board board);

    void removeBoard(Board board);

    void addActivity(Activity activity);
}
