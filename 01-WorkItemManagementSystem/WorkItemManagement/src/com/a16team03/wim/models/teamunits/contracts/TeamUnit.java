package com.a16team03.wim.models.teamunits.contracts;

import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.contracts.Item;

import java.util.List;

public interface TeamUnit {
    String getName();

    List<Item> getItems();

    List<Activity> getActivityHistory();

    void addItem(Item item);

    void removeItem(Item item);

    void addActivity(Activity activity);
}
