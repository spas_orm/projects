package com.a16team03.wim.models.teamunits;

import com.a16team03.wim.models.ValidationHelper;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.contracts.Item;
import com.a16team03.wim.models.teamunits.contracts.TeamUnit;

import java.util.ArrayList;
import java.util.List;

public abstract class TeamUnitImpl implements TeamUnit {
    private static final String ITEM_ALREADY_EXISTS_ERROR_MESSAGE = "Item with ID %d already exists in board %s.";
    private static final String INVALID_NAME_LENGTH_MESSAGE =
            "The name length cannot be less than %d or more than %d symbols long.";

    private String name;
    private final List<Item> items;
    private final List<Activity> activityHistory;

    TeamUnitImpl(String name) {
        setName(name);
        items = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Item> getItems() {
        return new ArrayList<>(items);
    }

    public List<Activity> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    public void addItem(Item item) {
        if (items.stream()
                .anyMatch(elem -> elem.getId() == item.getId())) {
            throw new IllegalArgumentException
                    (String.format(ITEM_ALREADY_EXISTS_ERROR_MESSAGE, item.getId(), getName()));
        }
        items.add(item);
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public void addActivity(Activity activity) {
        activityHistory.add(activity);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("%s\n" + str, getName()));
        if (!items.isEmpty()) {
            str.append("----List of items----\n");
            for (Item item : getItems()) {
                str.append("Item: ");
                str.append(item);
                str.append('\n');
            }
        }

        str.append("----Activity history----\n");
        for (Activity activity : getActivityHistory()) {
            str.append(activity);
        }
        return str.toString();
    }

    protected abstract int getMinNameLength();

    protected abstract int getMaxNameLength();

    private void setName(String name) {
        ValidationHelper.checkStringLength(name, getMinNameLength(), getMaxNameLength(), INVALID_NAME_LENGTH_MESSAGE);
        this.name = name;
    }
}
