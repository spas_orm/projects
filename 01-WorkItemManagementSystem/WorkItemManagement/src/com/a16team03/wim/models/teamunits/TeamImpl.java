package com.a16team03.wim.models.teamunits;

import com.a16team03.wim.models.ValidationHelper;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import com.a16team03.wim.models.teamunits.contracts.TeamUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TeamImpl implements Team {
    private static final int NAME_MIN_LENGTH = 4;
    private static final int NAME_MAX_LENGTH = 12;
    private static final String INVALID_NAME_LENGTH_MESSAGE =
            "The team`s name cannot be less than %d or more than %d symbols long.";
    private static final String INVALID_BOARD_NAME = "Board %s is not unique in team %s.";

    private String name;
    private final List<Person> memberList;
    private final List<Board> boardList;
    private final List<Activity> activityHistory;

    public TeamImpl(String name) {
        setName(name);
        memberList = new ArrayList<>();
        boardList = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public List<Person> getMemberList() {
        return new ArrayList<>(memberList);
    }

    public List<Board> getBoardList() {
        return new ArrayList<>(boardList);
    }

    public List<Activity> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    public void addMember(Person member) {
        memberList.add(member);
    }

    public void removeMember(Person member) {
        memberList.remove(member);
    }

    public void addBoard(Board board) {
        if (boardList
                .stream()
                .anyMatch(element -> element.getName().equals(board.getName()))) {
            throw new IllegalArgumentException(String.format(INVALID_BOARD_NAME, board.getName(), getName()));
        }
        boardList.add(board);
    }

    public void removeBoard(Board board) {
        boardList.remove(board);
    }

    public void addActivity(Activity activity) {
        activityHistory.add(activity);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("=================================================\n");
        str.append(String.format("Team name: %s\n", getName()));
        if (!memberList.isEmpty()) {
            str.append("Members: ");
            str.append(memberList.stream().map(TeamUnit::getName).collect(Collectors.joining(", ")));
            str.append("\n");
        }
        if (!boardList.isEmpty()){
            str.append("Boards in team: ");
            str.append(boardList.stream().map(TeamUnit::getName).collect(Collectors.joining(", ")));
            str.append("\n");
        }
        str.append("----Team Activity history----\n");
        str.append(activityHistory.stream().map(Object::toString).collect(Collectors.joining()));
        return str.toString();
    }

    private void setName(String name) {
        ValidationHelper.checkStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH,
                INVALID_NAME_LENGTH_MESSAGE);
        this.name = name;
    }
}
