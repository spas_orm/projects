package com.a16team03.wim.models.teamunits;

import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.ArrayList;
import java.util.List;

public final class PersonImpl extends TeamUnitImpl implements Person {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 15;
    private static final String MEMBER_NAME_NOT_UNIQUE_MESSAGE = "Member name is not unique.";

    private static final List<String> allPeopleNames = new ArrayList<>();

    public PersonImpl(String name) {
        super(name);
        validateUniqueName(name);
    }

    @Override
    public String toString() {
        return String.format("=================================================\nPerson name: %s",
                super.toString());
    }

    @Override
    protected int getMinNameLength() {
        return NAME_MIN_LENGTH;
    }

    @Override
    protected int getMaxNameLength() {
        return NAME_MAX_LENGTH;
    }

    private void validateUniqueName(String name) {
        if(allPeopleNames.contains(name)) {
            throw new IllegalArgumentException(MEMBER_NAME_NOT_UNIQUE_MESSAGE);
        } else {
            allPeopleNames.add(name);
        }
    }
}
