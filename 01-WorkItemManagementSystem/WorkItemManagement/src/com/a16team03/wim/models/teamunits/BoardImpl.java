package com.a16team03.wim.models.teamunits;

import com.a16team03.wim.models.teamunits.contracts.Board;

public final class BoardImpl extends TeamUnitImpl implements Board {
    private static final int NAME_MIN_LENGTH = 5;
    private static final int NAME_MAX_LENGTH = 10;

    public BoardImpl(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return String.format("=================================================\nBoard name: %s",
                super.toString());
    }

    @Override
    protected int getMinNameLength() {
        return NAME_MIN_LENGTH;
    }

    @Override
    protected int getMaxNameLength() {
        return NAME_MAX_LENGTH;
    }
}
