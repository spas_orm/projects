package com.a16team03.wim.models.items.contracts;

import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.enums.contracts.Status;

import java.util.List;

public interface Item {
    int getId();

    String getTitle();

    String getDescription();

    List<Comment> getComments();

    List<Activity> getHistory();

    <T extends Status> T getStatus();

    <T extends Status> void setStatus(T status);

    void addComment(Comment comment);

    void addActivity(Activity activity);
}
