package com.a16team03.wim.models.items.contracts;

import com.a16team03.wim.models.enums.StorySize;

public interface Story extends WorkItem {
    StorySize getSize();

    void setSize(StorySize size);
}
