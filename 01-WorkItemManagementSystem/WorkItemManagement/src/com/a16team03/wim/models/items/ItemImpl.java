package com.a16team03.wim.models.items;


import com.a16team03.wim.models.ValidationHelper;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.enums.contracts.Status;
import com.a16team03.wim.models.items.contracts.Item;

import java.util.ArrayList;
import java.util.List;

public abstract class ItemImpl implements Item {
    private static final int TITLE_MIN_LENGTH = 10;
    private static final int TITLE_MAX_LENGTH = 50;
    private static final String INVALID_TITLE_LENGTH_MESSAGE =
            "The title length cannot be less than %d or more than %d symbols long.";
    private static final String INVALID_DESCRIPTION_LENGTH_MESSAGE =
            "The description text cannot be less than %d or more than %d symbols long.";
    private static final int DESCRIPTION_MIN_LENGTH = 10;
    private static final int DESCRIPTION_MAX_LENGTH = 500;

    private static int idCount = 0;

    private int id;
    private String title;
    private String description;
    private final List<Comment> comments;
    private final List<Activity> history;
    protected Status status;

    ItemImpl(String title, String description) {
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        history = new ArrayList<>();
        setId();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<Activity> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public void addActivity(Activity activity) {
        history.add(activity);
    }

    @Override
    public abstract <T extends Status> T getStatus();

    @Override
    public abstract <T extends Status> void setStatus(T status);

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("=================================================\n");
        str.append(String.format("%s\t\t\t ID: %d\t\t Title: %s\n",
                this.getClass().getSimpleName().replace("Impl", "").toUpperCase(),
                getId(),
                getTitle()));
        str.append(String.format("Description: %s\n", getDescription()));
        str.append(additionalInfo());
        if (!comments.isEmpty()) {
            str.append("----List of comments----\n");
            for (Comment comment : getComments()) {
                str.append(comment);
            }
        }
        str.append("----Activity history----\n");
        for (Activity activityHistory : getHistory()) {
            str.append(activityHistory);
        }
        return str.toString();
    }


    protected abstract String additionalInfo();

    private void setId() {
        this.id = idCount;
        idCount++;
    }

    private void setTitle(String title) {
        ValidationHelper.checkStringLength(title, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, INVALID_TITLE_LENGTH_MESSAGE);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelper.checkStringLength(description, DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH,
                INVALID_DESCRIPTION_LENGTH_MESSAGE);
        this.description = description;
    }

}
