package com.a16team03.wim.models.items.contracts;

import com.a16team03.wim.models.enums.WorkItemPriority;
import com.a16team03.wim.models.teamunits.contracts.Person;

import java.util.List;

public interface WorkItem extends Item {
    WorkItemPriority getPriority();

    List<Person> getAssignees();

    void addAssignee(Person person);

    void removeAssignee(Person person);

    void setPriority(WorkItemPriority priority);
}
