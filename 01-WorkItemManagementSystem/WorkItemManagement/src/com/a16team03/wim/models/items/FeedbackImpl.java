package com.a16team03.wim.models.items;

import com.a16team03.wim.models.enums.FeedbackStatus;
import com.a16team03.wim.models.enums.contracts.Status;
import com.a16team03.wim.models.items.contracts.Feedback;

import static com.a16team03.wim.commands.CommandConstants.*;

public final class FeedbackImpl extends ItemImpl implements Feedback {
    private static final int MIN_RATING_VALUE = 0;
    private static final int MAX_RATING_VALUE = 10;

    private int rating;

    public FeedbackImpl(String title, String description) {
        super(title, description);
        this.status = FeedbackStatus.NEW;
        setRating(rating);
    }

    @Override
    public <FeedbackStatus extends Status> FeedbackStatus getStatus() {
        return (FeedbackStatus) status;
    }

    public <T extends Status> void setStatus(T status) {
        this.status = (FeedbackStatus) status;
    }

    @Override
    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if (rating < MIN_RATING_VALUE || rating > MAX_RATING_VALUE) {
            throw new IllegalArgumentException(FEEDBACK_ILLEGAL_RATING_MESSAGE);
        }
        this.rating = rating;
    }

    @Override
    protected String additionalInfo() {
        return String.format("Status: %s\t\t Rating: %d\n",
                getStatus(),
                getRating());
    }
}
