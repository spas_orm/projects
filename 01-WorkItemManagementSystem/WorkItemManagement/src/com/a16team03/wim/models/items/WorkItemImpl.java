package com.a16team03.wim.models.items;

import com.a16team03.wim.models.enums.WorkItemPriority;
import com.a16team03.wim.models.items.contracts.WorkItem;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.TeamUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class WorkItemImpl extends ItemImpl implements WorkItem {
    private WorkItemPriority priority;
    private final List<Person> assignees;

    WorkItemImpl(String title, String description) {
        super(title, description);
        this.priority = WorkItemPriority.UNSET;
        assignees = new ArrayList<>();
    }

    @Override
    public WorkItemPriority getPriority() {
        return priority;
    }

    @Override
    public List<Person> getAssignees() {
        return new ArrayList<>(assignees);
    }

    public void setPriority(WorkItemPriority priority) {
        this.priority = priority;
    }

    public void addAssignee(Person person) {
        this.assignees.add(person);
    }

    public void removeAssignee(Person person) {
        assignees.remove(person);
    }

    @Override
    protected String additionalInfo() {
        return String.format("Priority: %s\t\t Assigned to: %s\n",
                getPriority(),
                getAssignees().isEmpty() ? "Not assigned" : getAssignees()
                        .stream()
                        .map(TeamUnit::getName)
                        .collect(Collectors.joining(", ")));
    }
}
