package com.a16team03.wim.models.items.contracts;


public interface Feedback extends Item {
    int getRating();

    void setRating(int rating);
}
