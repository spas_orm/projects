package com.a16team03.wim.models.items.contracts;

import com.a16team03.wim.models.enums.BugSeverity;

import java.util.List;

public interface Bug extends WorkItem {
    List<String> getStepsToReproduce();

    BugSeverity getSeverity();

    void addStep(String stepDescription);

    void setSeverity(BugSeverity severity);
}
