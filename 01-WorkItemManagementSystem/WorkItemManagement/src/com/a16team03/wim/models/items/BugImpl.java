package com.a16team03.wim.models.items;

import com.a16team03.wim.models.ValidationHelper;
import com.a16team03.wim.models.enums.BugSeverity;
import com.a16team03.wim.models.enums.BugStatus;
import com.a16team03.wim.models.enums.contracts.Status;
import com.a16team03.wim.models.items.contracts.Bug;

import java.util.ArrayList;
import java.util.List;

public final class BugImpl extends WorkItemImpl implements Bug {
    private static final int STEP_DESCRIPTION_MIN_LENGTH = 5;
    private static final int STEP_DESCRIPTION_MAX_LENGTH = 50;
    private static final String INVALID_STEP_DESCRIPTION_LENGTH_MESSAGE =
            "The step description length cannot be less than %d or more than %d symbols long.";

    private BugSeverity severity;
    private final List<String> stepsToReproduce;

    public BugImpl(String title, String description) {
        super(title, description);
        this.status = BugStatus.ACTIVE;
        this.severity = BugSeverity.UNSET;
        stepsToReproduce = new ArrayList<>();
    }

    @Override
    public <BugStatus extends Status> BugStatus getStatus() {
        return (BugStatus) status;
    }

    public <T extends Status> void setStatus(T status) {
        this.status = (BugStatus) status;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public BugSeverity getSeverity() {
        return severity;
    }

    @Override
    public void addStep(String stepDescription) {
        ValidationHelper.checkStringLength(stepDescription, STEP_DESCRIPTION_MIN_LENGTH, STEP_DESCRIPTION_MAX_LENGTH,
                INVALID_STEP_DESCRIPTION_LENGTH_MESSAGE);
        stepsToReproduce.add(stepDescription);
    }

    public void setSeverity(BugSeverity severity) {
        this.severity = severity;
    }

    @Override
    protected String additionalInfo() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("%sStatus: %s\t\t\t\t Severity: %s\n",
                super.additionalInfo(), getStatus(), getSeverity()));
        if (!stepsToReproduce.isEmpty()) {
            str.append("----Steps To Reproduce----\n");
            for (String step : getStepsToReproduce()) {
                str.append(" - ");
                str.append(step);
                str.append(System.lineSeparator());
            }
        }
        return str.toString();
    }
}
