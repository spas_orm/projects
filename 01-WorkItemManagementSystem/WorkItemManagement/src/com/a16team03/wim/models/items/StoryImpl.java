package com.a16team03.wim.models.items;

import com.a16team03.wim.models.enums.StorySize;
import com.a16team03.wim.models.enums.StoryStatus;
import com.a16team03.wim.models.enums.contracts.Status;
import com.a16team03.wim.models.items.contracts.Story;

public final class StoryImpl extends WorkItemImpl implements Story {
    private StorySize size;

    public StoryImpl(String title, String description) {
        super(title, description);
        this.size = StorySize.UNSET;
        this.status = StoryStatus.NOTDONE;
    }

    @Override
    public StorySize getSize() {
        return size;
    }

    @Override
    public <StoryStatus extends Status> StoryStatus getStatus() {
        return (StoryStatus) status;
    }

    @Override
    public <T extends Status> void setStatus(T status) {
        this.status = (StoryStatus) status;
    }

    @Override
    public void setSize(StorySize size) {
        this.size = size;
    }

    @Override
    protected String additionalInfo() {
        return String.format("%sStatus: %s\t\t\t\t Size: %s\n",
                super.additionalInfo(),
                getStatus(),
                getSize());
    }
}
