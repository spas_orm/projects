package com.a16team03.wim.models;

public final class ValidationHelper {
    private ValidationHelper() {
    }

    public static void checkStringLength(String string, int minLength, int maxLength, String message) {
        if (string.length() < minLength || string.length() > maxLength) {
            throw new IllegalArgumentException(String.format(message, minLength, maxLength));
        }
    }
}
