package com.a16team03.wim.models;

import com.a16team03.wim.models.contracts.Comment;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public final class CommentImpl implements Comment {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private String author;
    private String comment;
    private final Timestamp timestamp;

    public CommentImpl(String author, String comment) {
        setAuthor(author);
        setComment(comment);
        timestamp = new Timestamp(System.currentTimeMillis());
    }

    public String getAuthor() {
        return author;
    }

    public String getComment() {
        return comment;
    }

    public String getTimestamp() {
        return sdf.format(timestamp);
    }

    @Override
    public String toString() {
        return String.format("%s -> %s: %s\n",
                getTimestamp(), getAuthor(), getComment());
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    private void setComment(String comment) {
        this.comment = comment;
    }
}
