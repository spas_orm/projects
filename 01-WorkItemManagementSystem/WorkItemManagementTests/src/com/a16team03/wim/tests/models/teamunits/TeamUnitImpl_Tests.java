package com.a16team03.wim.tests.models.teamunits;

import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.items.contracts.Item;
import com.a16team03.wim.models.teamunits.BoardImpl;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class TeamUnitImpl_Tests {
    private static Team team;
    private static Board board;

    @BeforeClass
    public static void initTests() {
        //Arrange
        team = new TeamImpl("TheMatrix");
        board = new BoardImpl("Board1");
        team.addBoard(board);
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void addItem_should_throwException_when_itemAlreadyExistsInBoard() {
        //Arrange
        Item bug = new BugImpl("BugTitle123", "Bug Description.");
        board.addItem(bug);
        //Act
        board.addItem(bug);
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void constructor_should_throwException_when_personNameLengthIsLessThanMinimum() {
        //Arrange, Act
        Person person = new PersonImpl("Name");
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void constructor_should_throwException_when_personNameLengthIsMoreThanMaximum() {
        //Arrange, Act
        Person person = new PersonImpl("1234567890123456");
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void constructor_should_throwException_when_boardNameLengthIsLessThanMinimum() {
        //Arrange, Act
        Board board = new BoardImpl("Name");
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void constructor_should_throwException_when_boardNameLengthIsMoreThanMaximum() {
        //Arrange, Act
        Board board = new BoardImpl("123456789012");
    }

    @Test
    public void getActivityHistory_should_returnShallowCopy() {
        // Arrange
        Board board = new BoardImpl("Board222");
        List<Activity> supposedShallowCopy = board.getActivityHistory();
        Activity activity = new ActivityImpl("Activity log message.");
        int sizeBefore = board.getActivityHistory().size();
        // Act
        board.addActivity(activity);
        // Assert
        Assert.assertEquals(sizeBefore, supposedShallowCopy.size());
    }

    @Test
    public void getItems_should_returnShallowCopy() {
        // Arrange
        List<Item> supposedShallowCopy = board.getItems();
        int sizeBefore = board.getItems().size();
        Item item = new BugImpl("BugTitleMustBeLong","BugDescription");
        // Act
        board.addItem(item);
        // Assert
        Assert.assertEquals(sizeBefore, supposedShallowCopy.size());
    }

    @Test
    public void removeItem_should_remove_Item_when_passedPresentItem() {
        // Arrange
        Item item = new BugImpl("BugTitleMustBeLong","BugDescription");
        int sizeBefore = team.getMemberList().size();
        board.addItem(item);
        // Act
        board.removeItem(item);
        // Assert
        Assert.assertEquals(sizeBefore, board.getItems().size());
    }
}
