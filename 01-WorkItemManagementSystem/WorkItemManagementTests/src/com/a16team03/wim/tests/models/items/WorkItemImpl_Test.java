package com.a16team03.wim.tests.models.items;

import com.a16team03.wim.models.enums.WorkItemPriority;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.WorkItem;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WorkItemImpl_Test {
    private WorkItem workitem;

    @Before
    public void initTests() {
        //Arrange
        workitem = new BugImpl("BugWithLongName", "Description of the bug with enough characters");
    }

    @Test
    public void getPriority_should_return_UNSET() {
        //Arrange
        WorkItemPriority priority = workitem.getPriority();
        //Assert
        Assert.assertEquals(WorkItemPriority.UNSET, priority);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setPriority_should_throwError_when_enumWrongValue() {
        workitem.setPriority(WorkItemPriority.valueOf("WRONGVALUE"));
    }

    @Test
    public void setPriority_should_change_toLOW() {
        //Act
        workitem.setPriority(WorkItemPriority.valueOf("LOW"));
        WorkItemPriority priority = workitem.getPriority();
        //Assert
        Assert.assertEquals(WorkItemPriority.LOW, priority);
    }

    @Test
    public void setAssignee_should_increment_assigneesList() {
        //Arrange
        Person person = new PersonImpl("SpasO");
        // Act
        int sizeBefore = workitem.getAssignees().size();
        workitem.addAssignee(person);
        int sizeAfter = workitem.getAssignees().size();
        //Assert
        Assert.assertEquals(sizeBefore + 1, sizeAfter);
    }

    @Test
    public void getAssignee_should_return_assignedPerson() {
        //Arrange
        Person personInit = new PersonImpl("SpasOr");
        workitem.addAssignee(personInit);
        // Act
        String personName = workitem.getAssignees().get(0).getName();
        //Assert
        Assert.assertEquals("SpasOr", personName);
    }

    @Test
    public void removeAssignee_should_remove_personFromAssigneesList() {
        //Arrange
        Person person = new PersonImpl("Member");
        workitem.addAssignee(person);
        // Act
        int sizeBefore = workitem.getAssignees().size();
        workitem.removeAssignee(person);
        int sizeAfter = workitem.getAssignees().size();
        //Assert
        Assert.assertEquals(sizeBefore - 1, sizeAfter);
    }


}
