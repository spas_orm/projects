package com.a16team03.wim.tests.models.items;

import com.a16team03.wim.models.enums.BugSeverity;
import com.a16team03.wim.models.enums.BugStatus;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.items.contracts.Item;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BugImpl_Tests {
    private Bug bug;

    @Before
    public void initTests() {
        //Arrange
        bug = new BugImpl("BugWithLongName", "Description of the bug with enough characters");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameTooShort() {
        // Arrange, Act, Assert
        Item bug = new BugImpl("ShortName", "Description should be long enough");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameTooLong() {
        // Arrange, Act, Assert
        Item bug = new BugImpl("NameOfItemShouldBeLongThan50Chars_asdaewfonalwfsdkfkawe", "Description should be long enough");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionTooShort() {
        // Arrange, Act, Assert
        Item bug = new BugImpl("NameOfItemOK", "Short");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionTooLong() {
        // Arrange, Act, Assert
        Item bug = new BugImpl("NameOfItemOK", "DescriptionOfItemShouldBeLessThan500Chars_asdaewfonalwfsdkfkawe asdlioawneflhadf EFUINLAIEUFHKABNDSF wef,ibalwheflNEWKNaw f,kewfnlnlEWF ALEWFIONLJEWFOL LWEUIFHLANL 89HAEWJKN,.M LEWUIFHN C,AIUEWFHLA CLIEWCN UIH CL CN LEC AKEJCNALUFNALEN FLI NADLCH WLEFNAL WEFLI WLIAUW EFUIWEF NLAEWUI NA,NL Nljaes rlf leruf lahf lairufh ,dsfh leru fh,f  lrifuh alfi alfuindsfn liuhal . awfuiaew faiwuef awief laewf kdsfn alwuif alwf alwfuih f;jerglagfiyqgfwtg'sglhqo gpq; flawfak, wfiuq;f0'gjqpfqfla;o fpa; qo4lf halg heo q4flaewfhwllre");
    }

    @Test
    public void getStatus_should_return_ACTIVE() {
        //Arrange, Act
        BugStatus status = bug.getStatus();
        //Assert
        Assert.assertEquals(BugStatus.ACTIVE, status);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setStatus_should_throwError_when_enumWrongValue() {
        bug.setStatus(BugStatus.valueOf("WRONGVALUE"));
    }

    @Test
    public void setStatus_should_change_toACTIVE() {
        //Act
        bug.setStatus(BugStatus.FIXED);
        BugStatus status = bug.getStatus();
        //Assert
        Assert.assertEquals(BugStatus.FIXED, status);
    }

    @Test
    public void getSeverity_should_return_UNSET() {
        //Arrange
        BugSeverity severity = bug.getSeverity();
        //Assert
        Assert.assertEquals(BugSeverity.UNSET, severity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setSeverity_should_throwError_when_enumWrongValue() {
        //Act
        bug.setSeverity(BugSeverity.valueOf("WRONGVALUE"));
    }

    @Test
    public void setSeverity_should_change_toMINOR() {
        //Act
        bug.setSeverity(BugSeverity.CRITICAL);
        bug.setSeverity(BugSeverity.MAJOR);
        bug.setSeverity(BugSeverity.MINOR);
        BugSeverity severity = bug.getSeverity();
        //Assert
        Assert.assertEquals(BugSeverity.MINOR, severity);
    }

    @Test
    public void addStep_should_increment_stepToReproduceSize() {
        //Arrange, Act
        bug.addStep("Step1");
        int sizeBefore = bug.getStepsToReproduce().size();
        bug.addStep("Step2");
        int sizeAfter = bug.getStepsToReproduce().size();
        //Assert
        Assert.assertEquals(sizeBefore + 1, sizeAfter);
    }

    @Test
    public void getStepToReproduce_should_get_theSameStringAddedWithSet() {
        //Arrange
        String testStr = "Step1Test";
        bug.addStep(testStr);
        //Act
        String resultString = bug.getStepsToReproduce().get(0).toString();
        //Assert
        Assert.assertEquals(testStr, resultString);
    }

}
