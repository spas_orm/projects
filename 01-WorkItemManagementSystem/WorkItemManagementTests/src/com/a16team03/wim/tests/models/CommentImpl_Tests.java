package com.a16team03.wim.tests.models;

import com.a16team03.wim.models.CommentImpl;
import com.a16team03.wim.models.contracts.Comment;
import org.junit.Assert;
import org.junit.Test;

public class CommentImpl_Tests {
    @Test
    public void constructor_should_createValidObject() {
        //Arrange, Act
        Comment comment = new CommentImpl("AuthorName", "This is a comment.");
    }

    @Test
    public void getAuthor_should_return_author() {
        //Arrange
        Comment comment = new CommentImpl("AuthorName", "This is a comment.");
        //Act
        String authorName = comment.getAuthor();
        //Assert
        Assert.assertEquals("AuthorName",authorName);
    }
}
