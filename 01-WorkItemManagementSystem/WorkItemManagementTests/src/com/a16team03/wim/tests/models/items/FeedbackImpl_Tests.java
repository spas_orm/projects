package com.a16team03.wim.tests.models.items;

import com.a16team03.wim.models.enums.FeedbackStatus;
import com.a16team03.wim.models.items.FeedbackImpl;
import com.a16team03.wim.models.items.contracts.Feedback;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FeedbackImpl_Tests {
    private Feedback feedback;

    @Before
    public void initTests() {
        //Arrange
        feedback = new FeedbackImpl("FeedbackWithLongName", "Description of the feedback with enough characters");
    }

    @Test
    public void getStatus_should_return_NEW() {
        //Arrange
        FeedbackStatus status = feedback.getStatus();
        //Assert
        Assert.assertEquals(FeedbackStatus.NEW, status);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setStatus_should_throwError_when_enumWrongValue() {
        feedback.setStatus(FeedbackStatus.valueOf("WRONGVALUE"));
    }

    @Test
    public void setStatus_should_change_toACTIVE() {
        //Act
        feedback.setStatus(FeedbackStatus.valueOf("DONE"));
        FeedbackStatus status = feedback.getStatus();
        //Assert
        Assert.assertEquals(FeedbackStatus.DONE, status);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setRating_should_throwError_when_ratingNegative() {
        feedback.setRating(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setRating_should_throwError_when_ratingAboveLimit() {
        feedback.setRating(11);
    }

    @Test
    public void getRating_should_return_5() {
        //Arrange
        feedback.setRating(5);
        //Act
        int rating = feedback.getRating();
        //Assert
        Assert.assertEquals(5, rating);
    }


}
