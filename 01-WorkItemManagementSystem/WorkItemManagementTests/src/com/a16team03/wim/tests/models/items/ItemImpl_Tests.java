package com.a16team03.wim.tests.models.items;

import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.CommentImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.Item;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ItemImpl_Tests {
    private Item commonBug;
    private Comment comment;
    private Activity activity;

    @Before
    public void initTests() {
        //Arrange
        commonBug = new BugImpl("BugWithLongName", "Description of the bug with enough characters");
        comment = new CommentImpl("Person2", "Comment long enough");
        activity = new ActivityImpl("Activity message for test");
    }

    @Test
    public void setId_should_incrementWithOne() {
        //Arrange
        Item bug2 = new BugImpl("Bug2TestName", "Description of the bug with enough characters");
        //Act
        int idBefore = commonBug.getId();
        int idAfter = bug2.getId();
        //Assert
        Assert.assertEquals(idBefore + 1, idAfter);
    }

    @Test
    public void getTitle_should_return_correctTitle() {
        //Act
        String title = commonBug.getTitle();
        //Assert
        Assert.assertEquals("BugWithLongName", title);
    }

    @Test
    public void getDescription_should_return_correctDescription() {
        //Act
        String description = commonBug.getDescription();
        //Assert
        Assert.assertEquals("Description of the bug with enough characters", description);
    }

    @Test
    public void addComment_should_increment_commentsList() {
        // Act
        int sizeBefore = commonBug.getComments().size();
        commonBug.addComment(comment);
        int sizeAfter = commonBug.getComments().size();
        //Assert
        Assert.assertEquals(sizeBefore + 1, sizeAfter);
    }

    @Test
    public void getComments_should_return_assignedPerson() {
        //Arrange
        commonBug.addComment(comment);
        // Act
        String commentResult = commonBug.getComments().get(0).getComment();
        //Assert
        Assert.assertEquals("Comment long enough", commentResult);
    }

    @Test
    public void addActivity_should_increment_historyList() {
        // Act
        int sizeBefore = commonBug.getHistory().size();
        commonBug.addActivity(activity);
        int sizeAfter = commonBug.getHistory().size();
        //Assert
        Assert.assertEquals(sizeBefore + 1, sizeAfter);
    }

    @Test
    public void getHistory_should_return_correctActivityMessage() {
        //Arrange
        commonBug.addActivity(activity);
        // Act
        String activityResult = commonBug.getHistory().get(0).getActivityLogMessage();
        //Assert
        Assert.assertEquals("Activity message for test", activityResult);
    }


}
