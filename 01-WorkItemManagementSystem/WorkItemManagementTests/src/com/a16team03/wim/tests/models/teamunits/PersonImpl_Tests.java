package com.a16team03.wim.tests.models.teamunits;

import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.Test;

public class PersonImpl_Tests {

    @Test(expected = IllegalArgumentException.class) //Assert
    public void constructor_should_throwException_when_personWithTheSameNameAlreadyExists() {
        //Arrange
        Person person = new PersonImpl("George");
        //Act
        Person person2 = new PersonImpl("George");
    }
}
