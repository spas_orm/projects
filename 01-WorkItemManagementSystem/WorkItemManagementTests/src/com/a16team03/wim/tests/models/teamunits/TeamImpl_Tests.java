package com.a16team03.wim.tests.models.teamunits;

import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.BoardImpl;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.*;

import java.util.List;

public class TeamImpl_Tests {
    private static Team team;

    @BeforeClass
    public static void initTests() {
        //Arrange
        team = new TeamImpl("TheMatrix");
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void addBoard_should_throwException_when_boardAlreadyExistsInTeam() {
        //Arrange
        Board board = new BoardImpl("BoardName");
        team.addBoard(board);
        //Act
        team.addBoard(board);
    }

    @Test
    public void getMemberList_should_returnShallowCopy() {
        // Arrange
        List<Person> supposedShallowCopy = team.getMemberList();
        Person person = new PersonImpl("Jonathan");
        // Act
        team.addMember(person);
        // Assert
        Assert.assertEquals(0, supposedShallowCopy.size());
    }

    @Test
    public void getBoardList_should_returnShallowCopy() {
        // Arrange
        List<Board> supposedShallowCopy = team.getBoardList();
        Board board1 = new BoardImpl("Board1");
        int sizeBefore = team.getBoardList().size();
        // Act
        team.addBoard(board1);
        // Assert
        Assert.assertEquals(sizeBefore, supposedShallowCopy.size());
    }

    @Test
    public void getActivityHistory_should_returnShallowCopy() {
        // Arrange
        List<Activity> supposedShallowCopy = team.getActivityHistory();
        Activity activity = new ActivityImpl("Activity log message.");
        int sizeBefore = team.getActivityHistory().size();
        // Act
        team.addActivity(activity);
        // Assert
        Assert.assertEquals(sizeBefore, supposedShallowCopy.size());
    }

    @Test
    public void removeMember_should_remove_Member_when_passedPresentMember() {
        // Arrange
        Person person = new PersonImpl("Jacob");
        int sizeBefore = team.getMemberList().size();
        // Act
        team.addMember(person);
        team.removeMember(person);
        // Assert
        Assert.assertEquals(sizeBefore, team.getMemberList().size());
    }

    @Test
    public void removeBoard_should_remove_Board_when_passedPresentBoard() {
        // Arrange
        Board board = new BoardImpl("Board123");
        int sizeBefore = team.getMemberList().size();
        team.addBoard(board);
        // Act
        team.removeBoard(board);
        // Assert
        Assert.assertEquals(sizeBefore, team.getBoardList().size());
    }
}
