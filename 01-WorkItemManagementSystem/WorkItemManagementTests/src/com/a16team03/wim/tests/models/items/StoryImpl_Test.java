package com.a16team03.wim.tests.models.items;

import com.a16team03.wim.models.enums.StorySize;
import com.a16team03.wim.models.enums.StoryStatus;
import com.a16team03.wim.models.items.StoryImpl;
import com.a16team03.wim.models.items.contracts.Story;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StoryImpl_Test {
    private Story story;

    @Before
    public void initTests() {
        //Arrange
        story = new StoryImpl("StoryWithLongName", "Description of the story with enough characters");
    }

    @Test
    public void getStatus_should_return_NOTDONE() {
        //Arrange
        StoryStatus status = story.getStatus();
        //Assert
        Assert.assertEquals(StoryStatus.NOTDONE, status);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setStatus_should_throwError_when_enumWrongValue() {
        story.setStatus(StoryStatus.valueOf("WRONGVALUE"));
    }

    @Test
    public void setStatus_should_change_toDONE() {
        //Act
        story.setStatus(StoryStatus.INPROGRESS);
        story.setStatus(StoryStatus.DONE);
        StoryStatus status = story.getStatus();
        //Assert
        Assert.assertEquals(StoryStatus.DONE, status);
    }

    @Test
    public void getSize_should_return_UNSET() {
        //Arrange
        StorySize size = story.getSize();
        //Assert
        Assert.assertEquals(StorySize.UNSET, size);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setSize_should_throwError_when_enumWrongValue() {
        story.setSize(StorySize.valueOf("WRONGVALUE"));
    }

    @Test
    public void setSize_should_change_toLARGE() {
        //Act
        story.setSize(StorySize.SMALL);
        story.setSize(StorySize.MEDIUM);
        story.setSize(StorySize.LARGE);
        StorySize size = story.getSize();
        //Assert
        Assert.assertEquals(StorySize.LARGE, size);
    }

}
