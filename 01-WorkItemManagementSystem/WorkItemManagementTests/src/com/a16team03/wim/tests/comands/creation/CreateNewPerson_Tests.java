package com.a16team03.wim.tests.comands.creation;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.creation.CreateNewPerson;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewPerson_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateNewPerson(wimFactory, wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("person");
        testList.add("name");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createPerson_when_inputIsValid() {
        //Arrange
        testList.add("PersonName");
        int sizeBefore = wimRepository.getPersons().size();
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals("PersonName", wimRepository.getPersons().get(sizeBefore).getName());
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void execute_should_throwException_when_PersonAlreadyExists() {
        //Arrange
        testList.add("PersonName");
        testCommand.execute(testList);
        //Act
        testCommand.execute(testList);
    }
}
