package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowTeamActivity;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_TEAM_ACTIVITY_MESSAGE;

public class ShowTeamActivity_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Team team;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowTeamActivity(wimRepository);
        testList = new ArrayList<>();
        team = wimFactory.createTeam("TheMatrix");
        wimRepository.addTeam(team);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArguments");
        testList.add("thanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesntExist() {
        //Arrange
        testList.add("NoSuchTeam");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_throwException_when_noTeamActivity() {
        //Arrange
        Team test = wimFactory.createTeam("Otbor");
        wimRepository.addTeam(test);
        testList.add("Otbor");
        // Act
        String output = testCommand.execute(testList);
        //Assert
        Assert.assertEquals(NO_TEAM_ACTIVITY_MESSAGE, output);
    }

    @Test
    public void execute_should_returnString_with_teamActivity() {
        //Arrange
        Activity testActivity = new ActivityImpl("This is a team activity.");
        team.addActivity(testActivity);

        testList.add("TheMatrix");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains("This is a team activity."));
    }
}
