package com.a16team03.wim.tests.comands.creation;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.creation.CreateNewTeam;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewTeam_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateNewTeam(wimFactory, wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("team");
        testList.add("name");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createTeam_when_inputIsValid() {
        //Arrange
        testList.add("TheMatrix");
        int sizeBefore = wimRepository.getTeams().size();
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals("TheMatrix", wimRepository.getTeams().get(sizeBefore).getName());
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void execute_should_throwException_when_TeamAlreadyExists() {
        //Arrange
        testList.add("TeamName");
        testCommand.execute(testList);
        //Act
        testCommand.execute(testList);
    }
}
