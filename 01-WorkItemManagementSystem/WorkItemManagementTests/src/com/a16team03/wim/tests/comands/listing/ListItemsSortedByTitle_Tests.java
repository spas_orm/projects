package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsSortedByTitle;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.contracts.Story;
import com.a16team03.wim.models.teamunits.contracts.Board;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_ITEMS_IN_THE_APPLICATION_MESSAGE;

public class ListItemsSortedByTitle_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsSortedByTitle(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsWithRatingExist() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_ITEMS_IN_THE_APPLICATION_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_thatContainsSortedItemsByRating() {
        //Arrange
        Story story1 = wimFactory.createStory("StoryTitleMustBeLonb", "Description of a story.");
        wimRepository.addStory(story1);

        Story story2 = wimFactory.createStory("StoryTitleMustBeLong", "Description of a story.");
        wimRepository.addStory(story2);

        Story story3 = wimFactory.createStory("StoryTitleMustBeLonger", "Description of a story.");
        wimRepository.addStory(story3);

        Story story4 = wimFactory.createStory("StoryTitleMustBeLony", "Description of a story.");
        wimRepository.addStory(story4);

        Board board1 = wimFactory.createBoard("Board1");
        board1.addItem(story1);
        board1.addItem(story2);
        board1.addItem(story3);
        board1.addItem(story4);

        String expectedOutput = board1.getItems().stream().map(Object::toString).collect(Collectors.joining());
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(expectedOutput, result);
    }
}
