package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.ChangeStorySize;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.StoryImpl;
import com.a16team03.wim.models.items.contracts.Story;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStorySize_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Story story;
    private static String idString;
    private static int id;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ChangeStorySize(wimFactory, wimRepository);
        testList = new ArrayList<>();
        story = new StoryImpl("StoryTitleLong", "Story description for test");
        wimRepository.addStory(story);
        id = story.getId();
        idString = String.valueOf(id);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("0");
        testList.add("MEDIUM");
        testList.add("extraArgument");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_giveTextInsteadID() {
        //Arrange
        testList.add("bla");
        testList.add("MEDIUM");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeStorySize_should_throwException_when_wrongPropertyValue() {
        //Arrange
        testList.add(idString);
        testList.add("wrong");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void changeStorySize_should_set_sizeToMedium() {
        //Arrange
        testList.add(idString);
        testList.add("MEDIUM");
        // Act
        testCommand.execute(testList);
        String newSize = wimRepository.getStoryById(id).getSize().toString();
        //Assert
        Assert.assertEquals("Medium", newSize);
    }
}
