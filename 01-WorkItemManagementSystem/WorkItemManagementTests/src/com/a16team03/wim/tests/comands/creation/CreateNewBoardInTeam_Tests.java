package com.a16team03.wim.tests.comands.creation;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.creation.CreateNewBoardInTeam;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

public class CreateNewBoardInTeam_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static Team testTeam;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateNewBoardInTeam(wimFactory, wimRepository);
        testTeam = new TeamImpl("TeamName");
        wimRepository.addTeam(testTeam);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        testList.add("board");
        testList.add("name");
        testList.add("team");
        testList.add("name");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class) //Assert
    public void execute_should_throwException_when_boardAlreadyExistsInTeam() {
        //Arrange
        testList.add("BoardName");
        testList.add("TeamName");
        testCommand.execute(testList);
        //Act
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBoardInTeam_when_inputIsValid() {
        // Arrange
        testList.add("Board1");
        testList.add("TeamName");
        int sizeBefore = wimRepository.getBoards().size();
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Board1", wimRepository.getBoards().get(sizeBefore).getName());
    }
}
