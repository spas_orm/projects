package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.ChangeFeedbackStatus;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.FeedbackStatus;
import com.a16team03.wim.models.items.FeedbackImpl;
import com.a16team03.wim.models.items.contracts.Feedback;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeFeedbackStatus_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Feedback feedback;
    private static String idString;
    private static int id;


    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ChangeFeedbackStatus(wimFactory, wimRepository);
        testList = new ArrayList<>();
        feedback = new FeedbackImpl("FeedbackTitle", "Feedback description for test");
        wimRepository.addFeedback(feedback);
        id = feedback.getId();
        idString = String.valueOf(id);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("0");
        testList.add("Scheduled");
        testList.add("extraArgument");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_giveTextInsteadID() {
        //Arrange
        testList.add("bla");
        testList.add("Scheduled");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeFeedbackStatus_should_throwException_when_wrongPropertyValue() {
        //Arrange
        testList.add(idString);
        testList.add("wrong");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void changeFeedbackStatus_should_set_statusToInScheduled() {
        //Arrange
        testList.add(idString);
        testList.add("Scheduled");
        // Act
        testCommand.execute(testList);
        FeedbackStatus newValue = wimRepository.getFeedbackById(id).getStatus();
        //Assert
        Assert.assertEquals(FeedbackStatus.SCHEDULED, newValue);
    }
}
