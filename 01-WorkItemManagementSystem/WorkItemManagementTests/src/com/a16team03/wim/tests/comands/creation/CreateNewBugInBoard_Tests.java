package com.a16team03.wim.tests.comands.creation;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.creation.CreateNewBugInBoard;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateNewBugInBoard_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new CreateNewBugInBoard(wimFactory, wimRepository);
        testList = new ArrayList<>();

        Team team = wimFactory.createTeam("TheMatrix");
        wimRepository.addTeam(team);
        Board board = wimFactory.createBoard("Board1");
        team.addBoard(board);
        wimRepository.addBoard(board);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBugInBoard_when_inputIsValid() {
        //Arrange
        testList.add("Board1");
        testList.add("BugTitleMustBeLong");
        testList.add("This is the bug`s description.");
        // Act
        testCommand.execute(testList);
        // Assert
        Assert.assertEquals("BugTitleMustBeLong",
                wimRepository.getTeamByName("TheMatrix")
                        .getBoardList()
                        .get(0)
                        .getItems()
                        .get(0)
                        .getTitle());
    }
}
