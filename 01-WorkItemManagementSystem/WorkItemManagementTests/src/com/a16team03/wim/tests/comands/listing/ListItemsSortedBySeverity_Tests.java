package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsSortedBySeverity;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.BugSeverity;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.teamunits.contracts.Board;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_BUG_ITEMS_IN_THE_APPLICATION_MESSAGE;

public class ListItemsSortedBySeverity_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsSortedBySeverity(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsWithRatingExist() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_BUG_ITEMS_IN_THE_APPLICATION_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_thatContainsSortedItemsByRating() {
        //Arrange
        Bug bugCritical = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugCritical.setSeverity(BugSeverity.CRITICAL);
        wimRepository.addBug(bugCritical);

        Bug bugMajor = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugMajor.setSeverity(BugSeverity.MAJOR);
        wimRepository.addBug(bugMajor);

        Bug bugMinor = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugMinor.setSeverity(BugSeverity.MINOR);
        wimRepository.addBug(bugMinor);

        Bug bugUnset = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugUnset.setSeverity(BugSeverity.UNSET);
        wimRepository.addBug(bugUnset);

        Board board1 = wimFactory.createBoard("Board1");
        board1.addItem(bugCritical);
        board1.addItem(bugMajor);
        board1.addItem(bugMinor);
        board1.addItem(bugUnset);

        String expectedOutput = board1.getItems().stream().map(Object::toString).collect(Collectors.joining());
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(expectedOutput, result);
    }
}
