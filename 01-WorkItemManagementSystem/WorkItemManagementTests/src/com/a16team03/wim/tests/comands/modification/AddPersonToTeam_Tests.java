package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.AddPersonToTeam;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonToTeam_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Person person;
    private static Team team;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AddPersonToTeam(wimFactory, wimRepository);
        testList = new ArrayList<>();
        person = new PersonImpl("DWAQEWFADS");
        wimRepository.addPerson(person);
        team = new TeamImpl("TheMatrix");
        wimRepository.addTeam(team);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("randomName");
        testList.add("TheMatrix");
        testList.add("extraArgument");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_addPersonToTeam_when_inputIsValid() {
        //Arrange
        testList.add("DWAQEWFADS");
        testList.add("TheMatrix");
        // Act
        String result = testCommand.execute(testList);
        //Assert
        Assert.assertEquals("Person DWAQEWFADS was added to TheMatrix team.", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfMemberAlreadyExistsInTeam_should_throwException_when_existingPersonPassedToMethod() {
        //Arrange
        Person person2 = new PersonImpl("NewName1");
        wimRepository.addPerson(person2);
        team.addMember(person2);
        testList.add("NewName1");
        testList.add("TheMatrix");
        // Act, Assert
        testCommand.execute(testList);
    }


}
