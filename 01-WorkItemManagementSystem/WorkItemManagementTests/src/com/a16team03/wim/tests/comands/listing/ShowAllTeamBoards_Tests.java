package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowAllTeamBoards;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.teamunits.BoardImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllTeamBoards_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Team testTeam;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowAllTeamBoards(wimRepository);
        testList = new ArrayList<>();
        testTeam = new TeamImpl("TeamName1");
        wimRepository.addTeam(testTeam);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("TeamName1");
        testList.add("blabla");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesntExist() {
        //Arrange
        testList.add("WrongTeamName");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnString_when_noBoardExists() {
        //Arrange
        testList.add("TeamName1");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Team TeamName1 has no boards.", result);
    }

    @Test
    public void execute_should_returnString_when_boardExist() {
        //Arrange
        testList.add("TeamName1");
        Board testBoard = new BoardImpl("TestName");
        testTeam.addBoard(testBoard);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Team TeamName1 has 1 board", result.substring(0, 26));
    }
}
