package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.ChangeBugPriority;
import com.a16team03.wim.commands.modification.ChangeBugStatus;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.WorkItemPriority;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.Bug;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeBugPriority_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Bug bug;
    private static int id;


    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ChangeBugPriority(wimFactory, wimRepository);
        testList = new ArrayList<>();
        bug = new BugImpl("BugTitleMustBeLong", "Bug description here.");
        wimRepository.addBug(bug);
        id = bug.getId();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("more");
        testList.add("arguments");
        testList.add("than");
        testList.add("expected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_givenTextInsteadOfID() {
        //Arrange
        testList.add("id");
        testList.add("High");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeBugStatus_should_throwException_when_propertyValueIsInvalid() {
        //Arrange
        testList.add(String.valueOf(id));
        testList.add("invalidValue");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void changeBugStatus_should_setStatus() {
        //Arrange
        testList.add(String.valueOf(id));
        testList.add("High");
        // Act
        testCommand.execute(testList);
        WorkItemPriority priorityAfter = wimRepository.getBugById(id).getPriority();
        //Assert
        Assert.assertEquals(WorkItemPriority.HIGH, priorityAfter);
    }
}
