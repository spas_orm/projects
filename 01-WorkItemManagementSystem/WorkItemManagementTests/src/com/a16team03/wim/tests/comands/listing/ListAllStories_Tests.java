package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListAllStories;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.contracts.Comment;
import com.a16team03.wim.models.items.contracts.Story;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE;

public class ListAllStories_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListAllStories(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsExist() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_thatContainsStoryComment() {
        //Arrange
        Story story = wimFactory.createStory
                ("StoryTitleMustBeLong", "Description of a Story.");
        wimRepository.addStory(story);

        Comment comment = wimFactory.createComment("Niki", "This is a comment.");
        story.addComment(comment);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains("This is a comment."));
    }
}
