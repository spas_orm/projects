package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowAllTeamMembers;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.TEAM_NO_MEMBERS_MESSAGE;

public class ShowAllTeamMembers_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Team testTeam;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowAllTeamMembers(wimRepository);
        testList = new ArrayList<>();
        testTeam = new TeamImpl("TeamName");
        wimRepository.addTeam(testTeam);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("TeamName");
        testList.add("blabla");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesntExist() {
        //Arrange
        testList.add("WrongTeamName");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnString_when_noPersonExists() {
        //Arrange
        testList.add("TeamName");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(String.format(TEAM_NO_MEMBERS_MESSAGE, "TeamName"), result);
    }

    @Test
    public void execute_should_returnString_when_personsExist() {
        //Arrange
        testList.add("TeamName");
        Person testPerson = new PersonImpl("TestName");
        testTeam.addMember(testPerson);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Team TeamName has 1", result.substring(0, 19));
    }
}
