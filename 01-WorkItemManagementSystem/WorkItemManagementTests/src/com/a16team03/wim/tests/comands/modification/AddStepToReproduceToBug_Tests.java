package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.AddStepToReproduceToBug;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.Bug;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddStepToReproduceToBug_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Bug bug;
    private static int id;
    private static String idString;


    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AddStepToReproduceToBug(wimFactory, wimRepository);
        testList = new ArrayList<>();
        bug = new BugImpl("BugTitleMustBeLong", "Bug description here.");
        wimRepository.addBug(bug);
        id = bug.getId();
        idString = String.valueOf(id);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_givenTextInsteadOfID() {
        //Arrange
        testList.add("id");
        testList.add("First");
        testList.add("step");
        testList.add("to");
        testList.add("reproduce");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getBugById_should_throwException_when_idIsInvalid() {
        //Arrange
        testList.add("564");
        testList.add("First");
        testList.add("step");
        testList.add("to");
        testList.add("reproduce");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_addStepToReproduce_when_inputIsValid() {
        //Arrange
        testList.add(idString);
        testList.add("First");
        testList.add("step");
        testList.add("to");
        testList.add("reproduce");
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals("First step to reproduce", bug.getStepsToReproduce().get(0));
    }
}
