package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.UnassignPersonFromItem;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.StoryImpl;
import com.a16team03.wim.models.items.contracts.Story;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UnassignPerson_FromItem_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Person person;
    //    private static Team team;
    private static Story story;
    private static String idString;
    //    private static Board board;
    private static int id;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new UnassignPersonFromItem(wimFactory, wimRepository);
        testList = new ArrayList<>();
        person = new PersonImpl("PersonName2");
        wimRepository.addPerson(person);
        story = new StoryImpl("StoryName88", "TestDescriptionStory88");
        wimRepository.addStory(story);
        ;
        id = story.getId();
        idString = String.valueOf(id);

    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
//        story.removeAssignee(person);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add(idString);
        testList.add("qfasde");
        testList.add("vasdvareg");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_giveTextInsteadID() {
        //Arrange
        testList.add("bla");
        testList.add("low");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAssignableWorkItemById_should_throwException_when_idIsInvalid() {
        //Arrange
        testList.add("564");
        testList.add("PersonName2");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfWorkItemIsAssignedToPerson_should_throwException_when_personNotAssigned() {
        //Arrange
        testList.add(idString);
        testList.add("PersonName2");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_unassignWorkitemFromPerson_when_inputIsValid() {
        //Arrange
        story.addAssignee(person);
        testList.add(idString);
        testList.add("PersonName2");
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertFalse(wimRepository.getStoryById(id).getAssignees().toString().contains("PersonName2"));
    }
}
