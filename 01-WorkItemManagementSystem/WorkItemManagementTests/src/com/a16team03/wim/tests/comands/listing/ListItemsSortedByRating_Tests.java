package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsSortedByRating;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.contracts.Feedback;
import com.a16team03.wim.models.teamunits.contracts.Board;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_FEEDBACK_ITEMS_IN_THE_APPLICATION_MESSAGE;

public class ListItemsSortedByRating_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsSortedByRating(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsWithRatingExist() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_FEEDBACK_ITEMS_IN_THE_APPLICATION_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_thatContainsSortedItemsByRating() {
        //Arrange
        Feedback feedbackMedium = wimFactory.createFeedback("FeedbackTitleMustBeLong", "Description of a feedback.");
        feedbackMedium.setRating(7);
        wimRepository.addFeedback(feedbackMedium);

        Feedback feedbackHigh = wimFactory.createFeedback("FeedbackTitleMustBeLong", "Description of a feedback.");
        feedbackHigh.setRating(3);
        wimRepository.addFeedback(feedbackHigh);

        Feedback feedbackLow = wimFactory.createFeedback("FeedbackTitleMustBeLong", "Description of a feedback.");
        feedbackLow.setRating(5);
        wimRepository.addFeedback(feedbackLow);

        Board board1 = wimFactory.createBoard("Board1");
        board1.addItem(feedbackHigh);
        board1.addItem(feedbackLow);
        board1.addItem(feedbackMedium);

        String expectedOutput = board1.getItems().stream().map(Object::toString).collect(Collectors.joining());
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(expectedOutput, result);
    }
}
