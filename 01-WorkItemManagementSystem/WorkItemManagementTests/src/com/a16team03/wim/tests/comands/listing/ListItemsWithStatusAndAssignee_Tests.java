package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsWithStatusAndAssignee;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.BugStatus;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_WORK_ITEMS_WITH_STATUS_AND_ASSIGNEE_MESSAGE;

public class ListItemsWithStatusAndAssignee_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Person person;
    private static Bug bug;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsWithStatusAndAssignee(wimRepository);
        testList = new ArrayList<>();

        person = wimFactory.createNewPerson("Jerry");
        wimRepository.addPerson(person);

        bug = wimFactory.createBug("ThisIsABugTitle", "ThatIsABugDescription.");
        bug.addAssignee(person);
        bug.setStatus(BugStatus.FIXED);
        wimRepository.addBug(bug);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        testList.add("lessArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("more");
        testList.add("arguments");
        testList.add("thanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsAreAssignedToPersonWithStatus() {
        // Act
        testList.add("active");
        testList.add("Jerry");
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(String.format(NO_WORK_ITEMS_WITH_STATUS_AND_ASSIGNEE_MESSAGE,
                "active",
                person.getName()),
                result);
    }

    @Test
    public void execute_should_returnString_thatContainsAssigneeNameAndStatus() {
        //Arrange
        testList.add(String.valueOf(BugStatus.FIXED));
        testList.add(person.getName());
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains(person.getName()) && result.contains(String.valueOf(BugStatus.FIXED)));
    }
}
