package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowAllPeople;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_PEOPLE_MESSAGE;

public class ShowAllPeople_Tests {
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test
    public void execute_should_returnString_when_noPeopleExists() {
        //Arrange
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowAllPeople(wimRepository);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_PEOPLE_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_when_onePersonExists() {
        //Arrange
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowAllPeople(wimRepository);
        Person testPerson = new PersonImpl("PersonNameBla");
        wimRepository.addPerson(testPerson);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals("There is 1 person", result.substring(0, 17));
    }
}
