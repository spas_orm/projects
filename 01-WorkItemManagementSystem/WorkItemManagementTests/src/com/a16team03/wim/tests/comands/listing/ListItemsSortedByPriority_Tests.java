package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsSortedByPriority;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.WorkItemPriority;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.teamunits.contracts.Board;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_BUGS_OR_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE;

public class ListItemsSortedByPriority_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsSortedByPriority(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsWithPriorityExist() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_BUGS_OR_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_thatContainsSortedItemsByPriority() {
        //Arrange
        Bug bugMedium = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugMedium.setPriority(WorkItemPriority.MEDIUM);
        wimRepository.addBug(bugMedium);

        Bug bugHigh = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugHigh.setPriority(WorkItemPriority.HIGH);
        wimRepository.addBug(bugHigh);

        Bug bugLow = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        bugLow.setPriority(WorkItemPriority.LOW);
        wimRepository.addBug(bugLow);

        Board board1 = wimFactory.createBoard("Board1");
        board1.addItem(bugHigh);
        board1.addItem(bugMedium);
        board1.addItem(bugLow);

        String expectedOutput = board1.getItems().stream().map(Object::toString).collect(Collectors.joining());
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(expectedOutput, result);
    }
}
