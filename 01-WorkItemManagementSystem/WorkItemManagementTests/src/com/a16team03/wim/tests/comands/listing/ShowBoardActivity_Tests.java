package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowBoardActivity;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.BoardImpl;
import com.a16team03.wim.models.teamunits.contracts.Board;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardActivity_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Board testBoard;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowBoardActivity(wimRepository);
        testList = new ArrayList<>();
        testBoard = new BoardImpl("BoardBla");
        wimRepository.addBoard(testBoard);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("BoardBla");
        testList.add("blabla");
        // Act, Assert
        testCommand.execute(testList);
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesntExist() {
        //Arrange
        testList.add("WrongBla");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnString_when_noActivities() {
        //Arrange
        testList.add("BoardBla");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals("Board BoardBla has no activity history.",result);
    }

    @Test
    public void execute_should_returnString_when_personsExist() {
        //Arrange
        testList.add("BoardBla");
        Activity testActivity = new ActivityImpl("TestActivity");
        testBoard.addActivity(testActivity);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains("TestActivity"));
    }
}
