package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsSortedBySize;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.StorySize;
import com.a16team03.wim.models.items.contracts.Story;
import com.a16team03.wim.models.teamunits.contracts.Board;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.a16team03.wim.commands.CommandConstants.NO_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE;

public class ListItemsSortedBySize_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsSortedBySize(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArgumentsThanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsWithRatingExist() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_STORY_ITEMS_IN_THE_APPLICATION_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_thatContainsSortedItemsByRating() {
        //Arrange
        Story storyLarge = wimFactory.createStory("StoryTitleMustBeLong", "Description of a story.");
        storyLarge.setSize(StorySize.LARGE);
        wimRepository.addStory(storyLarge);

        Story storyMedium = wimFactory.createStory("StoryTitleMustBeLong", "Description of a story.");
        storyMedium.setSize(StorySize.MEDIUM);
        wimRepository.addStory(storyMedium);

        Story storySmall = wimFactory.createStory("StoryTitleMustBeLong", "Description of a story.");
        storySmall.setSize(StorySize.SMALL);
        wimRepository.addStory(storySmall);

        Story storyUnset = wimFactory.createStory("StoryTitleMustBeLong", "Description of a story.");
        storyUnset.setSize(StorySize.UNSET);
        wimRepository.addStory(storyUnset);

        Board board1 = wimFactory.createBoard("Board1");
        board1.addItem(storyLarge);
        board1.addItem(storyMedium);
        board1.addItem(storySmall);
        board1.addItem(storyUnset);

        String expectedOutput = board1.getItems().stream().map(Object::toString).collect(Collectors.joining());
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(expectedOutput, result);
    }
}
