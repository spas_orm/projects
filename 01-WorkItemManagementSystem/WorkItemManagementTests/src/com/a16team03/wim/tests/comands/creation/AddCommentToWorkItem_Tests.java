package com.a16team03.wim.tests.comands.creation;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.creation.AddCommentToWorkItem;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.BugImpl;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddCommentToWorkItem_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Bug bug;
    private static Person person;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AddCommentToWorkItem(wimFactory, wimRepository);
        testList = new ArrayList<>();
        bug = new BugImpl("BugTitleLong", "Bug description for test");
        person = new PersonImpl("TestPerson");
        wimRepository.addBug(bug);
        wimRepository.addPerson(person);

    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_thereIsntSuchItem() {
        //Arrange
        testList.add("3");
        testList.add("TestPerson");
        testList.add("Description");
        testList.add("text");
        testList.add("for");
        testList.add("bug");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_personDoesntExists() {
        //Arrange
        testList.add("0");
        testList.add("WrongPerson");
        testList.add("Comment");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_giveTextInsteadID() {
        //Arrange
        testList.add("bla");
        testList.add("TestPerson");
        testList.add("Comment");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_addComment_when_inputIsValid() {
        //Arrange
        testList.add("0");
        testList.add("TestPerson");
        testList.add("CommentTest");
        // Act
        testCommand.execute(testList);
        String commentText = wimRepository.getBugById(0).getComments().get(0).getComment();
        // Assert
        Assert.assertEquals("CommentTest", commentText);
    }
}
