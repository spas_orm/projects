package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsWithStatus;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.enums.BugStatus;
import com.a16team03.wim.models.enums.FeedbackStatus;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.items.contracts.Feedback;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_WORK_ITEMS_WITH_STATUS_MESSAGE;

public class ListItemsWithStatus_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsWithStatus(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArguments");
        testList.add("thanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsHaveThatStatus() {
        // Act
        Feedback feedback = wimFactory.createFeedback
                ("FeedbackTitleMustBeLong", "Description of a feedback.");
        wimRepository.addFeedback(feedback);
        feedback.setStatus(FeedbackStatus.UNSCHEDULED);

        testList.add("scheduled");
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(String.format(NO_WORK_ITEMS_WITH_STATUS_MESSAGE, "scheduled"), result);
    }

    @Test
    public void execute_should_returnString_thatContainsItemWithStatusName() {
        //Arrange
        Bug bug = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        wimRepository.addBug(bug);
        bug.setStatus(BugStatus.FIXED);

        testList.add("fixed");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains("BugTitleMustBeLong"));
    }
}
