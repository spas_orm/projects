package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowAllTeams;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_TEAMS_MESSAGE;

public class ShowAllTeams_Tests {
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowAllTeams(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test
    public void execute_should_returnString_when_noTeamExists() {
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(NO_TEAMS_MESSAGE, result);
    }

    @Test
    public void execute_should_returnString_when_personsExist() {
        //Arrange
        Team testTeam = new TeamImpl("TeamName");
        wimRepository.addTeam(testTeam);
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals("There is 1 team", result.substring(0, 15));
    }
}
