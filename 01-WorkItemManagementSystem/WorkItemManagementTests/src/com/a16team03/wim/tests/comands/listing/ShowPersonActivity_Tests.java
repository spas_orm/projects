package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ShowPersonActivity;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.ActivityImpl;
import com.a16team03.wim.models.contracts.Activity;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_PERSON_ACTIVITY_MESSAGE;

public class ShowPersonActivity_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Person person;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ShowPersonActivity(wimRepository);
        testList = new ArrayList<>();
        person = new PersonImpl("Trevor");
        wimRepository.addPerson(person);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArguments");
        testList.add("thanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_personDoesntExist() {
        //Arrange
        testList.add("NoSuchPerson");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_throwException_when_noPersonActivity() {
        //Arrange
        Person test = wimFactory.createNewPerson("Lenny");
        wimRepository.addPerson(test);
        testList.add("Lenny");
        // Act
        String output = testCommand.execute(testList);
        //Assert
        Assert.assertEquals(String.format(NO_PERSON_ACTIVITY_MESSAGE, "Lenny"), output);
    }

    @Test
    public void execute_should_returnString_with_personActivity() {
        //Arrange
        Activity testActivity = new ActivityImpl("This is an activity.");
        person.addActivity(testActivity);

        testList.add("Trevor");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains("This is an activity."));
    }
}
