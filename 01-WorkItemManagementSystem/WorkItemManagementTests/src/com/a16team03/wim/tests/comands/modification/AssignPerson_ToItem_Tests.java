package com.a16team03.wim.tests.comands.modification;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.modification.AssignPersonToItem;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.StoryImpl;
import com.a16team03.wim.models.items.contracts.Story;
import com.a16team03.wim.models.teamunits.BoardImpl;
import com.a16team03.wim.models.teamunits.PersonImpl;
import com.a16team03.wim.models.teamunits.TeamImpl;
import com.a16team03.wim.models.teamunits.contracts.Board;
import com.a16team03.wim.models.teamunits.contracts.Person;
import com.a16team03.wim.models.teamunits.contracts.Team;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AssignPerson_ToItem_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;
    private static Person person;
    private static Team team;
    private static Story story;
    private static String idString;
    private static Board board;
    private static int id;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new AssignPersonToItem(wimFactory, wimRepository);
        testList = new ArrayList<>();
        person = new PersonImpl("PersonNameNew");
        wimRepository.addPerson(person);
        team = new TeamImpl("TheMatrix");
        wimRepository.addTeam(team);
        story = new StoryImpl("StoryName55", "TestDescriptionStory55");
        wimRepository.addStory(story);
        board = new BoardImpl("BoardName1");
        wimRepository.addBoard(board);
        id = story.getId();
        idString = String.valueOf(id);
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();
        story.removeAssignee(person);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add(idString);
        testList.add("qfasde");
        testList.add("vasdvareg");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseParameters_should_throwException_when_giveTextInsteadID() {
        //Arrange
        testList.add("bla");
        testList.add("low");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAssignableWorkItemById_should_throwException_when_idIsInvalid() {
        //Arrange
        testList.add("564");
        testList.add("PersonNameNew");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfWorkItemIsAlreadyAssignedToPerson_should_throwException_when_alreadyAssignedToThatPerson() {
        //Arrange
        story.addAssignee(person);
        testList.add(idString);
        testList.add("PersonNameNew");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfWorkItemCanBeAssignedToPerson_should_throwException_when_personNotInAnyTeam() {
        //Arrange
        Person newFPerson = new PersonImpl("PoredniqPerson");
        wimRepository.addPerson(newFPerson);
        testList.add(idString);
        testList.add("PoredniqPerson");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkIfWorkItemCanBeAssignedToPerson_should_throwException_when_checkFails() {
        //Arrange
        Person newPersonF = new PersonImpl("PoredniqPersonF");
        wimRepository.addPerson(newPersonF);
        Team newTeamF = new TeamImpl("TeamF");
        wimRepository.addTeam(newTeamF);
        newTeamF.addMember(newPersonF);
        testList.add(idString);
        testList.add("PoredniqPersonF");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_assignWorkitemToPerson_when_inputIsValid() {
        //Arrange
        testList.add(idString);
        testList.add("PersonNameNew");
        team.addMember(person);
        team.addBoard(board);
        board.addItem(story);
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertTrue(wimRepository.getStoryById(id).getAssignees().toString().contains("PersonNameNew"));
    }


}
