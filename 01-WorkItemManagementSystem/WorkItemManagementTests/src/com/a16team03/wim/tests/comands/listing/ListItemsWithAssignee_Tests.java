package com.a16team03.wim.tests.comands.listing;

import com.a16team03.wim.commands.contracts.Command;
import com.a16team03.wim.commands.listing.ListItemsWithAssignee;
import com.a16team03.wim.core.WIMRepositoryImpl;
import com.a16team03.wim.core.factories.WIMFactoryImpl;
import com.a16team03.wim.models.items.contracts.Bug;
import com.a16team03.wim.models.teamunits.contracts.Person;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.a16team03.wim.commands.CommandConstants.NO_WORK_ITEMS_ASSIGNED_TO_MESSAGE;

public class ListItemsWithAssignee_Tests {
    private static WIMFactoryImpl wimFactory;
    private static WIMRepositoryImpl wimRepository;
    private static Command testCommand;
    private static List<String> testList;

    @BeforeClass
    public static void initTests() {
        //Arrange
        wimFactory = new WIMFactoryImpl();
        wimRepository = new WIMRepositoryImpl();
        testCommand = new ListItemsWithAssignee(wimRepository);
        testList = new ArrayList<>();
    }

    @After
    public void afterEach() {
        //Arrange
        testList.clear();

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        testList.add("moreArguments");
        testList.add("thanExpected");
        // Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_returnAppropriateString_when_noItemsAreAssignedToPerson() {
        // Act
        Person person = wimFactory.createNewPerson("Pavel");
        wimRepository.addPerson(person);
        testList.add("Pavel");
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertEquals(String.format(NO_WORK_ITEMS_ASSIGNED_TO_MESSAGE, "Pavel"), result);
    }

    @Test
    public void execute_should_returnString_thatContainsAssignedItemName() {
        //Arrange
        Person person = wimFactory.createNewPerson("Miles");
        wimRepository.addPerson(person);

        Bug bug = wimFactory.createBug("BugTitleMustBeLong", "Description of a bug.");
        wimRepository.addBug(bug);
        bug.addAssignee(person);

        testList.add("Miles");
        // Act
        String result = testCommand.execute(testList);
        // Assert
        Assert.assertTrue(result.contains("BugTitleMustBeLong"));
    }
}
