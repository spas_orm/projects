# Work Item Management (WIM) Console Application
*by Nikola Kostadinov & Spas Ormandzhiev*   
*Team: A16-Team-03*   
   
The application is designed and developed according to the functional requirements described in the [OOP Teamwork Assignment](https://learn.telerikacademy.com/mod/page/view.php?id=1788)

Link to the team [Trello](https://trello.com/b/1TlHN0Do/a16-team-03-wim)   

### Models
All models and class hierarchy are described in the UML diagram available in the links below:  
[UML Diagram in creately.com](https://app.creately.com/diagram/C3vnVbPNu9L/edit)    
[UML Diagram in GitLab](https://gitlab.com/spas_orm/a16-team-03/blob/master/Additionals/WIM_UMLDiagram.png)


### Commands 
List of all commands available in the application.   
Commands format is *CommandName (parameter1) (parameter2)*. *CommandName* is not case sensitive. For the parameters - *cammelCase* denotes that the parameter should be one word.

 - **Add comment to work item**   
 AddCommentToWorkItem (itemId) (personName) (Text for comments)
 - **Add person to team**   
 AddPersonToTeam (personName) (teamName)
 - **Add step to reproduce to bug**   
 AddStepToReproduceToBug (bugId) (Description of the step)
 - **Assign a person to an item**   
 AssignPersonToItem (iteId) (personName)
 - **Change a bug status**  
 ChangeBugStatus (bugId) (newBugStatus)
 - **Change a bug severity**  
 ChangeBugSeverity (bugId) (newBugSeverity)
 - **Change a bug priority**   
 ChangeBugPriority (bugId) (newBugPriority)
 - **Chnage a feedback rating**   
 ChangeFeedbackRating (feedbackId) (intiger number in the range of 0-10)
 - **Chnage a feedback status**   
 ChangeFeedbackStatus (feedbackId) (newFeedbackStatus)
 - **Change a story priority**   
 ChangeStoryPriority (storyId) (newStoryPriority)
 - **Change a story size**   
 ChangeStorySize (storyId) (newStorySize)
 - **Change a story status**   
 ChangeStoryStatus (storyId) (newStoryStatus)
 - **Create a new board in a team**   
 CreateNewBoardInTeam (boardName) (teamName)
 - **Create a new person**   
 CreateNewPerson (personName)
 - **Create a new bug in a board**   
 CreateNewBugInBoard (boardName) (bugTitle) (Description of the bug)
 - **Create a new story in a board**   
 CreateNewStoryInBoard (boardName) (storyTitle) (Description of the story)
 - **Create a new feedback in a board**   
 CreateNewFeedbackInBoard (boardName) (feedbackTitle) (Description of the feedback)
 - **Create a new team**   
 CreateNewTeam (teamName)
 - **List all bugs**   
 ListAllBugs
 - **List all feedbacks**  
 ListAllFeedbacks
 - **List all stories**   
 ListAllStories
 - **List all items**  
 ListAllItems
 - **List items sorted by priority**   
 ListItemsSortedByPriority
 - **List items sorted by rating**   
 ListItemsSortedByRating
 - **List items sorted by severity**  
 ListItemsSortedBySeverity
 - **List items sorted by size**  
 ListItemsSortedBySize
 - **List items sorted by title**  
 ListItemsSortedByTitle
 - **List items with a specific assignee**   
 ListItemsWithAssignee (personName)
 - **List items with a specific status**   
 ListItemsWithStatus (status)
 - **List items with a specific status and assignee**   
 ListItemsWithStatusAndAssignee (status) (personName)
 - **Show all people in the system**   
 ShowAllPeople
 - **Show all boards in a team**   
 ShowAllTeamBoards (teamName)
 - **Show all members of a team**   
 ShowAllTeamMembers (teamName)
 - **Show all teams**   
 ShowAllTeams
 - **Show all activities for a board**   
 ShowBoardActivity (boardName)
 - **Show all activities for a person**   
 ShowPersonActivity (personName)
 - **Show all activities for a team**  
 ShowTeamActivity (teamName)
 - **Unassign a person from an item**   
 UnassignPersonFromItem (itemId) (personName)
 

### Sample Input
```
CreateNewTeam TheMatrix
CreateNewTeam OtherTeam
ShowAllTeams
CreateNewBoardInTeam WIMBoard TheMatrix
CreateNewBoardInTeam OtherBoard TheMatrix
ShowAllTeamBoards TheMatrix
ShowAllTeams
ShowAllPeople
CreateNewPerson Nikola
CreateNewPerson SpasO
CreateNewPerson Gosho
AddPersonToTeam SpasO TheMatrix
AddPersonToTeam Nikola TheMatrix
AddPersonToTeam Gosho TheMatrix
ShowAllPeople
ListAllItems
CreateNewBugInBoard WIMBoard BugTitleMustBeLong Description of the bug 
CreateNewBugInBoard WIMBoard Bug2TitleLong Description of the second bug
CreateNewStoryInBoard WIMBoard StoryTitle Description of the story
CreateNewStoryInBoard WIMBoard SecondStoryTitle Description of the second story
CreateNewFeedbackInBoard WIMBoard FeedbackTitle Description of the feedback
CreateNewFeedbackInBoard OtherBoard SecondFeedback Description of the second feedback
ListAllItems
AssignPersonToItem 0 Nikola
AssignPersonToItem 1 SpasO
AssignPersonToItem 3 Nikola
AssignPersonToItem 2 SpasO
ChangeStoryStatus 2 inprogress
ChangeStoryStatus 3 inprogress
ChangeBugSeverity 0 minor
ChangeBugSeverity 1 critical
ChangeBugStatus 1 active
ChangeBugPriority 1 high
AddStepToReproduceToBug 1 Do this
AddStepToReproduceToBug 1 Do that
AddCommentToWorkItem 1 Gosho I know how to fix it
ListAllBugs
ListItemsSortedByTitle
ListItemsWithAssignee Nikola
ListItemsSortedBySeverity
exit

```

### Sample output
```
Team with name TheMatrix has been created.
Team with name OtherTeam has been created.
There are 2 teams registered in the System:
=================================================
Team name: OtherTeam
----Team Activity history----
23.10.2019 16:10:25 -> Team with name OtherTeam has been created.
=================================================
Team name: TheMatrix
----Team Activity history----
23.10.2019 16:10:25 -> Team with name TheMatrix has been created.

Board with name WIMBoard has been created.
Board with name OtherBoard has been created.
Team TheMatrix has 2 boards:
=================================================
Board name: WIMBoard
----Activity history----
23.10.2019 16:10:25 -> Board with name WIMBoard has been created.
=================================================
Board name: OtherBoard
----Activity history----
23.10.2019 16:10:25 -> Board with name OtherBoard has been created.

There are 2 teams registered in the System:
=================================================
Team name: OtherTeam
----Team Activity history----
23.10.2019 16:10:25 -> Team with name OtherTeam has been created.
=================================================
Team name: TheMatrix
Boards in team: WIMBoard, OtherBoard
----Team Activity history----
23.10.2019 16:10:25 -> Team with name TheMatrix has been created.
23.10.2019 16:10:25 -> Board with name WIMBoard has been created.
23.10.2019 16:10:25 -> Board with name OtherBoard has been created.

There are no people registered in the system.
Person with name Nikola has been created.
Person with name SpasO has been created.
Person with name Gosho has been created.
Person SpasO was added to TheMatrix team.
Person Nikola was added to TheMatrix team.
Person Gosho was added to TheMatrix team.
There are 3 people registered in the System:
=================================================
Person name: Gosho
----Activity history----
23.10.2019 16:10:25 -> Person with name Gosho has been created.
23.10.2019 16:10:25 -> Person Gosho was added to TheMatrix team.
=================================================
Person name: Nikola
----Activity history----
23.10.2019 16:10:25 -> Person with name Nikola has been created.
23.10.2019 16:10:25 -> Person Nikola was added to TheMatrix team.
=================================================
Person name: SpasO
----Activity history----
23.10.2019 16:10:25 -> Person with name SpasO has been created.
23.10.2019 16:10:25 -> Person SpasO was added to TheMatrix team.

There are no items added to the application yet.
Bug BugTitleMustBeLong added to board WIMBoard.
Bug Bug2TitleLong added to board WIMBoard.
Story StoryTitle added to board WIMBoard.
Story SecondStoryTitle added to board WIMBoard.
Feedback FeedbackTitle added to board WIMBoard.
Feedback SecondFeedback added to board OtherBoard.
=================================================
BUG			 ID: 0		 Title: BugTitleMustBeLong
Description: Description of the bug
Priority: Not set yet		 Assigned to: Not assigned
Status: Active				 Severity: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Bug BugTitleMustBeLong added to board WIMBoard.
=================================================
BUG			 ID: 1		 Title: Bug2TitleLong
Description: Description of the second bug
Priority: Not set yet		 Assigned to: Not assigned
Status: Active				 Severity: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Bug Bug2TitleLong added to board WIMBoard.
=================================================
STORY			 ID: 2		 Title: StoryTitle
Description: Description of the story
Priority: Not set yet		 Assigned to: Not assigned
Status: NotDone				 Size: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Story StoryTitle added to board WIMBoard.
=================================================
STORY			 ID: 3		 Title: SecondStoryTitle
Description: Description of the second story
Priority: Not set yet		 Assigned to: Not assigned
Status: NotDone				 Size: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Story SecondStoryTitle added to board WIMBoard.
=================================================
FEEDBACK			 ID: 4		 Title: FeedbackTitle
Description: Description of the feedback
Status: New		 Rating: 0
----Activity history----
23.10.2019 16:10:25 -> Feedback FeedbackTitle added to board WIMBoard.
=================================================
FEEDBACK			 ID: 5		 Title: SecondFeedback
Description: Description of the second feedback
Status: New		 Rating: 0
----Activity history----
23.10.2019 16:10:25 -> Feedback SecondFeedback added to board OtherBoard.

Item with ID 0 was assigned to Nikola.
Item with ID 1 was assigned to SpasO.
Item with ID 3 was assigned to Nikola.
Item with ID 2 was assigned to SpasO.
Story with ID 2 status has been changed to 'INPROGRESS'.
Story with ID 3 status has been changed to 'INPROGRESS'.
Bug with ID 0 severity has been changed to 'MINOR'.
Bug with ID 1 severity has been changed to 'CRITICAL'.
Bug with ID 1 status has been changed to 'ACTIVE'.
Bug with ID 1 priority has been changed to 'HIGH'.
A step to reproduce has been added to Bug with ID 1.
A step to reproduce has been added to Bug with ID 1.
Comment added by Gosho.
=================================================
BUG			 ID: 0		 Title: BugTitleMustBeLong
Description: Description of the bug
Priority: Not set yet		 Assigned to: Nikola
Status: Active				 Severity: Minor
----Activity history----
23.10.2019 16:10:25 -> Bug BugTitleMustBeLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 0 was assigned to Nikola.
23.10.2019 16:10:25 -> Bug with ID 0 severity has been changed to 'MINOR'.
=================================================
BUG			 ID: 1		 Title: Bug2TitleLong
Description: Description of the second bug
Priority: High		 Assigned to: SpasO
Status: Active				 Severity: Critical
----Steps To Reproduce----
 - Do this
 - Do that
----List of comments----
23.10.2019 16:10:25 -> Gosho: I know how to fix it
----Activity history----
23.10.2019 16:10:25 -> Bug Bug2TitleLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 1 was assigned to SpasO.
23.10.2019 16:10:25 -> Bug with ID 1 severity has been changed to 'CRITICAL'.
23.10.2019 16:10:25 -> Bug with ID 1 status has been changed to 'ACTIVE'.
23.10.2019 16:10:25 -> Bug with ID 1 priority has been changed to 'HIGH'.
23.10.2019 16:10:25 -> A step to reproduce has been added to Bug with ID 1.
23.10.2019 16:10:25 -> A step to reproduce has been added to Bug with ID 1.
23.10.2019 16:10:25 -> Comment added by Gosho.

=================================================
BUG			 ID: 1		 Title: Bug2TitleLong
Description: Description of the second bug
Priority: High		 Assigned to: SpasO
Status: Active				 Severity: Critical
----Steps To Reproduce----
 - Do this
 - Do that
----List of comments----
23.10.2019 16:10:25 -> Gosho: I know how to fix it
----Activity history----
23.10.2019 16:10:25 -> Bug Bug2TitleLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 1 was assigned to SpasO.
23.10.2019 16:10:25 -> Bug with ID 1 severity has been changed to 'CRITICAL'.
23.10.2019 16:10:25 -> Bug with ID 1 status has been changed to 'ACTIVE'.
23.10.2019 16:10:25 -> Bug with ID 1 priority has been changed to 'HIGH'.
23.10.2019 16:10:25 -> A step to reproduce has been added to Bug with ID 1.
23.10.2019 16:10:25 -> A step to reproduce has been added to Bug with ID 1.
23.10.2019 16:10:25 -> Comment added by Gosho.
=================================================
BUG			 ID: 0		 Title: BugTitleMustBeLong
Description: Description of the bug
Priority: Not set yet		 Assigned to: Nikola
Status: Active				 Severity: Minor
----Activity history----
23.10.2019 16:10:25 -> Bug BugTitleMustBeLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 0 was assigned to Nikola.
23.10.2019 16:10:25 -> Bug with ID 0 severity has been changed to 'MINOR'.
=================================================
FEEDBACK			 ID: 4		 Title: FeedbackTitle
Description: Description of the feedback
Status: New		 Rating: 0
----Activity history----
23.10.2019 16:10:25 -> Feedback FeedbackTitle added to board WIMBoard.
=================================================
FEEDBACK			 ID: 5		 Title: SecondFeedback
Description: Description of the second feedback
Status: New		 Rating: 0
----Activity history----
23.10.2019 16:10:25 -> Feedback SecondFeedback added to board OtherBoard.
=================================================
STORY			 ID: 3		 Title: SecondStoryTitle
Description: Description of the second story
Priority: Not set yet		 Assigned to: Nikola
Status: InProgress				 Size: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Story SecondStoryTitle added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 3 was assigned to Nikola.
23.10.2019 16:10:25 -> Story with ID 3 status has been changed to 'INPROGRESS'.
=================================================
STORY			 ID: 2		 Title: StoryTitle
Description: Description of the story
Priority: Not set yet		 Assigned to: SpasO
Status: InProgress				 Size: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Story StoryTitle added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 2 was assigned to SpasO.
23.10.2019 16:10:25 -> Story with ID 2 status has been changed to 'INPROGRESS'.

=================================================
BUG			 ID: 0		 Title: BugTitleMustBeLong
Description: Description of the bug
Priority: Not set yet		 Assigned to: Nikola
Status: Active				 Severity: Minor
----Activity history----
23.10.2019 16:10:25 -> Bug BugTitleMustBeLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 0 was assigned to Nikola.
23.10.2019 16:10:25 -> Bug with ID 0 severity has been changed to 'MINOR'.
=================================================
STORY			 ID: 3		 Title: SecondStoryTitle
Description: Description of the second story
Priority: Not set yet		 Assigned to: Nikola
Status: InProgress				 Size: Not set yet
----Activity history----
23.10.2019 16:10:25 -> Story SecondStoryTitle added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 3 was assigned to Nikola.
23.10.2019 16:10:25 -> Story with ID 3 status has been changed to 'INPROGRESS'.

=================================================
BUG			 ID: 1		 Title: Bug2TitleLong
Description: Description of the second bug
Priority: High		 Assigned to: SpasO
Status: Active				 Severity: Critical
----Steps To Reproduce----
 - Do this
 - Do that
----List of comments----
23.10.2019 16:10:25 -> Gosho: I know how to fix it
----Activity history----
23.10.2019 16:10:25 -> Bug Bug2TitleLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 1 was assigned to SpasO.
23.10.2019 16:10:25 -> Bug with ID 1 severity has been changed to 'CRITICAL'.
23.10.2019 16:10:25 -> Bug with ID 1 status has been changed to 'ACTIVE'.
23.10.2019 16:10:25 -> Bug with ID 1 priority has been changed to 'HIGH'.
23.10.2019 16:10:25 -> A step to reproduce has been added to Bug with ID 1.
23.10.2019 16:10:25 -> A step to reproduce has been added to Bug with ID 1.
23.10.2019 16:10:25 -> Comment added by Gosho.
=================================================
BUG			 ID: 0		 Title: BugTitleMustBeLong
Description: Description of the bug
Priority: Not set yet		 Assigned to: Nikola
Status: Active				 Severity: Minor
----Activity history----
23.10.2019 16:10:25 -> Bug BugTitleMustBeLong added to board WIMBoard.
23.10.2019 16:10:25 -> Item with ID 0 was assigned to Nikola.
23.10.2019 16:10:25 -> Bug with ID 0 severity has been changed to 'MINOR'.
```
